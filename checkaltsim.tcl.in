proc extractAlphabet {lts} {
	set alph [list]
	set trans [$lts transitions]
	foreach tran $trans {
		set event [lindex $tran 1]
		if {$event >=2} {
			lappend alph $event
		}
	}
	return $alph
}

proc printAlphabet {file header lts num_alph txt_alph} {
	set count [llength $num_alph]
	incr count [llength $txt_alph]
	if {$count > 0} {
		puts $file "\t$header"
		foreach act $num_alph {
			incr count -1
			set eventname [$lts event $act]
			if { $count > 0 } {
				puts $file "\t\t$eventname,"
			} else {
				puts $file "\t\t$eventname"
			}
		}
		foreach eventname $txt_alph {
			incr count -1
			if { $count > 0 } {
				puts $file "\t\t$eventname,"
			} else {
				puts $file "\t\t$eventname"
			}
		}
	}
}

proc printStates {file trans errorstates} {
	set maxstate 0
	foreach tran $trans {
		if {[lindex $tran 0] > $maxstate } {set maxstate [lindex $tran 0]}
		if {[lindex $tran 2] > $maxstate } {set maxstate [lindex $tran 2]}
	}
	puts $file "\tstates"
	set state 0
	for {set state 0} { $state <= $maxstate } {incr state} {
		if { [lsearch $errorstates $state] == -1 } {
			puts $file "\t\ts$state,"
		}
	}
	puts $file "\t\tse"
	puts $file "\tstart"
	puts $file "\t\ts0"
}

proc findErrorStates {lts trans} {
	set errorstates [list]
	foreach tran $trans {
		set act [lindex $tran 1]
		set eventname [$lts event $act]
		if {$eventname == "err__"} {
			lappend errorstates [lindex $tran 2]
		}
	}
	return $errorstates
}

proc printErrorTransitions {file lts num_alph} {
	set counter 0
	foreach act $num_alph {
		if {$counter != 0} {
			puts $file ","
		}
		set eventname ""
		if {$act == 0} {
			set eventname "tau__"
		} else {
			set eventname [$lts event $act]
		}
		puts -nonewline $file "\t\tse -> se \[$eventname\]"
		incr counter
	}
	return $counter
}

proc printTransitions {file header lts trans num_alph include_states exclude_states} {
	set counter 0
	foreach tran $trans {
		set from [lindex $tran 0]
		set to [lindex $tran 2]
		set act [lindex $tran 1]
		if {[lsearch $include_states $from] != -1} {
			set from "e"
		}
		if {[lsearch $include_states $to] != -1} {
			set to "e"
		}
		if {($from != "e") && (([lsearch $num_alph $act] != -1) || ($to == "e")) && ([lsearch $exclude_states $to] == -1)} {
			set eventname ""
			if {$act == 0} {
				set eventname "tau__"
			} else {
				set eventname [$lts event $act]
				if {[string match "Must_*" $eventname]} {
					set eventname [string range $eventname 5 [string length $eventname]]
				}
			}
			if {$counter == 0} {
				puts $file "\t$header"
			} else {
				puts $file ","
			}
			puts -nonewline $file "\t\ts$from -> s$to \[$eventname\]"
			incr counter
		}
	}
	return $counter
}

proc printMIO {s file name} {
	set suffix "_Proc_1"
	set lts [$s compile "$name$suffix"]
	set chozinalph [$s compile "InAlph_$name"]
	set num_inalph [extractAlphabet $chozinalph]
	set chozoutalph [$s compile "OutAlph_$name"]
	set num_outalph [extractAlphabet $chozoutalph]
	set chozmustoutalph [$s compile "MustOutAlph_$name"]
	set num_mustoutalph [extractAlphabet $chozmustoutalph]
	set num_mustalph [concat $num_mustoutalph $num_inalph]
	set trans [$lts transitions]
	puts $file "mio IFace$name {"
	printAlphabet $file "inputs" $chozinalph $num_inalph [list]
	printAlphabet $file "outputs" $chozoutalph $num_outalph [list]
	printAlphabet $file "internals" [list] [list] [list "tau__"]
	set errorstates [findErrorStates $lts $trans]
	printStates $file $trans $errorstates
	lappend num_outalph 0
	set trancount [printTransitions $file "mayTransitions" $lts $trans $num_outalph $errorstates [list]]
	if {$trancount > 0} {puts $file ","}
	set trancount [printErrorTransitions $file $lts $num_inalph]
	if {$trancount > 0} {puts $file ","}
	printErrorTransitions $file $lts $num_outalph
	puts $file ""	
	set trancount [printTransitions $file "mustTransitions" $lts $trans $num_mustalph [list] $errorstates]
	if {$trancount > 0} {puts $file ""}
	puts $file "}"
	$lts delete
}

global env
if {$argc < 1} {
	puts ""
	puts "File name missing."
	exit
}
if {$argc > 1} {
	puts ""
	puts "Too many arguments."
	exit
}
if {![info exists env(FDRHOME)]} {
	puts ""
	puts "Environment variable FDRHOME is not set."
	exit
}
set fdrhome $env(FDRHOME)
source $fdrhome/lib/fdr/fdrDirect.tcl

set instancename "_instance_"
set insuffix ".csp"
set outsuffix ".miotxt"
set success 1
set complete 0
set sound 0
set islast 0
set finished 0
set counter 0

set filename "$argv$instancename$counter$insuffix"
if {![file exists $argv.cps]} {
	puts ""
	puts "File $argv.cps does not exist."
	exit 1
} elseif {![file exists $filename]} {
	puts ""
	puts "Nothing to be checked."
	puts "Run boundsInstanceGenerator $argv.cps first."
	exit 1
} else {
	file stat $filename inststat
	file stat $argv.cps cpsstat
	if {$cpsstat(mtime) > $inststat(ctime)} {
		puts ""
		puts "File $argv.cps modified after the instances have been created."
       		puts "Rerun boundsInstanceGenerator $argv.cps ."
 		exit 1
	}
}

while {!$finished && $success} {
	set filename "$argv$instancename$counter$insuffix"
	if {[file exists $filename]} {
		set fid [open $filename]
		gets $fid type
		if {$type == "-- Bounds2.2 trace refinement check --"} {
			puts ""
			puts "The file describes a trace refinement check."
			puts "Run boundsTraceRefinementChecker $argv instead."
			exit 1
		} elseif {$type == "-- Bounds2.2 compatibility check --"} {
			puts ""
			puts "The file describes a compatility check."
			puts "Run boundsCompatibilityChecker $argv instead."
			exit 1
		} elseif {$type == "-- Bounds2.3 end of alternating simulation check --"} {
			set islast 1
		} elseif {$type != "-- Bounds2.3 alternating simulation check --"} {
			puts ""
			puts "File $filename does not seem to describe an alternating simulation check created by this version of Bounds."
			puts "Have you run boundsInstanceGenerator $argv.cps ?"
			exit 1
		}
		gets $fid modelcomplete
		if {$modelcomplete == "-- The model is complete --"} {
			set complete 1
		}
		gets $fid modelsound
		if {$modelsound == "-- The model is sound --"} {
			set sound 1
		}
		close $fid
		if {$islast} {
			break
		}
		puts ""		
		puts "==== Checking instance $counter of $argv ===="
		puts ""		
		set s [load "$filename"]

		set r1 [checkRefinement $s "CHAOS(inalph_Spec_Proc_1)" "T" "CHAOS(inalph_Impl_Proc_1)"]
		set r2 [checkRefinement $s "CHAOS(inalph_Impl_Proc_1)" "T" "CHAOS(inalph_Spec_Proc_1)"]
		if {($r1 != "true" && $r1 != "xtrue") || ($r2 != "true" && $r2 != "xtrue")} {
			puts "==== The input alphabets of the system and the specification do not match. ===="
			set success 0
			break
		}

		set r1 [checkRefinement $s "CHAOS(outalph_Spec_Proc_1)" "T" "CHAOS(outalph_Impl_Proc_1)"]
		set r2 [checkRefinement $s "CHAOS(outalph_Impl_Proc_1)" "T" "CHAOS(outalph_Spec_Proc_1)"]
		if {($r1 != "true" && $r1 != "xtrue") || ($r2 != "true" && $r2 != "xtrue")} {
			puts "==== The output alphabets of the system and the specification do not match. ===="
			set success 0
			break
		}

		set miofile [open "$argv$instancename$counter$outsuffix" w]
		printMIO $s $miofile "Spec"
		puts $miofile "\n\n"
		printMIO $s $miofile "Impl"
		close $miofile
		set r3 [exec -ignorestderr eclipse -nosplash -application fi.aalto.bounds.mioverifier -mioBaseFileName "$argv$instancename$counter" -vmargs -Xmx4000m -Xss10m -Dorg.eclipse.equinox.p2.reconciler.dropins.directory=@datadir@/eclipse/dropins]
		puts $r3
		if { $r3 != "true" } {
			set success 0
			break
		}

		incr counter		
	} else {
		set finished 1
	}
}
puts ""
if {$success} {
	if {$islast} {
		if {$sound} {
			puts "==== The system is correct with respect to the specification! ===="
		} else {
			puts "==== The instances are correct with respect to the specification."
			puts "     However, there might a bug in an instance above the cut-offs. ===="
		}
	} else {
		puts "It looks like some instances are missing."
		puts "Rerun boundsInstanceGenerator $argv.cps ."
	}
} else {
	if {$complete} {
		puts "==== The system is *NOT* correct with respect to the specification. ===="
	} else {
		puts "==== The instance is not correct with respect to the specification."
		puts "     However, this may not be a real bug but a result of abstraction. ===="
	}
}
exit
