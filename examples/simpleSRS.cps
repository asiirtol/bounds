type U
type R

avar u : U
avar u1 : U
avar u2 : U
avar r : R

chan rdlock : U, R
chan wrlock : U, R
chan unlock : U, R
chan rdbeg : U, R
chan rdend : U, R
chan wrbeg : U, R
chan wrend : U, R

ltsc User =
	lts
		I =   	rdlock(u,r) -> RD
			[] wrlock(u,r) -> WR
			[] tau() -> I
		RD =   	rdbeg(u,r) -> R2
			[] wrlock(u,r) -> WR
			[] unlock(u,r) -> I
		R2 =  	rdend(u,r) -> RD
		WR =   	rdbeg(u,r) -> W2
			[] wrbeg(u,r) -> W3
			[] unlock(u,r) -> I
		W2 = 	rdend(u,r) -> WR
		W3 = 	wrend(u,r) -> WR
	from I

ltsc Lock =
	lts
		NO =   	rdlock(u1,r) -> R1
			[] wrlock(u1,r) -> W1
			[] rdlock(u2,r) -> R2
			[] wrlock(u2,r) -> W2
		R1 =   	wrlock(u1,r) -> W1
			[] unlock(u1,r) -> NO
			[] rdlock(u2,r) -> R12
		R2 =   	wrlock(u2,r) -> W2
			[] unlock(u2,r) -> NO
			[] rdlock(u1,r) -> R12
		W1 =   	unlock(u1,r) -> NO
		W2 =   	unlock(u2,r) -> NO
		R12 =   unlock(u1,r) -> R2
			[] unlock(u2,r) -> R1
	from NO

ltsc Mutex2 =
	lts
		NO =   	rdbeg(u1,r) -> R1
			[] rdbeg(u2,r) -> R2
			[] wrbeg(u1,r) -> N1 
			[] wrbeg(u2,r) -> N2 
		N1 = 	wrend(u1,r) -> NO
		N2 = 	wrend(u2,r) -> NO
		R1 =   	rdend(u1,r) -> NO
			[] rdbeg(u2,r) -> RR
		R2 =   	rdend(u2,r) -> NO
			[] rdbeg(u1,r) -> RR
		RR =   	rdend(u1,r) -> R2
			[] rdend(u2,r) -> R1
	from NO

ltsc SRS = (|| u, r : User) || (|| r, u1, u2 : [!u1=u2] Lock)

ltsc Mutex = (|| u1, u2, r : Mutex2)

ssc LcEv = (_) u, r : {rdlock(u,r), wrlock(u,r), unlock(u,r)}

trace refinement: verify SRS \ LcEv against Mutex
