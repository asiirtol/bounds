type P = {pr1,pr2}
type R
type D

var p : P
var p2 : P
var r : R
var r2 : R
var d : D
var d2 : D

chan rdbeg : P,R
chan rdend : P,R,D
chan wrbeg : P,R,D
chan wrend : P,R
chan rdmem : P,R,D
chan flush : P,R,D

plts CUnit = 
    lts
        I =           rdbeg(p,r) -> IR
            [] [] d : wrbeg(p,r,d) -> IW(d)
	    []        [!p=p2] rdbeg(p2,r) -> I
	    [] [] d : [!p=p2] wrbeg(p2,r,d) -> I
        IR =   [] d : rdmem(p,r,d) -> IR2(d)
            [] [!p=p2] rdbeg(p2,r) -> IR
	IR2(d) =      rdend(p,r,d) -> S(d)
                 [] [!p=p2] rdbeg(p2,r) -> IR2(d)
	IW(d) =       wrend(p,r) -> M(d)
	
	S(d) =        rdbeg(p,r) -> SR(d)
	       []     [!p=p2] rdbeg(p2,r) -> S(d)
	       [] [] d : wrbeg(p,r,d) -> SW(d)
	       []     flush(p,r,d) -> I
	       []     tau() -> I
	SR(d) =       rdend(p,r,d) -> S(d)
                  [] [!p=p2] rdbeg(p2,r) -> SR(d)
	SW(d) =       wrend(p,r) -> M(d)
	
	M(d) =        rdbeg(p,r) -> MR(d)
	       [] [] d : wrbeg(p,r,d) -> MW(d)
	       []     flush(p,r,d) -> S(d)
	       []     flush(p,r,d) -> I
	MR(d) =       rdend(p,r,d) -> M(d)
	MW(d) =       wrend(p,r) -> M(d)
    from I

plts Cache = || p,p2,r : CUnit

plts MUnit =
    lts
        I = []d: tau() -> M(d)
	M(d) =    [] p :    rdmem(p,r,d) -> M(d)
	       [] [] p,d2 : flush(p,r,d2) -> M(d2)
    from I
	      
plts Mem = || r : MUnit	      

plts CacheMem = Cache || Mem

plts SpecUnit =
    lts
        I =    [] p,d : rdend(p,r,d) -> M(d)
            [] [] p,d : wrbeg(p,r,d) -> M(d)
        M(d) =    [] p : rdend(p,r,d) -> M(d)
               [] [] p,d2 : wrbeg(p,r,d2) -> M(d2)
    from I

plts Choz =
    lts
         I =    [] p : rdbeg(p,r) -> I
             [] [] p : wrend(p,r) -> I
             [] [] p,d : rdmem(p,r,d) -> I
             [] [] p,d : flush(p,r,d) -> I
    from I

plts Consistency = || r : SpecUnit

pset MemAcc = (_) p,r,d : {rdmem(p,r,d), flush(p,r,d), rdbeg(p,r), wrend(p,r)}

trace refinement: verify CacheMem against Consistency || (|| r: Choz)
