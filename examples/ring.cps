type T

rels Ring : T,T

var r1 : T
var r2 : T
var r3 : T

chan token : T,T

vc uniqsucc = \/r1,r2,r3 : (Ring(r1,r2) & Ring(r1,r3)) -> r2 = r3
vc uniqpred = \/r1,r2,r3 : (Ring(r2,r1) & Ring(r3,r1)) -> r2 = r3
vc hassucc = \/ r1 : ! (\/ r2: (! Ring(r1,r2)))
vc ringtopo = uniqsucc & uniqpred & hassucc

plts Node =
	lts
		NO =   	token(r1,r2) -> NO 
	from NO

plts Sys = (|| r1, r2 : [Ring(r1,r2)] Node )

trace refinement: verify Sys against Sys when ringtopo

