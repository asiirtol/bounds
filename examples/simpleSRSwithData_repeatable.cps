type U = {u1,u2}
type R
type D

var k : U
var k1 : U
var k2 : U
var r : R
var d : D
var d1 : D

chan rdlock : U, R
chan wrlock : U, R
chan unlock : U, R
chan rdbeg : U, R
chan rdend : U, R, D
chan wrbeg : U, R, D
chan wrend : U, R

plts User =
	lts
		I =   	rdlock(k,r) -> RD
			[] wrlock(k,r) -> W
			[] tau() -> I
		RD =   	rdbeg(k,r) -> R2
			[] wrlock(k,r) -> W
			[] unlock(k,r) -> STP
		R2 =  	[] d : rdend(k,r,d) -> RD
		W =   	rdbeg(k,r) -> W2
			[] [] d : wrbeg(k,r,d) -> W3
			[] unlock(k,r) -> STP
		W2 = 	[] d : rdend(k,r,d) -> W
		W3 = 	wrend(k,r) -> W
	from I

plts Lock =
	lts
		NO =   	rdlock(k1,r) -> R1
			[] wrlock(k1,r) -> W1
			[] rdlock(k2,r) -> R2
			[] wrlock(k2,r) -> W2
		R1 =   	wrlock(k1,r) -> W1
			[] unlock(k1,r) -> NO
			[] rdlock(k2,r) -> R12
		R2 =   	wrlock(k2,r) -> W2
			[] unlock(k2,r) -> NO
			[] rdlock(k1,r) -> R12
		W1 =   	unlock(k1,r) -> NO
		W2 =   	unlock(k2,r) -> NO
		R12 =   unlock(k1,r) -> R2
			[] unlock(k2,r) -> R1
	from NO

plts Variable = 
	lts
		NO = 	   [] d : tau() -> S(d)
		S(d) = 	   [] k : rdend(k,r,d) -> S(d)
			[] [] k, d1 : wrbeg(k,r,d1) -> S(d1)
	from NO

plts RepRead1 =
	lts
		NO = 	       rdbeg(k,r) -> L0
			[] [] d : wrbeg(k,r,d) -> K(d)
		L0 = 	[] d : rdend(k,r,d) -> N(d)
		N(d) =         rdbeg(k,r) -> L(d)
                        [] [] d1 : wrbeg(k,r,d1) -> K(d1)
		L(d) = 	       rdend(k,r,d) -> N(d)
		K(d) =         wrend(k,r) -> N(d)

	from NO

plts Sys = (|| k, r : User) || (|| r, k1, k2 : [!k1=k2] Lock) || (|| r : Variable)

plts RepRead = (|| k, r : RepRead1)

pset LockEv = (_) k, r : {rdlock(k,r), wrlock(k,r), unlock(k,r)}

trace refinement: verify Sys \ LockEv against RepRead
