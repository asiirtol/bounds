type U
type R

avar r : R
avar u1 : U
avar u2 : U
avar u3 : U
avar uh : U
avar ut : U

relv Anc : U,U

vf nopred = \/u1: ! u1 Anc uh
vf nosucc = \/u1: ! ut Anc u1

vf irrAnc = \/u1:! u1 Anc u1
vf tran = \/u1,u2,u3: u1 Anc u2 & u2 Anc u3 -> u1 Anc u3 
vf total = \/u1,u2:!u1=u2 -> u1 Anc u2 | u2 Anc u1
vf RingTopo = irrAnc & total & tran & nopred & nosucc

chan get : U, R
chan rel : U, R
chan usebeg : U, R
chan useend : U, R

ltsc UR =
	lts
		I =   	get(u1,r) -> L
			[] tau() -> I
		L =   	usebeg(u1,r) -> W
			[] rel(u1,r) -> I
			[] tau() -> L
		W = 	useend(u1,r) -> L
	from I

ltsc TLR =
	lts
		NO =   	get(u1,r) -> N1
		N1 =   	rel(u1,r) -> N2
		N2 =   	get(u2,r) -> N3
		N3 =    rel(u2,r) -> NO
	from NO

ltsc THT =
	lts
		NO =   	get(uh,r) -> N1
		N1 =   	rel(uh,r) -> N2
		N2 =   	get(ut,r) -> N3
		N3 =    rel(ut,r) -> NO
	from NO

ltsc M2U =
	lts
		NO =   	usebeg(u1,r) -> N1 
			[] usebeg(u2,r) -> N2 
		N1 = 	useend(u1,r) -> NO
		N2 = 	useend(u2,r) -> NO
	from NO

ltsc Tk = (|| u1,u2 : [u1 Anc u2] TLR) || ([!uh = ut] THT)

ltsc SRS = (|| u1, r : UR) || (|| r: Tk)

ltsc Mtx = (|| u1, u2, r : M2U)

ssc LckE = (_) u1, r : {get(u1,r), rel(u1,r)}

trace refinement: verify SRS \ LckE against Mtx when RingTopo
