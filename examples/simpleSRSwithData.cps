type U
type R
type D

avar k : U
avar k1 : U
avar k2 : U
avar r : R
avar d : D

chan rdlock : U, R
chan wrlock : U, R
chan unlock : U, R
chan rdbeg : U, R
chan rdend : U, R, D
chan wrbeg : U, R, D
chan wrend : U, R

ltsc User =
	lts
		I =   	rdlock(k,r) -> RD
			[] wrlock(k,r) -> W
			[] tau() -> I
		RD =   	rdbeg(k,r) -> R2
			[] wrlock(k,r) -> W
			[] unlock(k,r) -> I
		R2 =  	[] d : rdend(k,r,d) -> RD
		W =   	rdbeg(k,r) -> W2
			[] [] d : wrbeg(k,r,d) -> W3
			[] unlock(k,r) -> I
		W2 = 	[] d : rdend(k,r,d) -> W
		W3 = 	wrend(k,r) -> W
	from I

ltsc Lock =
	lts
		NO =   	rdlock(k1,r) -> R1
			[] wrlock(k1,r) -> W1
			[] rdlock(k2,r) -> R2
			[] wrlock(k2,r) -> W2
		R1 =   	wrlock(k1,r) -> W1
			[] unlock(k1,r) -> NO
			[] rdlock(k2,r) -> R12
		R2 =   	wrlock(k2,r) -> W2
			[] unlock(k2,r) -> NO
			[] rdlock(k1,r) -> R12
		W1 =   	unlock(k1,r) -> NO
		W2 =   	unlock(k2,r) -> NO
		R12 =   unlock(k1,r) -> R2
			[] unlock(k2,r) -> R1
	from NO

ltsc Mutex1 =
	lts
		NO =   	rdbeg(k,r) -> R1
			[] [] d : wrbeg(k,r,d) -> N1 
		N1 = 	wrend(k,r) -> NO
		R1 =   	[] d : rdend(k,r,d) -> NO
	from NO

ltsc Mutex2 =
	lts
		NO =   	rdbeg(k1,r) -> R1
			[] rdbeg(k2,r) -> R2
			[] [] d : wrbeg(k1,r,d) -> N1 
			[] [] d : wrbeg(k2,r,d) -> N2 
		N1 = 	wrend(k1,r) -> NO
		N2 = 	wrend(k2,r) -> NO
		R1 =   	[] d : rdend(k1,r,d) -> NO
			[] rdbeg(k2,r) -> RR
		R2 =   	[] d : rdend(k2,r,d) -> NO
			[] rdbeg(k1,r) -> RR
		RR =   	[] d : rdend(k1,r,d) -> R2
			[] [] d : rdend(k2,r,d) -> R1
	from NO

ltsc SRS = (|| k, r : User) || (|| r, k1, k2 : [!k1=k2] Lock)

ltsc Mutex = (|| k1, k2, r : [!k1=k2] Mutex2) || (|| k,r : Mutex1)

ssc LcEv = (_) k, r : {rdlock(k,r), wrlock(k,r), unlock(k,r)}

trace refinement: verify SRS \ LcEv against Mutex
