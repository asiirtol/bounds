type U
relv Anc : U,U
avar u : U
avar unxt : U
avar u1 : U
avar u2 : U
avar u3 : U
avar u4 : U
avar rt : U
avar lf : U
chan tok : U
chan accbeg : U
chan accend : U

vf irrAnc = \/u:! u Anc u
vf asymAnc = \/u1,u2:! (u1 Anc u2 & u2 Anc u1)
vf disjAncRtLf = \/u1,u2:! (u1 Anc u2 & u2 = rt & u1 = lf)
vf rootNotLeaf = ! rt = lf
vf tran = \/u1,u2,u3: u1 Anc u2 & u2 Anc u3 -> u1 Anc u3 
vf rootleaf = \/u2: !rt = u2 & !lf = u2 -> rt Anc u2 & u2 Anc lf
vf total = \/u1,u2:!u1=u2 -> u1 Anc u2 | u2 Anc u1
vf RingTopo = irrAnc & asymAnc & disjAncRtLf & total & tran & rootleaf & rootNotLeaf

ltsc UserT1 = 
	lts
		INIT =     tok(lf) -> TOK1
		TOK1 = 	   accbeg(lf) -> TK1A
			[] tok(rt) -> INIT
		TK1A = 	   accend(lf) -> TOK1
	from TOK1

ltsc User = 
	lts
		INIT =     tok(u) -> TOK1
		TOK1 = 	   accbeg(u) -> TK1A
			[] tok(unxt) -> INIT
			[] tok(u) -> CHOS	
		TK1A = 	   accend(u) -> TOK1
		CHOS =    tok(u) -> CHOS
		        [] tok(unxt) -> CHOS
			[] accbeg(u) -> CHOS
			[] accend(u) -> CHOS
	from INIT

ltsc Prop =
	lts
		NO =       accbeg(u1) -> R1
			[] accbeg(u2) -> R2
		R1 =       accend(u1) -> NO
			[] accbeg(u2) -> R12
		R2 =       accend(u2) -> NO
			[] accbeg(u1) -> R12
		R12 =      accend(u1) -> R2
			[] accend(u2) -> R1
	from NO

ltsc Prop1 = 
	lts 
		CHOS = 	   accbeg(u) -> CHOS 
			[] accend(u) -> CHOS
	from CHOS

ltsc Spec = (|| u : Prop1) || (|| u1, u2 : [!u1 =u2] Prop)
ltsc Sys = [true] (UserT1 || (|| u, unxt : [u Anc unxt] User))
ssc Tok = (_) u : {tok(u)}

trace refinement: verify Sys\Tok against Spec when RingTopo
