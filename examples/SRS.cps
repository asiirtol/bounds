type[1] U
type[2] R

rels[3] Anc : R,R

var u : U
var u1 : U
var u2 : U
var r : R
var r1 : R
var r2 : R
var r3 : R

chan srlock : U,R
chan sxlock : U,R
chan irlock : U,R
chan ixlock : U,R
chan unlock : U,R
chan nolock : U,R
chan rdbeg : U,R,R
chan rdend : U,R,R
chan wrbeg : U,R,R
chan wrend : U,R,R

vc irrefl = \/r : !r Anc r
vc asymm = \/r1, r2 : ! r1 Anc r2 | ! r2 Anc r1
vc trans = \/r1, r2, r3 : r1 Anc r2 & r2 Anc r3 -> r1 Anc r3
vc ancprop = \/r1, r2, r3 : ! r1 = r2 & r1 Anc r3 & r2 Anc r3 -> r1 Anc r2 | r2 Anc r1
vc ForestTopo = irrefl & asymm & trans & ancprop
 
plts User1 =
	lts
		I =   	srlock(u,r1) -> RD
			[] sxlock(u,r1) -> W
			[] tau() -> I
		RD =   	rdbeg(u,r2,r1) -> R2
			[] srlock(u,r1) -> RD
			[] sxlock(u,r1) -> W
			[] unlock(u,r1) -> I
			[] tau() -> RD
		R2 =  	rdend(u,r2,r1) -> RD
		W =   	rdbeg(u,r2,r1) -> W2
			[] wrbeg(u,r2,r1) -> W3
			[] srlock(u,r1) -> W
			[] sxlock(u,r1) -> W
			[] unlock(u,r1) -> I
			[] tau() -> W
		W2 = 	rdend(u,r2,r1) -> W
		W3 = 	wrend(u,r2,r1) -> W
	from I

plts User2 = 
	lts
		I =    	irlock(u,r1) -> IR
		    	[] srlock(u,r1) -> I
		    	[] ixlock(u,r1) -> IX
		    	[] sxlock(u,r1) -> I
			[] nolock(u,r1)-> UN
			[] unlock(u,r2) -> UN
			[] nolock(u,r2) -> UN
		IR =   	irlock(u,r2) -> I
		    	[] srlock(u,r2) -> I
			[] tau() -> I
		IX =   	ixlock(u,r2) -> I
		    	[] sxlock(u,r2) -> I
			[] tau() -> I
		UN =   	unlock(u,r1) -> I
			[] tau() -> I
	from I
	
plts Lock =
	lts
		NO =   	irlock(u1,r) -> IR
			[] srlock(u1,r) -> SR
			[] ixlock(u1,r) -> IX
			[] sxlock(u1,r) -> SX
			[] unlock(u1,r) -> NO
			[] nolock(u1,r) -> NO
			[] irlock(u2,r) -> NO
			[] srlock(u2,r) -> NO
			[] ixlock(u2,r) -> NO
			[] sxlock(u2,r) -> NO
		IR =   	irlock(u1,r) -> IR
			[] srlock(u1,r) -> SR
			[] ixlock(u1,r) -> IX
			[] sxlock(u1,r) -> SX
			[] unlock(u1,r) -> NO
			[] irlock(u2,r) -> IR
			[] srlock(u2,r) -> IR
			[] ixlock(u2,r) -> IR
		SR =   	irlock(u1,r) -> SR
			[] srlock(u1,r) -> SR
			[] ixlock(u1,r) -> SX
			[] sxlock(u1,r) -> SX
			[] unlock(u1,r) -> NO
			[] irlock(u2,r) -> SR
			[] srlock(u2,r) -> SR
		IX =   	irlock(u1,r) -> IX
			[] srlock(u1,r) -> SX
			[] ixlock(u1,r) -> IX
			[] sxlock(u1,r) -> SX
			[] unlock(u1,r) -> NO
			[] irlock(u2,r) -> IX
			[] ixlock(u2,r) -> IX
		SX =   	irlock(u1,r) -> SX
			[] srlock(u1,r) -> SX
			[] ixlock(u1,r) -> SX
			[] sxlock(u1,r) -> SX
			[] unlock(u1,r) -> NO
	from NO

plts Prop2 =
	lts
		NO =   	rdbeg(u1,r3,r1) -> R1
			[] [!u1=u2] rdbeg(u2,r3,r2) -> R2
			[] wrbeg(u1,r3,r1) -> N1 
			[] [!u1=u2] wrbeg(u2,r3,r2) -> N2 
		N1 = 	wrend(u1,r3,r1) -> NO
		N2 = 	[!u1=u2] wrend(u2,r3,r2) -> NO
		R1 =   	rdend(u1,r3,r1) -> NO
			[] [!u1=u2] rdbeg(u2,r3,r2) -> RR
		R2 =   	[!u1=u2] rdend(u2,r3,r2) -> NO
			[] rdbeg(u1,r3,r1) -> RR
		RR =   	rdend(u1,r3,r1) -> R2
			[] [!u1=u2] rdend(u2,r3,r2) -> R1
	from NO

plts Prop1 =
	lts
		NO =   	rdbeg(u,r2,r1) -> N1
			[] wrbeg(u,r2,r1) -> N2
		N1 = 	rdend(u,r2,r1) -> NO
		N2 = 	wrend(u,r2,r1) -> NO
	from NO

vc Anc12 = r1 Anc r2
vc AncEq12 = r1 = r2 | Anc12
vc Neq12 = ! u1 = u2

plts Sys = (|| u, r1, r2 : [AncEq12] User1) || (|| u, r1, r2 : [Anc12] User2) || (|| r, u1, u2 : [Neq12] Lock)

vc AncEq13 = r1 = r3 | r1 Anc r3
vc AncEq23 = r2 = r3 | r2 Anc r3

plts Spec = (|| u1, u2, r1, r2, r3 : [AncEq13 & AncEq23] Prop2)

pset LockAct = (_) u, r : {irlock(u,r), srlock(u,r), ixlock(u,r), sxlock(u,r), nolock(u,r), unlock(u,r)}

trace refinement: verify Sys \ LockAct against Spec when ForestTopo
