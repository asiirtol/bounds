type T
var t : T
chan ev1 : T
chan ev2 : T

plts P1 =
	lts
		I1 = ev1(t) -> I2
		I2 = ev2(t) -> I1
	from I1

plts P2 = || t: P1

trace refinement: verify P2 against P2

