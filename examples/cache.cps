type P
type R
type D

var p : P
var p2 : P
var r : R
var r2 : R
var d : D
var d2 : D

chan rdbeg : P,R
chan rdend : P,R,D
chan wrbeg : P,R,D
chan wrend : P,R
chan rdmem : P,R,D
chan flush : R,D

plts CUnit = 
    lts
        I =           rdbeg(p,r) -> IR
            [] [] d : wrbeg(p,r,d) -> IW(d)
	    []        rdbeg(p2,r) -> I
	    [] [] d : wrbeg(p2,r,d) -> I
	    [] [] d : flush(r,d) -> I
        IR =   [] d : rdmem(p,r,d) -> IR2(d)
            []        rdbeg(p2,r) -> IR
	IR2(d) =      rdend(p,r,d) -> S(d)
            []        rdbeg(p2,r) -> IR2(d)
	IW(d) =       wrend(p,r) -> M(d)
	
	S(d) =        rdbeg(p,r) -> SR(d)
	       []     rdbeg(p2,r) -> S(d)
	       [] [] d : wrbeg(p,r,d) -> SW(d)
	       []     flush(r,d) -> I
	       []     tau() -> I
	SR(d) =       rdend(p,r,d) -> S(d)
            []        rdbeg(p2,r) -> SR(d)
	SW(d) =       wrend(p,r) -> M(d)
	
	M(d) =        rdbeg(p,r) -> MR(d)
	       [] [] d : wrbeg(p,r,d) -> MW(d)
	       []     flush(r,d) -> S(d)
	       []     flush(r,d) -> I
	MR(d) =       rdend(p,r,d) -> M(d)
	MW(d) =       wrend(p,r) -> M(d)
    from I

plts Cache = || p,p2,r : [!p=p2] CUnit

plts MUnit =
    lts
        I = []d: tau() -> M(d)
	M(d) =            rdmem(p,r,d) -> M(d)
	       [] [] d2 : flush(r,d2) -> M(d2)
    from I
	      
plts Mem = || p,r : MUnit	      

plts CacheMem = Cache || Mem

plts Mut2 =
    lts
        I =    rdbeg(p,r) -> R1
	    [] rdbeg(p2,r) -> R2
	    [] [] d : wrbeg(p,r,d) -> W1
	    [] [] d : wrbeg(p2,r,d) -> W2
	R1 =    [] d : rdend(p,r,d) -> I
             []        rdbeg(p2,r) -> R12
	R2 =    [] d : rdend(p2,r,d) -> I
             []        rdbeg(p,r) -> R12
	R12 =    [] d : rdend(p,r,d) -> R2
	      [] [] d : rdend(p2,r,d) -> R1
	W1 =        wrend(p,r) -> I
	W2 =        wrend(p2,r) -> I
    from I

plts Mutex = || p,p2,r : [!p=p2] Mut2

pset MemAcc = (_) p,r,d : {rdmem(p,r,d), flush(r,d)}

trace refinement: verify CacheMem \ MemAcc against Mutex 
