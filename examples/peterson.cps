type U
type R

var u1 : U
var u2 : U
var r : R

chan lockreq : U,R
chan lockget : U,R
chan lockrel : U,R
chan lockfree : U,R

chan flagset : U,R
chan flagclear : U,R
chan flagread : U,U,R
chan flagtrue : U,U,R
chan flagfalse : U,U,R


chan turnunset : U,R
chan turnread : U,U,R
chan turntrue : U,U,R
chan turnfalse : U,U,R

plts LockIF = 
	lts
		I =    [] u1: lockreq(u1,r) -> I
		    [] [] u1: lockget(u1,r) -> L(u1)
		L(u1) =       lockrel(u1,r) -> I
	from I

plts ChozPart =
	lts
		I =    flagset(u1,r) -> I
		    [] flagclear(u1,r) -> I
		    [] flagtrue(u1,u2,r) -> I
		    [] flagread(u1,u2,r) -> I
		    [] flagfalse(u1,u2,r) -> I
		    [] turnunset(u1,r) -> I
		    [] turntrue(u1,u2,r) -> I
		    [] turnread(u1,u2,r) -> I
		    [] turnfalse(u1,u2,r) -> I
	from I

plts Choz = || u1,u2 : [! u1 = u2] ChozPart

plts LockPartImpl =
	lts
		I = lockreq(u1,r) -> SF
		SF = flagset(u1,r) -> CT
		CT = turnunset(u1,r) -> LR
		LR = [!u1=u2] flagread(u1,u2,r) -> FR
		     [] [u1=u2] tau() -> CS
		FR =    flagtrue(u1,u2,r) -> TT
		     [] flagfalse(u1,u2,r) -> CS
		TT = turnread(u1,u2,r) -> TR
		TR = 	turntrue(u1,u2,r) -> LR
		     [] turnfalse(u1,u2,r) -> CS
		CS = lockget(u1,r) -> EC
		EC = lockrel(u1,r) -> CF
		CF = flagclear(u1,r) -> I
	from I

plts LockImpl = || u1,u2 : LockPartImpl

plts FlagPart =
	lts 
		F =    flagset(u1,r) -> T
		    [] flagclear(u1,r) -> F
	            [] flagread(u2,u1,r) -> FR
		FR =   flagset(u1,r) -> TR
		    [] flagclear(u1,r) -> FR
		    [] flagfalse(u2,u1,r) -> F
		T =    flagset(u1,r) -> T
		    [] flagclear(u1,r) -> F
	            [] flagread(u2,u1,r) -> TR
		TR =   flagset(u1,r) -> TR
		    [] flagclear(u1,r) -> FR
		    [] flagtrue(u2,u1,r) -> T
	from F

plts Flag = || u1,u2 : [! u1 = u2] FlagPart

plts TurnPart =
	lts
		T =    turnread(u2,u1,r) -> TR
		    [] turnunset(u1,r) -> T2
		    [] turnunset(u2,r) -> T1
		T1 =   turnread(u2,u1,r) -> TR1
		    [] turnunset(u1,r) -> T2
		    [] turnunset(u2,r) -> T1
		    [] tau() -> T
		T2 =   turnread(u2,u1,r) -> TR2
		    [] turnunset(u1,r) -> T2
		    [] turnunset(u2,r) -> T1
		    [] tau() -> T
		TR =   turntrue(u2,u1,r) -> T
		    [] turnunset(u1,r) -> TR2
		    [] turnunset(u2,r) -> TR1
		TR1 =  turntrue(u2,u1,r) -> T1
		    [] turnunset(u1,r) -> TR2
		    [] turnunset(u2,r) -> TR1
		    [] tau() -> TR
		TR2 =  turnfalse(u2,u1,r) -> TR2
		    [] turnunset(u1,r) -> TR2
		    [] turnunset(u2,r) -> TR1
		    [] tau() -> TR
	from T

plts Turn = || u1,u2 : [! u1 = u2] TurnPart

pset FlagAct = (_) u1,u2: {flagset(u1,r),flagclear(u1,r),flagread(u1,u2,r),flagset(u1,r),flagfalse(u1,u2,r),flagtrue(u1,u2,r)}

pset TurnAct = (_) u1,u2: {turnunset(u1,r),turnread(u1,u2,r),turnfalse(u1,u2,r),turntrue(u1,u2,r)}

plts Peterson = (LockImpl || Flag || Turn) \ TurnAct \ FlagAct

trace refinement: verify Peterson against LockIF 

