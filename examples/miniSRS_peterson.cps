type U
type R

avar u : U
avar u1 : U
avar u2 : U
avar r : R

chan lockreq : U,R
chan lockget : U, R
chan lockrel : U, R

chan usebeg : U, R
chan useend : U, R

chan flagset : U,R
chan flagclear : U,R
chan flagread : U,U,R
chan flagtrue : U,U,R
chan flagfalse : U,U,R


chan turnunset : U,R
chan turnread : U,U,R
chan turntrue : U,U,R
chan turnfalse : U,U,R

plts LockPartIF =
	lts
		I =    lockreq(u1,r) -> LR1
		    [] lockreq(u2,r) -> LR2
		LR1 =  lockget(u1,r) -> LG1
		    [] lockreq(u2,r) -> LR12
		LR2 =  lockget(u2,r) -> LG2
		    [] lockreq(u1,r) -> LR12
		LR12 = lockget(u1,r) -> LG1R
		    [] lockget(u2,r) -> LG2R
		LG1 =  lockrel(u1,r) -> I
		    [] lockreq(u2,r) -> LG1R
		LG2 =  lockrel(u2,r) -> I
		    [] lockreq(u1,r) -> LG2R
		LG1R = lockrel(u1,r) -> LR2
		LG2R = lockrel(u2,r) -> LR1
	from I

plts LockIF = || u1,u2 : [! u1 = u2] LockPartIF

plts LockPartImpl =
	lts
		I = lockreq(u1,r) -> SF
		SF = flagset(u1,r) -> CT
		CT = turnunset(u1,r) -> LR
		LR = flagread(u1,u2,r) -> FR
		FR =    flagtrue(u1,u2,r) -> TT
		     [] flagfalse(u1,u2,r) -> CS
		TT = turnread(u1,u2,r) -> TR
		TR = 	turntrue(u1,u2,r) -> LR
		     [] turnfalse(u1,u2,r) -> CS
		CS = lockget(u1,r) -> EC
		EC = lockrel(u1,r) -> CF
		CF = flagclear(u1,r) -> I
	from I

plts LockImpl = || u1,u2 : [! u1 = u2] LockPartImpl

plts FlagPart =
	lts 
		F =    flagset(u1,r) -> T
		    [] flagclear(u1,r) -> F
	            [] flagread(u2,u1,r) -> FR
		FR =   flagset(u1,r) -> TR
		    [] flagclear(u1,r) -> FR
		    [] flagfalse(u2,u1,r) -> F
		T =    flagset(u1,r) -> T
		    [] flagclear(u1,r) -> F
	            [] flagread(u2,u1,r) -> TR
		TR =   flagset(u1,r) -> TR
		    [] flagclear(u1,r) -> FR
		    [] flagtrue(u2,u1,r) -> T
	from F

plts Flag = || u1,u2 : [! u1 = u2] FlagPart

plts TurnPart =
	lts
		T =    turnread(u2,u1,r) -> TR
		    [] turnunset(u1,r) -> T2
		    [] turnunset(u2,r) -> T1
		T1 =   turnread(u2,u1,r) -> TR1
		    [] turnunset(u1,r) -> T2
		    [] turnunset(u2,r) -> T1
		    [] tau() -> T
		T2 =   turnread(u2,u1,r) -> TR2
		    [] turnunset(u1,r) -> T2
		    [] turnunset(u2,r) -> T1
		    [] tau() -> T
		TR =   turntrue(u2,u1,r) -> T
		    [] turnunset(u1,r) -> TR2
		    [] turnunset(u2,r) -> TR1
		TR1 =  turnread(u2,u1,r) -> T1
		    [] turnunset(u1,r) -> TR2
		    [] turnunset(u2,r) -> TR1
		    [] tau() -> TR
		TR2 =  turnfalse(u2,u1,r) -> TR2
		    [] turnunset(u1,r) -> TR2
		    [] turnunset(u2,r) -> TR1
		    [] tau() -> TR
	from T

plts Turn = || u1,u2 : [! u1 = u2] TurnPart

plts Peterson = LockImpl || Flag || Turn

pset PetAct = (_) u1,u2 : {flagset(u1,r), flagclear(u1,r), flagread(u1,u2,r), flagtrue(u1,u2,r), flagfalse(u1,u2,r), turnunset(u1,r), turnread(u1,u2,r), turntrue(u1,u2,r), turnfalse(u1,u2,r)}

ltsc UR =
	lts
		I =   	lockreq(u,r) -> RD
			[] tau() -> I
		RD =	lockget(u,r) -> L
		L =   	usebeg(u,r) -> W
			[] lockrel(u,r) -> I
			[] tau() -> L
		W = 	useend(u,r) -> L
	from I

ltsc M2U =
	lts
		NO =   	usebeg(u1,r) -> N1 
			[] usebeg(u2,r) -> N2 
		N1 = 	useend(u1,r) -> NO
		N2 = 	useend(u2,r) -> NO
	from NO

ltsc SRS = (|| u, r : UR) || (|| r : (Peterson \ PetAct))

ltsc Mtx = (|| u1, u2, r : M2U)

ssc LckE = (_) u, r : {lockreq(u,r), lockget(u,r), lockrel(u,r)}

trace refinement: verify SRS \ LckE against Mtx
