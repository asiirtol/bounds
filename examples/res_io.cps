type U
type R

var u : U
var u1 : U
var u2 : U
var r : R

chan usebeg : U,R
chan useend : U,R

plts IR2U =
	lts
		NO =   	?usebeg(u1,r) -> N1 
			[] ?usebeg(u2,r) -> N2 
		N1 = 	useend(u1,r) => NO
		N2 = 	useend(u2,r) => NO
	from NO

plts IRU =
	lts
		NO =   	?usebeg(u,r) -> N1 
		N1 = 	useend(u,r) => NO
	from NO

plts IRes = (|| u1, u2 : [!u1 = u2] IR2U) || (|| u : IRU)

plts Res = || u : IRU

alternating simulation: verify Res against IRes

