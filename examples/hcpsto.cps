type H
type A

var h : H
var h1: H
var h2: H
var h3: H
var a : A
var a2: A

rels Sto : H,H

vc irrefl = \/h1 : ! h1 Sto h1
vc trans = \/h1,h2,h3 : h1 Sto h2 & h2 Sto h3 -> h1 Sto h3
vc total = \/h1,h2 : h1 Sto h2 | h2 Sto h1 | h1 = h2
vc SysTopo = irrefl & trans & total

chan ihave : H,A

ltsc Host =
	lts
		I =	   []a2:ihave(h2,a2) -> N(a2)
		N(a2) =    []a:[!a=a2]ihave(h,a) -> Stp
		Unreach =  []a:ihave(h,a) -> Stp
	from I

ltsc DifAdr =
	lts
		I =    	     []a2:ihave(h2,a2) -> I
			  [] []a: ihave(h,a) -> S1(a) 	
		S1(a) =	     []a2:[!a2=a] ihave(h2,a2) -> S1(a)
			  [] ihave(h,a) -> S1(a)
	from I

ltsc HCP = ||h,h2:[h2 Sto h] Host

ltsc Spec = ||h,h2:[!h = h2] DifAdr

trace refinement: verify HCP against Spec when SysTopo
 


