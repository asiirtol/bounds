sort[1] T
sort[2] S

chan vote : S, T, S
chan candidate : S, T
chan leader : S, T

pred QS : S,T,S
pred FS : S,T,S,S

var s0 : S
var s1 : S
var s2 : S
var s3 : S
var t : T

frml qstofs = \/ s0,t : ((\/ s1: !QS(s0,t,s1)) | (QS(s0,t,s0) & (\/ s1: ((!QS(s0,t,s1)) -> !(\/ s2: !FS(s0,t,s1,s2))))))
frml fstoqs = \/ s0,s1,t,s2 : (FS(s0,t,s1,s2) -> (QS(s0,t,s2) & !QS(s0,t,s1)))
frml nonempty = \/ s0,t : !(\/ s1 : ! (\/ s2 : (!FS(s0,t,s2,s1) & !FS(s0,t,s1,s2))))
frml inj = \/ s0,s1,s2,s3,t : ((FS(s0,t,s1,s3) & FS(s0,t,s2,s3)) -> s1=s2)
// This is an extra conjunct for speeding up the computation
frml irrefl = \/ s0,s1,t: !FS(s0,t,s1,s1)
frml Qrm = qstofs & fstoqs & nonempty & inj & irrefl

plts Spec1 =
    lts
        I   =       leader(s0,t) -> I
    from I

plts Spec2 =
    lts
        I   =       leader(s0,t) -> S0
                []  leader(s1,t) -> S1
        S0  =       leader(s0,t) -> S0
        S1  =       leader(s1,t) -> S1
    from I

// plts Spec = (|| s0,s1,s2,t : [QS(s0,t,s0) & QS(s0,t,s2) & QS(s1,t,s1) & QS(s1,t,s2) & (!s0=s1) & (!s1=s2) & (!s0=s2)] Spec2) || (|| s0,s1,t : [QS(s0,t,s0) & QS(s0,t,s1) & QS(s1,t,s1) & QS(s1,t,s0)] Spec2) || (|| s0,t : [QS(s0,t,s0)] Spec1)

plts Spec = (|| s0,s1,s2,t : [QS(s0,t,s2) & QS(s1,t,s2)] Spec2) 

plts Ldr2 =
    lts
        C   =       candidate(s0,t) -> C1
                []  vote(s1,t,s0) -> C
        C1  =       vote(s1,t,s0) -> L
        L   =       leader(s0,t) -> L
                []  vote(s1,t,s0) -> L
    from C

plts Flw3 =
    lts
        F   =       candidate(s0,t) -> F0
                []  vote(s0,t,s1) -> F1
                []  vote(s0,t,s2) -> F2
        F1  =       vote(s0,t,s1) -> F1
        F2  =       vote(s0,t,s2) -> F2
	F0  =       vote(s0,t,s0) -> F0
    from F

plts Raft = || s0 : ((|| t,s1 : [QS(s0,t,s0) & QS(s0,t,s1)] Ldr2) || (|| s1, s2 : [!s1=s2] || t: Flw3))

pset LE = (_) s0,s1,t: {candidate(s0,t), vote(s0,t,s1)}

trace refinement: verify Raft \ LE against Spec when Qrm
