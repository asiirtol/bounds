type T
type N

relv Par : N,N
relv Anc : N,N

avar t : T
avar t1 : T
avar t2 : T
avar n : N
avar n1 : N
avar n2 : N
avar n3 : N
avar rt : N

type Bool = {xtrue,xfalse}

// the sets of subtree, level and node write lock modes
type Lck = {ir,nr,lr,sr,ix,lrix,srix,cx,lrcx,srcx,su,sx}
rf Lsw = {sx}
rf Llw = Lsw
rf Lnw = Llw
// the sets of subtree, level and node read lock modes
rf Lsr = {sr,srix,srcx,su}
rf Llr = Lsr + {lr,lrix,lrcx}
rf Lnr = Llr + {nr,ix,cx}
// the sets of lock modes that give no subtree, level and node access
rf Lso = Lck - (Lsw + Lsr)
rf Llo = Lck - (Llw + Llr)
rf Lno = Lck - (Lnw + Lnr)

rf Lckrd = {ir,nr,lr,sr}
rf Lckix = {ix,lrix,srix}
rf Lckcx = {cx,lrcx,srcx}
rf Lcksr = {sr,srix,srcx}
rf Lcklr = {lr,lrix,lrcx}

chan lockbeg : T
chan lockend : T
chan trend : T
chan read : T,N,N
chan write : T,N,N
chan lockget : Lck,T,N
chan lock : Lck,Lck,T,N

avar r : Bool
avar w : Bool

avar x : Lck
avar y : Lck
avar l : Lck
avar l1 : Lck
avar l2 : Lck
avar ln : Lck
avar rn : Lck

rf Comp = {(ir,x), (nr,x) | ! x in {sx,su} } + 
	  {(lr,x) | x in Lckrd+Lckix} + 
	  {(sr,x) | x in Lckrd} +
	  {(ix,x) | x in Lckrd+Lckix+Lckcx-Lcksr} +
	  {(lrix,x) | x in Lckrd+Lckix-Lcksr} +
	  {(srix,x) | x in Lckrd-Lcksr} +
	  {(cx,x) | x in Lckrd+Lckix+Lckcx-Lcksr-Lcklr} +
	  {(lrcx,x) | x in Lckrd+Lckix-Lcksr-Lcklr} +
	  {(srcx,x) | x in Lckrd-Lcksr-Lcklr} +
	  {(su,x) | x in Lckrd}

rf cnv = 
	{(ir,x,x), (x,ir,x)} + 
	{(sx,x,sx), (x,sx,sx)} +
	{(x,x,x)} +
	{(nr,x,x), (x,nr,x) | !x = ir} +
	{(srcx,x,srcx), (x,srcx,srcx) | ! x in {su,sx}} + 
	{(srcx,su,sx), (su,srcx,sx)} +
	{(su,lr,su), (lr,su,su)} + {(su,sr,su), (sr,su,sr)} + 
	{(su,x,sx), (x,su,sx) | ! x in {ir,nr,lr,sr,su}} +
	{(sr,lr,sr), (lr,sr,sr)} + 
	{(sr,x,srix), (x,sr,srix) | x in {ix,lrix,srix}} + 
	{(sr,x,srcx), (x,sr,srcx) | x in {cx,lrcx}} +
	{(srix,x,srix), (x,srix,srix) | x in {lr,ix,lrix}} + 
	{(srix,x,srcx), (x,srix,srcx) | x in {lrcx,cx}} +
	{(lrcx,x,lrcx), (x,lrcx,lrcx) | x in {lr,ix,lrix,cx}} + 
	{(cx,ix,cx), (ix,cx,cx)} + 
	{(cx,x,lrcx), (x,cx,lrcx) | x in {lr,lrix}} +
	{(lrix,x,lrix), (x,lrix,lrix) | x in {ix,lr}} +
	{(lr,ix,lrix), (ix,lr,lrix)}

// precedence function
rf prv = {(x,ir) | x in {ir,nr,lr,sr}} + {(x,ix) | ! x in {ir,nr,lr,sr,sx}} + {(sx,cx)}

rf Aprv = {(x,ir) | x in {ir,nr,lr,sr}} + {(x,y) | y in {ix,cx}}


vf irrPar = \/n:! n Par n
vf irrAnc = \/n:! n Anc n
vf asymPar = \/n1,n2:! (n1 Par n2 & n2 Par n1)
vf asymAnc = \/n1,n2:! (n1 Anc n2 & n2 Anc n1)
vf disjParAnc = \/n1,n2:! (n1 Anc n2 & n1 Par n2)
vf tran = \/n1,n2,n3: (n1 Par n2 | n1 Anc n2) & (n2 Par n3 | n2 Anc n3) -> n1 Anc n3 
vf uniqPar = \/n1,n2,n3: n2 Par n1 & n3 Par n1 -> n2 = n3
vf predTriangle = \/n1,n2,n3: (n2 Par n1 | n2 Anc n1) & (n3 Par n1 | n3 Anc n1) & !n2 = n3 -> n2 Anc n3 | n2 Par n3 | n3 Anc n2 | n3 Par n2
vc conn = \/n1 : (n1 = rt | rt Anc n1 | rt Par n1)
vf TreeTopo = irrPar & irrAnc & asymPar & asymAnc & disjParAnc & predTriangle & tran & uniqPar & (!t1 = t2) & conn 

ltsc TNP = 
	lts
		M	= lockbeg(t) -> L
		L     	= lockend(t) -> M
			[] []l1, l2: lock(l1,l2,t,n2) -> L1(l2)
			[] []l1, l2: lock(l1,l2,t,n1) -> LE
		L1(l) = [] l2: lock(prv(l),l2,t,n1) -> LE
		LE    = lockend(t) -> M
		UNRCH	= []l1, l2: lock(l1,l2,t,n1) ->UNRCH
			[] []l1, l2: lock(l1,l2,t,n2) ->UNRCH
	from M

ltsc TORD = 
  	lts
		M	= lockbeg(t) -> L
		L	= lockend(t) -> M
			[] []l1, l2: lock(l1,l2,t,n3) -> L2
			[] []l1, l2: lock(l1,l2,t,n2) -> L1
			[] []l1, l2: lock(l1,l2,t,n1) -> LE
		L2	= []l1, l2: lock(l1,l2,t,n2) -> L1
		L1	= []l1, l2: lock(l1,l2,t,n1) -> LE
		LE	= lockend(t) -> M
		UNRCH	= []l1, l2: lock(l1,l2,t,n1) ->UNRCH
			[] []l1, l2: lock(l1,l2,t,n2) ->UNRCH
			[] []l1, l2: lock(l1,l2,t,n3) ->UNRCH
	from M

ltsc TNA =
	lts
		M	= lockbeg(t) -> L
		L	= lockend(t) -> M
			[] []l1, l2: lock(l1,l2,t,n2) -> L1(l2)
			[] []l1, l2: lock(l1,l2,t,n1) -> LE
		L1(l)	= []rn, ln: [l Aprv rn] lock(rn,ln,t,n1) -> LE
		LE	= lockend(t) -> M
		UNRCH	= []l1, l2: lock(l1,l2,t,n1) ->UNRCH
			[] []l1, l2: lock(l1,l2,t,n2) ->UNRCH

	from M

ltsc TNRW = 
  	lts
    		M(r,w)	= lockbeg(t) -> L(r,w)
			[] []l: [l in Lno] lockget(l,t,n) -> M(r,w)
			[] []l: [l in Lnr] lockget(l,t,n) -> M(xtrue,w)
			[] []l: [l in Lnw] lockget(l,t,n) -> M(xtrue,xtrue)
			[] [r = xtrue] read(t,n,n) -> M(r,w)
			[] [w = xtrue] write(t,n,n) -> M(r,w)
			[] trend(t) -> HALT
		L(r,w)	= lockend(t) -> M(r,w)
			[] []l1, l2: [l1 in Lno] lock(l1,l2,t,n) -> LE(r,w)
			[] []l1, l2: [l1 in Lnr] lock(l1,l2,t,n) -> LE(xtrue,w)
			[] []l1, l2: [l1 in Lnw] lock(l1,l2,t,n) -> LE(xtrue,xtrue)
		LE(r,w)	= lockend(t) -> M(r,w)
		UNRCH	= []l1, l2: lock(l1,l2,t,n) ->UNRCH
			[] []l: lockget(l,t,n) -> UNRCH
	from M(xfalse,xfalse)

ltsc TLRW = 
	lts
		M(r,w)	= lockbeg(t) -> L(r,w)
			[] []l: [l in Llo] lockget(l,t,n1) -> M(r,w)
			[] []l: [l in Llr] lockget(l,t,n1) -> M(xtrue,w)
			[] []l: [l in Llw] lockget(l,t,n1) -> M(xtrue,xtrue)
			[] [r = xtrue] read(t,n2,n1) -> M(r,w)
			[] [w = xtrue] write(t,n2,n1) -> M(r,w)
			[] trend(t) -> HALT
		L(r,w)  = lockend(t) -> M(r,w)
			[] []l1, l2: [l1 in Llo] lock(l1,l2,t,n1) -> LE(r,w)
			[] []l1, l2: [l1 in Llr] lock(l1,l2,t,n1) -> LE(xtrue,w)
			[] []l1, l2: [l1 in Llw] lock(l1,l2,t,n1) -> LE(xtrue,xtrue)
		LE(r,w) = lockend(t) -> M(r,w)
		UNRCH	= []l1, l2: lock(l1,l2,t,n1) ->UNRCH
			[] []l: lockget(l,t,n1) -> UNRCH
	from M(xfalse,xfalse)

ltsc TSRW = 
	lts
		M(r,w)	= lockbeg(t) -> L(r,w)
			[] []l: [l in Lso] lockget(l,t,n1) -> M(r,w)
			[] []l: [l in Lsr] lockget(l,t,n1) -> M(xtrue,w)
			[] []l: [l in Lsw] lockget(l,t,n1) -> M(xtrue,xtrue)
			[] [r = xtrue] read(t,n2,n1) -> M(r,w)
			[] [w = xtrue] write(t,n2,n1) -> M(r,w)
			[] trend(t) -> HALT
		L(r,w)	= lockend(t) -> M(r,w)
			[] []l1, l2: [l1 in Lso] lock(l1,l2,t,n1) -> LE(r,w)
			[] []l1, l2: [l1 in Lsr] lock(l1,l2,t,n1) -> LE(xtrue,w)
			[] []l1, l2: [l1 in Lsw] lock(l1,l2,t,n1) -> LE(xtrue,xtrue)
		LE(r,w)	= lockend(t) -> M(r,w)
		UNRCH	= []l1, l2: lock(l1,l2,t,n1) ->UNRCH
			[] []l: lockget(l,t,n1) -> UNRCH
	from M(xfalse,xfalse)

ltsc NT =
	lts
		N0	= []l: lock(l,l,t,n) -> S1(l)
			[] trend(t) -> N0
		S1(l)	= []l1: lock(l1,cnv(l1,l),t,n) -> S1(cnv(l1,l))
			[] lockget(l,t,n) -> S1(l)
			[] trend(t) -> N0
		UNRCH	= []l1, l2: lock(l1,l2,t,n) ->UNRCH
			[] []l: lockget(l,t,n) -> UNRCH
	from N0
	
ltsc NTT =
	lts
		N0	= []l: lock(l,l,t,n) -> S1(l)
			[] []l1, l2: lock(l1,l2,t2,n) -> N0
			[] trend(t) -> N0
		S1(l)	= []l1: lock(l1,cnv(l1,l),t,n) -> S1(cnv(l1,l))
			[] []l1, l2: [l2 Comp l] lock(l1,l2,t2,n) -> S1(l)
			[] lockget(l,t,n) -> S1(l)
			[] trend(t) -> N0
		UNRCH	= []l1, l2: lock(l1,l2,t,n) ->UNRCH
			[] []l1, l2: lock(l1,l2,t2,n) ->UNRCH
			[] []l: lockget(l,t,n) -> UNRCH
			[] []l: lockget(l,t2,n) -> UNRCH
	from N0

ltsc PROP2 =
	lts
		P	= read(t1,n3,n1) -> PR1
			[] read(t1,n3,n2) -> PR1
			[] read(t2,n3,n1) -> PR2
			[] read(t2,n3,n2) -> PR2
			[] write(t1,n3,n2) -> PX1
			[] write(t1,n3,n1) -> PX1
			[] write(t2,n3,n1) -> PX2
			[] write(t2,n3,n2) -> PX2
			[] trend(t1) -> P
			[] trend(t2) -> P
		PR1	= read(t1,n3,n1) -> PR1
			[] read(t1,n3,n2) -> PR1
			[] read(t2,n3,n1) -> PR12
			[] read(t2,n3,n2) -> PR12
			[] write(t1,n3,n1) -> PX1
			[] write(t1,n3,n2) -> PX1
			[] trend(t1) -> P
			[] trend(t2) -> PR1
		PR2	= read(t2,n3,n1) -> PR2
			[] read(t2,n3,n2) -> PR2
			[] read(t1,n3,n1) -> PR12
			[] read(t1,n3,n2) -> PR12
			[] write(t2,n3,n1) -> PX2
			[] write(t2,n3,n2) -> PX2
			[] trend(t2) -> P
			[] trend(t1) -> PR2
		PR12	= read(t1,n3,n1) -> PR12
			[] read(t1,n3,n2) -> PR12
			[] read(t2,n3,n1) -> PR12
			[] read(t2,n3,n2) -> PR12
			[] trend(t1) -> PR2
			[] trend(t2) -> PR1
		PX1	=  read(t1,n3,n1) -> PX1
			[] read(t1,n3,n2) -> PX1
			[] write(t1,n3,n1) -> PX1
			[] write(t1,n3,n2) -> PX1
			[] trend(t1) -> P
			[] trend(t2) -> PX1
		PX2	=  read(t2,n3,n1) -> PX2
			[] read(t2,n3,n2) -> PX2
			[] write(t2,n3,n1) -> PX2
			[] write(t2,n3,n2) -> PX2
			[] trend(t2) -> P
			[] trend(t1) -> PX2
	from P

ltsc PROP1 =
	lts
		P	= read(t,n2,n1) -> P
			[] write(t,n2,n1) -> P
			[] trend(t) -> HALT
	from P

ltsc CHOZ =
	lts
		I	= []l1, l2: lock(l1,l2,t,n) -> I
			[] []l: lockget(l,t,n) -> I
			[] lockbeg(t) -> I
			[] lockend(t) -> I
	from I
	
ltsc Sys = ||t: ((||n1, n2: [n1 Par n2] TNP) || (||n1, n2: [n1 Anc n2] TNA) || (||n1, n2, n3: [(n1 Par n2 | n1 Anc n2) & (n2 Par n3 | n2 Anc n3)] TORD) || (||n: TNRW) || (||n1, n2: [n1 Par n2] TLRW) || (||n1, n2: [n1 Anc n2] TSRW) || (||n: NT) || (||n, t2: [!t = t2] NTT))

ssc LcAc = (_)t, n: (_)l1, l2: {lockbeg(t), lockend(t), lockget(l1,t,n), lock(l1,l2,t,n)}

ltsc Spec = (||t, n1, n2: [n1 = n2 | n1 Anc n2 | n1 Par n2] PROP1) || (||t1, t2: [!t1 = t2] ||n1, n2, n3: [(n1 = n3 | n1 Anc n3 | n1 Par n3) & (n2 = n3 | n2 Anc n3 | n2 Par n3)] PROP2)


trace refinement: verify Sys \ LcAc against Spec when TreeTopo
