type U
type R

var u1 : U
var u2 : U
var r : R

chan lockreq : U,R
chan lockget : U,R
chan lockrel : U,R
chan lockfree : U,R

chan flagset : U,R
chan flagclear : U,R
chan flagread : U,U,R
chan flagtrue : U,U,R
chan flagfalse : U,U,R


chan turnunset : U,R
chan turnread : U,U,R
chan turntrue : U,U,R
chan turnfalse : U,U,R

plts IM2U =
	lts
		I =    ?lockreq(u1,r) -> LR1
		    [] ?lockreq(u2,r) -> LR2
		LR1 =  lockget(u1,r) -> LG1
		    [] ?lockreq(u2,r) -> LR12
		LR2 =  lockget(u2,r) -> LG2
		    [] ?lockreq(u1,r) -> LR12
		LR12 = lockget(u1,r) -> LG1R
		    [] lockget(u2,r) -> LG2R
		LG1 =  ?lockrel(u1,r) -> LF1
		    [] ?lockreq(u2,r) -> LG1R
		LG2 =  ?lockrel(u2,r) -> LF2
		    [] ?lockreq(u1,r) -> LG2R
		LG1R = ?lockrel(u1,r) -> LF1R
		LG2R = ?lockrel(u2,r) -> LF2R
		LF1 =  lockfree(u1,r) -> I
		    [] ?lockreq(u2,r) -> LF1R
		LF2 =  lockfree(u2,r) -> I
		    [] ?lockreq(u1,r) -> LF2R
		LF1R = lockfree(u1,r) -> LR2
		    [] lockget(u2,r) -> LF1G
		LF2R = lockfree(u2,r) -> LR1
		    [] lockget(u1,r) -> LF2G
		LF1G = lockfree(u1,r) -> LG2
		    [] ?lockrel(u2,r) -> LF12
		LF2G = lockfree(u2,r) -> LG1
		    [] ?lockrel(u1,r) -> LF12
		LF12 = lockfree(u1,r) -> LF2
		    [] lockfree(u2,r) -> LF1
	from I

plts IMtx = || u1,u2 : [! u1 = u2] IM2U

plts MtxPartImpl =
	lts
		I = ?lockreq(u1,r) -> SF
		SF = flagset(u1,r) -> CT
		CT = turnunset(u1,r) -> LR
		LR = flagread(u1,u2,r) -> FR
		FR =    ?flagtrue(u1,u2,r) -> TT
		     [] ?flagfalse(u1,u2,r) -> CS
		TT = turnread(u1,u2,r) -> TR
		TR = 	?turntrue(u1,u2,r) -> LR
		     [] ?turnfalse(u1,u2,r) -> CS
		CS = lockget(u1,r) -> EC
		EC = ?lockrel(u1,r) -> CF
		CF = flagclear(u1,r) -> LF
		LF = lockfree(u1,r) -> I
	from I

plts MtxImpl = || u1,u2 : [! u1 = u2] MtxPartImpl

plts FlagPart =
	lts 
		F =    ?flagset(u1,r) -> T
		    [] ?flagclear(u1,r) -> F
	            [] ?flagread(u2,u1,r) -> FR
		FR =   ?flagset(u1,r) -> TR
		    [] ?flagclear(u1,r) -> FR
		    [] flagfalse(u2,u1,r) -> F
		T =    ?flagset(u1,r) -> T
		    [] ?flagclear(u1,r) -> F
	            [] ?flagread(u2,u1,r) -> TR
		TR =   ?flagset(u1,r) -> TR
		    [] ?flagclear(u1,r) -> FR
		    [] flagtrue(u2,u1,r) -> T
	from F

plts Flag = || u1,u2 : [! u1 = u2] FlagPart

plts TurnPart =
	lts
		T =    ?turnread(u2,u1,r) -> TR
		    [] ?turnunset(u1,r) -> T2
		    [] ?turnunset(u2,r) -> T1
		T1 =   ?turnread(u2,u1,r) -> TR1
		    [] ?turnunset(u1,r) -> T2
		    [] ?turnunset(u2,r) -> T1
		    [] tau() -> T
		T2 =   ?turnread(u2,u1,r) -> TR2
		    [] ?turnunset(u1,r) -> T2
		    [] ?turnunset(u2,r) -> T1
		    [] tau() -> T
		TR =   turntrue(u2,u1,r) -> T
		    [] ?turnunset(u1,r) -> TR2
		    [] ?turnunset(u2,r) -> TR1
		TR1 =  turntrue(u2,u1,r) -> T1
		    [] ?turnunset(u1,r) -> TR2
		    [] ?turnunset(u2,r) -> TR1
		    [] tau() -> TR
		TR2 =  turnfalse(u2,u1,r) -> T2
		    [] ?turnunset(u1,r) -> TR2
		    [] ?turnunset(u2,r) -> TR1
		    [] tau() -> TR
	from T

plts Turn = || u1,u2 : [! u1 = u2] TurnPart

plts Peterson = MtxImpl || Flag || Turn

alternating simulation: verify Peterson against IMtx

