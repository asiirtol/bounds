type H
type A

var h : H
var h2: H
var a : A
var a2: A

chan timeout
chan whohas : H,A
chan ihave : H,A

plts Host =
	lts
		I =	   timeout() -> I
			[] []a:whohas(h2,a) -> I
			[] []a:ihave(h2,a) -> I
			[] []a:whohas(h,a) -> W(a)
		W(a) =	   timeout() -> S(a)
			[] timeout() -> I
			[] ihave(h2,a) -> I
			[] whohas(h2,a) -> I
			[] []a2:[!a2=a] ihave(h2,a2) -> W(a)
			[] []a2:[!a2=a] whohas(h2,a2) -> W(a)
		S(a) =      timeout() -> S(a)
			[] ihave(h,a) -> S(a)
			[] whohas(h2,a) -> R(a)
			[] []a2:[!a2=a] whohas(h2,a2) -> S(a)
			[] []a2:ihave(h2,a2) -> S(a)
		R(a) =	    ihave(h,a) -> S(a)
	from I

plts DifAdr =
	lts
		I =    	     []a2:ihave(h2,a2) -> I
			  [] []a: ihave(h,a) -> S1(a) 	
		S1(a) =	     []a2:[!a2=a] ihave(h2,a2) -> S1(a)
			  [] ihave(h,a) -> S1(a)
	from I

pset WTEv = (_)h,a:{timeout(), whohas(h,a)}

plts Sys = ||h,h2:[!h=h2] Host

plts Spec = ||h,h2:[!h=h2] DifAdr

plts Choz =
	lts
		I = 	timeout() -> I
		     [] []a: whohas(h,a) -> I
	from I

trace refinement: verify Sys \ WTEv against Spec 
 


