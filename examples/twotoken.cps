type U
relv Par : U,U
relv Anc : U,U
avar u : U
avar unxt : U
avar u1 : U
avar u2 : U
avar u3 : U
avar rt : U
avar lf : U
chan tok : U
chan accbeg : U
chan accend : U

vf irrPar = \/u:! u Par u
vf irrAnc = \/u:! u Anc u
vf rootNotLeaf = ! rt = lf
vf asymPar = \/u1,u2:! (u1 Par u2 & u2 Par u1)
vf asymAnc = \/u1,u2:! (u1 Anc u2 & u2 Anc u1)
vf disjParAnc = \/u1,u2:! (u1 Anc u2 & u1 Par u2)
vf disjAncRtLf = \/u1,u2:! (u1 Anc u2 & u2 = rt & u1 = lf)
vf disjParRtLf = \/u1,u2:! (u1 Par u2 & u2 = rt & u1 = lf)
vf tran = \/u1,u2,u3: (u1 Par u2 | u1 Anc u2) & (u2 Par u3 | u2 Anc u3) -> u1 Anc u3 
vf uniqSucc = \/u1,u2,u3: u1 Par u2 & u1 Par u3 -> u2 = u3
vf uniqPred = \/u1,u2,u3: u2 Par u1 & u3 Par u1 -> u2 = u3
vf root = \/u1,u2: u1 Par u2 | u1 Anc u2 -> !u2=rt
vf leaf = \/u1,u2: u1 Par u2 | u1 Anc u2 -> !u1=lf
vf total = \/u1,u2:!u1=u2 -> u1 Par u2 | u1 Anc u2 | u2 Par u1 | u2 Anc u1
vf RingTopo = irrPar & irrAnc & rootNotLeaf & asymPar & asymAnc & disjParAnc & disjParRtLf & disjAncRtLf & total & tran & uniqSucc & uniqPred & root & leaf

ltsc UserT1 = 
	lts
		INIT =     tok(lf) -> TOK1
		TOK1 = 	   accbeg(lf) -> TK1A
			[] tok(rt) -> INIT
			[] tok(lf) -> TOK2
		TK1A = 	   accend(lf) -> TOK1
		TOK2 = 	   accbeg(lf) -> TK2A
			[] tok(rt) -> TOK1
		TK2A =     accend(lf) -> TOK2
	from TOK2

ltsc User = 
	lts
		INIT =     tok(u) -> TOK1
		TOK1 = 	   accbeg(u) -> TK1A
			[] tok(unxt) -> INIT
			[] tok(u) -> TOK2
		TK1A = 	   accend(u) -> TOK1
		TOK2 = 	   accbeg(u) -> TK2A
			[] tok(unxt) -> TOK1
			[] tok(u) -> CHOS	
		TK2A =     accend(u) -> TOK2
		CHOS =    tok(u) -> CHOS
		        [] tok(unxt) -> CHOS
			[] accbeg(u) -> CHOS
			[] accend(u) -> CHOS
	from INIT

ltsc UserC = 
	lts
		ININ =     tok(u) -> T1IN
		T1IN = 	   accbeg(u) -> A1IN
			[] tau() -> INT1
			[] tok(u) -> T2IN
		A1IN = 	   accend(u) -> T1IN
		T2IN = 	   accbeg(u) -> A2IN
			[] tau() -> T1T1
			[] tok(u) -> CHOS
		A2IN = 	   accend(u) -> T2IN
		INT1 =     tok(u) -> T1T1
			[] tok(unxt) -> ININ
		T1T1 = 	   accbeg(u) -> A1T1
			[] tau() -> INT2
			[] tok(unxt) -> T1IN
			[] tok(u) -> T2T1
		A1T1 = 	   accend(u) -> T1T1
			[] tok(unxt) -> A1IN
		T2T1 = 	   accbeg(u) -> A2T1
			[] tau() -> T1T2
			[] tok(unxt) -> T2IN
			[] tok(u) -> CHOS	
		A2T1 = 	   accend(u) -> T2T1
			[] tok(unxt) -> A2IN
		INT2 =     tok(u) -> T1T2
			[] tok(unxt) -> INT1
		T1T2 = 	   accbeg(u) -> A1T2
			[] tau() -> CHOS
			[] tok(unxt) -> T1T1
			[] tok(u) -> T2T2
		A1T2 = 	   accend(u) -> T1T2
			[] tok(unxt) -> A1T1
		T2T2 = 	   accbeg(u) -> A2T2
			[] tau() -> CHOS
			[] tok(unxt) -> T2T1
			[] tok(u) -> CHOS	
		A2T2 = 	   accbeg(u) -> T2T2
			[] tok(unxt) -> A2T1
		CHOS =    tok(u) -> CHOS
		        [] tok(unxt) -> CHOS
			[] accbeg(u) -> CHOS
			[] accend(u) -> CHOS
	from ININ

ltsc Prop =
	lts
		NO =       accbeg(u1) -> R1
			[] accbeg(u2) -> R2
			[] accbeg(u3) -> R3
		R1 =       accend(u1) -> NO
			[] accbeg(u2) -> R12
			[] accbeg(u3) -> R13
		R2 =       accend(u2) -> NO
			[] accbeg(u1) -> R12
			[] accbeg(u3) -> R23
		R3 =       accend(u3) -> NO
			[] accbeg(u1) -> R13
			[] accbeg(u2) -> R23
		R12 =      accend(u1) -> R2
			[] accend(u2) -> R1
			[] accbeg(u3) -> R123
		R13 =      accend(u1) -> R3
			[] accend(u3) -> R1
			[] accbeg(u2) -> R123
		R23 =      accend(u2) -> R3
			[] accend(u3) -> R2
			[] accbeg(u1) -> R123
		R123 =     accend(u1) -> R23
			[] accend(u2) -> R13
			[] accend(u3) -> R12
	from NO

ltsc Prop1 = 
	lts 
		CHOS = 	   accbeg(u) -> CHOS 
			[] accend(u) -> CHOS
	from CHOS

ltsc Spec = (|| u : Prop1) || (|| u1, u2, u3 : [!u1 = u2 & !u1 = u3 & !u2 = u3] Prop)
ltsc Sys = UserT1 || (|| u, unxt : [u Par unxt] User) || (|| u, unxt : [u Anc unxt] UserC)
ssc Tok = (_) u : {tok(u)}

trace refinement: verify Sys\Tok against Spec when RingTopo
