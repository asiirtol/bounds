type U
type R

var u : U
var u1 : U
var u2 : U
var r : R

chan lockreq : U,R
chan lockget : U, R
chan lockrel : U, R
chan usebeg : U, R
chan useend : U, R

plts User =
	lts
		I =   	lockreq(u,r) -> RD
			[] tau() -> I
		RD =	lockget(u,r) -> L
		L =   	usebeg(u,r) -> WR
			[] lockrel(u,r) -> I
			[] tau() -> L
		WR = 	useend(u,r) -> L
	from I

plts LockIF = 
	lts
		I =    [] u1: lockreq(u1,r) -> I
		    [] [] u1: lockget(u1,r) -> L(u1)
		L(u1) =       lockrel(u1,r) -> I
	from I

plts MtxRes =
	lts
		NO =   	[] u: usebeg(u,r) -> N1(u) 
		N1(u) =	useend(u,r) -> NO
	from NO

plts SRS = (|| u, r : User) || (|| r : LockIF)

plts Mtx = (|| r : MtxRes)

pset LckE = (_) u, r : {lockreq(u,r), lockget(u,r), lockrel(u,r)}

trace refinement: verify SRS \ LckE against Mtx 
