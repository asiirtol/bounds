type S
type T

chan vote : S, T, S
chan candidate : S, T
chan leader : S, T

qfv Maj : S <- S,T

var s0 : S
var s1 : S
var s2 : S
var t : T

plts Spec2 =
    lts
        I   =       leader(s0,t) -> S0
                []  leader(s1,t) -> S1
        S0  =       leader(s0,t) -> S0
        S1  =       leader(s1,t) -> S1
    from I

plts Spec = || s0,s1,t : [s0 in Maj(s0,t) & s1 in Maj(s1,t)] Spec2

plts Ldr2 =
    lts
        C   =       candidate(s0,t) -> C1
                []  vote(s1,t,s0) -> C        
        C1  =       vote(s1,t,s0) -> L
        L   =       leader(s0,t) -> L
                []  vote(s1,t,s0) -> L
    from C

plts Flw3 =
    lts
        F   =       candidate(s0,t) -> F0
                []  vote(s0,t,s1) -> F1
                []  vote(s0,t,s2) -> F2
        F1  =       vote(s0,t,s1) -> F1
        F2  =       vote(s0,t,s2) -> F2
	F0  =       vote(s0,t,s0) -> F0
    from F

plts Raft = || s0 : ((|| t,s1 : [s0 in Maj(s0,t) & s1 in Maj(s0,t)] Ldr2 ) || (|| s1, s2 : [!s1=s2] || t: Flw3))

pset LE = (_) s0,s1,t: {candidate(s0,t), vote(s0,t,s1)}

trace refinement: verify Raft \ LE against Spec
