type U
type R

var u : U
var u1 : U
var u2 : U
var r : R

chan lockreq : U,R
chan lockget : U,R
chan lockrel : U,R
chan lockfree : U,R
chan usebeg : U,R
chan useend : U,R

plts IM2U =
	lts
		I =    ?lockreq(u1,r) -> LR1
		    [] ?lockreq(u2,r) -> LR2
		LR1 =  lockget(u1,r) -> LG1
		    [] ?lockreq(u2,r) -> LR12
		LR2 =  lockget(u2,r) -> LG2
		    [] ?lockreq(u1,r) -> LR12
		LR12 = lockget(u1,r) -> LG1R
		    [] lockget(u2,r) -> LG2R
		LG1 =  ?lockrel(u1,r) -> LF1
		    [] ?lockreq(u2,r) -> LG1R
		LG2 =  ?lockrel(u2,r) -> LF2
		    [] ?lockreq(u1,r) -> LG2R
		LG1R = ?lockrel(u1,r) -> LF1R
		LG2R = ?lockrel(u2,r) -> LF2R
		LF1 =  lockfree(u1,r) -> I
		    [] ?lockreq(u2,r) -> LF1R
		LF2 =  lockfree(u2,r) -> I
		    [] ?lockreq(u1,r) -> LF2R
		LF1R = lockfree(u1,r) -> LR2
		    [] lockget(u2,r) -> LF1G
		LF2R = lockfree(u2,r) -> LR1
		    [] lockget(u1,r) -> LF2G
		LF1G = lockfree(u1,r) -> LG2
		    [] ?lockrel(u2,r) -> LF12
		LF2G = lockfree(u2,r) -> LG1
		    [] ?lockrel(u1,r) -> LF12
		LF12 = lockfree(u1,r) -> LF2
		    [] lockfree(u2,r) -> LF1
	from I

plts IMtx = || u1,u2 : [! u1 = u2] IM2U

plts IUR =
	lts
		I =   	lockreq(u,r) -> RD
			[] tau() -> I
		RD =	?lockget(u,r) -> L
		L =   	usebeg(u,r) -> WR
			[] lockrel(u,r) -> F
			[] tau() -> L
		WR = 	?useend(u,r) -> L
		F = 	?lockfree(u,r) -> I
	from I

plts IUser = || r : IUR

plts IR2U =
	lts
		NO =   	?usebeg(u1,r) -> N1 
			[] ?usebeg(u2,r) -> N2 
		N1 = 	useend(u1,r) => NO
		N2 = 	useend(u2,r) => NO
	from NO

plts IRU =
	lts
		NO =   	?usebeg(u,r) -> N1 
		N1 = 	useend(u,r) => NO
	from NO

plts IRes = (|| u1, u2 : IR2U) || (|| u : IRU)

plts ISRS = (|| r: IMtx) || (|| u: IUser) || (|| r: IRes)

compatibility: verify ISRS

