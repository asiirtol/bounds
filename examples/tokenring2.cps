type U
relv Anc : U,U
relv RtLf : U,U
avar u : U
avar unxt : U
avar u1 : U
avar u2 : U
avar u3 : U
avar u4 : U
chan tok : U
chan accbeg : U
chan accend : U

vf irrAnc = \/u:! u Anc u
vf irrRtLf = \/u:! u RtLf u
vf asymAnc = \/u1,u2:! (u1 Anc u2 & u2 Anc u1)
vf disjAncRtLf = \/u1,u2:! (u1 Anc u2 & u1 RtLf u2)
vf singRtLf = \/u1,u2,u3,u4:u1 RtLf u2 & u3 RtLf u4 -> u1 = u3 & u2 = u4
vf tran = \/u1,u2,u3: u1 Anc u2 & u2 Anc u3 -> u1 Anc u3 
vf rootleaf = \/u1,u2,u3: u2 RtLf u1 & !u1 = u3 & !u2 = u3 -> u1 Anc u3 & u3 Anc u2
vf total = \/u1,u2:!u1=u2 -> u1 Anc u2 | u2 Anc u1
vf RingTopo = irrAnc & irrRtLf & asymAnc & disjAncRtLf & singRtLf & total & tran & rootleaf

ltsc UserT1 = 
	lts
		INIT =     tok(u) -> TOK1
		TOK1 = 	   accbeg(u) -> TK1A
			[] tok(unxt) -> INIT
		TK1A = 	   accend(u) -> TOK1
	from TOK1

ltsc User = 
	lts
		INIT =     tok(u) -> TOK1
		TOK1 = 	   accbeg(u) -> TK1A
			[] tok(unxt) -> INIT
			[] tok(u) -> CHOS	
		TK1A = 	   accend(u) -> TOK1
		CHOS =    tok(u) -> CHOS
		        [] tok(unxt) -> CHOS
			[] accbeg(u) -> CHOS
			[] accend(u) -> CHOS
	from INIT

ltsc Prop =
	lts
		NO =       accbeg(u1) -> R1
			[] accbeg(u2) -> R2
		R1 =       accend(u1) -> NO
			[] accbeg(u2) -> R12
		R2 =       accend(u2) -> NO
			[] accbeg(u1) -> R12
		R12 =      accend(u1) -> R2
			[] accend(u2) -> R1
	from NO

ltsc Prop1 = 
	lts 
		CHOS = 	   accbeg(u) -> CHOS 
			[] accend(u) -> CHOS
	from CHOS

ltsc Spec = || u, unxt : [u RtLf unxt] ((|| u : Prop1) || (|| u1, u2 : [!u1 =u2] Prop))
ltsc Sys = || u, unxt : [u RtLf unxt] (UserT1 || (|| u, unxt : [u Anc unxt] User))
ssc Tok = (_) u : {tok(u)}

trace refinement: verify Sys\Tok against Spec when RingTopo
