type U
type R
type D

relv Anc : R,R

avar u : U
avar u1 : U
avar u2 : U
avar r : R
avar r1 : R
avar r2 : R
avar r3 : R
avar d : D

chan srlock : U,R
chan sxlock : U,R
chan irlock : U,R
chan ixlock : U,R
chan unlock : U,R
chan nolock : U,R
chan rdbeg : U,R,R
chan rdend : U,R,R,D
chan wrbeg : U,R,R,D
chan wrend : U,R,R

vc irrefl = \/r : !r Anc r
vc asymm = \/r1, r2 : ! r1 Anc r2 | ! r2 Anc r1
vc trans = \/r1, r2, r3 : r1 Anc r2 & r2 Anc r3 -> r1 Anc r3
vc ancprop = \/r1, r2, r3 : ! r1 = r2 & r1 Anc r3 & r2 Anc r3 -> r1 Anc r2 | r2 Anc r1
vc ForestTopo = irrefl & asymm & trans & ancprop
 
plts User1 =
	lts
		I =   	srlock(u,r1) -> RD
			[] sxlock(u,r1) -> W
			[] tau() -> I
		RD =   	rdbeg(u,r2,r1) -> R2
			[] srlock(u,r1) -> RD
			[] sxlock(u,r1) -> W
			[] unlock(u,r1) -> I
			[] tau() -> RD
		R2 =  	[] d : rdend(u,r2,r1,d) -> RD
		W =   	rdbeg(u,r2,r1) -> W2
			[] [] d : wrbeg(u,r2,r1,d) -> W3
			[] srlock(u,r1) -> W
			[] sxlock(u,r1) -> W
			[] unlock(u,r1) -> I
			[] tau() -> W
		W2 = 	[] d : rdend(u,r2,r1,d) -> W
		W3 = 	wrend(u,r2,r1) -> W
	from I

plts User2 = 
	lts
		I =    	irlock(u,r1) -> IR
		    	[] srlock(u,r1) -> I
		    	[] ixlock(u,r1) -> IX
		    	[] sxlock(u,r1) -> I
			[] nolock(u,r1)-> UN
			[] unlock(u,r2) -> UN
			[] nolock(u,r2) -> UN
		IR =   	irlock(u,r2) -> I
		    	[] srlock(u,r2) -> I
			[] tau() -> I
		IX =   	ixlock(u,r2) -> I
		    	[] sxlock(u,r2) -> I
			[] tau() -> I
		UN =   	unlock(u,r1) -> I
			[] tau() -> I
	from I
	
plts Lock =
	lts
		NO =   	irlock(u1,r) -> IR
			[] srlock(u1,r) -> SR
			[] ixlock(u1,r) -> IX
			[] sxlock(u1,r) -> SX
			[] unlock(u1,r) -> NO
			[] nolock(u1,r) -> NO
			[] irlock(u2,r) -> NO
			[] srlock(u2,r) -> NO
			[] ixlock(u2,r) -> NO
			[] sxlock(u2,r) -> NO
		IR =   	irlock(u1,r) -> IR
			[] srlock(u1,r) -> SR
			[] ixlock(u1,r) -> IX
			[] sxlock(u1,r) -> SX
			[] unlock(u1,r) -> NO
			[] irlock(u2,r) -> IR
			[] srlock(u2,r) -> IR
			[] ixlock(u2,r) -> IR
		SR =   	irlock(u1,r) -> SR
			[] srlock(u1,r) -> SR
			[] ixlock(u1,r) -> SX
			[] sxlock(u1,r) -> SX
			[] unlock(u1,r) -> NO
			[] irlock(u2,r) -> SR
			[] srlock(u2,r) -> SR
		IX =   	irlock(u1,r) -> IX
			[] srlock(u1,r) -> SX
			[] ixlock(u1,r) -> IX
			[] sxlock(u1,r) -> SX
			[] unlock(u1,r) -> NO
			[] irlock(u2,r) -> IX
			[] ixlock(u2,r) -> IX
		SX =   	irlock(u1,r) -> SX
			[] srlock(u1,r) -> SX
			[] ixlock(u1,r) -> SX
			[] sxlock(u1,r) -> SX
			[] unlock(u1,r) -> NO
	from NO

plts Prop2 =
	lts
		NO =   	rdbeg(u1,r3,r1) -> R1
			[] rdbeg(u2,r3,r2) -> R2
			[] [] d : wrbeg(u1,r3,r1,d) -> N1 
			[] [] d : wrbeg(u2,r3,r2,d) -> N2 
		N1 = 	wrend(u1,r3,r1) -> NO
		N2 = 	wrend(u2,r3,r2) -> NO
		R1 =   	[] d : rdend(u1,r3,r1,d) -> NO
			[] rdbeg(u2,r3,r2) -> RR
		R2 =   	[] d : rdend(u2,r3,r2,d) -> NO
			[] rdbeg(u1,r3,r1) -> RR
		RR =   	[] d : rdend(u1,r3,r1,d) -> R2
			[] [] d : rdend(u2,r3,r2,d) -> R1
	from NO

plts Prop1 =
	lts
		NO =   	rdbeg(u,r2,r1) -> N1
			[] [] d : wrbeg(u,r2,r1,d) -> N2
		N1 = 	[] d : rdend(u,r2,r1,d) -> NO
		N2 = 	wrend(u,r2,r1) -> NO
	from NO

vc Anc12 = r1 Anc r2
vc AncEq12 = r1 = r2 | Anc12
vc Neq12 = ! u1 = u2

plts Sys = (|| u, r1, r2 : [AncEq12] User1) || (|| u, r1, r2 : [Anc12] User2) || (|| r, u1, u2 : [Neq12] Lock)

vc AncEq13 = r1 = r3 | r1 Anc r3
vc AncEq23 = r2 = r3 | r2 Anc r3

plts Spec = (|| u1, u2, r1, r2, r3 : [AncEq13 & AncEq23 & Neq12] Prop2) || (|| u, r1, r2 : [AncEq12] Prop1)

pset LockAct = (_) u, r : {irlock(u,r), srlock(u,r), ixlock(u,r), sxlock(u,r), nolock(u,r), unlock(u,r)}

trace refinement: verify Sys \ LockAct against Spec when ForestTopo
