grammar CPS;

options
{
    language=C;
}

tokens
{
TRUEVF = 'true';
FALSEVF = 'false';
ATOMVAR = 'avar';
VAR = 'var';
RELVAR = 'relv';
RELSYM = 'rels';
PREDSYM = 'pred';
TYPEVAR = 'type';
SORT = 'sort';
QFVAR = 'qfv';
VALF = 'vf';
VALC = 'vc';
FORMULA = 'frml';
SSC = 'ssc';
PSET = 'pset';
LTSC = 'ltsc';
PLTS = 'plts';
VERIFY = 'verify';
AGAINST = 'against';
WHEN = 'when';
IMPL = '->';
MAPS = '<-';
MUSTIMPL = '=>';
EQ ='<->';
NEG = '!';
AND = '&';
OR = '|';
FORALL = '\\/';
LTS ='lts';
FROM ='from';
PAR = '||';
CUP = '(_)';
PLUS = '+';
MINUS = '-';
HIDE = '\\';
CHOICE = '[]';
SET = 'set';
RELFUNC = 'rf';
IN = 'in';
CHANNEL = 'chan';
TAU = 'tau';
ERREVENT = 'err';
ERRSTATE = 'ERR';
}

@header
{
#include <string>
#include <iostream>
#include <set>
#include <limits>
#include <iomanip>
#include <sstream>
#include "element.h"
#include "elementschema.h"
#include "valuationformula.h"
#include "CPSLexer.h"
#include "context.h"
#include "timer.h"
#include "reduce.h"
#include "fdr2output4trref.h"
#include "fdr2output4altsim.h"
#include "fdr2output4comp.h"
}

@members
{
Context context;
pANTLR3_INPUT_STREAM           input;
pCPSParser parser;
std::string filename;
Timer timer(4);
int instCount = 0;
int valCount = 0;
int canCount = 0;
int storedMax = 0;
bool error =false;
VerType vertype = NOVERIFY;
int argcount = 0;
char** args = 0;

void printPosition(pANTLR3_COMMON_TOKEN tok)
{
    std::cout << "Line " << tok->line << ", Column " << tok->charPosition << ": ";
    error = true;
}

int main(int argc, char * argv[])
{
	int retval = -1;
	argcount = argc;
	args = argv;

	std::cout << std::endl << "This is Bounds 3.1!" << std::endl;
	std::cout << "Created by Antti Siirtola 2010-2019 (contact: antti.siirtola@oulu.fi)" << std::endl << std::endl; 

	if(argc >= 2)
	{
		pCPSLexer               lex;
		pANTLR3_COMMON_TOKEN_STREAM    tokens;
		
                // process command line arguments
                for(int i = 1; i < argc-1; i++)
                {
                    std::cout << "Unknown option: " << argv[i] << std::endl;
                    exit(1);
                }

		input  = antlr3AsciiFileStreamNew          ((pANTLR3_UINT8)argv[argc-1]);
		if(input != 0)
		{
			lex    = CPSLexerNew                (input);
			tokens = antlr3CommonTokenStreamSourceNew  (ANTLR3_SIZE_HINT, TOKENSOURCE(lex));
			parser = CPSParserNew               (tokens);

			filename = std::string(argv[argc-1]);
			size_t pos = filename.find_last_of('.');
			if(pos != std::string::npos)
			{
				if(filename.find_first_of('/',pos) == std::string::npos) filename.erase(pos);
			}

			timer.start();
                        try
                        {
        			             parser->cps(parser);
                        }
                        catch(std::bad_alloc& ba)
                        {
                                std::cout << "Bounds run out of memory." << std::endl;
                                std::cout << "Try to rerun the tool and provide smaller cut-offs manually." << std::endl << std::endl;
                        }
			timer.stop();
			if(error == true || parser-> pParser -> rec -> state -> errorCount != 0) 
                        {
                            std::cout << "Errors encountered!" << std::endl << std::endl;
                        }
                        else if(vertype == NOVERIFY)
                        {
                            std::cout << "No verification targets found." << std::endl << std::endl;
                        }
                        else
                        {
                            std::cout << "The total number of instances generated: " << instCount << std::endl;
                            std::cout << "    (the total number of valuations generated: " << valCount << std::endl;
                            std::cout << "     the total number of canonical forms computed: " << canCount << std::endl;
                            std::cout << "     the maximum number of valuations stored all at once: " << storedMax << ')' << std::endl;
                            std::cout << "Total time taken: " << timer.getTotalTime() << " seconds" << std::endl;
                            std::cout << "    (time taken by input processing: " << timer.getValue(0) << " seconds" << std::endl;
                            std::cout << "     time taken by the computation of valuations: " << timer.getValue(1) << " seconds" << std::endl;
                            std::cout << "     time taken by output processing: " << timer.getValue(2) << " seconds";
                            std::string tool;
                            switch(vertype)
                            {
                                case TRACEREF:
                                    std::cout << std::endl << "     time taken by trace refinement checking: " << timer.getValue(3) << " seconds)" << std::endl << std::endl;
                                    break;
                                case ALTSIM:
                                    tool = "boundsAlternatingSimulationChecker";
                                    std::cout << ')' << std::endl << std::endl;
                                    std::cout << "Run " << tool << " " << filename << " to complete the verification." << std::endl << std::endl;
                                    break;
                                case COMPAT:
                                    std::cout << std::endl << "     time taken by compatibility checking: " << timer.getValue(3) << " seconds)" << std::endl << std::endl;
                                    break;
                            }
                        }
                        
			parser ->free(parser);
			tokens ->free(tokens);
			lex    ->free(lex);
			input  ->close(input);
			retval = 0;
		}
		else
		{
			std::cout << "Unable to open file!" << std::endl;
			retval = -1;
		}
	}
	else
	{
		std::cout << "File name missing!" << std::endl;
		retval = -1;
	}
	context.clear();
	return retval;

}

}

cps	: 
	(
	((TYPEVAR | SORT)'[' valstr=POSINT ']' name=capitstr)
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else
		{
                    int id = atoi((char*)(($valstr.text)->chars));
                    context.insertTypeVariable($name.str, id);
		
                    #ifdef DEBUG
                    std::cout << "new type variable: " <<* $name.str << std::endl;
                    #endif
		}
                delete $name.str;
            }
	| ((TYPEVAR | SORT) 
            (
            (CAPITID'=')=> 
            (
            (name = capitstr) '=' '{'
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else
		{
                    context.insertFiniteType($name.str);

                    #ifdef DEBUG
                    std::cout << "new finite type: " << *$name.str << std::endl;
                    #endif
                }
                delete $name.str;
            }
            a = smallstr
            {
                if(!context.isNotDefined($a.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($a.str) << " already defined.\n";
                }
                else
                {                    
                    context.insertAtom($a.str);
                }
                delete $a.str;
            }
            (
            ',' a = smallstr
            {
                if(!context.isNotDefined($a.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($a.str) << " already defined.\n";
                }
                else 
                {                    
                    context.insertAtom($a.str);
                }
                delete $a.str;
            }
            )*
            '}'
                {
                    context.finiteTypeInserted();
                }
            )
            | 
            (
            name=capitstr
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else
                {
                    context.insertTypeVariable($name.str, std::numeric_limits<int>::max());

                    #ifdef DEBUG
                    std::cout << "new type variable: " << *$name.str << std::endl;
                    #endif
                }
                delete $name.str;
            }
            )
            )
        )
	| ((ATOMVAR | VAR)'[' valstr = POSINT ']' name=smallstr ':' type = capitstr)
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if ($type.str == 0)
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else if (!context.isTypeVariable($type.str))
                {   
                    printPosition($start);
                    std::cout << "Identifier " << *($type.str) << " is not a type.\n";
                }
                else
		{
                    int id = atoi((char*)(($valstr.text)->chars));
                    context.insertAtomVariable($name.str, $type.str, id);

                    #ifdef DEBUG
                    std::cout << "new atom variable: " << *$name.str << " of type " << *$type.str << std::endl;
                    #endif
		}
		delete $name.str;
		delete $type.str;
            }
	| ((ATOMVAR | VAR) name=smallstr ':' type = capitstr)
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if ($type.str == 0)
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else if (!context.isTypeVariable($type.str) && !context.isFiniteType($type.str))
                {   
                    printPosition($start);
                    std::cout << "Identifier " << *($type.str) << " is not a type.\n";
                }
                else
		{
                    context.insertAtomVariable($name.str, $type.str, std::numeric_limits<int>::max());

                    #ifdef DEBUG
                    std::cout << "new atom variable: " << *$name.str << " of type " << *$type.str << std::endl;
                    #endif
                }

		delete $name.str;
		delete $type.str;
            }
	| ((RELVAR | RELSYM | PREDSYM)'[' valstr =POSINT ']' name=string ':' types = capitstrvec )
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else if ($types.strv != 0)
                {   
                    int id = atoi((char*)(($valstr.text)->chars));
                    context.insertRelationVariable($name.str, $types.strv, id);

                    #ifdef DEBUG
                    std::cout << "new relation variable: " << *$name.str << " of type " << *$types.strv << std::endl;
                    #endif
                }
		        delete $name.str;
                delete $types.strv;
            }
	| ((RELVAR | RELSYM | PREDSYM) name=string ':' types = capitstrvec )
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else if ($types.strv != 0)
                {   
                    context.insertRelationVariable($name.str, $types.strv, std::numeric_limits<int>::max());

                    #ifdef DEBUG
                    std::cout << "new relation variable: " << *$name.str << " of type " << *$types.strv << std::endl;
                    #endif
                }
        		delete $name.str;
                delete $types.strv;
            }
	| (QFVAR'[' valstr =POSINT ']' name=string ':' type = capitstr )
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if ($type.str == 0)
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else if (!context.isTypeVariable($type.str) && !context.isFiniteType($type.str))
                {   
                    printPosition($start);
                    std::cout << "Identifier " << *($type.str) << " is not a type variable.\n";
                }
                else
		{
                    int id = atoi((char*)(($valstr.text)->chars));
                    context.insertQuorumFunctionVariable($name.str, $type.str, id);

                    #ifdef DEBUG
                    std::cout << "new quorum function variable: " << *$name.str << " of type " << *$type.str;
                    #endif
                }
		delete $name.str;
		delete $type.str;
            }
            (MAPS type = capitstr
            {
                if($type.str == 0)
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else if (!context.isTypeVariable($type.str) && !context.isFiniteType($type.str))
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else
                {
                    context.appendQuorumFunctionType($type.str);

                    #ifdef DEBUG
                    std::cout << " <- " << *$type.str;
                    #endif
                }
		delete $type.str;
            }
            (',' type = capitstr
            {
                if($type.str == 0)
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else if (!context.isTypeVariable($type.str) && !context.isFiniteType($type.str))
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else
                {
                    context.appendQuorumFunctionType($type.str);

                    #ifdef DEBUG
                    std::cout << " x " << *$type.str;
                    #endif
                }
		delete $type.str;
            }
            )*
            )?
            {
                    #ifdef DEBUG
                    std::cout << std::endl;
                    #endif
             }

	| (QFVAR name=string ':' type = capitstr )
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if ($type.str == 0)
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else if (!context.isTypeVariable($type.str) && !context.isFiniteType($type.str))
                {   
                    printPosition($start);
                    std::cout << "Identifier " << *($type.str) << " is not a type variable.\n";
                }
                else
		{
                    context.insertQuorumFunctionVariable($name.str, $type.str, std::numeric_limits<int>::max());

                    #ifdef DEBUG
                    std::cout << "new quorum function variable: " << *$name.str << " of type " << *$type.str;
                    #endif
                }
		delete $name.str;
		delete $type.str;
            }
            (MAPS type = capitstr
            {
                if($type.str == 0)
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else if (!context.isTypeVariable($type.str) && !context.isFiniteType($type.str))
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else
                {
                    context.appendQuorumFunctionType($type.str);

                    #ifdef DEBUG
                    std::cout << " <- " << *$type.str;
                    #endif
                }
		delete $type.str;
            }
            (',' type = capitstr
            {
                if($type.str == 0)
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else if (!context.isTypeVariable($type.str) && !context.isFiniteType($type.str))
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else
                {
                    context.appendQuorumFunctionType($type.str);

                    #ifdef DEBUG
                    std::cout << " x " << *$type.str;
                    #endif
                }
		delete $type.str;
            }
            )*
            )?
            {
                    #ifdef DEBUG
                    std::cout << std::endl;
                    #endif
             }

	| ((VALF | VALC | FORMULA) name=string '=' v = valf)
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if($v.vf == 0)
                {
                    printPosition($start);
                    std::cout << "Valuation constraint expected.\n";
                }
                else if(!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else
		{
                    context.insertValuationFormula($name.str, $v.vf);

                    #ifdef DEBUG
                    std::cout << "new valuation formula: " << *$name.str << " = " << $v.vf->toString(context) << std::endl;
                    #endif
                }
    		delete $name.str;
            }
	| ((SSC | PSET) name=string '=' ss = setschema)
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if ($ss.ssc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised set expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else
		{
                    context.insertSetSchema($name.str, $ss.ssc);

                    #ifdef DEBUG
                    std::cout << "new set schema: " << *$name.str << " = " << $ss.ssc->toString(context) << std::endl;
                    #endif
                }
    		delete $name.str;
            }
	| ((LTSC | PLTS) name=string '=' l = ltsschema)
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if ($l.ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else
		{
                    context.insertLTSSchema($name.str, $l.ltsc);

                    #ifdef DEBUG
                    std::cout << "new lts schema: " << *$name.str << " = " << $l.ltsc->toString(context) << std::endl;
                    #endif
                }
    		delete $name.str;
            }
	| (RELFUNC name=string '=' r = atomrel)
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if ($r.rel == 0)
                {
                    printPosition($start);
                    std::cout << "Relation expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else
		{
                    context.insertAtomRelation($name.str, $r.rel);

                    #ifdef DEBUG
                    std::cout << "new relation/function: " << *$name.str << " = " << $r.rel->toString(context) << std::endl;
                    #endif
                }
    		delete $name.str;
    		}
    	| (CHANNEL name=smallstr)
            {
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "String expected.\n";
                }
                else if (!context.isNotDefined($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is already defined.\n";
                }
                else
                {
                    context.insertChannel($name.str);

                    #ifdef DEBUG
                    std::cout << "new channel: " << *$name.str;
                    #endif
    		}
    		delete $name.str;
            }
            (':'
            type = capitstr
            {
                if($type.str == 0)
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else if (!context.isTypeVariable($type.str) && !context.isFiniteType($type.str))
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else
                {
                    context.appendChannelType($type.str);

                    #ifdef DEBUG
                    std::cout << " of type " << *$type.str;
                    #endif
                }
		delete $type.str;
            }
            (',' type = capitstr
            {
                if($type.str == 0)
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else if (!context.isTypeVariable($type.str) && !context.isFiniteType($type.str))
                {
                    printPosition($start);
                    std::cout << "Type expected.\n";
                }
                else
                {
                    context.appendChannelType($type.str);

                    #ifdef DEBUG
                    std::cout << " x " << *$type.str;
                    #endif
                }
		delete $type.str;
            }
            )*    		
            )?
            {
                #ifdef DEBUG
                std::cout << std::endl;
                #endif
            }
        )*
        (
    	('trace refinement:' VERIFY sys = ltsschema AGAINST spec = ltsschema (when = WHEN v = valf)? )
            {
    		if($spec.ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else if($sys.ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else if(error == true && parser-> pParser -> rec -> state -> errorCount > 0)
                {
                    printPosition($start);
                    std::cout << "Reduction failed due to previous errors.\n";
                }
                else
                {
                    std::cout << "Reducing a parameterised trace refinement task to a finitary one." << std::endl;
                    std::cout << "(implementation: " << $sys.ltsc->toString(context) << ", specification: " << $spec.ltsc->toString(context);
                    if($when != 0)
                    {
                        std::cout << ", topology: " << $v.vf->toString(context);
                    }
                    std::cout << ')' << std::endl << std::endl;
                    timer.switchToTimer(1);
                    vertype = TRACEREF;
                    ValuationFormula* truevf = new TopValuationFormula();
                    std::tuple<std::set<Valuation*,PointerCompare<Valuation> >*, int, int, int, std::map<TypeVariable,bool>*, bool, bool> vals{0,0,0,0,0,false,false};
                    if($when != 0)
                    {
                        vals = computeCutoffSet(context, $v.vf, $spec.ltsc, $sys.ltsc, vertype);
                    }
                    else
                    {
                        vals = computeCutoffSet(context, truevf, $spec.ltsc, $sys.ltsc, vertype);
                    }
                    valCount += std::get<1>(vals);
                    canCount += std::get<2>(vals);
                    storedMax = std::max(storedMax, std::get<3>(vals));
                    timer.switchToTimer(2);
                    if($when != 0)
                    {
                        instCount += TraceRefinementOutput::generateInstances(context, $spec.ltsc, $sys.ltsc, $v.vf, std::get<0>(vals), filename, *(std::get<4>(vals)), std::get<5>(vals), std::get<6>(vals));
                    }
                    else
                    {
                         instCount += TraceRefinementOutput::generateInstances(context, $spec.ltsc, $sys.ltsc, truevf, std::get<0>(vals), filename, *(std::get<4>(vals)), std::get<5>(vals), std::get<6>(vals));
                    }
                    timer.switchToTimer(3);
                    TraceRefinementOutput::checkInstances(filename, instCount, std::get<5>(vals), std::get<6>(vals), argcount, args);
                    delete std::get<4>(vals);
                    delete std::get<0>(vals);
                    delete truevf;
                    timer.switchToTimer(0);
		}
    		delete $spec.ltsc;
    		delete $sys.ltsc;
    		if($when != 0) delete $v.vf;
            }
    	| ('alternating simulation:' VERIFY sys = ltsschema AGAINST spec = ltsschema (when = WHEN v = valf)? )
            {
    		if($spec.ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else if($sys.ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
    		else if(!($spec.ltsc->isHidingFree()))
                {
                    printPosition($start);
                    std::cout << "Hiding is not allowed in the specification when checking for alternating simulation.\n";
                }
    		else if(!($sys.ltsc->isHidingFree()))
                {
                    printPosition($start);
                    std::cout << "Hiding is not allowed in the implementation when checking for alternating simulation.\n";
                }
                else if(error == true || parser-> pParser -> rec -> state -> errorCount > 0)
                {
                    printPosition($start);
                    std::cout << "Reduction failed due to previous errors.\n";
                }
                else
                {
                    std::cout << "Reducing a parameterised alternating simulation checking task to a finitary one." << std::endl;
                    std::cout << "(implementation: " << $sys.ltsc->toString(context) << ", specification: " << $spec.ltsc->toString(context);
                    if($when != 0)
                    {
                        std::cout << ", topology: " << $v.vf->toString(context);
                    }
                    std::cout << ')' << std::endl << std::endl;
                    timer.switchToTimer(1);
                    vertype = ALTSIM;
                    ValuationFormula* truevf = new TopValuationFormula();
                    std::tuple<std::set<Valuation*,PointerCompare<Valuation> >*, int, int, int, std::map<TypeVariable,bool>*, bool, bool> vals{0,0,0,0,0,false,false};
                    if($when != 0)
                    {
                        vals = computeCutoffSet(context, $v.vf, $spec.ltsc, $sys.ltsc, vertype);
                    }
                    else
                    {
                        vals = computeCutoffSet(context, truevf, $spec.ltsc, $sys.ltsc, vertype);
                    }
                    valCount += std::get<1>(vals);
                    canCount += std::get<2>(vals);
                    storedMax = std::max(storedMax, std::get<3>(vals));
                    timer.switchToTimer(2);
                    if($when != 0)
                    {
                        instCount += AlternatingSimulationOutput::generateInstances(context, $spec.ltsc, $sys.ltsc, $v.vf, std::get<0>(vals), filename, *(std::get<4>(vals)), std::get<5>(vals), std::get<6>(vals));
                    }
                    else
                    {
                        instCount += AlternatingSimulationOutput::generateInstances(context, $spec.ltsc, $sys.ltsc, truevf, std::get<0>(vals), filename, *(std::get<4>(vals)), std::get<5>(vals), std::get<6>(vals));
                    }
                    delete std::get<4>(vals);
                    delete std::get<0>(vals);
                    delete truevf;
                    timer.switchToTimer(0);
                    
		}
    		delete $spec.ltsc;
    		delete $sys.ltsc;
    		if($when != 0) delete $v.vf;
            }
    	| ('compatibility:' VERIFY sys = ltsschema (when = WHEN v = valf)? )
            {
                if($sys.ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
    		else if(!$sys.ltsc->isHidingFree())
                {
                    printPosition($start);
                    std::cout << "Hiding is not allowed when checking for compatibility.\n";
                }
                else if(error == true || parser-> pParser -> rec -> state -> errorCount > 0)
                {
                    printPosition($start);
                    std::cout << "Reduction failed due to previous errors.\n";
                }
                else
                {
                    LTSSchema* unitltsc = new ElementaryLTSSchema(new TransitionSetSchema(), new StateSchema(0,0));
                    std::cout << "Reducing a parameterised compatibility checking task to a finitary one." << std::endl;
                    std::cout << "(implementation: " << $sys.ltsc->toString(context);
                    if($when != 0)
                    {
                        std::cout << ", topology: " << $v.vf->toString(context);
                    }
                    std::cout << ')' << std::endl << std::endl;
                    timer.switchToTimer(1);
                    vertype = COMPAT;
                    ValuationFormula* truevf = new TopValuationFormula();
                    std::tuple<std::set<Valuation*,PointerCompare<Valuation> >*, int, int, int, std::map<TypeVariable,bool>*, bool, bool> vals{0,0,0,0,0,false,false};
                    if($when != 0)
                    {
                        vals = computeCutoffSet(context, $v.vf, unitltsc, $sys.ltsc, vertype);
                    }
                    else
                    {
                        vals = computeCutoffSet(context, truevf, unitltsc, $sys.ltsc, vertype);
                    }
                    valCount += std::get<1>(vals);
                    canCount += std::get<2>(vals);
                    storedMax = std::max(storedMax, std::get<3>(vals));
                    timer.switchToTimer(2);
                    if($when != 0)
                    {
                        instCount += CompatibilityOutput::generateInstances(context, $sys.ltsc, $v.vf, std::get<0>(vals), filename, *(std::get<4>(vals)), std::get<5>(vals), std::get<6>(vals));
                    }
                    else
                    {
                        instCount += CompatibilityOutput::generateInstances(context, $sys.ltsc, truevf, std::get<0>(vals), filename, *(std::get<4>(vals)), std::get<5>(vals), std::get<6>(vals));
                    }
                    timer.switchToTimer(3);
                    CompatibilityOutput::checkInstances(filename, instCount, std::get<5>(vals), std::get<6>(vals), argcount, args);
                    delete std::get<4>(vals);
                    delete std::get<0>(vals);
                    delete truevf;
                    timer.switchToTimer(0);
                    delete unitltsc;
                }
    		delete $sys.ltsc;
    		if($when != 0) delete $v.vf;
            }
	)
	;

ltsschema	returns [LTSSchema* ltsc]
	: (l = parltsc)
            {
                $ltsc = 0;
                if($l.ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else
                {
                    $ltsc = $l.ltsc;
                }
            }
	(
	(HIDE s = setschema)
            {
                if($s.ssc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised set expected.\n";
                }
                else if($ltsc != 0)
                {
                    $ltsc = new HidingLTSSchema($ltsc, $s.ssc);
                }
            }
	)*
	;

parltsc returns [LTSSchema* ltsc]
	: ({$ltsc = 0;} l1=repparltsc {$ltsc = $l1.ltsc;} | l2 = condltsc {$ltsc = $l2.ltsc;} | l3 = elemltsc { $ltsc = $l3.ltsc;})
	(
	PAR (l1 = repparltsc
            {
                if($l1.ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else if($ltsc != 0)
                {
                    $ltsc = new ParallelLTSSchema($ltsc, $l1.ltsc);
		}
            }
            | l2 = condltsc 
            {
                if($l2.ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else if($ltsc != 0)
                {
                    $ltsc = new ParallelLTSSchema($ltsc, $l2.ltsc);
		}
            }
            | l3 = elemltsc)
            {
                if($l3.ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else if($ltsc != 0)
                {
                    $ltsc = new ParallelLTSSchema($ltsc, $l3.ltsc);
		}
            }
	)*
	;

repparltsc  returns [LTSSchema* ltsc]
	: (PAR avl=boundparlist ':' {$ltsc = 0;} (l1 = repparltsc {$ltsc = $l1.ltsc;} | l2 = condltsc {$ltsc = $l2.ltsc;} | l3 = elemltsc {$ltsc = $l3.ltsc;}))
            {
                if($ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else if($avl.bal == 0)
                {
                    printPosition($start);
                    std::cout << "Variable list expected.\n";
                }
                else if(!$ltsc->hasDisjointInputOutputChannels())
                {
                    printPosition($start);
                    std::cout << "The input and output channels are not disjoint.\n";
                }
                else
                {
                    for(int i = 0; i < $avl.bal->size(); ++i)
                    {
                        AtomVariable* av = new AtomVariable($avl.bal->operator[](i)->getAtomVariable());
                        $ltsc = new ReplicatedLTSSchema(av, $ltsc);
                    }
                }
		delete $avl.bal;
            }
	;

condltsc    returns [LTSSchema* ltsc]
	: ('[' v = valf ']' {$ltsc = 0;} (l1 = condltsc {$ltsc = $l1.ltsc;} | l2 = elemltsc {$ltsc = $l2.ltsc;} | l3 = repparltsc {$ltsc = $l3.ltsc;}))
            {
                if($ltsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else if($v.vf == 0)
                {
                    printPosition($start);
                    std::cout << "Guard expected.\n";
                }
                else if(!($v.vf->isUnquantified()))
                {
                    printPosition($start);
                    std::cout << "Unquantified guard expected.\n";
                }
                else
		{
                    $ltsc = static_cast<LTSSchema*>(new ConditionLTSSchema($ltsc, $v.vf));
		}
            }
	;
			
elemltsc	returns [LTSSchema* ltsc]
	: ( LTS 
            {
		context.insertNewProcess();
            }
            (t = transetschema FROM init = stateschema)
            {
                $ltsc = 0;
		if($init.stsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised state expected.\n";
                }
                else if ($t.tss == 0)
                {
                    printPosition($start);
                    std::cout << "List of parametrised transitions expected.\n";
                }
                else if(!$t.tss->hasDisjointInputOutputChannels())
                {
                    printPosition($start);
                    std::cout << "The input and output channels are not disjoint.\n";
                }
                else
		{
                    $ltsc = static_cast<LTSSchema*>(new ElementaryLTSSchema($t.tss, $init.stsc));
		}
            }
	  )
	| (name = string)
            {
                $ltsc = 0;
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else if(!context.isLTSSchema($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is not a parametrised LTS.\n";
                }
                else
		{
                    $ltsc = static_cast<LTSSchema*>(new NamedLTSSchema($name.str, context.getLTSSchema($name.str)->clone()));
		}
                if($ltsc == 0) delete $name.str;
            }
	| ('(' l = ltsschema ')')
            {
            $ltsc = $l.ltsc;
            }
	;

transetschema	returns [TransitionSetSchema* tss]
	:
	{
	$tss = new TransitionSetSchema();
	}
	(
	t1 = transchema
            {
                if($t1.ts == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised transition expected.\n";
                }
                else if(!$t1.ts->hasVisibleGuard())
                {
                    printPosition($start);
                    std::cout << "Parametrised transition has an invisible guard.\n";
                }
                else
		{
                    $tss->insert($t1.ts);
		}
            }
	(t2 =choicetranschema[$t1.ts]
            {
                if($t2.ts == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised transition expected.\n";
                }
                else if(!$t2.ts->hasVisibleGuard())
                {
                    printPosition($start);
                    std::cout << "Parametrised transition has an invisible guard.\n";
                }
                else
		{
                    $tss->insert($t2.ts);
		}
            }
	)*
	)*
	;

transchema	returns [TransitionSchema* ts]
		: s1= fromstateschema
                    {
			$ts = new TransitionSchema();
                        if($s1.stsc == 0)
                        {
                            printPosition($start);
                            std::cout << "Parametrised state expected.\n";
                        }
                        else
			{
                            $ts->setFromState($s1.stsc);
			}
                    }
		'='
		( CHOICE bpl = boundparlist ':'
                    {
			if($bpl.bal == 0)
                        {
                            printPosition($start);
                            std::cout << "Variable list expected.\n";
                        }
                        else
                        {
                            $ts->setBoundParameters($bpl.bal);
                        }
                    }
		)?
		 ('[' g = valf ']'
                    {
		 	if($g.vf == 0)
                        {
                            printPosition($start);
                            std::cout << "Guard expected.\n";
                        }
                        else if(!$g.vf->isUnquantified())
                        {
                            printPosition($start);
                            std::cout << "Unquantified guard expected.\n";
                        }
                        else
		 	{
                            $ts->setGuard($g.vf);
		 	}
                    }
		 )? 
		 (a = actionschema
                    {
                        if($a.actsc == 0)
                        {
                            printPosition($start);
                            std::cout << "Parametrised event expected.\n";
                        }
                        else if(!context.isCorrectlyTyped($a.actsc))
                        {
                            printPosition($start);
                            std::cout << "The types of the parametrised event and the channel do not match.\n";
                        }
                        else
		 	{
                            $ts->setActionSchema($a.actsc);
		 	}
                    }
		 )
 		(IMPL | (MUSTIMPL {$ts->isMust=true;}) ) s2 = stateschema
                    {
                        if($s2.stsc == 0)
                        {
                            printPosition($start);
                            std::cout << "Parametrised state expected.\n";
                        }
                        else
                        {
                            $ts->setToState($s2.stsc);
 			}
                    }
		;

choicetranschema [TransitionSchema* tsin]
		returns [TransitionSchema* ts]
		: CHOICE
			{
                            if($tsin == 0)
                            {
                                $ts = new TransitionSchema(0,0,0,0,0);
                            }
                            else
                            {
        			$ts = new TransitionSchema(new StateSchema($tsin->getFromState()), 0, 0, 0, 0);
                            }
			} 
		( CHOICE bpl = boundparlist ':'
                    {
			if($bpl.bal == 0)
                        {
                            printPosition($start);
                            std::cout << "Variable list expected.\n";
                        }
                        else
                        {
                            $ts->setBoundParameters($bpl.bal);
                        }
                    }
		)?
		('[' g = valf ']'
                    {
		 	if($g.vf == 0)
                        {
                            printPosition($start);
                            std::cout << "Guard expected.\n";
                        }
                        else if(!$g.vf->isUnquantified())
                        {
                            printPosition($start);
                            std::cout << "Unquantified guard expected.\n";
                        }
                        else
		 	{
                            $ts->setGuard($g.vf);
		 	}
                    }
		)? 
		 (
		 a = actionschema
                    {
                        if($a.actsc == 0)
                        {
                            printPosition($start);
                            std::cout << "Parametrised event expected.\n";
                        }
                        else if(!context.isCorrectlyTyped($a.actsc))
                        {
                            printPosition($start);
                            std::cout << "The types of the parametrised event and the channel do not match.\n";
                        }
                        else
		 	{
                            $ts->setActionSchema($a.actsc);
		 	}
                    }
		 )
 		(IMPL | (MUSTIMPL {$ts->isMust=true;})) s2 = stateschema
                    {
                        if($s2.stsc == 0)
                        {
                            printPosition($start);
                            std::cout << "Parametrised state expected.\n";
                        }
                        else
                        {
                            $ts->setToState($s2.stsc);
 			}
                    }
		;

setschema	returns [SetSchema* ssc]
	: {$ssc = 0;} (s1 = elemssc {$ssc = $s1.ssc;} | s2 = repssc {$ssc = $s2.ssc;} | s3 = condssc {$ssc = $s3.ssc;})
	(
	(CUP | PLUS) (s1 = elemssc 
            {
                if($s1.ssc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised set expected.\n";
                }
                else if($ssc != 0)
                {
                    $ssc = new UnionSetSchema($ssc, $s1.ssc);
                }
            }
            | s2 = repssc 
            {
                if($s2.ssc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised set expected.\n";
                }
                else if($ssc != 0)
                {
                    $ssc = new UnionSetSchema($ssc, $s2.ssc);
                }
            }
            | s3 = condssc)
            {
                if($s3.ssc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised set expected.\n";
                }
                else if($ssc != 0)
                {
                    $ssc = new UnionSetSchema($ssc, $s3.ssc);
                }
            }
	)*
	;

repssc	returns [SetSchema* ssc]
	: ((CUP | PLUS) avl = boundparlist ':' {$ssc = 0;} (s1 = elemssc {$ssc = $s1.ssc;} | s2 = repssc {$ssc = $s2.ssc;} | s3 = condssc {$ssc = $s3.ssc;}))
            {
                if($ssc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised set expected.\n";
                }
                else if($avl.bal == 0)
                {
                    printPosition($start);
                    std::cout << "Variable list expected.\n";
                }
                else
                {
                    for(int i = 0; i < $avl.bal->size(); ++i)
                    {
                            const AtomVariable& av = dynamic_cast<const AtomVariableSchema*>($avl.bal->operator[](i))->getAtomVariable();
                            $ssc = new ReplicatedSetSchema(new AtomVariable(av), $ssc);
                    }
                }
		delete $avl.bal;
            }
	;

elemssc 	returns [SetSchema* ssc]
	: (a = actionsetsc)
            {
                if($a.asc == 0)
                {
                    $ssc = 0;
                    printPosition($start);
                    std::cout << "A set of parametrised events expected.\n";
                }
                else
                {
                    $ssc = new ElementarySetSchema($a.asc);
                }
            }
	| (name = string)
            {
                if(!context.isSetSchema($name.str))
                {
                    $ssc = 0;
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is not a parametrised set.\n";
                }
                else
		{
                    $ssc = new NamedSetSchema($name.str, context.getSetSchema($name.str));
		}
            }
	| '(' s = setschema ')'
            {
		$ssc = $s.ssc;
            }
	;

condssc		returns [SetSchema* ssc]
	: '[' v = valf ']' {$ssc = 0;} (s1 = condssc {$ssc = $s1.ssc;} | s2 = elemssc {$ssc = $s2.ssc;} | s3 = repssc {$ssc = $s3.ssc;} )
            {
                if($ssc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised LTS expected.\n";
                }
                else if($v.vf == 0)
                {
                    printPosition($start);
                    std::cout << "Guard expected.\n";
                }
                else if(!$v.vf->isUnquantified())
                {
                    printPosition($start);
                    std::cout << "Unquantified guard expected.\n";           
                }
                else
		{
                    $ssc = new ConditionSetSchema($ssc, $v.vf);
		}
            }
	;
			

actionsetsc	returns [ActionSetSchema* asc]
	:
	'{'
	{
	$asc = new ActionSetSchema();
	//std::cout << "Action set created" << std::endl;
	}
	(
	a = actionschema
            {
		if($a.actsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised event expected.\n";           
                }
                else if(!context.isCorrectlyTyped($a.actsc))
                {
                    std::cout << "The types of the parametrised event and the channel do not match.\n";
                }
                else
		{
                    $asc->push_back($a.actsc);
		}
            }
	(
	',' a = actionschema
            {
		if($a.actsc == 0)
                {
                    printPosition($start);
                    std::cout << "Parametrised event expected.\n";           
                }
                else if(!context.isCorrectlyTyped($a.actsc))
                {
                    std::cout << "The types of the parametrised event and the channel do not match.\n";
                }
                else
		{
                    $asc->push_back($a.actsc);
		}
            }
	)*
	)?
	'}'
	;

valf 	returns  [ValuationFormula* vf]
	: (v1 = equivf)
		{
		$vf = $v1.vf;
		}
	| (v2 = univf ) 
		{
		$vf = $v2.vf;
		}
	;
		
univf	returns  [ValuationFormula* vf]
	: FORALL avl=boundparlist ':' v=valf
            {
                $vf = 0;
                if($v.vf == 0)
                {
                    printPosition($start);
                    std::cout << "Valuation constraint expected.\n";
                }
                else if($avl.bal == 0)
                {
                    printPosition($start);
                    std::cout << "Variable list expected.\n";
                }
                else
                {
                    $vf = $v.vf;
                    for(int i = 0; i < $avl.bal->size(); ++i)
                    {
                            const AtomVariable& av = dynamic_cast<const AtomVariableSchema*>($avl.bal->operator[](i))->getAtomVariable();
                            $vf = new UniversalValuationFormula(new AtomVariable(av), $vf);
                    }
                }
		delete $avl.bal;
            }
	;
	
equivf	returns  [ValuationFormula* vf]
	:
	(
	v=implvf
            {
		$vf = $v.vf;
            }
	)
	(
	EQ v=implvf
            {
                if($v.vf == 0)
                {
                    printPosition($start);
                    std::cout << "Valuation constraint expected.\n";
                }
		else if($vf != 0)
                {
                    ValuationFormula* ivf = 0;
                    if($vf != 0 && $v.vf != 0)
                    {
                            ivf = new NegatedValuationFormula(new ConjunctiveValuationFormula($v.vf->clone(),new NegatedValuationFormula($vf->clone())));
                    }
                    $vf = new ConjunctiveValuationFormula(new NegatedValuationFormula(new ConjunctiveValuationFormula($vf,new NegatedValuationFormula($v.vf))), ivf);
                }
            }
	)*
	;
	
implvf	returns  [ValuationFormula* vf]
	:
	(
	v = disjvf
            {
		$vf = $v.vf;
            }
	)
	(
	IMPL v = disjvf
            {
                if($v.vf == 0)
                {
                    printPosition($start);
                    std::cout << "Valuation constraint expected.\n";
                }
		else if($vf != 0)
                {
                    $vf = new NegatedValuationFormula(new ConjunctiveValuationFormula($vf,new NegatedValuationFormula($v.vf)));
		}
            }
	)*
	;
	
disjvf	returns  [ValuationFormula* vf]
	:
	(
	v = conjvf 
		{
		$vf = $v.vf;
		}
	)
	(
	OR v = conjvf
            {
                if($v.vf == 0)
                {
                    printPosition($start);
                    std::cout << "Valuation constraint expected.\n";
                }
                else if($vf != 0)
                {
                    $vf = new DisjunctiveValuationFormula($vf, $v.vf);
		}
            }
	)*
	;
	
conjvf 	returns  [ValuationFormula* vf]
	: 
	({$vf = 0;} v1 =elemvf { $vf = $v1.vf;} | v2 = negvf { $vf = $v2.vf;} | '(' v3 = valf ')' { $vf = $v3.vf;})
	(
	AND (v1 =elemvf 
            {
                if($v1.vf == 0)
                {
                    printPosition($start);
                    std::cout << "Valuation constraint expected.\n";
                }
                else if($vf != 0)
                {
                    $vf = new ConjunctiveValuationFormula($vf,$v1.vf);
                }
            }
            | v2 = negvf 
            {
                if($v2.vf == 0)
                {
                    printPosition($start);
                    std::cout << "Valuation constraint expected.\n";
                }
                else if($vf != 0)
                {
                    $vf = new ConjunctiveValuationFormula($vf,$v2.vf);
                }
            }
            | '(' v3 = valf ')' 
            {
                if($v3.vf == 0)
                {
                    printPosition($start);
                    std::cout << "Valuation constraint expected.\n";
                }
                else if($vf != 0)
                {
                    $vf = new ConjunctiveValuationFormula($vf,$v3.vf);
                }
            }
            )
	)*
	;

negvf	returns  [ValuationFormula* vf]
	: NEG {$vf = 0;} (v1 = negvf {$vf = $v1.vf;} | v2 = elemvf {$vf = $v2.vf;} | '(' v3 =valf ')' {$vf = $v3.vf;}) 
            {
                if($vf == 0)
                {
                    printPosition($start);
                    std::cout << "Valuation constraint expected.\n";
                }
                else if($vf != 0)
                {
                    $vf = new NegatedValuationFormula($vf);
                }
            }
	;
	
elemvf	returns  [ValuationFormula* vf]
	: ((TRUEVF) => (TRUEVF))
		{
		$vf = new TopValuationFormula();
		}
	| ((FALSEVF) => (FALSEVF))
		{
		$vf = new NegatedValuationFormula(new TopValuationFormula());
		}
    | ((string '(') => (rel = capitstr par = atomschematuple[0]))
        {
            $vf = nullptr;
            if($rel.str == 0)
            {
                printPosition($start);
                std::cout << "Relation variable expected.\n";
            }
            else if(context.isRelationVariable($rel.str) && $par.actsc != 0)
            {
                RelationVariable* rv = context.getRelationVariable($rel.str);
                if(($par.actsc)->isEquallyTyped(*rv))
                {
                    $vf = new ElementaryValuationFormula(rv, $par.actsc);
                }
                else
                {
                    printPosition($start);
                    std::cout << "The relation variable and its parameters have mismatching types.\n";
                    delete rv;
                }
            }
            if($vf == nullptr)
            {
                delete $par.actsc;            
            }
            delete $rel.str;
        }
	| ((string ('=' | IN | string)) => a1=atomschema 
		( '=' a2=atomschema
                    {
                        $vf = 0;
			if($a1.atsc == 0)
                        {
                            printPosition($start);
                            std::cout << "Constant/variable expected.\n";
                        }
                        else if($a2.atsc == 0)
			{
                            printPosition($start);
                            std::cout << "Constant/variable expected.\n";
                        }
                        else
                        {
                            $vf = new EquivalenceValuationFormula($a1.atsc, $a2.atsc);
			}
                        if($vf == 0)
                        {
                            delete $a1.atsc;
                            delete $a2.atsc;
                        }
                    }
		| IN s=atomrel
                    {
                        $vf = 0;
			if($a1.atsc == 0)
                        {
                            printPosition($start);
                            std::cout << "Constant/variable expected.\n";
                        }
                        else if($s.rel == 0)
			{
                            printPosition($start);
                            std::cout << "Set of constants expected.\n";
                        }
                        else if($s.rel->getDimension()!=1 || $s.rel->getFunctionType() != $a1.atsc->getType())
                        {
                            printPosition($start);
                            std::cout << "Types of the set and constant/variable do not match.\n";
                        }
                        else
                        {
                            AtomSchemaTuple* atsct = new AtomSchemaTuple();
                            atsct->push_back($a1.atsc);
                            $vf = new InclusionValuationFormula(atsct,$s.rel);
			}
                        if($vf == 0)
                        {
                            delete $a1.atsc;
                            delete $s.rel;
			}
                    }	
		| IN name =string '(' p = boundparlist ')'
                    {
                        $vf = 0;
                        AtomVariableSchema* avs = dynamic_cast<AtomVariableSchema*>($a1.atsc);
                 	  if(avs == 0)
                        {
                            printPosition($start);
                            std::cout << "Variable expected.\n";
                        }
                        else if($p.bal == 0)
			{
                            printPosition($start);
                            std::cout << "List of variables expected.\n";
                        }
                        else if(!context.isQuorumFunctionVariable($name.str))
                        {
                            printPosition($start);
                            std::cout << "Quorum function variable expected.\n";
                        }
                        else
                        {
                            QuorumFunctionVariable* qf = context.getQuorumFunctionVariable($name.str);
                            $vf = new QuorumValuationFormula(qf, new AtomVariable(avs->getAtomVariable()), $p.bal);
                        }
                        delete $name.str;
                        delete $a1.atsc;
                        if($vf == 0)
                        {
                            delete $p.bal;
                        }
                    }	
		| rel=string a2=atomschema
                    {
                        if(context.isAtomRelation($rel.str))
                        {
                            $vf = 0;
                            if($a1.atsc == 0)
                            {
                                printPosition($start);
                                std::cout << "Constant/variable expected.\n";
                            }
                            else if($a2.atsc == 0)
                            {
                                printPosition($start);
                                std::cout << "Constant/variable expected.\n";
                            }
                            else
                            {
                                AtomRelation* ar = context.getAtomRelation($rel.str);
                                AtomSchemaTuple* atsct = new AtomSchemaTuple();
                                atsct->push_back($a1.atsc);
                                atsct->push_back($a2.atsc);
                                if(ar->isEquallyTyped(*atsct))
                                {
                                    $vf = new InclusionValuationFormula(atsct, ar);
                                }
                                else
                                {
                                    printPosition($start);
                                    std::cout << "Types of the relation and the constants/variables do not match.\n";
                                    delete atsct;
                                    delete ar;
                                }
                            }
                            delete $rel.str;
                            if($vf == 0)
                            {
                                delete $a1.atsc;
                                delete $a2.atsc;
                            }
                        }
                        else if(context.isRelationVariable($rel.str))
                        {
                            $vf = 0;
                            AtomVariableSchema* avsc1 = dynamic_cast<AtomVariableSchema*>($a1.atsc);
                            AtomVariableSchema* avsc2 = dynamic_cast<AtomVariableSchema*>($a2.atsc);
                            if(avsc1 == 0)
                            {
                                printPosition($start);
                                std::cout << "Variable expected.\n";
                            }
                            else if(avsc2 == 0)
                            {
                                printPosition($start);
                                std::cout << "Variable expected.\n";
                            }
                            else
                            {
                                const AtomVariable& av1 = dynamic_cast<const AtomVariable&>(avsc1->getAtomVariable());
                                const AtomVariable& av2 = dynamic_cast<const AtomVariable&>(avsc2->getAtomVariable());
                                $vf = new ElementaryValuationFormula(context.getRelationVariable($rel.str),new AtomVariable(av1), new AtomVariable(av2));
                            }
                            delete $rel.str;
                            delete $a1.atsc;
                            delete $a2.atsc;
			}
			else
			{
                            $vf = 0;
                            printPosition($start);
                            std::cout << "Relation (variable) expected.\n";
                            delete $rel.str;
                            delete $a1.atsc;
                            delete $a2.atsc;
			}
                    }
		)
	)
    | (name=string)
            {
                if(!context.isValuationFormula($name.str))
                {
                    $vf = 0;
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is not a valuation constraint.\n";
                    delete $name.str;
                }
                else
		{
                    $vf = new NamedValuationFormula($name.str, context.getValuationFormula($name.str));
                }
            }
	;

boundparlist	returns [ParameterList* bal]
	: var = atomschema 
            {
		$bal = new ParameterList();
                if(dynamic_cast<AtomVariableSchema*>($var.atsc) == 0)
                {
                    printPosition($start);
                    std::cout << "Variable expected.\n";
                    delete $var.atsc;
                }
                else
		{
                    $bal->push_back($var.atsc);
		}
            }
	(
	',' var = atomschema
            {
                if(dynamic_cast<AtomVariableSchema*>($var.atsc) == 0)
                {
                    printPosition($start);
                    std::cout << "Variable expected.\n";                   
                    delete $var.atsc;
                }
                else
		{
                    $bal->push_back($var.atsc);
		}
            }
	)*
	;
	
stateschema	returns [StateSchema* stsc]
	: 	c = string
                    {
                        if(!context.isNotDefined($c.str,false)) 
                        {
                            printPosition($start);
                            std::cout << "Control state expected.\n";                   
                            $stsc = new StateSchema();
                        }
                        else
		 	{
                            $stsc = new StateSchema(context.insertControlState($c.str));
		 	}
		 	delete $c.str;
                    }
		('(')=>
		(
		'('
		(
		( a =atomschema
                    {
			if($a.atsc == 0)
                        {
                            printPosition($start);
                            std::cout << "Constant/variable/function application expected.\n";                   
                        }
                        else
                        {
                            $stsc->push_back($a.atsc);
                        }
                    }
		)
		(
		',' a = atomschema
                    {
			if($a.atsc == 0)
                        {
                            printPosition($start);
                            std::cout << "Constant/variable/function application expected.\n";                   
                        }
                        else
                        {
                            $stsc->push_back($a.atsc);
                        }
                    }
		)*
		)?
		')'
		)?
	;

fromstateschema	returns [StateSchema* stsc]
	: 	c = string
                    {
                        if(!context.isNotDefined($c.str,false)) 
                        {
                            printPosition($start);
                            std::cout << "Control state expected.\n";                   
                            $stsc = new StateSchema();
                        }
                        else
		 	{
                            $stsc = new StateSchema(context.insertControlState($c.str));
		 	}
		 	delete $c.str;
                    }
		('(')=>
		(
		'('
		(
		( a =atomschema
                    {
			if(dynamic_cast<AtomVariableSchema*>($a.atsc) == 0)
                        {
                            printPosition($start);
                            std::cout << "Variable expected.\n";
                            delete $a.atsc;
                        }
                        else
                        {
                            $stsc->push_back($a.atsc);
                        }
                    }
		)
		(
		',' a = atomschema
                    {
			if(dynamic_cast<AtomVariableSchema*>($a.atsc) == 0)
                        {
                            printPosition($start);
                            std::cout << "Variable expected.\n";
                            delete $a.atsc;
                        }
                        else
                        {
                            $stsc->push_back($a.atsc);
                        }
                    }
		)*
		)?
		')'
		)?
	;

atomschematuple	[AtomSchemaTuple* ast]
	returns [AtomSchemaTuple* actsc]
	:   a =atomschema
                {
                    if($ast == 0)
                    {
                        $actsc = new AtomSchemaTuple();
                    }
                    else
                    {
                        $actsc = $ast;
                    }
                    if($a.atsc == 0)
                    {
                        printPosition($start);
                        std::cout << "Constant/variable expected.\n";
                    }
                    else
                    {
                        $actsc->push_back($a.atsc);
                    }
                }
            |
            '('
            (
		( a =atomschema
                    {
                    	if($ast == 0)
                    	{
                    	    $actsc = new AtomSchemaTuple();
                    	}
                    	else
                    	{
                    	    $actsc = $ast;
                    	}
			if($a.atsc == 0)
                        {
                            printPosition($start);
                            std::cout << "Constant/variable/function application expected.\n";
                        }
                        else
                        {
                            $actsc->push_back($a.atsc);
			}
                    }
		)
		(
		',' a = atomschema
                    {
			if($a.atsc == 0)
                        {
                            printPosition($start);
                            std::cout << "Constant/variable/function application expected.\n";
                        }
                        else
                        {
                            $actsc->push_back($a.atsc);
			}
                    }
		)*
            )?
            ')'
	;

actionschema	returns [ActionSchema* actsc]
	:   (   ('!')? c = smallstr
                    {
                        if(!context.isChannel($c.str))
                        {
                            printPosition($start);
                            std::cout << "Channel expected.\n";
                            $actsc = new ActionSchema();
                        }
                        else
                        {
                            $actsc = new ActionSchema(context.getChannel($c.str));
                        }
                        delete $c.str;
                    }
                | '?' c = smallstr 
                    {
                        if(!context.isChannel($c.str))
                        {
                            printPosition($start);
                            std::cout << "Channel expected.\n";
                            $actsc = new ActionSchema();
                        }
                        else
                        {
                            $actsc = new ActionSchema(context.getChannel($c.str), true);
                        }
                        delete $c.str;
                    }
            )
            ('(')=> atomschematuple[$actsc]
	|
            (TAU ('('')')?
                {
                $actsc = new ActionSchema(0);
                }
            )
	;

atomschema	returns [AtomSchema* atsc]
	: (string'(')=> (name = string a = atomschematuple[0])
            {
                $atsc = 0;
                if($name.str == 0)
                {
                    printPosition($start);
                    std::cout << "Function expected.\n";                    
                }
                else if(!context.isAtomRelation($name.str))
                {
                    printPosition($start);
                    std::cout << "Identifier " << *($name.str) << " is not a function.\n";                                  
                }
                else if($a.actsc == 0)
                {
                    printPosition($start);
                    std::cout << "A tuple of constants expected.\n";                                    
                }
                else
		{
                    AtomRelation* f = context.getAtomRelation($name.str);
                    $atsc = new FunctionAtomSchema(f, $a.actsc);
		}
                if($atsc == 0) delete $a.actsc;
		delete $name.str;
            }
	| (smallstr)=> name = smallstr
            {
                $atsc = 0;
		if(context.isAtom($name.str))
                {
                    $atsc = new ElementaryAtomSchema(context.getAtom($name.str));
                }
                else if(context.isAtomVariable($name.str))
                {
                    $atsc = new AtomVariableSchema(context.getAtomVariable($name.str));
                }
                else
                {
                    printPosition($start);
                    std::cout << "Constant/variable expected.\n";
                }
		delete $name.str;
            }
	;	

relschema   returns [RelationSchema* rs]
        : '{'
	a = atomschematuple[0]
            {
                $rs = 0;
		if($a.actsc == 0)
                {
                    printPosition($start);
                    std::cout << "A tuple of constants/variables expected.\n";                   
                }
                else
                {
	            $rs = new RelationSchema($a.actsc, context.getTypeSizes($a.actsc));
                }
            }
	
	(',' a = atomschematuple[0]
            {
		if($a.actsc == 0)
                {
                    printPosition($start);
                    std::cout << "A tuple of constants/variables expected.\n";                   
                }
                else if($rs != 0)
                { 
                    if(!$rs->isEquallyTyped(*$a.actsc))
                    {
                        printPosition($start);
                        std::cout << "Types of the relation and the tuple do not match.\n";                   
                    }
                    else 
                    {
                        $rs->insert($a.actsc);
                    }
                }
            }
	)*
	(
	'|' g = valf
	    {
	        if($g.vf == 0)
	        {
                    printPosition($start);
                    std::cout << "Guard expected.\n";                   
	        }
	        else if(!$g.vf->isUnquantified())
	        {
                    printPosition($start);
                    std::cout << "Unquantified guard expected.\n";                   
	        }
	        else if($rs != 0)
	        {
	            $rs->setGuard($g.vf);
	        }
	    }
	)?
	'}'
        ;
        
atomrel	returns [AtomRelation* rel]
	: rs = elematomrel
            {
                $rel = $rs.rel;
            }
	(PLUS rs = elematomrel
            {
		if($rs.rel == 0)
                {
                    printPosition($start);
                    std::cout << "Relation expected.\n";                   
                }
                else if($rel != 0)
		{
		    $rel->plus(*($rs.rel));
		}
                delete $rs.rel;
            }
	| MINUS rs = elematomrel
            {
		if($rs.rel == 0)
                {
                    printPosition($start);
                    std::cout << "Relation expected.\n";                   
                }
                else if($rel != 0)
		{
		    $rel->diff(*($rs.rel));
		}
                delete $rs.rel;
            }
	)*
	;
	
elematomrel	returns [AtomRelation* rel]
	: rs = relschema
            {
            $rel = new AtomRelation($rs.rs->instance());
            delete $rs.rs;
            }
	| name = string
            {
                if(!context.isAtomRelation($name.str))
                {
                    printPosition($start);
                    std::cout << "Relation expected.\n";                   
                }
                else
		{
                    $rel = context.getAtomRelation($name.str);
                }
		delete $name.str;
            }
	| '(' ar = atomrel ')'
		{
		$rel = $ar.rel;
		}
	;

capitstrvec    returns [std::vector<std::string>* strv]
    : type1 = capitstr
        {
            $strv = new std::vector<std::string>{};
            if ($type1.str == 0)
            {
                printPosition($start);
                std::cout << "Type expected.\n";
            }
            else if (!context.isTypeVariable($type1.str))
            {   
                printPosition($start);
                std::cout << "Identifier " << *($type1.str) << " is not a type.\n";
            }
            else
            {
                $strv->push_back(*($type1.str));
            }
            delete $type1.str;
        }
    (',' type2 = capitstr
        {
            if ($type2.str == 0)
            {
                printPosition($start);
                std::cout << "Type expected.\n";
            }
            else if (!context.isTypeVariable($type2.str))
            {   
                printPosition($start);
                std::cout << "Identifier " << *($type2.str) << " is not a type.\n";
            }
            else
            {
                $strv->push_back(*($type2.str));
            }
            delete $type2.str;
        }
    )*
    ;

/*
atom	returns [Atom* at]
	: name = smallstr
            {
                if(!context.isAtom($name.str))
                {
                    printPosition($start);
                    std::cout << "Constant expected.\n";                   
                }
                else
		{
                    $at = context.getAtom($name.str);
                }
		delete $name.str;
            }
	;
*/

smallstr  	returns [std::string* str]
	: name = SMALLID
	{
	$str = new std::string((char*)(($name.text)->chars));
	}
    	;

capitstr  	returns [std::string* str]
	: name= CAPITID
	{
	$str = new std::string((char*)(($name.text)->chars));
	}
    	;

string  	returns [std::string* str]
	: name= (SMALLID | CAPITID | CAPITNUMID)
	{
	$str = new std::string((char*)(($name.text)->chars));
	}
    	;

SMALLID  	: ('a'..'z') ('a'..'z'|'A'..'Z'|'0'..'9')*
    	;

CAPITID  	: ('A'..'Z') ('a'..'z'|'A'..'Z')*
    	;

CAPITNUMID  	: ('A'..'Z') ('a'..'z'|'A'..'Z')* ('0'..'9') ('a'..'z'|'A'..'Z'|'0'..'9')*
    	;

POSINT :	('0'..'9')+
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {$channel=HIDDEN;}
    ;
