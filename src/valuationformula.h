#ifndef VALUATIONFORMULA_H
#define VALUATIONFORMULA_H

#include "element.h"
#include "valuation.h"
#include <set>
#include <list>
#include "elementschema.h"
#include <z3++.h>

class ValuationFormula
{
protected:
	std::set<TypeVariable> types;
	std::set<RelationVariable> relvars;
	std::set<AtomVariable> atomvars;
	std::set<QuorumFunctionVariable> qfvars;
	
	ValuationFormula();
	ValuationFormula(const std::set<TypeVariable>&, const std::set<RelationVariable>&, const std::set<AtomVariable>&, const std::set<QuorumFunctionVariable>&);
	ValuationFormula(const ValuationFormula&);
public:
	virtual ~ValuationFormula();
	virtual bool instance(const Valuation&) const = 0;
	virtual bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const = 0;
	const std::set<TypeVariable>& getTypeVariables() const;
	const std::set<RelationVariable>& getRelationVariables() const;
	const std::set<AtomVariable>& getAtomVariables() const;
	const std::set<QuorumFunctionVariable>& getQuorumFunctionVariables() const;
	virtual int numOfNestedQFVs(const Domain&) const = 0;
	virtual std::list<ValuationFormula*> conjuncts() const;
	virtual std::list<ValuationFormula*> disjuncts() const;
	virtual bool isExistentialFree(int = 0) const = 0;
	virtual bool isMembershipNegated(int = 0) const = 0;
	virtual bool isUnquantified() const = 0;
	virtual ValuationFormula* clone() const = 0;
	virtual ValuationFormula* createPrimedVersion(const AtomVariable&) const = 0;
	virtual void setNumericValue(VarType, int, int);
	virtual void setNumericValuesToBoundVariables(int) = 0;
	virtual std::string toString(const Context& c) const = 0;
	virtual z3::expr toZ3Expr(z3::context&, unsigned int = 0, const std::map<TypeVariable,int>& = std::map<TypeVariable,int>{}) const = 0;
	virtual std::set<RelationVariable> getPredicates(bool mode, bool within = true) const = 0;
};

class ConjunctiveValuationFormula : public ValuationFormula
{
	ValuationFormula* vf1;
	ValuationFormula* vf2;
public:
	ConjunctiveValuationFormula(const ValuationFormula&, const ValuationFormula&);
	ConjunctiveValuationFormula(ValuationFormula*, ValuationFormula*);
	ConjunctiveValuationFormula(const ConjunctiveValuationFormula&);
	ConjunctiveValuationFormula& operator=(const ConjunctiveValuationFormula&);
	~ConjunctiveValuationFormula();
	const ValuationFormula& oneValuationFormula() const;
	const ValuationFormula& theOtherValuationFormula() const;
	bool instance(const Valuation&) const;
	bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const;
	int numOfNestedQFVs(const Domain&) const;
	std::list<ValuationFormula*> conjuncts() const override;
	std::list<ValuationFormula*> disjuncts() const override;
	bool isExistentialFree(int = 0) const;
	bool isMembershipNegated(int = 0) const;
	bool isUnquantified() const;
	ValuationFormula* clone() const;
	ValuationFormula* createPrimedVersion(const AtomVariable&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context&, unsigned int, const std::map<TypeVariable,int>&) const override;
	std::set<RelationVariable> getPredicates(bool mode, bool within = true) const override;
};

class DisjunctiveValuationFormula : public ValuationFormula
{
	ValuationFormula* vf1;
	ValuationFormula* vf2;
public:
	DisjunctiveValuationFormula(const ValuationFormula&, const ValuationFormula&);
	DisjunctiveValuationFormula(ValuationFormula*, ValuationFormula*);
	DisjunctiveValuationFormula(const DisjunctiveValuationFormula&);
	DisjunctiveValuationFormula& operator=(const DisjunctiveValuationFormula&);
	~DisjunctiveValuationFormula();
	const ValuationFormula& oneValuationFormula() const;
	const ValuationFormula& theOtherValuationFormula() const;
	bool instance(const Valuation&) const;
	bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const;
	int numOfNestedQFVs(const Domain&) const;
	std::list<ValuationFormula*> disjuncts() const override;
	bool isExistentialFree(int = 0) const;
	bool isMembershipNegated(int = 0) const;
	bool isUnquantified() const;
	ValuationFormula* clone() const;
	ValuationFormula* createPrimedVersion(const AtomVariable&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context&, unsigned int, const std::map<TypeVariable,int>&) const override;
	std::set<RelationVariable> getPredicates(bool mode, bool within = true) const override;
};

class NegatedValuationFormula : public ValuationFormula
{
	ValuationFormula* vf;
public:
	// the second parameter is used only to differentiate the constructor from the copy constructor
	NegatedValuationFormula(const ValuationFormula&, int);
	NegatedValuationFormula(ValuationFormula*);
	NegatedValuationFormula(const NegatedValuationFormula&);
	NegatedValuationFormula& operator=(const NegatedValuationFormula&);
	~NegatedValuationFormula();
	const ValuationFormula& valuationFormula() const;
	bool instance(const Valuation&) const;
	bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const;
	int numOfNestedQFVs(const Domain&) const;
	bool isExistentialFree(int = 0) const;
	bool isMembershipNegated(int = 0) const;
	bool isUnquantified() const;
	ValuationFormula* clone() const;
	ValuationFormula* createPrimedVersion(const AtomVariable&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context&, unsigned int, const std::map<TypeVariable,int>&) const override;
	std::set<RelationVariable> getPredicates(bool mode, bool within = true) const override;
};

class UniversalValuationFormula : public ValuationFormula
{
	AtomVariable* av;
	ValuationFormula* vf;
public:
	UniversalValuationFormula(const AtomVariable&, const ValuationFormula&);
	UniversalValuationFormula(AtomVariable*, ValuationFormula*);
	UniversalValuationFormula(const UniversalValuationFormula&);
	UniversalValuationFormula& operator=(const UniversalValuationFormula&);
	~UniversalValuationFormula();
	const ValuationFormula& valuationFormula() const;
	const AtomVariable& getAtomVariable() const;
	bool instance(const Valuation&) const;
	bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const;
	int numOfNestedQFVs(const Domain&) const;
	bool isExistentialFree(int = 0) const;
	bool isMembershipNegated(int = 0) const;
	bool isUnquantified() const;
	ValuationFormula* clone() const;
	ValuationFormula* createPrimedVersion(const AtomVariable&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context&, unsigned int, const std::map<TypeVariable,int>&) const override;
	std::set<RelationVariable> getPredicates(bool mode, bool within = true) const override;
};

class ExistentialValuationFormula : public ValuationFormula
{
	AtomVariable* av;
	ValuationFormula* vf;
public:
	ExistentialValuationFormula(const AtomVariable&, const ValuationFormula&);
	ExistentialValuationFormula(AtomVariable*, ValuationFormula*);
	ExistentialValuationFormula(const ExistentialValuationFormula&);
	ExistentialValuationFormula& operator=(const ExistentialValuationFormula&);
	~ExistentialValuationFormula();
	const ValuationFormula& valuationFormula() const;
	const AtomVariable& getAtomVariable() const;
	bool instance(const Valuation&) const;
	bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const;
	int numOfNestedQFVs(const Domain&) const;
	bool isExistentialFree(int = 0) const;
	bool isMembershipNegated(int = 0) const;
	bool isUnquantified() const;
	ValuationFormula* clone() const;
	ValuationFormula* createPrimedVersion(const AtomVariable&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context&, unsigned int, const std::map<TypeVariable,int>&) const override;
	std::set<RelationVariable> getPredicates(bool mode, bool within = true) const override;
};

class ElementaryValuationFormula : public ValuationFormula
{
	RelationVariable* rel;
	AtomSchemaTuple* atoms;
public:
	ElementaryValuationFormula(const RelationVariable&, const AtomVariable&, const AtomVariable&);
	ElementaryValuationFormula(RelationVariable*, AtomVariable*, AtomVariable*);
	ElementaryValuationFormula(RelationVariable*, AtomSchemaTuple*);
	ElementaryValuationFormula(const ElementaryValuationFormula&);
	ElementaryValuationFormula& operator=(const ElementaryValuationFormula&);
	~ElementaryValuationFormula();
	const RelationVariable& getRelation() const;
	const AtomVariable& getOneAtomVariable() const;
	const AtomVariable& getTheOtherAtomVariable() const;
	bool instance(const Valuation&) const;
	bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const;
	int numOfNestedQFVs(const Domain&) const;
	bool isExistentialFree(int = 0) const;
	bool isMembershipNegated(int = 0) const;
	bool isUnquantified() const;
	ValuationFormula* clone() const;
	ValuationFormula* createPrimedVersion(const AtomVariable&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context&, unsigned int, const std::map<TypeVariable,int>&) const override;
	std::set<RelationVariable> getPredicates(bool mode, bool within = true) const override;
};

class EquivalenceValuationFormula : public ValuationFormula
{
	AtomSchema* atom1;
	AtomSchema* atom2;
public:
	EquivalenceValuationFormula(const AtomSchema&, const AtomSchema&);
	EquivalenceValuationFormula(AtomSchema*, AtomSchema*);
	EquivalenceValuationFormula(const EquivalenceValuationFormula&);
	EquivalenceValuationFormula& operator=(const EquivalenceValuationFormula&);
	~EquivalenceValuationFormula();
	const AtomSchema& getOneAtomSchema() const;
	const AtomSchema& getTheOtherAtomSchema() const;
	bool instance(const Valuation&) const;
	bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const;
	int numOfNestedQFVs(const Domain&) const;
	bool isExistentialFree(int = 0) const;
	bool isMembershipNegated(int = 0) const;
	bool isUnquantified() const;
	ValuationFormula* clone() const;
	ValuationFormula* createPrimedVersion(const AtomVariable&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context&, unsigned int, const std::map<TypeVariable,int>&) const override;
	std::set<RelationVariable> getPredicates(bool mode, bool within = true) const override;
};

class InclusionValuationFormula : public ValuationFormula
{
	AtomSchemaTuple* atom;
	AtomRelation* set;
public:
	InclusionValuationFormula(const AtomSchemaTuple&, const AtomRelation&);
	InclusionValuationFormula(AtomSchemaTuple*, AtomRelation*);
	InclusionValuationFormula(const InclusionValuationFormula&);
	InclusionValuationFormula& operator=(const InclusionValuationFormula&);
	~InclusionValuationFormula();
	const AtomSchemaTuple& getAtomSchema() const;
	const AtomRelation& getDomain() const;
	bool instance(const Valuation&) const;
	bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const;
	int numOfNestedQFVs(const Domain&) const;
	bool isExistentialFree(int = 0) const;
	bool isMembershipNegated(int = 0) const;
	bool isUnquantified() const;
	ValuationFormula* clone() const;
	ValuationFormula* createPrimedVersion(const AtomVariable&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context&, unsigned int, const std::map<TypeVariable,int>&) const override;
	std::set<RelationVariable> getPredicates(bool mode, bool within = true) const override;
};

class QuorumValuationFormula : public ValuationFormula
{
	QuorumFunctionVariable* qfv;
	AtomVariable* av;
	ParameterList* params;
public:
	QuorumValuationFormula(const QuorumFunctionVariable&, const AtomVariable&, const ParameterList&);
	QuorumValuationFormula(QuorumFunctionVariable*, AtomVariable*, ParameterList*);
	QuorumValuationFormula(const QuorumValuationFormula&);
	QuorumValuationFormula& operator=(const QuorumValuationFormula&);
	~QuorumValuationFormula();
	const AtomVariable& getAtomVariable() const;
	const ParameterList& getParameters() const;
	const QuorumFunctionVariable& getQuorumFunction() const;
	bool instance(const Valuation&) const;
	bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const;
	int numOfNestedQFVs(const Domain&) const;
	bool isExistentialFree(int = 0) const;
	bool isMembershipNegated(int = 0) const;
	bool isUnquantified() const;
	ValuationFormula* clone() const;
	ValuationFormula* createPrimedVersion(const AtomVariable&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context&, unsigned int, const std::map<TypeVariable,int>&) const override;
	std::set<RelationVariable> getPredicates(bool mode, bool within = true) const override;
};

class TopValuationFormula : public ValuationFormula
{
public:
	TopValuationFormula();
	TopValuationFormula(const TopValuationFormula&);
	TopValuationFormula& operator=(const TopValuationFormula&);
	~TopValuationFormula();
	bool instance(const Valuation&) const;
	bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const;
	int numOfNestedQFVs(const Domain&) const;
	bool isExistentialFree(int = 0) const;
	bool isMembershipNegated(int = 0) const;
	bool isUnquantified() const;
	ValuationFormula* clone() const;
	ValuationFormula* createPrimedVersion(const AtomVariable&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context&, unsigned int, const std::map<TypeVariable,int>&) const override;
	std::set<RelationVariable> getPredicates(bool mode, bool within = true) const override;
};

class NamedValuationFormula : public ValuationFormula
{
	const std::string* name;
	ValuationFormula* vf;
public:
	NamedValuationFormula(const std::string&, const ValuationFormula&);
	NamedValuationFormula(const std::string*, ValuationFormula*);
	NamedValuationFormula(const NamedValuationFormula&);
	NamedValuationFormula& operator=(const NamedValuationFormula&);
	~NamedValuationFormula();
	bool instance(const Valuation&) const;
	bool instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet&) const;
	const std::string& getName() const;
	const ValuationFormula& getValuationFormula() const;
	int numOfNestedQFVs(const Domain&) const;
	std::list<ValuationFormula*> conjuncts() const override;
	std::list<ValuationFormula*> disjuncts() const override;
	bool isExistentialFree(int = 0) const;
	bool isMembershipNegated(int = 0) const;
	bool isUnquantified() const;
	ValuationFormula* clone() const;
	ValuationFormula* createPrimedVersion(const AtomVariable&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context&, unsigned int, const std::map<TypeVariable,int>&) const override;
	std::set<RelationVariable> getPredicates(bool mode, bool within = true) const override;
};

#endif
