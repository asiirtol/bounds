#include "reduce.h"
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <ctime>
#include <sstream>
#include <limits>
#include <iomanip>
#include <thread>
#include <future>
#include <z3++.h>
#include <assert.h>

std::tuple<std::set<Valuation*,PointerCompare<Valuation> >*, int, int, int, std::map<TypeVariable,bool>*, bool, bool> computeCutoffSet(const Context& ctxt, ValuationFormula*& vf, LTSSchema*& spec, LTSSchema*& sys, VerType vtype)
{
	bool sound = true;
	bool complete = true;

	std::set<TypeVariable, VariableCompare<TypeVariable> > typevars;
	typevars.insert(vf->getTypeVariables().begin(), vf->getTypeVariables().end());
	typevars.insert(spec->getTypeVariables().begin(), spec->getTypeVariables().end());
	typevars.insert(sys->getTypeVariables().begin(), sys->getTypeVariables().end());
	for(std::set<TypeVariable, VariableCompare<TypeVariable> >::iterator cit = typevars.begin(); cit != typevars.end(); ++cit)
	{
		if(!((sys->isProcessTypeVariable(*cit) && spec->isProcessTypeVariable(*cit)) || (sys->isDataTypeVariable(*cit) && spec->isDataTypeVariable(*cit))))
		{
			LTSSchema* modsys = sys->convertChoiceToParallelComposition(*cit);
			LTSSchema* modspec = spec->convertChoiceToParallelComposition(*cit);
			delete sys;
			delete spec;
			sys = modsys;
			spec = modspec;
//			std::cout << "Sys = " << sys->toString(ctxt) << std::endl << std::endl;
//			std::cout << "Spec = " << spec->toString(ctxt) << std::endl << std::endl;
		}
	}

	std::set<RelationVariable, VariableCompare<RelationVariable> > relvars;
	relvars.insert(vf->getRelationVariables().begin(), vf->getRelationVariables().end());
	relvars.insert(spec->getRelationVariables().begin(), spec->getRelationVariables().end());
	relvars.insert(sys->getRelationVariables().begin(), sys->getRelationVariables().end());
	std::set<QuorumFunctionVariable, VariableCompare<QuorumFunctionVariable> > qfvars;
	qfvars.insert(vf->getQuorumFunctionVariables().begin(), vf->getQuorumFunctionVariables().end());
	qfvars.insert(spec->getQuorumFunctionVariables().begin(), spec->getQuorumFunctionVariables().end());
	qfvars.insert(sys->getQuorumFunctionVariables().begin(), sys->getQuorumFunctionVariables().end());
	std::set<AtomVariable, VariableCompare<AtomVariable> > atomvars;
	atomvars.insert(vf->getAtomVariables().begin(), vf->getAtomVariables().end());
	atomvars.insert(spec->getAtomVariables().begin(), spec->getAtomVariables().end());
	atomvars.insert(sys->getAtomVariables().begin(), sys->getAtomVariables().end());
	std::map<TypeVariable,int>* cutoffs = new std::map<TypeVariable,int>();
	std::map<TypeVariable,bool>* cutoffisexact = new std::map<TypeVariable,bool>;
	std::map<TypeVariable,bool> determine_dynamically{};
	LTSSchema* sysspec = new ParallelLTSSchema(*sys, *spec);

	int i = 0;
	for(std::set<TypeVariable, VariableCompare<TypeVariable> >::iterator cit = typevars.begin(); cit != typevars.end(); ++cit)
	{
		std::cout << "Determining a static cut-off (size) for the type " << ctxt.typeVariableToString(*cit) << "..." << std::unitbuf << std::endl;
		const_cast<TypeVariable&>(*cit).id = i;
		spec->setNumericValue(TYPEVAR,cit->first, i);
		sys->setNumericValue(TYPEVAR,cit->first, i);
		vf->setNumericValue(TYPEVAR,cit->first, i);

		determine_dynamically[*cit] = false;
		int datacutoff = sysspec->numOfParallelOccurrences(*cit,*sysspec,*cutoffs);
		int freevars = 0;
		for(std::set<AtomVariable, VariableCompare<AtomVariable> >::iterator cait = atomvars.begin(); cait != atomvars.end(); ++cait)
		{
			const_cast<AtomVariable&>(*cait).setNumericValue(TYPEVAR,cit->first,i);
			if(*cit == cait->second) freevars++;
		}
		int qfvcount = sysspec->numOfNestedQFVs(*cit);
/*
		datacutoff+=freevars;
		int proccutoff = 0;
		auto ocount = sysspec->occurrencesInBranches(*cit);
		for(auto& opair : ocount)
		{
			proccutoff = std::max(proccutoff, quorumBound(opair.first, opair.second + freevars));
		}
		int cutoff = std::min(proccutoff,datacutoff);
*/
		int proccutoff = sysspec->numOfNestedOccurrences(*cit);
		int cutoff = std::min(proccutoff,datacutoff);
		cutoff += freevars;
		cutoff = quorumBound(qfvcount, cutoff);
		(*cutoffs)[*cit] = std::max(1,cutoff);

		ValuationFormula* relevantConjunct = new TopValuationFormula();
		std::list<ValuationFormula*> conjuncts = vf->conjuncts();
		int conjunctCount;
		std::set<TypeVariable> tvs;
		tvs.insert(*cit);
		do
		{
			conjunctCount = conjuncts.size();
			std::list<ValuationFormula*>::iterator lit = conjuncts.begin();
			while(lit != conjuncts.end())
			{
				if(disjoint<TypeVariable>((*lit)->getTypeVariables(), tvs))
				{
					++lit;
				}
				else
				{
					relevantConjunct = new ConjunctiveValuationFormula(relevantConjunct, *lit);
					tvs.insert((*lit)->getTypeVariables().begin(), (*lit)->getTypeVariables().end());
					lit = conjuncts.erase(lit);
				}
			}
		}
		while(conjunctCount != conjuncts.size());
		for(std::list<ValuationFormula*>::iterator lit = conjuncts.begin(); lit != conjuncts.end(); lit++)
		{
			delete *lit;
		}
		conjuncts.clear();

		int relevantRelvars = 0;
		for(std::set<RelationVariable>::iterator crit = relvars.begin(); crit != relvars.end(); crit++)
		{
			const_cast<RelationVariable&>(*crit).setNumericValue(TYPEVAR,cit->first,i);
			for(auto& tv : std::get<1>(*crit))
			{
				if(tv == *cit)
				{
					relevantRelvars++;
					break;
				}
			}
		}

		(*cutoffisexact)[*cit] = true;
		if(!(spec->isHidingFree()))
		{
			std::cout << "...Failed. The specification involves hiding." << std::endl;
			(*cutoffisexact)[*cit] = false;
			cutoff = CUTOFFMAX+1;
			sound = false;
		}
		else if(sys->involvesConditionalHiding())
		{
			(*cutoffisexact)[*cit] = false;
			std::cout << "...Failed. The system involves conditional hiding." << std::endl;
			cutoff = CUTOFFMAX+1;
			sound = false;
		}
		else if(vtype == TRACEREF && !(relevantConjunct->isExistentialFree()))
		{
//			(*cutoffisexact)[*cit] = false;
			std::cout << "...Failed. Relations related to the type " << ctxt.typeVariableToString(*cit) << " involve existential quantification." << std::endl;
			cutoff = CUTOFFMAX+1;
//			sound = false;
			determine_dynamically[*cit] = true;
		}
		else if(vtype == COMPAT && dynamic_cast<TopValuationFormula*>(vf) == 0)
		{
			(*cutoffisexact)[*cit] = false;
			std::cout << "...Failed. Relations related to parametric types are not allowed when checking for compatibility." << std::endl;
			cutoff = CUTOFFMAX+1;
			sound = false;
		}
		else if(vtype == ALTSIM && dynamic_cast<TopValuationFormula*>(vf) == 0)
		{
			(*cutoffisexact)[*cit] = false;
			std::cout << "...Failed. Relations related to parametric types are not allowed when checking for alternating simulation." << std::endl;
			cutoff = CUTOFFMAX+1;
			sound = false;
		}
		else if(vtype == COMPAT && qfvcount > 0)
		{
			(*cutoffisexact)[*cit] = false;
			std::cout << "...Failed. Quorum function variables are not allowed when checking for compatibility." << std::endl;
			cutoff = CUTOFFMAX+1;
			sound = false;			
		}
		else if(vtype == ALTSIM && qfvcount > 0)
		{
			(*cutoffisexact)[*cit] = false;
			std::cout << "...Failed. Quorum function variables are not allowed when checking for alternating simulation." << std::endl;
			cutoff = CUTOFFMAX+1;
			sound = false;			
		}
		else if(relevantRelvars > 0 && !sysspec->isProcessTypeVariable(*cit))
		{
			(*cutoffisexact)[*cit] = false;
			std::cout << "...Failed. There are both relations with the domain " << ctxt.typeVariableToString(*cit) << " and variables of the type " << ctxt.typeVariableToString(*cit) << " that are bound in an LTS." << std::endl;
			cutoff = CUTOFFMAX+1;
			sound = false;
		}
		else if(qfvcount > 3)
		{
			(*cutoffisexact)[*cit] = false;
			std::cout << "...Failed. There are too many nested quorum functions of the type " << ctxt.typeVariableToString(*cit) << "." << std::endl;
			cutoff = CUTOFFMAX+1;
			sound = false;			
		}
		else if(vf->getQuorumFunctionVariables().size() != 0)
		{
			(*cutoffisexact)[*cit] = false;
			std::cout << "...Failed. Quorum function variables are not allowed in the valuation constraint." << std::endl;
			cutoff = CUTOFFMAX+1;
			sound = false;						
		}
		else if(!((sys->isProcessTypeVariable(*cit) && spec->isProcessTypeVariable(*cit)) || (sys->isDataTypeVariable(*cit) && spec->isDataTypeVariable(*cit))))
		{
			std::cout << "...Failed. A variable of the type " << ctxt.typeVariableToString(*cit) << " appear bound both in a parallel composition and within an LTS." << std::endl;
			std::string input;
			do
			{
				if(vtype == TRACEREF && spec->isProcessTypeVariable(*cit))
				{
					std::cout << "Enter 'A' to apply abstraction or provide a cut-off (size) for the type " << ctxt.typeVariableToString(*cit) << std::endl;
					std::getline(std::cin, input);
					(*cutoffisexact)[*cit] = (input.compare("A") == 0 || input.compare("a") == 0);
					(*cutoffs)[*cit] = strtol(input.c_str(),0,0);
				}
				else
				{
					std::cout << "Please, provide a cut-off (size) for the type " << ctxt.typeVariableToString(*cit) << std::endl;
					(*cutoffisexact)[*cit] = false;
					std::cin >> (*cutoffs)[*cit];
				}
			}
			while(((*cutoffs)[*cit] < 1 || (*cutoffs)[*cit] > CUTOFFMAX) && !(*cutoffisexact)[*cit]);
			if((*cutoffisexact)[*cit])
			{
				LTSSchema* modsys = sys->convertChoiceToParallelComposition(*cit,true);
//				std::cout << "Sys = " << modsys->toString(ctxt) << std::endl << std::endl;
				if(!modsys->isProcessTypeVariable(*cit))
				{
					(*cutoffisexact)[*cit] = false;
					std::cout << "...Failed. The abstraction did not yield a cut-off." << std::endl;
					delete modsys;
					sound = false;
				}
				else
				{
					delete sys;
					delete sysspec;
					sys = modsys;
					sysspec = new ParallelLTSSchema(*sys, *spec);
					proccutoff = sysspec->numOfNestedOccurrences(*cit);
					datacutoff = sysspec->numOfParallelOccurrences(*cit,*sysspec,*cutoffs);
					cutoff = std::min(proccutoff,datacutoff) + freevars;
					(*cutoffs)[*cit] = std::max(1,cutoff);
					complete = false;
				}
			}
			else
			{
				sound = false;
			}
		}
		if((*cutoffisexact)[*cit] && cutoff > CUTOFFMAX && !determine_dynamically[*cit])
		{
			(*cutoffisexact)[*cit] = false;
			std::cout << "...Failed. The computed cut-off succeeds the maximum value of " << CUTOFFMAX << '.' << std::endl;
			sound = false;
		}
		else if((*cutoffisexact)[*cit] && cutoff <= CUTOFFMAX)
		{
			std::cout << "...Succeeded. The cut-off size is " << std::max(1,cutoff) << "." << std::endl;
			std::string input;
			do
			{
				std::cout << "Press enter to accept it or provide another value to change it " << std::endl;
				std::getline(std::cin, input);
				(*cutoffisexact)[*cit] = (input.compare("") == 0);
				(*cutoffs)[*cit] = strtol(input.c_str(),0,0);
			}
			while(((*cutoffs)[*cit] < 1 || (*cutoffs)[*cit] > CUTOFFMAX) && !(*cutoffisexact)[*cit]);
			if((*cutoffisexact)[*cit])
			{
				(*cutoffs)[*cit] = cutoff;
			}
			else
			{
				sound &= (cutoff <= (*cutoffs)[*cit]);
			}
		}
		if((*cutoffisexact)[*cit] == false && cutoff > CUTOFFMAX)
		{
			do
			{
				std::cout << "Please, provide a cut-off (size) for the type " << ctxt.typeVariableToString(*cit) << std::endl;
				std::cin >> (*cutoffs)[*cit];
			}
			while((*cutoffs)[*cit] < 1 || (*cutoffs)[*cit] > CUTOFFMAX);
			sound = false;
		}
		delete relevantConjunct;
		i++;
	}
	delete sysspec;
	auto vals = computeDynamicCutoff(ctxt, spec, sys, vf, *cutoffs);

/*
	i = 0;
	for(std::set<RelationVariable, VariableCompare<RelationVariable> >::iterator cit = relvars.begin(); cit != relvars.end(); ++cit)
	{
		const_cast<RelationVariable&>(*cit).id = i;
		spec->setNumericValue(RELVAR,std::get<0>(*cit), i);
		sys->setNumericValue(RELVAR,std::get<0>(*cit), i);
		vf->setNumericValue(RELVAR,std::get<0>(*cit), i);
		i++;
	}

	i = 0;
	for(std::set<AtomVariable, VariableCompare<AtomVariable> >::iterator cit = atomvars.begin(); cit != atomvars.end(); ++cit)
	{
		const_cast<AtomVariable&>(*cit).id = i;
		spec->setNumericValue(ATOMVAR, cit->first, i);
		sys->setNumericValue(ATOMVAR, cit->first, i);
		vf->setNumericValue(ATOMVAR, cit->first, i);
		i++;
	}

	i = 0;
	for(std::set<QuorumFunctionVariable, VariableCompare<QuorumFunctionVariable> >::iterator cit = qfvars.begin(); cit != qfvars.end(); ++cit)
	{
		const_cast<QuorumFunctionVariable&>(*cit).id = i;
		spec->setNumericValue(QFVAR, std::get<0>(*cit), i);
		sys->setNumericValue(QFVAR, std::get<0>(*cit), i);
		vf->setNumericValue(QFVAR, std::get<0>(*cit), i);
		i++;
	}
	auto vals = computeValuations(ctxt, vf, spec, sys, *cutoffs);
*/
	
	spec->setNumericValuesToBoundVariables(atomvars.size());
	sys->setNumericValuesToBoundVariables(atomvars.size());
	vf->setNumericValuesToBoundVariables(atomvars.size());

	std::set<RelationVariable> sysspecrvs{};
	sysspecrvs.insert(spec->getRelationVariables().begin(), spec->getRelationVariables().end());
	sysspecrvs.insert(sys->getRelationVariables().begin(), sys->getRelationVariables().end());
	std::set<RelationVariable, VariableCompare<RelationVariable> > vfonlyrvs{};
	for(auto& rv : vf->getRelationVariables())
	{
		auto it = sysspecrvs.find(rv);
		if(it == sysspecrvs.end())
		{
			vfonlyrvs.insert(rv);
		}
	}

	// remove extra relation variables
	if(vfonlyrvs.size() > 0)
	{
		std::set<Valuation*, PointerCompare<Valuation>>* tempvals = new std::set<Valuation*, PointerCompare<Valuation>>{};
		for(auto v : *std::get<0>(vals))
		{
			for(auto& rv : vfonlyrvs)
			{
				v->clearRelationVariable(rv);
			}
			v->computeCanonicalGraph();
			std::get<2>(vals)++;
			auto vit = tempvals->find(v);
			if(vit == tempvals->end())
			{
				tempvals->insert(v);
			}
			else
			{
				delete v;
			}
		}
		std::swap(std::get<0>(vals), tempvals);
		delete tempvals;
	}

	return std::tuple<std::set<Valuation*,PointerCompare<Valuation> >*, int, int, int, std::map<TypeVariable,bool>*, bool, bool>(std::get<0>(vals),std::get<1>(vals),std::get<2>(vals),std::get<3>(vals),cutoffisexact,sound,complete);
}

std::tuple<std::set<Valuation*,PointerCompare<Valuation> >*, int, int, int> computeDynamicCutoff(Context ctxt, LTSSchema* spec, LTSSchema* sys, ValuationFormula* vf, const std::map<TypeVariable, int>& staticcutoffs)
{
	std::cout << std::endl << "Computing the optimal cut-off set." << std::unitbuf << std::endl;
	std::cout << "The size of the cut-off set is now 0." << std::unitbuf << std::endl;

	int totvalCount{0};
	int totcanCount{0};
	int totstoredMax{0};

	std::set<TypeVariable, VariableCompare<TypeVariable>> typevars{};
	typevars.insert(vf->getTypeVariables().begin(), vf->getTypeVariables().end());
	typevars.insert(spec->getTypeVariables().begin(), spec->getTypeVariables().end());
	typevars.insert(sys->getTypeVariables().begin(), sys->getTypeVariables().end());
	int i = 0;
	for(auto cit = typevars.begin(); cit != typevars.end(); ++cit)
	{
		const_cast<TypeVariable&>(*cit).id = i;
		spec->setNumericValue(TYPEVAR, cit->first, i);
		sys->setNumericValue(TYPEVAR, cit->first, i);
		vf->setNumericValue(TYPEVAR, cit->first, i);
		i++;
	}

	std::set<AtomVariable, VariableCompare<AtomVariable>> sysatomvars{};
	sysatomvars.insert(vf->getAtomVariables().begin(), vf->getAtomVariables().end());
	sysatomvars.insert(spec->getAtomVariables().begin(), spec->getAtomVariables().end());
	sysatomvars.insert(sys->getAtomVariables().begin(), sys->getAtomVariables().end());
	i = 0;
	for(auto cit = sysatomvars.begin(); cit != sysatomvars.end(); ++cit)
	{
		const_cast<AtomVariable&>(*cit).id = i;
		spec->setNumericValue(ATOMVAR, cit->first, i);
		sys->setNumericValue(ATOMVAR, cit->first, i);
		vf->setNumericValue(ATOMVAR, cit->first, i);
		i++;
	}

	std::set<RelationVariable, VariableCompare<RelationVariable>> sysspecrvs{};
	sysspecrvs.insert(spec->getRelationVariables().begin(), spec->getRelationVariables().end());
	sysspecrvs.insert(sys->getRelationVariables().begin(), sys->getRelationVariables().end());
	std::set<RelationVariable, VariableCompare<RelationVariable>> relvars{sysspecrvs};
	relvars.insert(vf->getRelationVariables().begin(), vf->getRelationVariables().end());
	i = 0;
	for(auto cit = relvars.begin(); cit != relvars.end(); ++cit)
	{
		const_cast<RelationVariable&>(*cit).id = i;
		for(auto& rv : sysspecrvs)
		{
			const_cast<RelationVariable&>(rv).setNumericValue(RELVAR,std::get<0>(*cit),i);
		}
		spec->setNumericValue(RELVAR,std::get<0>(*cit), i);
		sys->setNumericValue(RELVAR,std::get<0>(*cit), i);
		vf->setNumericValue(RELVAR,std::get<0>(*cit), i);
		i++;
	}
	spec->setNumericValuesToBoundVariables(sysatomvars.size());
	sys->setNumericValuesToBoundVariables(sysatomvars.size());
	vf->setNumericValuesToBoundVariables(sysatomvars.size());
	
	std::set<RelationVariable> pospreds = spec->getPredicates(true);
	std::set<RelationVariable> syspospreds = sys->getPredicates(true);
	pospreds.insert(syspospreds.begin(), syspospreds.end());
	std::set<RelationVariable> negpreds = spec->getPredicates(false);
	std::set<RelationVariable> sysnegpreds = sys->getPredicates(false);
	negpreds.insert(sysnegpreds.begin(), sysnegpreds.end());
	
	std::map<TypeVariable, int> dyncutoffs{};
	for(auto& tv : typevars)
	{
		dyncutoffs[tv] = 1;
//		std::cout << "Initialising cutoff: " << ctxt.typeVariableToString(tv) << " -> " << dyncutoffs[tv] << std::endl;
	}
	std::cout << std::endl;
//	auto vals = computeValuations(ctxt, vf, spec, sys, dyncutoffs, nullptr);
	auto vals = std::tuple<std::set<Valuation*,PointerCompare<Valuation>>*, int, int, int>{new std::set<Valuation*,PointerCompare<Valuation>>{}, 0, 0, 0};

	std::list<SyntaxBranchOfLTSSchema> branches = sys->getSyntaxBranches();
	std::list<SyntaxBranchOfLTSSchema> specbranches = spec->getSyntaxBranches();
	branches.splice(branches.end(), specbranches);
	int branchcount = branches.size();
	for(auto& b : branches)
	{
		branchcount--;
		auto valsdelta = std::get<0>(vals);
		auto bvf0 = std::unique_ptr<ValuationFormula>(b.toValuationFormula(ctxt));
		int i = 0;
		for(auto cit = typevars.begin(); cit != typevars.end(); ++cit)
		{
			bvf0->setNumericValue(TYPEVAR, cit->first, i);
			i++;
		}
		i = 0;
		for(auto cit = sysatomvars.begin(); cit != sysatomvars.end(); ++cit)
		{
			bvf0->setNumericValue(ATOMVAR, cit->first, i);
			i++;
		}
		std::set<AtomVariable> branchatomvars = bvf0->getAtomVariables();
		for(auto cit = branchatomvars.begin(); cit != branchatomvars.end(); ++cit)
		{
			const_cast<AtomVariable&>(*cit).id = i;
			bvf0->setNumericValue(ATOMVAR, cit->first, i);
			i++;
		}
		i = 0;
		for(auto cit = relvars.begin(); cit != relvars.end(); ++cit)
		{
			bvf0->setNumericValue(RELVAR,std::get<0>(*cit), i);
			i++;
		}
		std::set<AtomVariable, VariableCompare<AtomVariable>> atomvars{sysatomvars};
		atomvars.insert(branchatomvars.begin(), branchatomvars.end());

/*
		for(auto& av : sysatomvars)
		{
			branchatomvars.erase(av);
			std::cout << ctxt.atomVariableToString(av) << " = " << av << std::endl;
		}
		for(auto& av : branchatomvars)
		{
			std::cout << ctxt.atomVariableToString(av) << " = " << av << std::endl;
		}
		std::cout << std::endl;		
*/
/*
		std::set<AtomVariable, VariableCompare<AtomVariable>> allavs{};
		allavs.insert(atomvars.begin(), atomvars.end());
		for(auto& av : b.getBoundAtomVariables())
		{
			allavs.insert(av.getPrimedVersion());
		}
*/
		// generate all possible equivalence classes among atom variables
/*		std::set<std::vector<std::set<AtomVariable>>> partitions{};
		partitions.insert(std::vector<std::set<AtomVariable>>{});
		for(auto& av : atomvars)
		{
			std::set<std::vector<std::set<AtomVariable>>> ext_partitions{};
			for(auto& part : partitions)
			{
				for(int i = 0; i < part.size(); i++)
				{
					if(av.second == part[i].begin()->second)
					{
						auto ext_part{part};
						ext_part[i].insert(av);
						ext_partitions.insert(std::move(ext_part));
					}
				}
				auto ext_part{part};
				std::set<AtomVariable> pset{};
				pset.insert(av);
				ext_part.push_back(std::move(pset));
				ext_partitions.insert(std::move(ext_part));
			}
			partitions = std::move(ext_partitions);
		}
*/
		// go through all partitions
//		std::cout << "go through all partitions" << std::endl;
//		for(auto& part : partitions)
		{
/*
			for(auto& pset : part)
			{
				std::cout << pset << std::endl;
			}
*/
			ValuationFormula* bvf = new ConjunctiveValuationFormula(vf->clone(), bvf0->clone());
/*			std::cout << bvf0->toString(ctxt) << std::endl;
			for(auto& tv : typevars)
			{
/*				int setcount = 0;
				for(auto& pset : part)
				{
					if(pset.begin()->second == tv) setcount++;
				}
				dyncutoffs[tv] = std::max<int>(1, setcount);

				std::cout << ctxt.typeVariableToString(tv) << " -> " << dyncutoffs[tv] << std::endl;
			}
*/
			// std::cout << "Branch:" << std::endl;
			// std::cout << b.back()->toString(ctxt) << std::endl;
    		z3::context z3ctxt;
			z3::solver slvr(z3ctxt);
			// check for the existence of a valuation
   		 	z3::expr z3vf = bvf->toZ3Expr(z3ctxt);
   			//std::cout << "bvf: " << z3vf << std::endl;
    		slvr.add(z3vf);
			//auto sat = slvr.check();
			//std::cout << sat << std::endl;
/*
			for(int i = 0; i < part.size(); i++)
			{
				auto avit = part[i].begin();
				auto& avf = *avit;
				auto vari = avf.toZ3Expr(z3ctxt);
				// atom variables in the same partition are equal
				for(avit++; avit != part[i].end(); avit++)
				{
					slvr.add(vari == avit->toZ3Expr(z3ctxt));
					auto eqvf = new EquivalenceValuationFormula(new AtomVariableSchema(new AtomVariable(*avit)), new AtomVariableSchema(new AtomVariable(avf)));
					bvf = new ConjunctiveValuationFormula(bvf, eqvf);
				}
				//atom variables in different partitions are inequal
				for(int j = i+1; j < part.size(); j++)
				{
					if(part[i].begin()->second == part[j].begin()->second)
					{
						auto& avfj = *(part[j].begin());
						auto varj = avfj.toZ3Expr(z3ctxt);
						slvr.add(vari != varj);							
						auto eqvf = new EquivalenceValuationFormula(new AtomVariableSchema(new AtomVariable(avfj)), new AtomVariableSchema(new AtomVariable(avf)));
						bvf = new ConjunctiveValuationFormula(bvf, new NegatedValuationFormula(eqvf));
					}
				}
			}    	
*/
			auto sat = slvr.check();
			// std::cout << sat << std::endl;
	    	if(sat == z3::sat)
			{
//				std::cout << "An instance found." << std::endl;
				//auto model = slvr.get_model();
    			//std::cout << model << std::endl;
			}
			else if(sat == z3::unsat)
			{
//				std::cout << "No instances in this branch." << std::endl;	
	    		delete bvf;
				continue;
			}
			else
			{
				std::cout << std::endl << "...Computation of dynamic cutoffs failed." << std::endl;
				for(auto& tv : typevars)
				{
					dyncutoffs[tv] = CUTOFFMAX;
				}
	    		delete bvf;
				break;
			}
			while(sat == z3::sat)
			{
				//slvr.push();
				// define functions from small fixed types to unrestricted ones
				/*
				std::map<TypeVariable, z3::func_decl> typemaps{};
				for(auto& tv : typevars)
				{
					auto z3type = std::get<0>(tv.toZ3Sort(z3ctxt));
					auto z3fintype = std::get<0>(tv.toZ3Sort(z3ctxt, dyncutoffs));
					z3::func_decl sv = z3::function(("sbij" + std::to_string(dyncutoffs[tv]) + "_" + ctxt.typeVariableToString(tv)).c_str(), z3fintype, z3type);
					z3::expr vx = z3ctxt.constant(("tx" + std::to_string(dyncutoffs[tv])).c_str(), z3fintype);
					z3::expr vy = z3ctxt.constant(("ty" + std::to_string(dyncutoffs[tv])).c_str(), z3fintype);
					slvr.add(z3::forall(vx, vy, z3::implies(!(vx == vy), !(sv(vx) == sv(vy)))));
					typemaps.emplace(tv, std::move(sv));
				}
				*/
				// compute extended valuations up to the proposed cutoffs
				std::cout << "Computing the extended valuations..." << std::endl;
				spec->setNumericValuesToBoundVariables(atomvars.size());
				sys->setNumericValuesToBoundVariables(atomvars.size());
				vf->setNumericValuesToBoundVariables(atomvars.size());
				bvf0->setNumericValuesToBoundVariables(atomvars.size());
				bvf->setNumericValuesToBoundVariables(atomvars.size());
				auto bvals = new std::set<Valuation*,PointerCompare<Valuation>>{};
				for(auto& v : *valsdelta)
				{
					bvals->insert(new Valuation(*v));
				}
				if(valsdelta != std::get<0>(vals))
				{
					delete valsdelta;
				}
				//std::cout << "bvals size: " << bvals->size() << std::endl;
				ValuationFormula* msnvf = new TopValuationFormula{};
				std::list<ValuationFormula*> msnvfRelnegConjuncts = vf->conjuncts();
				std::list<ValuationFormula*>::iterator lit = msnvfRelnegConjuncts.begin();
				while(lit != msnvfRelnegConjuncts.end())
				{
					if((*lit)->isMembershipNegated())
					{
						msnvf = new ConjunctiveValuationFormula(msnvf, *lit);
						++lit;
					}
					else
					{
						delete *lit;
						lit = msnvfRelnegConjuncts.erase(lit);
					}

				}				
				ValuationFormula* pvf = vf->clone();
				std::list<ValuationFormula*> conjuncts = bvf0->conjuncts();
				std::list<ValuationFormula*> relnegConjuncts = bvf0->conjuncts();
				lit = relnegConjuncts.begin();
				while(lit != relnegConjuncts.end())
				{
					if((*lit)->isMembershipNegated())
					{
						++lit;
					}
					else
					{
						delete *lit;
						lit = relnegConjuncts.erase(lit);
					}

				}
				std::atomic<double> computed{0.0};
				std::atomic<int> valCount{0};
				std::atomic<int> canCount{0};
				std::atomic<int> storedMax{0};
				std::atomic<int> canReductionCount{0};
				std::atomic<int> pruneCount{0};
				std::atomic<int> storedCount{0};
				double portion = 100;
				if(branchatomvars.size() > 0)
				{
					portion = portion / branchatomvars.size();
				}
				std::set<AtomVariable, VariableCompare<AtomVariable>> branchatomvars_vc{};
				branchatomvars_vc.insert(branchatomvars.begin(), branchatomvars.end());
				bvals = computeExtendedValuations({}, {}, branchatomvars_vc, {}, dyncutoffs, msnvf, pvf, conjuncts, relnegConjuncts, computed, portion, valCount, canCount, canReductionCount, pruneCount, storedCount, storedMax, bvals, false, nullptr);
				totvalCount += valCount;
				totcanCount += canCount;                 
				totstoredMax = std::max<int>(totstoredMax, storedMax + bvals->size());
				std::cout << "...Done!"  << std::setw(70) << " " << std::endl;
				std::cout << "Found " << bvals->size() << " non-isomorphic valuations." << std::endl;
				std::cout << "(#valuations: " << valCount << ", #canonical forms: " << canCount << ", max #valuations stored: " << storedMax << ", #isomorphs removed: " << canReductionCount << ", #branches pruned: " << pruneCount << ')' << std::endl;
				/*
				if(std::get<0>(vals)->size() > 0)
				{
					auto val = *(std::get<0>(vals)->begin());
					for(auto& av : atomvars)
					{
						auto& f = typemaps.at(av.second);
						auto fc = f(val->getValue(av).toZ3Expr(z3ctxt, dyncutoffs)) == av.toZ3Expr(z3ctxt);
						std::cout << fc << std::endl;
						slvr.add(fc);
						for(auto val2 : *(std::get<0>(vals)))
						{
							assert(val->getValue(av) == val2->getValue(av));
						}
					}
				}
				*/

				// safety check
				//sat = slvr.check();
				//assert(sat == z3::sat);
				
				// check whether none of the small valuations is contained in the big one
				for(auto val : *bvals)
				{
					// introduce variables to represent the bijective image of fixed types
					std::map<TypeVariable, z3::expr_vector> xvarsbig{};
					std::stringstream output{};
					output << "Encoding instance generated by valuation" << std::endl;
					for(auto& tv : typevars)
					{
						AtomSet* as = val->getValue(tv);
						output << tv.toString(ctxt) << " -> " << as->toString(ctxt) << std::endl;
						auto z3type = std::get<0>(tv.toZ3Sort(z3ctxt));
						z3::expr_vector bigvars{z3ctxt};
						for(int i = 0; i < as->size(); i++)
						{
							bigvars.push_back(z3ctxt.constant(("bx" + ctxt.typeVariableToString(tv) + std::to_string(as->size()) + "_" + std::to_string(i)).c_str(), z3type));
						}
						xvarsbig.emplace(tv, std::move(bigvars));
						delete as;
					}
					for(auto crit = sysspecrvs.begin(); crit != sysspecrvs.end(); ++crit)
					{
						AtomTupleSet* ats = val->getValue(*crit);
						output << crit->toString(ctxt) << " -> " << ats->toString(ctxt) << std::endl;
						delete ats;
					}
					for(auto cait = atomvars.begin(); cait != atomvars.end(); ++cait)
					{
						output << cait->toString(ctxt) << " -> " << ctxt.atomToString(val->getValue(*cait)) << std::endl;
					}
//					std::cout << output.str();

					z3::expr contained = z3ctxt.bool_val(false);
					// check whether the values of type variables are contained
					for(auto& xvb : xvarsbig)
					{
						for(int i = 0; i < xvb.second.size(); i++)
						{
							for(int j = i+1; j < xvb.second.size(); j++)
							{
								contained = (xvb.second[i] == xvb.second[j]) || contained;
							}
						}
					}
					// check whether the values of atom variables are contained
					for(auto& av : atomvars)
					{
						TypeVariable tv = av.second;
						auto at = val->getValue(av);
						contained = (xvarsbig.at(tv)[at.first] != av.toZ3Expr(z3ctxt)) || contained;
					}
					// check whether the values of relation variables are contained 
					for(auto& rv : pospreds)
					{
						auto rel = val->getValue(rv);
						auto z3rel = rv.toZ3Func(z3ctxt);
						for(auto& tuple : *rel)
						{
							z3::expr_vector vals{z3ctxt};
							for(int i = 0; i < tuple.size(); i++)
							{
								auto& tv = std::get<1>(rv)[i];
								vals.push_back(xvarsbig.at(tv)[tuple[i].first]);
								/*
								z3::expr z3val = tuple[i].toZ3Expr(z3ctxt, dyncutoffs);
								auto& f = typemaps.at(tv);
								vals.push_back(f(z3val));
								*/
							}
							//std::cout << z3rel(vals) << std::endl;
							contained = contained || !z3rel(vals);
						}
						delete rel;
					}
					// check whether the complement values of relation variables are contained 
					for(auto& rv : negpreds)
					{
						auto rel = val->getComplementValue(rv);
						auto z3rel = rv.toZ3Func(z3ctxt);
						for(auto& tuple : *rel)
						{
							z3::expr_vector vals{z3ctxt};
							for(int i = 0; i < tuple.size(); i++)
							{
								auto& tv = std::get<1>(rv)[i];
								vals.push_back(xvarsbig.at(tv)[tuple[i].first]);
								/*
								z3::expr z3val = tuple[i].toZ3Expr(z3ctxt, dyncutoffs);
								auto& f = typemaps.at(tv);
								vals.push_back(f(z3val));
								*/
							}
							//std::cout << z3rel(vals) << std::endl;
							contained = contained || z3rel(vals);
						}
						delete rel;
					}
					// check all permutations
					for(auto& xvb : xvarsbig)
					{
						if(xvb.second.size() != 0)
						{
							contained = z3::forall(xvb.second, contained);
						}						
					}					
					slvr.add(std::move(contained));
					delete val;
				}
				delete bvals;
//				std::cout << "Testing for the sufficiency of the cut-off set..." << std::endl;
				std::cout << "Searching for satisfying valuations..." << std::endl;
				sat = slvr.check();
				if(sat == z3::sat)
				{
/*
					auto model = slvr.get_model();
					auto z3mod = Z3_model(model);
					auto z3c_ctxt = Z3_context(z3ctxt);
					TypeVariable stv{*(typevars.begin())};
					int stv_size = std::numeric_limits<int>::max();
					int stv_size_in_model = 0;
					for(auto& tv : typevars)
					{
						auto z3type = std::get<0>(tv.toZ3Sort(z3ctxt));
						auto z3sort = Z3_sort(z3type);
						auto z3uni = Z3_model_get_sort_universe(z3c_ctxt, z3mod, z3sort);
						if(z3uni > 0)
						{
							auto uni_size = Z3_ast_vector_size(z3c_ctxt, z3uni);
							if(uni_size > dyncutoffs[tv] && (dyncutoffs[tv] < stv_size || (dyncutoffs[tv] == stv_size && uni_size > stv_size_in_model)))
							{
								stv_size_in_model = uni_size;
								stv_size = dyncutoffs[tv];
								stv = tv;
							}
						}
					}
					dyncutoffs[stv]++;
					std::cout << "...Failed." << std::endl << "Incrementing cutoff: " << ctxt.typeVariableToString(stv) << " -> " << dyncutoffs[stv] << std::endl << std::endl;
					spec->setNumericValuesToBoundVariables(sysatomvars.size());
					sys->setNumericValuesToBoundVariables(sysatomvars.size());
					vf->setNumericValuesToBoundVariables(sysatomvars.size());
					auto newvals = computeValuations(ctxt, vf, spec, sys, dyncutoffs, &stv);
					totvalCount += std::get<1>(newvals);
					totcanCount += std::get<2>(newvals);
					totstoredMax = std::max<int>(totstoredMax, std::get<3>(newvals) + std::get<0>(vals)->size());
					for(auto& val : *std::get<0>(newvals))
					{
						std::get<0>(vals)->insert(val);
					}
					valsdelta = std::get<0>(newvals);
*/

					
				auto model = slvr.get_model();
				std::list<Valuation*> cexval{};
				bool checktypes = true;
		    	while(sat == z3::sat)
    			{
//					std::cout << "...Failed. Increasing cutoff size." << std::endl;
					//std::cout << model << std::endl;
					//slvr.pop();
					auto z3mod = Z3_model(model);
					auto z3c_ctxt = Z3_context(z3ctxt);
					cexval.push_back(new Valuation{});
					std::map<TypeVariable, Z3_ast_vector> domains{};
					for(auto& tv : typevars)
					{
						auto z3type = std::get<0>(tv.toZ3Sort(z3ctxt));
						auto z3sort = Z3_sort(z3type);
						auto z3uni = Z3_model_get_sort_universe(z3c_ctxt, z3mod, z3sort);
						domains.emplace(tv, std::move(z3uni));
						Z3_ast_vector_inc_ref(z3c_ctxt, z3uni);
						auto uni_size = Z3_ast_vector_size(z3c_ctxt, z3uni);
						auto cexval2 = cexval.back()->addTypeVariable(uni_size, uni_size);
						delete cexval.back();
						cexval = std::move(cexval2);
					}
					for(auto& av : sysatomvars)
					{
						auto z3var = av.toZ3Expr(z3ctxt);
						
						for(int i = 0; i < Z3_ast_vector_size(z3c_ctxt, domains.at(av.second)); i++)
						{
							z3::expr z3atom = z3::expr{z3ctxt, Z3_ast_vector_get(z3c_ctxt, domains.at(av.second), i)};
							if(model.eval(z3var == z3atom).bool_value() == Z3_L_TRUE)
							{
								auto cexval2 = cexval.back()->addAtomVariable(av.second,i);
								delete cexval.back();
								cexval = std::move(cexval2);
							}
						}
					}
					for(auto& rv : relvars)
					{
						auto newval = cexval.back()->addRelationVariable(std::get<1>(rv));
						delete cexval.back();
						cexval.clear();
						cexval.push_back(newval);

						std::vector<int> val(std::get<1>(rv).size(),0);
						int domsize = 1;
						for(auto& tv : std::get<1>(rv))
						{
							domsize *= Z3_ast_vector_size(z3c_ctxt, domains.at(tv));
						}
						for(int i = 0; i < domsize; i++)
						{
							auto z3rv = rv.toZ3Func(z3ctxt);
							z3::expr_vector z3vals{z3ctxt};
							for(int j = 0; j < val.size(); j++)
							{
								z3::expr e = z3::expr{z3ctxt, Z3_ast_vector_get(z3c_ctxt, domains.at(std::get<1>(rv)[j]), val[j])};
								z3vals.push_back(e);
							}
							if(model.eval(z3rv(z3vals)).bool_value() == Z3_L_TRUE)
							{
								cexval.back()->setMember(rv.id, val);
							}
							for(int j = 0; j < std::get<1>(rv).size(); j++)
							{
								val[j] = (val[j] + 1) % Z3_ast_vector_size(z3c_ctxt, domains.at(std::get<1>(rv).at(j)));
								if(val[j] != 0) break;
							}
						}
					}

					std::stringstream output{};
					output << "...New valuation found:" << std::endl;
					for(auto& tv : typevars)
					{
						AtomSet* as = cexval.back()->getValue(tv);
						output << tv.toString(ctxt) << " -> " << as->toString(ctxt) << std::endl;
						delete as;
					}
					for(auto crit = relvars.begin(); crit != relvars.end(); ++crit)
					{
						AtomTupleSet* ats = cexval.back()->getValue(*crit);
						output << crit->toString(ctxt) << " -> " << ats->toString(ctxt) << std::endl;
						delete ats;
					}
					for(auto cait = sysatomvars.begin(); cait != sysatomvars.end(); ++cait)
					{
						output << cait->toString(ctxt) << " -> " << ctxt.atomToString(cexval.back()->getValue(*cait)) << std::endl;
					}
					std::cout << output.str();
/*
					do
					{
						slvr.push();
						std::map<TypeVariable, z3::expr_vector> xvarsbig{};
						std::cout << "Searching for valuations with smaller types..." << std::endl;
						for(auto& tv : typevars)
						{
							//output << tv.toString(ctxt) << " -> " << as->toString(ctxt) << std::endl;
							auto z3type = std::get<0>(tv.toZ3Sort(z3ctxt));
							z3::expr_vector bigvars{z3ctxt};
							int cutoff = std::min<int>(dyncutoffs[tv]+1, Z3_ast_vector_size(z3c_ctxt, domains.at(tv)));
							for(int i = 0; i < cutoff; i++)
							{
								bigvars.push_back(z3ctxt.constant(("bx" + std::to_string(cutoff) + "_" + std::to_string(i)).c_str(), z3type));
							}
							xvarsbig.emplace(tv, std::move(bigvars));
							//std::cout << ctxt.typeVariableToString(tv) << " -> " << as->size() << std::endl;
						}
						z3::expr contained = z3ctxt.bool_val(false);
						// check whether the values of type variables are contained
						for(auto& xvb : xvarsbig)
						{
							for(int i = 0; i < xvb.second.size(); i++)
							{
								for(int j = i+1; j < xvb.second.size(); j++)
								{
									contained = (xvb.second[i] == xvb.second[j]) || contained;
								}
							}
						}
						// check all permutations
						for(auto& xvb : xvarsbig)
						{
							if(xvb.second.size() != 0)
							{
								contained = z3::forall(xvb.second, contained);
							}						
						}					
						slvr.add(std::move(contained));
						sat = slvr.check();
						if(sat == z3::unsat)
						{
							sat = z3::unknown;
							for(auto& tv : typevars)
							{
								if(dyncutoffs[tv] < Z3_ast_vector_size(z3c_ctxt, domains.at(tv)))
								{
									dyncutoffs[tv]++;
									std::cout << "...Failed. Incrementing cutoff: " << ctxt.typeVariableToString(tv) << " -> " << dyncutoffs[tv] << std::endl;
									sat = z3::unsat;
									break;
								}
							}
						}
						else if(sat == z3::sat)
						{
							model = slvr.get_model();
							delete cexval.back();
							cexval.clear();
						}
						slvr.pop();
					}while(sat == z3::unsat);
					//std::cout << sat << std::endl;
*/
//					if(sat == z3::unknown)
					{
						slvr.push();
						std::map<TypeVariable, z3::expr_vector> xvarsbig{};
						std::cout << "Minimising the values of sorts..." << std::endl;
						for(auto& tv : typevars)
						{
							//output << tv.toString(ctxt) << " -> " << as->toString(ctxt) << std::endl;
							auto z3type = std::get<0>(tv.toZ3Sort(z3ctxt));
							z3::expr_vector bigvars{z3ctxt};
							int cutoff = Z3_ast_vector_size(z3c_ctxt, domains.at(tv));
							for(int i = 0; i < cutoff; i++)
							{
								bigvars.push_back(z3ctxt.constant(("bx" + ctxt.typeVariableToString(tv) + std::to_string(cutoff) + "_" + std::to_string(i)).c_str(), z3type));
							}
							xvarsbig.emplace(tv, std::move(bigvars));
							//std::cout << ctxt.typeVariableToString(tv) << " -> " << as->size() << std::endl;
						}
						z3::expr domaincovered = z3ctxt.bool_val(true);
						for(auto& xvb : xvarsbig)
						{
							z3::expr noextravals = z3ctxt.bool_val(false);
							auto z3type = std::get<0>(xvb.first.toZ3Sort(z3ctxt));
							auto extravar = z3ctxt.constant("bxe", z3type);
							for(int i = 0; i < xvb.second.size(); i++)
							{
								noextravals = (extravar == xvb.second[i]) || noextravals;
							}
							domaincovered = domaincovered && z3::forall(extravar, noextravals);
						}
						z3::expr contained = z3ctxt.bool_val(true);
						for(auto& av : sysatomvars)
						{
							TypeVariable tv = av.second;
							auto at = cexval.back()->getValue(av);
							contained = (xvarsbig.at(tv)[at.first] == av.toZ3Expr(z3ctxt)) && contained;
						}
						auto matchingatoms = contained;
						z3::expr properlycontained = z3ctxt.bool_val(false);;
						for(auto& rv : pospreds)
						{
							auto rel = cexval.back()->getComplementValue(rv);
							auto z3rel = rv.toZ3Func(z3ctxt);
							for(auto& tuple : *rel)
							{
								z3::expr_vector vals{z3ctxt};
								for(int i = 0; i < tuple.size(); i++)
								{
									auto& tv = std::get<1>(rv)[i];
									vals.push_back(xvarsbig.at(tv)[tuple[i].first]);
								}
								//std::cout << z3rel(vals) << std::endl;
								contained = contained && !z3rel(vals);
							}
							delete rel;
							rel = cexval.back()->getValue(rv);
							for(auto& tuple : *rel)
							{
								z3::expr_vector vals{z3ctxt};
								for(int i = 0; i < tuple.size(); i++)
								{
									auto& tv = std::get<1>(rv)[i];
									vals.push_back(xvarsbig.at(tv)[tuple[i].first]);
								}
								//std::cout << z3rel(vals) << std::endl;
								properlycontained = properlycontained || !z3rel(vals);
							}
							delete rel;
						}
						for(auto& rv : negpreds)
						{
							auto rel = cexval.back()->getValue(rv);
							auto z3rel = rv.toZ3Func(z3ctxt);
							for(auto& tuple : *rel)
							{
								z3::expr_vector vals{z3ctxt};
								for(int i = 0; i < tuple.size(); i++)
								{
									auto& tv = std::get<1>(rv)[i];
									vals.push_back(xvarsbig.at(tv)[tuple[i].first]);
								}
								//std::cout << z3rel(vals) << std::endl;
								contained = contained && z3rel(vals);
							}
							delete rel;
							rel = cexval.back()->getComplementValue(rv);
							for(auto& tuple : *rel)
							{
								z3::expr_vector vals{z3ctxt};
								for(int i = 0; i < tuple.size(); i++)
								{
									auto& tv = std::get<1>(rv)[i];
									vals.push_back(xvarsbig.at(tv)[tuple[i].first]);
								}
								//std::cout << z3rel(vals) << std::endl;
								properlycontained = properlycontained || z3rel(vals);
							}
							delete rel;
						}
						z3::expr smallertypes = z3ctxt.bool_val(false);;
						// check whether the values of type variables are contained
						for(auto& xvb : xvarsbig)
						{
							for(int i = 0; i < xvb.second.size(); i++)
							{
								for(int j = i+1; j < xvb.second.size(); j++)
								{
									smallertypes = (xvb.second[i] == xvb.second[j]) || smallertypes;
								}
							}
						}
						contained = domaincovered && !smallertypes && contained && properlycontained;
						// check all permutations
						for(auto& xvb : xvarsbig)
						{
							if(xvb.second.size() != 0)
							{
								contained = z3::exists(xvb.second, contained);
								smallertypes = z3::exists(xvb.second, smallertypes && domaincovered && matchingatoms);
							}						
						}
						if(checktypes)
						{
							slvr.add(std::move(smallertypes));
							sat = slvr.check();
							if(sat == z3::sat)
							{
								model = slvr.get_model();
								delete cexval.back();
								cexval.clear();
							}
							else if(sat == z3::unsat)
							{
								checktypes = false;
								slvr.pop();
								slvr.push();
								std::cout << "...Done!" << std::endl;
								std::cout << "Minimising the values of predicates..." << std::endl;
							}
						}					
						if(!checktypes)
						{
							slvr.add(std::move(contained));
							sat = slvr.check();
							if(sat == z3::sat)
							{
								model = slvr.get_model();
								delete cexval.back();
								cexval.clear();
							}
						}
						slvr.pop();
					}
					//std::cout << sat << std::endl;
					totvalCount++;

					for(auto& tv : typevars)
					{
						Z3_ast_vector_dec_ref(z3c_ctxt, domains.at(tv));						
					}
				}
				std::cout << "...Done!" << std::endl;
				valsdelta = new std::set<Valuation*,PointerCompare<Valuation>>{};
				cexval.back()->computeCanonicalGraph();
				std::get<0>(vals)->insert(cexval.back());
				valsdelta->insert(cexval.back());
				vf->setNumericValuesToBoundVariables(sysatomvars.size());
				assert(vf->instance(*cexval.back()));
				totcanCount++;
				totstoredMax++;
				std::cout << "The size of the cut-off set is now " << std::get<0>(vals)->size() << '.' << std::endl << std::endl;
				sat = z3::sat;										
				}
				else if(sat == z3::unsat)
	    		{
	    			if(branchcount > 0)
	    			{
						std::cout << "...All found for this component, continuing to the next one." << std::endl;	
	    			}
	    			else
	    			{
						std::cout << "...All found." << std::endl;
						std::cout << " The optimal cut-off set is found." << std::endl << std::endl;
/*
						for(auto& tv : typevars)
						{
							std::cout << ctxt.typeVariableToString(tv) << " -> " << dyncutoffs[tv] << std::endl;
						}
						std::cout << std::endl;   				
*/
	    			}
				}
				else
	    		{
					std::cout << "...Computation of dynamic cutoffs failed!" << std::endl;	
					for(auto& tv : typevars)
					{
						dyncutoffs[tv] = CUTOFFMAX;
					}
	    		}
    		}
	   		delete bvf;
 		}
	}
	spec->setNumericValuesToBoundVariables(sysatomvars.size());
	sys->setNumericValuesToBoundVariables(sysatomvars.size());
	vf->setNumericValuesToBoundVariables(sysatomvars.size());
	return std::tuple<std::set<Valuation*,PointerCompare<Valuation> >*, int, int, int>{std::get<0>(vals), totvalCount, totcanCount, totstoredMax};
}

std::tuple<std::set<Valuation*,PointerCompare<Valuation> >*, int, int, int> computeValuations(const Context& ctxt, ValuationFormula*& vf, LTSSchema*& spec, LTSSchema*& sys, const std::map<TypeVariable,int>& cutoffs, TypeVariable* fixedtype)
{
	std::atomic<double> computed{0.0};
	std::atomic<int> valCount{0};
	std::atomic<int> canCount{0};
	std::atomic<int> canReductionCount{0};
	std::atomic<int> pruneCount{0};
	std::atomic<int> storedCount{0};
	std::atomic<int> storedMax{0};
	double portion = 100;

	std::set<TypeVariable, VariableCompare<TypeVariable> > typevars;
	typevars.insert(spec->getTypeVariables().begin(), spec->getTypeVariables().end());
	typevars.insert(sys->getTypeVariables().begin(), sys->getTypeVariables().end());
	typevars.insert(vf->getTypeVariables().begin(), vf->getTypeVariables().end());
	std::set<RelationVariable, VariableCompare<RelationVariable> > relvars;
	relvars.insert(spec->getRelationVariables().begin(), spec->getRelationVariables().end());
	relvars.insert(sys->getRelationVariables().begin(), sys->getRelationVariables().end());
	relvars.insert(vf->getRelationVariables().begin(), vf->getRelationVariables().end());
	std::set<AtomVariable, VariableCompare<AtomVariable> > atomvars;
	atomvars.insert(vf->getAtomVariables().begin(), vf->getAtomVariables().end());
	atomvars.insert(spec->getAtomVariables().begin(), spec->getAtomVariables().end());
	atomvars.insert(sys->getAtomVariables().begin(), sys->getAtomVariables().end());
	std::set<QuorumFunctionVariable, VariableCompare<QuorumFunctionVariable> > qfvars;
	qfvars.insert(vf->getQuorumFunctionVariables().begin(), vf->getQuorumFunctionVariables().end());
	qfvars.insert(spec->getQuorumFunctionVariables().begin(), spec->getQuorumFunctionVariables().end());
	qfvars.insert(sys->getQuorumFunctionVariables().begin(), sys->getQuorumFunctionVariables().end());

	portion = portion / (typevars.size() + relvars.size() + atomvars.size() + qfvars.size());

	ValuationFormula* msnvf = new TopValuationFormula();
	ValuationFormula* pvf = new TopValuationFormula();

	std::list<ValuationFormula*> conjuncts = vf->conjuncts();
	std::list<ValuationFormula*> relnegConjuncts = vf->conjuncts();
	std::list<ValuationFormula*>::iterator lit = relnegConjuncts.begin();
	while(lit != relnegConjuncts.end())
	{
		if((*lit)->isMembershipNegated())
		{
			++lit;
		}
		else
		{
			delete *lit;
			lit = relnegConjuncts.erase(lit);
		}

	}

//	std::cout << std::endl << "Cut-offs for the (size of the values of) type variables: " << mapToString<TypeVariable, int>(cutoffs) << std::unitbuf << std::endl << std::endl;

	std::set<Valuation*,PointerCompare<Valuation> >* vals = new std::set<Valuation*,PointerCompare<Valuation> >();
	vals->insert(new Valuation());
	valCount = 1;
	storedCount = 1;

	std::cout << "Computing the values of parameters..." << std::endl;

	vals = computeExtendedValuations(typevars, relvars, atomvars, qfvars, cutoffs, msnvf, pvf, conjuncts, relnegConjuncts, computed, portion, valCount, canCount, canReductionCount, pruneCount, storedCount, storedMax, vals, false, fixedtype);

	std::cout << "...Done!" << std::setw(70) << " " << std::endl;

	std::cout << "Found " << vals->size() << " non-isomorphic valuations." << std::endl;
	std::cout << "(#valuations: " << valCount << ", #canonical forms: " << canCount << ", max #valuations stored: " << storedMax << ", #isomorphs removed: " << canReductionCount << ", #branches pruned: " << pruneCount << ')' << std::endl << std::endl;

	return std::tuple<std::set<Valuation*,PointerCompare<Valuation> >*, int, int, int>(vals, valCount, canCount, storedMax);
}


std::set<Valuation*,PointerCompare<Valuation>>* computeExtendedValuations(std::set<TypeVariable, VariableCompare<TypeVariable> > typevars, std::set<RelationVariable, VariableCompare<RelationVariable> > relvars, std::set<AtomVariable, VariableCompare<AtomVariable> > atomvars, std::set<QuorumFunctionVariable, VariableCompare<QuorumFunctionVariable>> qfvars, const std::map<TypeVariable,int>& cutoffs, ValuationFormula* msnvf, ValuationFormula* pvf, std::list<ValuationFormula*> conjuncts, std::list<ValuationFormula*> relnegConjuncts, std::atomic<double>& completed, double portion, std::atomic<int>& valCount, std::atomic<int>& canCount, std::atomic<int>& canReductionCount, std::atomic<int>& pruneCount, std::atomic<int>& storedCount, std::atomic<int>& storedMax, std::set<Valuation*,PointerCompare<Valuation> >* vals, bool parallelised, TypeVariable* fixedtype)
{
	boolean varpicked = false;

	if(typevars.size() > 0 || relvars.size() > 0 || atomvars.size() > 0 || qfvars.size() > 0)
	{
		bool notaleaf = (typevars.size() + relvars.size() + atomvars.size() + qfvars.size() > 1);
		int avid = std::numeric_limits<int>::max();
		int rvid = std::numeric_limits<int>::max();
		int tvid = std::numeric_limits<int>::max();
		int qfvid = std::numeric_limits<int>::max();
		if(atomvars.size() > 0)
		{
			avid = atomvars.begin()->id;
		}
		if(relvars.size() > 0)
		{
			rvid = relvars.begin()->id;
		}
		if(typevars.size() > 0)
		{
			tvid = typevars.begin()->id;
		}
		if(qfvars.size() > 0)
		{
			qfvid = qfvars.begin()->id;
		}
		if(avid < std::min({tvid,rvid,qfvid}) || (avid == std::min({rvid,tvid,qfvid}) && atomvars.size() > 0))
		{
			AtomVariable av = *(atomvars.begin());
			TypeVariable tv = av.second;
			if(typevars.find(tv) ==  typevars.end())
			{
				//std::cout << "Computing values for atom variable " << av.id << " ..." << std::endl;

				atomvars.erase(atomvars.begin());
				std::list<ValuationFormula*>::iterator lit = relnegConjuncts.begin();
				while(lit != relnegConjuncts.end())
				{
					if(disjoint<TypeVariable, VariableCompare<TypeVariable> >(typevars, (*lit)->getTypeVariables()) && disjoint<RelationVariable, VariableCompare<RelationVariable> >(relvars, (*lit)->getRelationVariables()) && disjoint<AtomVariable, VariableCompare<AtomVariable> >(atomvars, (*lit)->getAtomVariables()) && disjoint<QuorumFunctionVariable, VariableCompare<QuorumFunctionVariable> >(qfvars, (*lit)->getQuorumFunctionVariables()))
					{
						msnvf = new ConjunctiveValuationFormula(msnvf, *lit);
						lit = relnegConjuncts.erase(lit);
					}
					else
					{
						++lit;
					}
				}
				msnvf->setNumericValuesToBoundVariables(avid+1);
				//std::cout << "Relation-negated valuation formula is now " << msnvf->toString() << std::endl;
				lit = conjuncts.begin();
				while(lit != conjuncts.end())
				{
					if(disjoint<TypeVariable, VariableCompare<TypeVariable> >(typevars, (*lit)->getTypeVariables()) && disjoint<RelationVariable, VariableCompare<RelationVariable> >(relvars, (*lit)->getRelationVariables()) && disjoint<AtomVariable, VariableCompare<AtomVariable> >(atomvars, (*lit)->getAtomVariables()) && disjoint<QuorumFunctionVariable, VariableCompare<QuorumFunctionVariable> >(qfvars, (*lit)->getQuorumFunctionVariables()))
					{
						pvf = new ConjunctiveValuationFormula(pvf, *lit);
						lit = conjuncts.erase(lit);
					}
					else
					{
						++lit;
					}
				}
				pvf->setNumericValuesToBoundVariables(avid+1);
				//std::cout << "Valuation formula is now " << pvf->toString() << std::endl;

				int count = 0;
				int total = vals->size();
				std::set<Valuation*,PointerCompare<Valuation> >* tempvals = new std::set<Valuation*,PointerCompare<Valuation> >();
				std::swap(tempvals, vals);
				for(std::set<Valuation*,PointerCompare<Valuation> >::iterator sit = tempvals->begin(); sit != tempvals->end(); sit++)
				{
					//std::cout << "   " << std::setw(3) << std::right << 100*count++/total << "%   #val:" << std::setw(12) << std::left << valCount << " #can:" << std::setw(12) << canCount << " #sto:" << std::setw(12) << storedCount << '\r';
					std::list<Valuation*> vals2 = (*sit)->addAtomVariable(av.second);
					delete *sit;
					valCount += vals2.size();
					updateStoredCount(storedCount, vals2.size()-1, storedMax);
					//printProgress(completed, portion*count++/total, valCount, canCount, storedCount);
					printProgress(completed, 0, valCount, canCount, storedCount);

					//std::cout << "   " << std::setw(3) << std::right << 100*count++/total << "%   #val:" << std::setw(12) << std::left << valCount << " #can:" << std::setw(12) << canCount << " #sto:" << std::setw(12) << storedCount << '\r';
					for(std::list<Valuation*>::iterator lit = vals2.begin(); lit != vals2.end(); ++lit)
					{
						if(pvf->instance(**lit))
						{
							(*lit)->computeCanonicalGraph();
							canCount++;
							std::set<Valuation*, PointerCompare<Valuation> >::iterator it = vals->find(*lit);
							if(it == vals->end())
							{
								vals->insert(it, *lit);
							}
							else
							{
								canReductionCount++;
								delete *lit;
								updateStoredCount(storedCount, -1, storedMax);
							}
						}
						else
						{
							if(notaleaf)
							{
								pruneCount++;
							}
							delete *lit;
							updateStoredCount(storedCount, -1, storedMax);
						}
					}
				}
				updateCompleted(completed, portion);
				delete tempvals;

				//std::cout << "...Done." << std::setw(60) << " " << std::endl;

				varpicked = true;
			}
		}

		if(!varpicked && (qfvid < std::min(rvid, tvid) || (qfvid == std::min(rvid, tvid) && qfvars.size() > 0)))
		{
			QuorumFunctionVariable qfv = *(qfvars.begin());
			bool typesadded = (typevars.find(std::get<1>(qfv)) == typevars.end());
			for(auto& type : std::get<2>(qfv))
			{
				typesadded = typesadded && (typevars.find(type) == typevars.end());
			}
			if(typesadded)
			{
				//std::cout << "Computing values for quorum function variable " << std::get<0>(qfv) << " ..." << std::endl;
				auto* tempvals = new std::set<Valuation*,PointerCompare<Valuation> >();
				int qsetcount = 1;
				std::swap(tempvals, vals);
				for(std::set<Valuation*,PointerCompare<Valuation> >::const_iterator sit = tempvals->begin(); sit != tempvals->end(); sit++)
				{
					Valuation* val = (*sit)->addQuorumFunctionVariable(std::get<1>(qfv), std::get<2>(qfv));
					delete *sit;
					valCount++;
					qsetcount = std::max(qsetcount, val->getGreatestNonemptySet());
					val->computeCanonicalGraph();
					canCount++;
					vals->insert(val);
				}
				delete tempvals;
				int greatestNonemptySet = qsetcount - 1;

				do
				{
					//std::cout << "Round " << greatestNonemptySet << "/" << qsetcount << std::endl;
					if(greatestNonemptySet == 0)
					{
						qfvars.erase(qfvars.begin());
						std::list<ValuationFormula*>::iterator lit = relnegConjuncts.begin();
						while(lit != relnegConjuncts.end())
						{
							if(disjoint<TypeVariable, VariableCompare<TypeVariable> >(typevars, (*lit)->getTypeVariables()) && disjoint<RelationVariable, VariableCompare<RelationVariable> >(relvars, (*lit)->getRelationVariables()) && disjoint<AtomVariable, VariableCompare<AtomVariable> >(atomvars, (*lit)->getAtomVariables()) && disjoint<QuorumFunctionVariable, VariableCompare<QuorumFunctionVariable> >(qfvars, (*lit)->getQuorumFunctionVariables()))
							{
								msnvf = new ConjunctiveValuationFormula(msnvf, *lit);
								lit = relnegConjuncts.erase(lit);
							}
							else
							{
								++lit;
							}
						}
						msnvf->setNumericValuesToBoundVariables((*(vals->begin()))->getAtomVariableCount());
						//std::cout << "Relation-negated valuation formula is now " << msnvf->toString() << std::endl;
						lit = conjuncts.begin();
						while(lit != conjuncts.end())
						{
							//(*lit)->setNumericValue(rv, relvarCount);
							if(disjoint<TypeVariable, VariableCompare<TypeVariable> >(typevars, (*lit)->getTypeVariables()) && disjoint<RelationVariable, VariableCompare<RelationVariable> >(relvars, (*lit)->getRelationVariables()) && disjoint<AtomVariable, VariableCompare<AtomVariable> >(atomvars, (*lit)->getAtomVariables()) && disjoint<QuorumFunctionVariable, VariableCompare<QuorumFunctionVariable> >(qfvars, (*lit)->getQuorumFunctionVariables()))
							{
								pvf = new ConjunctiveValuationFormula(pvf, *lit);
								lit = conjuncts.erase(lit);
							}
							else
							{
								++lit;
							}
						}
						pvf->setNumericValuesToBoundVariables((*(vals->begin()))->getAtomVariableCount());
						//std::cout << "Valuation formula is now " << pvf->toString() << std::endl;
					}

					int total = vals->size();
					int count = 0;
					tempvals = new std::set<Valuation*,PointerCompare<Valuation> >();
					std::swap(tempvals, vals);
					for(std::set<Valuation*,PointerCompare<Valuation> >::const_iterator sit = tempvals->begin(); sit != tempvals->end(); sit++)
					{
						std::list<Valuation*> vallist;
						int valCountIncrement = search(*sit, &vallist, true);
						valCount += valCountIncrement;
						updateStoredCount(storedCount, vallist.size(), storedMax);
						//printProgress(completed, portion * count++ / total / qsetcount, valCount, canCount, storedCount);
						printProgress(completed, 0, valCount, canCount, storedCount);

						vals->insert(*sit);
						for(std::list<Valuation*>::iterator lit = vallist.begin(); lit != vallist.end(); ++lit)
						{
							if(pvf->instance(**lit))
							{
								(*lit)->computeCanonicalGraph();
								canCount++;
								std::set<Valuation*, PointerCompare<Valuation> >::iterator it = vals->find(*lit);
								if(it == vals->end())
								{
									vals->insert(it, *lit);
								}
								else
								{
									canReductionCount++;
									delete *lit;
									updateStoredCount(storedCount, -1, storedMax);
								}
							}
							else
							{
								if(notaleaf)
								{
									pruneCount++;
								}
								delete *lit;
								updateStoredCount(storedCount, -1, storedMax);
							}
						}
					}
					updateCompleted(completed, portion / qsetcount);
					delete tempvals;
					greatestNonemptySet--;
				}
				while(greatestNonemptySet != -1);
				//std::cout << "...Done." << std::setw(60) << " " << std::endl;
				varpicked = true;
			}
		}

		if(!varpicked && (rvid < tvid || (rvid == tvid && relvars.size() > 0)))
		{
			RelationVariable rv = *(relvars.begin());
			bool typesadded = true;
			for(auto& tv : std::get<1>(rv))
			{
				if(typevars.find(tv) != typevars.end()) typesadded = false;
			}
			if(typesadded)
			{
				//std::cout << "Computing values for relation variable " << std::get<0>(rv) << " ..." << std::endl;

				relvars.erase(relvars.begin());
				//spec->setNumericValue(rv, relvarCount);
				//sys->setNumericValue(rv, relvarCount);
				std::list<ValuationFormula*>::iterator lit = relnegConjuncts.begin();
				while(lit != relnegConjuncts.end())
				{
					//(*lit)->setNumericValue(rv, relvarCount);
					if(disjoint<TypeVariable, VariableCompare<TypeVariable> >(typevars, (*lit)->getTypeVariables()) && disjoint<RelationVariable, VariableCompare<RelationVariable> >(relvars, (*lit)->getRelationVariables()) && disjoint<AtomVariable, VariableCompare<AtomVariable> >(atomvars, (*lit)->getAtomVariables()) && disjoint<QuorumFunctionVariable, VariableCompare<QuorumFunctionVariable> >(qfvars, (*lit)->getQuorumFunctionVariables()))
					{
						msnvf = new ConjunctiveValuationFormula(msnvf, *lit);
						lit = relnegConjuncts.erase(lit);
					}
					else
					{
						++lit;
					}
				}
				msnvf->setNumericValuesToBoundVariables((*(vals->begin()))->getAtomVariableCount());
				//std::cout << "Relation-negated valuation formula is now " << msnvf->toString() << std::endl;
				lit = conjuncts.begin();
				while(lit != conjuncts.end())
				{
					//(*lit)->setNumericValue(rv, relvarCount);
					if(disjoint<TypeVariable, VariableCompare<TypeVariable> >(typevars, (*lit)->getTypeVariables()) && disjoint<RelationVariable, VariableCompare<RelationVariable> >(relvars, (*lit)->getRelationVariables()) && disjoint<AtomVariable, VariableCompare<AtomVariable> >(atomvars, (*lit)->getAtomVariables()) && disjoint<QuorumFunctionVariable, VariableCompare<QuorumFunctionVariable> >(qfvars, (*lit)->getQuorumFunctionVariables()))
					{
						pvf = new ConjunctiveValuationFormula(pvf, *lit);
						lit = conjuncts.erase(lit);
					}
					else
					{
						++lit;
					}
				}
				pvf->setNumericValuesToBoundVariables((*(vals->begin()))->getAtomVariableCount());
				//std::cout << "Valuation formula is now " << pvf->toString() << std::endl;

				int count = 0;
				int total = vals->size();
				std::set<Valuation*,PointerCompare<Valuation> >* tempvals = new std::set<Valuation*,PointerCompare<Valuation> >();
				std::swap(tempvals, vals);
				for(std::set<Valuation*,PointerCompare<Valuation> >::const_iterator sit = tempvals->begin(); sit != tempvals->end(); sit++)
				{
					//std::cout << "   " << std::setw(3) << std::right << 100*count++/total << "%   #val:" << std::setw(12) << std::left << valCount << " #can:" << std::setw(12) << canCount << " #sto:" << std::setw(12) << storedCount << '\r';
					std::list<Valuation*> vallist;
					Valuation* initval = (*sit)->addRelationVariable(std::get<1>(rv));
					delete *sit;
					int valCountIncrement = search(initval, &vallist, msnvf, pruneCount);
					valCount += valCountIncrement;
					updateStoredCount(storedCount, vallist.size()-1, storedMax);
					//printProgress(completed, portion*count++/total, valCount, canCount, storedCount);
					printProgress(completed, 0, valCount, canCount, storedCount);

					//std::cout << "   " << std::setw(3) << std::right << 100*count++/total << "%   #val:" << std::setw(12) << std::left << valCount << " #can:" << std::setw(12) << canCount << " #sto:" << std::setw(12) << storedCount << '\r';
					for(std::list<Valuation*>::iterator lit = vallist.begin(); lit != vallist.end(); ++lit)
					{
						if(pvf->instance(**lit))
						{
							(*lit)->computeCanonicalGraph();
							canCount++;
							std::set<Valuation*, PointerCompare<Valuation> >::iterator it = vals->find(*lit);
							if(it == vals->end())
							{
								vals->insert(it, *lit);
							}
							else
							{
								canReductionCount++;
								delete *lit;
								updateStoredCount(storedCount, -1, storedMax);
							}
						}
						else
						{
							if(notaleaf)
							{
								pruneCount++;
							}
							delete *lit;
							updateStoredCount(storedCount, -1, storedMax);
						}
					}
				}
				updateCompleted(completed, portion);
				delete tempvals;
				//std::cout << "...Done." << std::setw(60) << " " << std::endl;

				varpicked = true;
			}
		}

		if(!varpicked && (typevars.size() > 0))
		{
			TypeVariable tv = *(typevars.begin());
			//std::cout << "Computing values for type variable " << tv << " ..." << std::endl;

			typevars.erase(typevars.begin());

			int count = 0;
			int total = vals->size();
			std::set<Valuation*,PointerCompare<Valuation> >* tempvals = new std::set<Valuation*,PointerCompare<Valuation> >();
			std::swap(tempvals, vals);
			for(std::set<Valuation*,PointerCompare<Valuation> >::iterator sit = tempvals->begin(); sit != tempvals->end(); sit++)
			{
				std::list<Valuation*> vals2{};
				int cutoff = cutoffs.find(tv)->second;
				if(fixedtype == nullptr)
				{
					vals2 = (*sit)->addTypeVariable(cutoff);
				}
				else if(*fixedtype == tv)
				{
					vals2 = (*sit)->addTypeVariable(cutoff, cutoff);
				}
				else
				{
					vals2 = (*sit)->addTypeVariable(cutoff);					
				}
				delete *sit;
				valCount += vals2.size();
				updateStoredCount(storedCount, vals2.size()-1, storedMax);
				printProgress(completed, 0, valCount, canCount, storedCount);
				//printProgress(completed, portion*count++/total, valCount, canCount, storedCount);
				vals->insert(vals2.begin(), vals2.end());
			}
			updateCompleted(completed, portion);
			delete tempvals;

			//std::cout << "...Done." << std::setw(60) << " " << std::endl;
			varpicked = true;
		}

		// int thread_count = 1;
		int thread_count = std::max(1, static_cast<int>(std::thread::hardware_concurrency()));
		int task_count = std::min(thread_count, static_cast<int>(vals->size()) / 2);
//		std::cout << "Thread count " << thread_count << std::endl;
		if(!parallelised && task_count > 1)
		{
//			std::cout << "about to start " << task_count << " threads\n";
			std::set<Valuation*,PointerCompare<Valuation> >* vals_part[task_count];
			int partindex;
			for(partindex = 0; partindex < task_count; partindex++)
			{
				vals_part[partindex] = new std::set<Valuation*,PointerCompare<Valuation> >();
			}
			partindex = 0;
			for(std::set<Valuation*,PointerCompare<Valuation> >::iterator sit = vals->begin(); sit != vals->end(); sit++)
			{
				vals_part[partindex]->insert(*sit);
				partindex = (partindex + 1) % task_count;
			}
			vals->clear();

			std::vector<std::future<std::set<Valuation*,PointerCompare<Valuation>>*>> vals_futures;
			for(partindex = 0; partindex < task_count; partindex++)
			{
/*				std::list<ValuationFormula*> newConjuncts = cloneConjuncts(conjuncts);
				std::list<ValuationFormula*> newRelnegConjuncts = cloneConjuncts(relnegConjuncts);
*/				auto vals_part_fut = std::async(std::launch::async, computeExtendedValuations, typevars, relvars, atomvars, qfvars, std::cref(cutoffs), msnvf->clone(), pvf->clone(), cloneConjuncts(conjuncts), cloneConjuncts(relnegConjuncts), std::ref(completed), portion/task_count, std::ref(valCount), std::ref(canCount), std::ref(canReductionCount), std::ref(pruneCount), std::ref(storedCount), std::ref(storedMax), vals_part[partindex], true, fixedtype);
				vals_futures.push_back(std::move(vals_part_fut));
			}
			deleteConjuncts(conjuncts);
			deleteConjuncts(relnegConjuncts);

			for(partindex = 0; partindex < task_count; partindex++)
			{
				vals_part[partindex] = vals_futures[partindex].get();
				for(std::set<Valuation*,PointerCompare<Valuation> >::iterator sit = vals_part[partindex]->begin(); sit != vals_part[partindex]->end(); sit++)
				{
					vals->insert(*sit);
				}
				delete vals_part[partindex];
			}
		}
		else if(vals->size() > 0)
		{
			vals = computeExtendedValuations(typevars, relvars, atomvars, qfvars, cutoffs, msnvf->clone(), pvf->clone(), conjuncts, relnegConjuncts, completed, portion, valCount, canCount, canReductionCount, pruneCount, storedCount, storedMax, vals, parallelised, fixedtype);
		}
	}

	delete msnvf;
	delete pvf;

	return vals;
}

void updateStoredCount(std::atomic<int>& storedCount, int storedCountIncrement, std::atomic<int>& storedMax)
{
	static std::mutex mtx;
	std::unique_lock<std::mutex> lock{mtx};

	storedCount += storedCountIncrement;
	if(storedCountIncrement > 0) if(storedCount > storedMax) storedMax.store(storedCount);
}

void updateCompleted(std::atomic<double>& completed, double increment)
{
	double completedbefore{completed};
	double completedafter = completed + increment;
	while(!completed.compare_exchange_weak(completedbefore, completedafter))
	{
		completedafter = completed + increment;
	}
}

void printProgress(std::atomic<double>& completed, double increment, std::atomic<int>& valCount, std::atomic<int>& canCount, std::atomic<int>& storedCount)
{
	static std::mutex mtx;
	std::unique_lock<std::mutex> lock{mtx};
	std::cout << "   " << std::setw(3) << std::right << static_cast<int>(completed + increment) << "%   #val:" << std::setw(12) << std::left << valCount << " #can:" << std::setw(12) << canCount << " #sto:" << std::setw(12) << storedCount << '\r';
}

int search(Valuation* val, std::list<Valuation*>* vals, const ValuationFormula* vf, std::atomic<int>& pruneCount)
{
	int iter = 1;
	if(vf->instance(*val))
	{
		vals->push_back(val);
		std::list<Valuation*> children = val->children();
		for(std::list<Valuation*>::iterator lit = children.begin(); lit != children.end(); ++lit)
		{
			iter+= search(*lit, vals, vf, pruneCount);
		}
	}
	else
	{
		pruneCount++;
		delete val;
	}
	return iter;
}

int search(Valuation* val, std::list<Valuation*>* vals, bool newIteration)
{
	int iter = 0;
	std::list<Valuation*> children = val->childrenQFV(newIteration);
	for(std::list<Valuation*>::iterator lit = children.begin(); lit != children.end(); ++lit)
	{
		iter++;
		vals->push_back(*lit);
		iter+= search(*lit, vals);
	}
	return iter;
}

std::list<ValuationFormula*> cloneConjuncts(const std::list<ValuationFormula*>& conjuncts)
{
	std::list<ValuationFormula*> newConjuncts = std::list<ValuationFormula*>();
	for(std::list<ValuationFormula*>::const_iterator clit = conjuncts.begin(); clit != conjuncts.end(); clit++)
	{
		newConjuncts.push_back((*clit)->clone());
	}
	return newConjuncts;
}

void deleteConjuncts(std::list<ValuationFormula*>& conjuncts)
{
	for(std::list<ValuationFormula*>::iterator clit = conjuncts.begin(); clit != conjuncts.end(); clit++)
	{
		delete *clit;
	}
	conjuncts.clear();
}

int quorumBound(int qfcount, int varcount)
{
	int bound0 = 0;
	if(qfcount == 1 || qfcount == 2)
	{
		bound0 = 1;
	}
	else if(qfcount >= 3)
	{
		bound0 = qfcount * (qfcount - 1) / 2;
		if(bound0 % 2 == 0) bound0--;
	}
	return bound0 + (varcount * (bound0 + 1));
}
