#ifndef VALUATION_H
#define VALUATION_H

#include "element.h"
#include <string>
#include <vector>
#include <list>
#include <set>
#include <utility>
#include <nautinv.h>
#include <map>

class ParameterList;

class Valuation
{
	int typeCount;
	int relvarCount;
	int avarCount;
	int qfvCount;
	std::vector<int> typeSizes;
	std::vector<std::vector<int> > relType;
	std::vector<std::pair<int, int> > relTypeCumSizes;
	std::vector<int> relTypeOffsets;
	std::vector<std::pair<int, std::vector<int>>> qfvType;
	std::vector<std::pair<int, int>> qfvTypeCumSizes;
	std::vector<int> avarType;
	std::vector<int> avarVal;
	std::vector<int> colourCount;
	std::vector<std::vector<int> > colours;
	int greatestNonemptyRelvar;
	int greatestNonemptyRow;
	int greatestNonemptyCol;
	int greatestNonemptySet;
	int greatestNonemptyAtom;
	graph *valg;
	graph *cang;
	int n;
	int m;
	Valuation(const Valuation&, const std::vector<int>&);
	Valuation(const Valuation&, int, std::vector<int>&);
	void initialiseGraphs();
	void initialiseRelationVariable(int, int = 0, int = 0, int = -1);
	int space(int, int = 0, int = -1) const;
	int typeSize(int, int = 0, int = -1) const;
	int type1Size(int) const;
	int type2Size(int) const;
	std::pair<int,int> coord(int, const std::vector<int>&, int = 0, int = -1) const;
	int pos(int, const std::vector<int>&, int, int) const;
	int typeOffset(int, int = 0, int = -1) const;
	int baseTypeOffset(int, int, int = 0, int = -1) const;
	bool isSquareType(int, int = 0, int = -1) const;
	std::vector<std::tuple<int,int,int>> offsets(int, int = 0, int = -1) const;
public:
	Valuation();
	//Valuation(const std::vector<int>&, const std::vector<std::pair<int,int> >&);
	Valuation(const Valuation&);
	Valuation& operator=(const Valuation&);
	~Valuation();
	std::list<Valuation*> children() const;
	std::list<Valuation*> childrenQFV(bool = false) const;
	std::list<Valuation*> addAtomVariable(const TypeVariable&, int = -1) const;
	Valuation* addRelationVariable(const std::vector<TypeVariable>&) const;
	Valuation* addQuorumFunctionVariable(const TypeVariable&, const std::vector<TypeVariable>&) const;
	std::list<Valuation*> addTypeVariable(int, int=1) const;
	AtomSet* getValue(const TypeVariable&) const;
	AtomSet* getValue(const std::set<TypeVariable>&) const;
	AtomTupleSet* getValue(const RelationVariable&) const;
	AtomTupleSet* getComplementValue(const RelationVariable&) const;
	std::vector<std::pair<AtomTuple,AtomSet>> getValue(const QuorumFunctionVariable&) const;
	Atom getValue(const AtomVariable&) const;
	AtomSet getValue(const QuorumFunctionVariable&, const ParameterList&) const;
	bool isMember(int, const std::vector<int>&) const;
	void setMember(int, const std::vector<int>&);
	bool isMember(int, int, const std::vector<int>&) const;
	bool areEqual(int, int) const;
	void computeCanonicalGraph();
	void clearRelationVariable(const RelationVariable&);
	int getTypeVariableCount() const;
	int getRelationVariableCount() const;
	int getAtomVariableCount() const;
	int getQuorumFunctionVariableCount() const;
	int getGreatestNonemptySet() const;
	bool operator<(const Valuation&) const;
	bool operator==(const Valuation&) const;
	bool almostEquals(const Valuation&) const;
	std::vector<int>& modinc(std::vector<int>&, const std::vector<int>&, int = 0, int = -1) const;
	int vec2int(const std::vector<int>&, const std::vector<int>&, int = 0, int = -1) const;
	std::vector<int> int2vec(int, const std::vector<int>&) const;
	//std::string toString(const std::set<TypeVariable>&, const std::set<RelationVariable>&, const std::set<AtomVariable>&) const;
};

#endif
