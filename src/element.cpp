#include "element.h"
#include "valuation.h"
#include "context.h"
#include <assert.h>
#include <algorithm>

bool operator<(const std::string& s1, const std::string& s2)
{
//	std::cout << s1 << " less than " << s2 << std::endl;
	int b = s1.compare(s2);
	return b < 0;
}

bool operator==(const std::string& s1, const std::string& s2)
{
//	std::cout << "equal to\n";
	return s1.compare(s2) == 0;
}

bool operator!=(const std::string& s1, const std::string& s2)
{
//	std::cout << "not equal to\n";
	return s1.compare(s2) != 0;
}

AtomSet::AtomSet(int as_id) : std::set<Atom>(), id(as_id)
{
}

AtomSet::AtomSet(const AtomSet& as) : std::set<Atom>(as), id(as.id)
{
}

bool AtomSet::isElement(const Atom& a) const
{
	return (std::set<Atom>::find(const_cast<Atom&>(a)) != end());
}

std::string AtomSet::toString(const Context& c) const
{
	AtomSet::const_iterator cit = begin();
	std::stringstream ss;
	ss << '{';
	if(cit != end())
	{
		ss << c.atomToString(*cit);
		++cit;
	}
	for(; cit != end(); ++cit)
	{
		ss << ',' << c.atomToString(*cit);
	}
	ss << '}';
	return ss.str();
}

TypeVariable::TypeVariable(int type_id, int val_id, bool type) : Domain(type_id,type), id(val_id)
{
}

TypeVariable::TypeVariable(const TypeVariable& t) : Domain(t), id(t.id)
{
}

void TypeVariable::setNumericValue(VarType vt, int type_id, int var_id)
{
	if(vt == second && type_id == first) id=var_id;
}

const std::string& TypeVariable::toString(const Context& c) const
{
	return c.typeVariableToString(*this);
}

std::pair<z3::sort,z3::func_decl_vector> TypeVariable::toZ3Sort(z3::context& c, const std::map<TypeVariable,int>& cutoffs) const
{
	z3::func_decl_vector enum_consts{c};
	auto it = cutoffs.find(*this);
	if(second == TYPEVAR && it == cutoffs.end())
	{
		z3::sort s = c.uninterpreted_sort(("T" + std::to_string(first)).c_str());
	   	return std::make_pair(s, enum_consts);
	}

	std::string type_name{};
	int size = -1;
	if(second == TYPEVAR)
	{
		size = it->second;
		type_name = "T" + std::to_string(first) + "S" + std::to_string(size);
	}
	else
	{
		size = id;			
		type_name = "F" + std::to_string(first);
	}

	assert(size > 0);

	std::vector<std::string> enum_strnames{};
	enum_strnames.reserve(size);
	const char* enum_names[size];
	for(int i = 0; i < size; i++)
	{
		enum_strnames.push_back(type_name + "-" + std::to_string(i));
		enum_names[i] = enum_strnames[i].c_str();
	}
   	z3::func_decl_vector enum_testers{c};
   	z3::sort s = c.enumeration_sort(type_name.c_str(), size, enum_names, enum_consts, enum_testers);
   	return std::make_pair(s, enum_consts);
}

AtomVariable::AtomVariable(int av_id, const TypeVariable& t, int var_id) : std::pair<int,TypeVariable>(av_id,t), id(var_id)
{
}

AtomVariable::AtomVariable(const AtomVariable& a) : std::pair<int,TypeVariable>(a), id(a.id)
{
}


void AtomVariable::setNumericValue(VarType vt, int av_id, int var_id)
{
	if(vt == ATOMVAR && av_id == first) id=var_id;
	second.setNumericValue(vt,av_id,var_id);
}

AtomVariable AtomVariable::getPrimedVersion() const
{
	AtomVariable pv(*this);
	pv.first = -pv.first-1;
	return pv;
}

const std::string& AtomVariable::toString(const Context& c) const
{
	return c.atomVariableToString(*this);
}

z3::expr AtomVariable::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	return toZ3Expr(c, "I" + std::to_string(inst_num), cutoffs);
}

z3::expr AtomVariable::toZ3Expr(z3::context& c, const std::string& suffix, const std::map<TypeVariable,int>& cutoffs) const
{
	auto z3type = std::get<0>(second.toZ3Sort(c, cutoffs));
	z3::expr z3var = c.constant(("V" + std::to_string(first) + suffix).c_str(), z3type);
	return z3var;
}

RelationVariable::RelationVariable(int rv_id, const TypeVariable& d1, const TypeVariable& d2, int var_id) : std::tuple<int,std::vector<TypeVariable>>(rv_id, {d1, d2}), id(var_id)
{
}

RelationVariable::RelationVariable(int rv_id, const std::vector<TypeVariable>& dom, int var_id) : std::tuple<int,std::vector<TypeVariable>>(rv_id, dom), id(var_id)
{
}

RelationVariable::RelationVariable(const RelationVariable& rv) : std::tuple<int,std::vector<TypeVariable>>(rv), id(rv.id)
{
}

const std::string& RelationVariable::toString(const Context& c) const
{
	return c.relationVariableToString(*this);
}

void RelationVariable::setNumericValue(VarType vt, int rv_id, int var_id)
{
	if(vt == RELVAR && rv_id == std::get<0>(*this)) id=var_id;
	for(auto& t : std::get<1>(*this))
	{
		t.setNumericValue(vt,rv_id,var_id);
	}
}

z3::func_decl RelationVariable::toZ3Func(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	z3::sort_vector types{c};
	for(auto& t : std::get<1>(*this))
	{
		types.push_back(std::get<0>(t.toZ3Sort(c, cutoffs)));
	}
	z3::sort z3bool = c.bool_sort();
	return c.function(("P" + std::to_string(std::get<0>(*this)) + "I" + std::to_string(inst_num)).c_str(), types, z3bool);
}

QuorumFunctionVariable::QuorumFunctionVariable(int qf_id, const TypeVariable& t, const std::vector<TypeVariable>& d, int var_id) : std::tuple<int,TypeVariable,std::vector<TypeVariable>>(qf_id, t, d), id(var_id)
{
}

void QuorumFunctionVariable::setNumericValue(VarType vt, int qf_id, int var_id)
{
	if(vt == QFVAR && qf_id == std::get<0>(*this)) id = var_id;
	std::get<1>(*this).setNumericValue(vt, qf_id, var_id);
	for(auto& d : std::get<2>(*this))
	{
		d.setNumericValue(vt, qf_id, var_id);
	}
}

const std::string& QuorumFunctionVariable::toString(const Context& c) const
{
	return c.quorumFunctionVariableToString(*this);
}

z3::expr Atom::toZ3Expr(z3::context& c, const std::map<TypeVariable,int>& cutoffs) const
{
	TypeVariable tv{second.first, -1, second.second};
	auto it = cutoffs.find(tv);

	assert(it != cutoffs.end());

	tv.id = it->second;
	auto z3sort = tv.toZ3Sort(c, cutoffs);
	return std::get<1>(z3sort)[first]();
}

AtomSet AtomTuple::getAtoms() const
{
	AtomSet atoms;
	atoms.insert(begin(), end());
	return atoms;
}

std::string AtomTuple::toString(const Context& c, char first, char last, char delimiter) const
{
	std::string str;
	std::vector<Atom>::const_iterator cit = begin();
	if(cit != end())
	{
		str.append(c.atomToString(*cit));
		++cit;
	}
	for(; cit != end(); ++cit)
	{
		str.push_back(delimiter);
		str.append(c.atomToString(*cit));
	}
	return str;
}

AtomRelation::AtomRelation(const std::vector<Domain>& t, const std::vector<int>& s) : std::vector<bool>(), types(t), typesizes(s), cumsizes(std::vector<int>())
{
	assert(t.size() == s.size());

	int prod = 1;
	for(int i = 0; i < typesizes.size(); i++)
	{
		cumsizes.push_back(prod);
		prod = prod * typesizes[i];
	}
	resize(prod,false);
}

AtomRelation::AtomRelation(const AtomRelation& ar) : std::vector<bool>(ar), types(ar.types), typesizes(ar.typesizes), cumsizes(ar.cumsizes)
{
}

bool AtomRelation::operator==(const AtomRelation& ar)
{
	if(types != ar.types) return false;
	if(typesizes != ar.typesizes) return false;
	if(cumsizes != ar.cumsizes) return false;
	return (static_cast<const std::vector<bool>&>(*this) == static_cast<const std::vector<bool>&>(ar));
}

void AtomRelation::plus(const AtomRelation& ar)
{
	assert(size() == ar.size());

	for(int i = 0; i < ar.size(); i++)
	{
		if(ar[i]) operator[](i) = true;
	}
}

void AtomRelation::diff(const AtomRelation& ar)
{
	assert(size() == ar.size());

	for(int i = 0; i < ar.size(); i++)
	{
		if(ar[i]) operator[](i) = false;
	}
}

void AtomRelation::insert(const AtomTuple& at)
{
	assert(at.size() == types.size());

	int pos = 0;
	for(int i = 0; i < at.size(); i++)
	{
		pos = pos + (at[i].first)*cumsizes[i];
	}

	assert(pos < size());

	operator[](pos) = true;
}

bool AtomRelation::isElement(const AtomTuple& at) const
{
	assert(at.size() == types.size());

	int pos = 0;
	for(int i = 0; i < at.size(); i++)
	{
		pos = pos + (at[i].first)*cumsizes[i];
	}

	assert(pos < size());

	return operator[](pos);
}

Atom AtomRelation::operator()(const AtomTuple& at) const
{
	assert(types.size() == at.size() + 1);

	Atom a(-1,Domain(-1,FINTYPE));
	int pos = 0;
	int i = 0;
	for(; i < at.size(); i++)
	{
		pos = pos + at[i].first*cumsizes[i];
	}
	for(int j = 0; j < typesizes[i]; j++)
	{
		if(operator[](pos + j*cumsizes[i]))
		{
			if(a == Atom(-1,Domain(-1,FINTYPE)))
			{
				a = Atom(j,types[i]);
			}
			else
			{
				return Atom(-1,Domain(-1,FINTYPE));
			}
		}
	}
	return a;
}

bool AtomRelation::isEquallyTyped(const AtomRelation& ar) const
{
	if(types.size() != ar.types.size()) return false;
	for(int i = 0; i < types.size(); i++)
	{
		if(types[i] != ar.types[i]) return false;
	}
	return true;
}

bool AtomRelation::isEquallyTyped(const AtomTuple& at) const
{
	if(types.size() != at.size()) return false;
	for(int i = 0; i < types.size(); i++)
	{
		if(types[i] != at[i].second) return false;
	}
	return true;
}

bool AtomRelation::isEquallyTyped(const AtomSchemaTuple& at) const
{
	if(types.size() != at.size()) return false;
	for(int i = 0; i < types.size(); i++)
	{
		if(types[i] != at.operator[](i)->getType()) return false;
	}
	return true;
}

AtomTuple AtomRelation::get(int pos) const
{
	assert(pos < size());

	AtomTuple at;
	for(int i = 0; i < typesizes.size(); i++)
	{
		at.push_back(Atom(pos % typesizes[i], types[i]));
		pos = pos / typesizes[i];
	}
	return at;
}

const Domain& AtomRelation::getFunctionType() const
{
	assert(types.size() > 0);
	return types[types.size()-1];
}

int AtomRelation::getDimension() const
{
	return types.size();
}

std::string AtomRelation::toString(const Context& c) const
{
	int isnotfirst = false;
	std::stringstream ss;
	ss << '{';
	for(int i = 0; i < size(); i++)
	{
		if(operator[](i))
		{
			if(isnotfirst) ss << ',';
			ss << '(' << get(i).toString(c) << ')';
			isnotfirst = true;
		}
	}
	ss << '}';
	return ss.str();
}

std::string AtomTupleSet::toString(const Context& c) const
{
	AtomTupleSet::const_iterator cit = begin();
	std::stringstream ss;
	ss << '{';
	if(cit != end())
	{
		ss << '(' << cit->toString(c) << ')';
		++cit;
	}
	for(; cit != end(); ++cit)
	{
		ss << ',' << '(' << cit->toString(c) << ')';
	}
	ss << '}';
	return ss.str();
}

Action::Action(Channel c, bool i) : AtomTuple(), chan(c), isInput(i)
{
}

Action::Action(const Action& act) : AtomTuple(act), chan(act.chan), isInput(act.isInput)
{
}

std::string Action::toString(const Context& c, char first, char last, char delimiter, char outputmark, char inputmark) const
{
	std::string str;
	if(isInput == true && inputmark != ' ')
	{
		str.push_back(inputmark);
	}
	else if(isInput == false && outputmark != ' ')
	{
		str.push_back(outputmark);
	}
	str.append(c.channelToString(chan));
	str.push_back(first);
	str.append(AtomTuple::toString(c,first,last,delimiter));
	str.push_back(last);
	return str;
}

bool Action::operator<(const Action& act2) const
{
//	if(isInput != act2.isInput) return act2.isInput;
	if(chan < act2.chan) return true;
	if(act2.chan < chan) return false;
	if(size() != act2.size()) return (size() < act2.size());
	for(int pos = 0; pos != size(); pos++)
	{
		if(operator[](pos) != act2[pos]) return (operator[](pos) < act2[pos]);
	}
	return false;
}

bool Action::operator==(const Action& act2) const
{
//	if(isInput != act2.isInput) return false;
	if(chan != act2.chan) return false;
	if(size() != act2.size()) return false;
	for(int pos = 0; pos < size(); pos++)
	{
		if(operator[](pos) != act2[pos]) return false;
	}
	return true;
}

bool Action::operator!=(const Action& act2) const
{
//	if(isInput != act2.isInput) return true;
	if(chan != act2.chan) return true;
	if(size() != act2.size()) return true;
	for(int pos = 0; pos < size(); pos++)
	{
		if(operator[](pos) != act2[pos]) return true;
	}
	return false;
}

State::State(const ControlState& cs) : ControlState(cs), AtomTuple()
{
}

State::State(const State& s) : ControlState(s), AtomTuple(s)
{
}

std::string State::toString(const Context& c, char first, char last, char delimiter) const
{
	std::string str(c.controlStateToString(*this));
	str.push_back(first);
	str.append(AtomTuple::toString(c,first,last,delimiter));
	str.push_back(last);
	return str;
}

bool operator<(const State& act1, const State& act2)
{
	if(static_cast<const ControlState&>(act1) < static_cast<const ControlState&>(act2)) return true;
	if(static_cast<const ControlState&>(act2) < static_cast<const ControlState&>(act1)) return false;
	if(act1.size() != act2.size()) return (act1.size() < act2.size());
	for(int pos = 0; pos != act1.size(); pos++)
	{
		if(act1[pos] != act2[pos]) return (act1[pos] < act2[pos]);
	}
	return false;
}

bool operator==(const State& act1, const State& act2)
{
	if(static_cast<const ControlState&>(act1) != static_cast<const ControlState&>(act2)) return false;
	if(act1.size() != act2.size()) return false;
	for(int pos = 0; pos < act1.size(); pos++)
	{
		if(act1[pos] != act2[pos]) return false;
	}
	return true;
}

bool operator!=(const State& act1, const State& act2)
{
	if(static_cast<const ControlState&>(act1) != static_cast<const ControlState&>(act2)) return true;
	if(act1.size() != act2.size()) return true;
	for(int pos = 0; pos < act1.size(); pos++)
	{
		if(act1[pos] != act2[pos]) return true;
	}
	return false;
}

std::string StateSet::toString(const Context& c) const
{
	StateSet::const_iterator cit = begin();
	std::stringstream ss;
	ss << '{';
	if(cit != end())
	{
		ss << cit->toString(c);
		++cit;
	}
	for(; cit != end(); ++cit)
	{
		ss << ',' << cit->toString(c);
	}
	ss << '}';
	return ss.str();
}

ActionSet::ActionSet(int as_id) : std::set<Action>(), id(as_id)
{
}

ActionSet::ActionSet(const ActionSet& as) : std::set<Action>(as), id(as.id)
{
}

Atom ActionSet::operator()(const Action& a1) const
{
	for(ActionSet::const_iterator cit = begin(); cit != end(); ++cit)
	{
		if(a1.size() + 1  == cit->size())
		{
			int i = 0;
			while(i < a1.size() && a1[i] == cit->operator[](i))
			{
				i++;
			}
			if(i == a1.size())
			{
				return cit->operator[](i);
			}
		}
	}

	return Atom(-1,Domain(-1,false));
}

bool ActionSet::operator()(const Action& a1, const Atom& a2) const
{
	Action act(a1);
	act.push_back(a2);
	return (std::set<Action>::find(act) != std::set<Action>::end());
}

bool ActionSet::isElement(const Action& a) const
{
	return (std::set<Action>::find(a) != std::set<Action>::end());
}

std::set<Channel> ActionSet::getInputChannels() const
{
	std::set<Channel> ichans;
	for(ActionSet::const_iterator cit = begin(); cit != end(); cit++)
	{
		if(cit->isInput && cit->chan != 0) ichans.insert(cit->chan);
	}
	return ichans;
}

std::set<Channel> ActionSet::getOutputChannels() const
{
	std::set<Channel> ochans;
	for(ActionSet::const_iterator cit = begin(); cit != end(); cit++)
	{
		if(!cit->isInput && cit->chan != 0) ochans.insert(cit->chan);
	}
	return ochans;
}

AtomSet ActionSet::getAtoms() const
{
	AtomSet atoms;
	for(ActionSet::const_iterator cit = begin(); cit != end(); cit++)
	{
		atoms.insert(cit->begin(), cit->end());
	}
	return atoms;
}

std::string ActionSet::toString(const Context& c) const
{
	ActionSet::const_iterator cit = begin();
	std::stringstream ss;
	ss << '{';
	if(cit != end())
	{
		ss << cit->toString(c);
		++cit;
	}
	for(; cit != end(); ++cit)
	{
		ss << ',' << cit->toString(c);
	}
	ss << '}';
	return ss.str();
}

Transition::Transition(const State& from, const Action& act, const State& to, bool must) : triplet<State,Action,State>(from,act,to), isMust(must)
{
}

Transition::Transition(const Transition& t) : triplet<State,Action,State>(t.first,t.second,t.third), isMust(t.isMust)
{
}

std::string Transition::toString(const Context& c) const
{
	std::string s;
	s.push_back('(');
	s.append(first.toString(c));
	s.push_back(',');
	s.append(second.toString(c));
	s.push_back(',');
	s.append(third.toString(c));
	s.push_back(')');
	return s;
}

AtomSet Transition::getAtoms() const
{
	AtomSet atoms = first.getAtoms();
	AtomSet atoms2 = second.getAtoms();
	atoms.insert(atoms2.begin(), atoms2.end());
	atoms2 = third.getAtoms();
	atoms.insert(atoms2.begin(), atoms2.end());
	return atoms;
}

StateSet TransitionSet::getStateSet() const
{
	StateSet ss;
	for(std::vector<Transition>::const_iterator cit = begin(); cit != end(); cit++)
	{
		ss.insert(cit->first);
		ss.insert(cit->third);
	}
	return ss;
}

ActionSet TransitionSet::getActionSet() const
{
	ActionSet as;
	for(std::vector<Transition>::const_iterator cit = begin(); cit != end(); cit++)
	{
		const Action& act = cit->second;
		if(act.chan != 0) as.insert(act);
	}
	return as;
}

ActionSet TransitionSet::getOutputActionSet() const
{
	ActionSet as;
	for(std::vector<Transition>::const_iterator cit = begin(); cit != end(); cit++)
	{
		const Action& act = cit->second;
		if(act.chan != 0 && !act.isInput) as.insert(act);
	}
	return as;
}

ActionSet TransitionSet::getInputActionSet() const
{
	ActionSet as;
	for(std::vector<Transition>::const_iterator cit = begin(); cit != end(); cit++)
	{
		const Action& act = cit->second;
		if(act.chan != 0 && act.isInput) as.insert(act);
	}
	return as;
}

std::string TransitionSet::toString(const Context& c) const
{
	TransitionSet::const_iterator cit = begin();
	std::stringstream ss;
	ss << '{';
	if(cit != end())
	{
		ss << cit->toString(c);
		++cit;
	}
	for(; cit != end(); ++cit)
	{
		ss << ',' << cit->toString(c);
	}
	ss << '}';
	return ss.str();
}

bool TransitionSet::insert(const Transition& t)
{
	int first = 0;
	int last = size();
	if(last == 0)
	{
		push_back(t);
		return true;
	}
	while(last - first > 1)
	{
		int middle = first + ((last - first) / 2);
		if(t < operator[](middle))
		{
			last = middle;
		}
		else
		{
			first = middle;
		}
	}
	if(t < operator[](first))
	{
		std::vector<Transition>::insert(std::vector<Transition>::begin() + first, t);
		return true;
	}
	else if(operator[](first) < t)
	{
		std::vector<Transition>::insert(std::vector<Transition>::begin() + last, t);
		return true;
	}
	return false;
}

void TransitionSet::insert(const TransitionSet::const_iterator& first, const TransitionSet::const_iterator& last)
{
	int middle = size();
	std::vector<Transition>::insert(end(), first, last);
	std::inplace_merge(std::vector<Transition>::begin(), std::vector<Transition>::begin()+middle, std::vector<Transition>::end());
	std::vector<Transition>::iterator it = std::unique(std::vector<Transition>::begin(), std::vector<Transition>::end());
	if(size() != 0) resize(it - std::vector<Transition>::begin(), *begin());
}

bool TransitionSet::isElement(const Transition& t) const
{
	return std::binary_search(begin(), end(), t);
}

std::pair<TransitionSet::const_iterator, TransitionSet::const_iterator> TransitionSet::getSuccessors(const State& st) const
{
	Transition tr(st,Action(),st);
	return std::equal_range(std::vector<Transition>::begin(), std::vector<Transition>::end(), tr, FirstComponentCompare<Transition>());
}

void TransitionSet::sortByAction()
{
	std::sort(std::vector<Transition>::begin(), std::vector<Transition>::end(), SecondComponentCompare<Transition>());
}

std::pair<TransitionSet::const_iterator, TransitionSet::const_iterator> TransitionSet::getTransitions(const Action& act) const
{
	Transition tr(State(ControlState(0,0)),act,State(ControlState(0,0)));
	return std::equal_range(std::vector<Transition>::begin(), std::vector<Transition>::end(), tr, SecondComponentCompare<Transition>());
}

int TransitionSet::width() const
{
	int w = 0;
	for(std::vector<Transition>::const_iterator cit = begin(); cit != end(); cit++)
	{
		w = std::max(w,(int)(cit->first.size() + cit->second.size() + cit->third.size()));
	}
	return w;
}

int TransitionSet::size() const
{
	return std::vector<Transition>::size();
}

void TransitionSet::clear()
{
	std::vector<Transition>::clear();
}

TransitionSet::const_iterator::const_iterator(const std::vector<Transition>::const_iterator& it) : std::vector<Transition>::const_iterator(it)
{
}

TransitionSet::const_iterator::const_iterator(const TransitionSet::iterator& it) : std::vector<Transition>::const_iterator(it)
{
}

TransitionSet::iterator::iterator(const std::vector<Transition>::iterator& it) : std::vector<Transition>::iterator(it)
{
}

TransitionSet::const_iterator TransitionSet::begin() const
{
	return TransitionSet::const_iterator(std::vector<Transition>::begin());
}

TransitionSet::iterator TransitionSet::begin()
{
	return TransitionSet::iterator(std::vector<Transition>::begin());
}

TransitionSet::const_iterator TransitionSet::end() const
{
	return TransitionSet::const_iterator(std::vector<Transition>::end());
}

TransitionSet::iterator TransitionSet::end()
{
	return TransitionSet::iterator(std::vector<Transition>::end());
}
