#ifndef TIMER_H
#define TIMER_H

#include <sys/time.h>

class Timer
{
	int timerCount;
	int activeTimer;
	long unsigned int* timers;
	struct timeval startTime;
	struct timeval switchTime;
	struct timeval stopTime;
	bool running;
public:
	Timer(int=1);
	~Timer();
	void start();
	void stop();
	int switchToTimer(int);
	double getValue(int) const;
	double getTotalTime() const;
};

#endif