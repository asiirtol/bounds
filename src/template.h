#ifndef TEMPLATE_H
#define TEMPLATE_H

#include <string>
#include <utility>
#include <iostream>
#include <set>
#include <map>
#include <sstream>

template<class T1, class T2, class T3> class triplet
{
public:
	T1 first;
	T2 second;
	T3 third;
	triplet(const T1& f, const T2& s, const T3& t) : first(f), second(s), third(t) {}
	template<class U1, class U2, class U3> triplet(const triplet<U1,U2,U3>& t) : first(t.first), second(t.second), third(t.third) {}
	bool operator<(const triplet t2) const
	{
		if(first != t2.first) return (first < t2.first);
		if(second != t2.second)	return (second < t2.second);
		return third < t2.third;
	}
	bool operator==(const triplet t2) const
	{
		return (first == t2.first && second == t2.second && third == t2.third);
	}
};

template<class T1, class T2, class T3, class T4> class quartet
{
public:
	T1 first;
	T2 second;
	T3 third;
	T4 fourth;
	quartet(const T1& f, const T2& s, const T3& t, const T4& fth) : first(f), second(s), third(t), fourth(fth) {}
	template<class U1, class U2, class U3, class U4> quartet(const quartet<U1,U2,U3,U4>& q) : first(q.first), second(q.second), third(q.third), fourth(q.fourth) {}
};

template<class T1, class T2, class T3, class T4, class T5> class quintet
{
public:
	T1 first;
	T2 second;
	T3 third;
	T4 fourth;
	T5 fifth;
	quintet(const T1& f, const T2& s, const T3& t, const T4& f4th, const T5& f5th) : first(f), second(s), third(t), fourth(f4th), fifth(f5th) {}
	template<class U1, class U2, class U3, class U4, class U5> quintet(const quintet<U1,U2,U3,U4,U5>& q) : first(q.first), second(q.second), third(q.third), fourth(q.fourth), fifth(q.fifth) {}
};

template<class T> class PointerCompare
{
public:
	bool operator()(const T* v1, const T* v2) const
	{
		return *v1  < *v2;
	}
};

template<class T> class PointerCompareAgainst
{
	const T* v2;
public:
	PointerCompareAgainst(const T* v) : v2(v) {}
	bool operator()(const T* v1) const
	{
		return *v1  < *v2;
	}
};

template<class T> class FirstComponentCompare
{
public:
	bool operator()(const T& v1, const T& v2) const
	{
		return v1.first  < v2.first;
	}
};

template<class T> class SecondComponentCompare
{
public:
	bool operator()(const T& v1, const T& v2) const
	{
		return v1.second  < v2.second;
	}
};

template<class T, class U> class PointerPairCompare
{
public:
	bool operator()(const typename std::pair<T*,U*>& p1, const typename std::pair<T*,U*>& p2) const
	{
		if(*(p1.first) < *(p2.first)) return true;
		if(*(p2.first) < *(p1.first)) return false;
		return *(p1.second) < *(p2.second);
	}
};

template<class T1, class T2, class T3> class PointerTripletCompare
{
public:
	bool operator()(const triplet<T1*,T2*,T3*>& p1, const triplet<T1*,T2*,T3*>& p2)
	{
		if(*(p1.first) != *(p2.first)) return (*(p1.first) < *(p2.first));
		if(*(p1.second) != *(p2.second)) return (*(p1.second) < *(p2.second));
		return *(p1.third) < *(p2.third);
	}
};

template<class T> class VariableCompare
{
public:
	bool operator()(const T& v1, const T& v2)
	{
		if(v1.id != v2.id) return (v1.id < v2.id);
		return std::get<0>(v1) < std::get<0>(v2);
	}
};

template<class T1, class T2> std::ostream& operator<<(std::ostream& os, const std::pair<T1,T2>& p)
{
	return (os << '(' << p.first << ',' << p.second << ')');
};

template<class T> std::string setToString(const std::set<T, std::less<T> >& s)
{
	typename std::set<T, std::less<T> >::const_iterator cit = s.begin();
	std::stringstream ss;
	ss << '{';
	if(cit != s.end())
	{
		ss << *cit;
		++cit;
	}
	for(; cit != s.end(); ++cit)
	{
		ss << ',' << *cit;
	}
	ss << '}';
	return ss.str();
};

template<class T, class C> std::string setToString(const std::set<T, C>& s)
{
	typename std::set<T, C>::const_iterator cit = s.begin();
	std::stringstream ss;
	ss << '{';
	if(cit != s.end())
	{
		ss << *cit;
		++cit;
	}
	for(; cit != s.end(); ++cit)
	{
		ss << ',' << *cit;
	}
	ss << '}';
	return ss.str();
};

template<class T> std::ostream& operator<<(std::ostream& os, const std::set<T>& s)
{
	return os << setToString<T>(s);
};

template<class T, class U> std::string mapToString(const std::map<T,U>& m)
{
	typename std::map<T,U>::const_iterator cit = m.begin();
	std::stringstream ss;
	if(cit != m.end())
	{
		ss << cit->first << '-' << '>' << cit->second;
		++cit;
	}
	for(; cit != m.end(); ++cit)
	{
		ss << ',' << cit->first << '-' << '>' << cit->second;
	}
	return ss.str();
};

template<class T, class U> std::ostream& operator<<(std::ostream& os, const std::map<T,U>& m)
{
	return os << mapToString<T>(m);
};

template<class T, class C1> bool disjoint(const std::set<T,C1>& l1, const std::set<T>& l2)
{
	bool aredisjoint = true;
	typename std::set<T,C1>::const_iterator cit = l1.begin();
	while(cit != l1.end())
	{
		if(l2.find(*cit) != l2.end())
		{
			aredisjoint = false;
			break;
		}
		cit++;
	}
	return aredisjoint;
};

template<class T> bool disjoint(const std::set<T>& l1, const std::set<T>& l2)
{
	bool aredisjoint = true;
	typename std::set<T>::const_iterator cit = l1.begin();
	while(cit != l1.end())
	{
		if(l2.find(*cit) != l2.end())
		{
			aredisjoint = false;
			break;
		}
		cit++;
	}
	return aredisjoint;
};

template<class T> bool subset(const std::set<T>& l1, const std::set<T>& l2)
{
	bool issubset = true;
	typename std::set<T>::const_iterator cit = l1.begin();
	while(cit != l1.end())
	{
		if(l2.find(*cit) == l2.end())
		{
			issubset = false;
			break;
		}
		cit++;
	}
	return issubset;
};

template<class T> int disjointCount(const std::set<T>& l1, const std::set<T>& l2)
{
	int count = 0;
	typename std::set<T>::const_iterator cit = l1.begin();
	while(cit != l1.end())
	{
		if(l2.find(*cit) == l2.end())
		{
			count++;
		}
		cit++;
	}
	return count;
};

template<class T> int jointCount(const std::set<T>& l1, const std::set<T>& l2)
{
	int count = 0;
	typename std::set<T>::const_iterator cit = l1.begin();
	while(cit != l1.end())
	{
		if(l2.find(*cit) != l2.end())
		{
			count++;
		}
		cit++;
	}
	return count;
};

#endif
