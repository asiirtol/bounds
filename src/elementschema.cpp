#include <algorithm>
#include "elementschema.h"
#include <assert.h>
#include "valuationformula.h"
#include "context.h"
#include "template.h"

AtomSchema::AtomSchema() : atomvars(std::set<AtomVariable>()), types(std::set<TypeVariable>())
{
}

AtomSchema::AtomSchema(const AtomSchema& as) : atomvars(as.atomvars), types(as.types)
{
}

AtomSchema::~AtomSchema()
{
}

const std::set<AtomVariable>& AtomSchema::getAtomVariables() const
{
	return atomvars;
}

const std::set<TypeVariable>& AtomSchema::getTypeVariables() const
{
	return types;
}

ElementaryAtomSchema::ElementaryAtomSchema(Atom* a) : AtomSchema(), atom(a)
{
}

ElementaryAtomSchema::ElementaryAtomSchema(const ElementaryAtomSchema& eas) : AtomSchema(eas), atom(new Atom(*(eas.atom)))
{
}

ElementaryAtomSchema::~ElementaryAtomSchema()
{
	delete atom;
}

ElementaryAtomSchema& ElementaryAtomSchema::operator=(const ElementaryAtomSchema& eas)
{
	if(this == &eas) return *this;
	ElementaryAtomSchema neas(eas);
	std::swap(atomvars, neas.atomvars);
	std::swap(types, neas.types);
	std::swap(atom, neas.atom);
	return *this;
}

bool ElementaryAtomSchema::equals(const AtomSchema* as) const
{
	assert(atom != 0 && as != 0);
	
	const ElementaryAtomSchema* eas = dynamic_cast<const ElementaryAtomSchema*>(as);
	if(eas == 0) return false;
	return (*atom == *(eas->atom));
}

AtomSchema* ElementaryAtomSchema::clone() const
{
	return new ElementaryAtomSchema(*this);
}

AtomSchema* ElementaryAtomSchema::createPrimedVersion(const AtomVariable&) const
{
	return new ElementaryAtomSchema(*this);
}

Atom ElementaryAtomSchema::instance(const Valuation& val) const
{
	return *atom;
}

void ElementaryAtomSchema::setNumericValue(VarType, int, int)
{
}

Domain ElementaryAtomSchema::getType() const
{
	return atom->second;
}

std::string ElementaryAtomSchema::toString(const Context& c) const
{
	return c.atomToString(*atom);
}

z3::expr ElementaryAtomSchema::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(false);
	return c.bool_val(false);
}

AtomVariableSchema::AtomVariableSchema(AtomVariable* a) : AtomSchema(), avar(a)
{
	assert(avar != 0);
	
	atomvars.insert(*avar);
	if(avar->second.second == TYPEVAR)
	{
		types.insert(avar->second);
	}
}

AtomVariableSchema::AtomVariableSchema(const AtomVariableSchema& avs) : AtomSchema(avs), avar(new AtomVariable(*(avs.avar)))
{
}

AtomVariableSchema::~AtomVariableSchema()
{
	delete avar;
}

AtomVariableSchema& AtomVariableSchema::operator=(const AtomVariableSchema& avs)
{
	if(this == &avs) return *this;
	AtomVariableSchema navs(avs);
	std::swap(atomvars, navs.atomvars);
	std::swap(types, navs.types);
	std::swap(avar, navs.avar);
	return *this;
}

bool AtomVariableSchema::equals(const AtomSchema* as) const
{
	assert(avar != 0 && as != 0);
	
	const AtomVariableSchema* avs = dynamic_cast<const AtomVariableSchema*>(as);
	if(avs == 0) return false;
	return (*avar == *(avs->avar));
}

AtomSchema* AtomVariableSchema::clone() const
{
	return new AtomVariableSchema(*this);
}

AtomSchema* AtomVariableSchema::createPrimedVersion(const AtomVariable& av) const
{
	assert(avar != 0);
	
	if(*avar == av)
	{
		AtomVariable* primedvar = new AtomVariable(av);
		primedvar->first = -(primedvar->first)-1;
		return new AtomVariableSchema(primedvar);
	}
	else
	{
		return new AtomVariableSchema(*this);
	}
}

Atom AtomVariableSchema::instance(const Valuation& val) const
{
	assert(avar != 0);
	
	return val.getValue(*avar);
}

const AtomVariable& AtomVariableSchema::getAtomVariable() const
{
	assert(avar != 0);
	
	return *avar;
}

void AtomVariableSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(avar != 0);
	
	avar->setNumericValue(vt,id,var_id);
	for(std::set<AtomVariable>::iterator it = atomvars.begin(); it != atomvars.end(); ++it)
	{
		static_cast<AtomVariable>(*it).setNumericValue(vt,id,var_id);
	}
	for(std::set<TypeVariable>::iterator it = types.begin(); it != types.end(); ++it)
	{
		static_cast<TypeVariable>(*it).setNumericValue(vt,id,var_id);
	}
}

Domain AtomVariableSchema::getType() const
{
	assert(avar != 0);
	
	return avar->second;
}

std::string AtomVariableSchema::toString(const Context& c) const
{
	assert(avar != 0);
	
	return avar->toString(c);
}

z3::expr AtomVariableSchema::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(avar != 0);
	
	return avar->toZ3Expr(c, inst_num, cutoffs);	
}

FunctionAtomSchema::FunctionAtomSchema(AtomRelation* f, AtomSchemaTuple* a) : AtomSchema(), actsc(a), func(f)
{
	assert(actsc != 0 && func != 0);
	
	atomvars.insert(actsc->getAtomVariables().begin(), actsc->getAtomVariables().end());
	types.insert(actsc->getTypeVariables().begin(), actsc->getTypeVariables().end());
}

FunctionAtomSchema::FunctionAtomSchema(const FunctionAtomSchema& fas) : AtomSchema(fas), actsc(new AtomSchemaTuple(*(fas.actsc))), func(new AtomRelation(*(fas.func)))
{
}

FunctionAtomSchema::~FunctionAtomSchema()
{
	delete func;
	delete actsc;
}

FunctionAtomSchema& FunctionAtomSchema::operator=(const FunctionAtomSchema& fas)
{
	if(this == &fas) return *this;
	FunctionAtomSchema nfas(fas);
	std::swap(atomvars, nfas.atomvars);
	std::swap(types, nfas.types);
	std::swap(actsc, nfas.actsc);
	std::swap(func, nfas.func);
	return *this;
}

bool FunctionAtomSchema::equals(const AtomSchema* as) const
{
	assert(func != 0 && actsc != 0);
	
	const FunctionAtomSchema* fas = dynamic_cast<const FunctionAtomSchema*>(as);
	if(fas == 0) return false;
	if(!(*actsc == *(fas->actsc))) return false;
	return (*func == *(fas->func));
}

AtomSchema* FunctionAtomSchema::clone() const
{
	return new FunctionAtomSchema(*this);
}

AtomSchema* FunctionAtomSchema::createPrimedVersion(const AtomVariable& av) const
{
	assert(func != 0 && actsc != 0);

	return new FunctionAtomSchema(new AtomRelation(*func), actsc->createPrimedVersion(av));
}

Atom FunctionAtomSchema::instance(const Valuation& val) const
{
	assert(func != 0 && actsc != 0);
	
	AtomTuple apar = actsc->instance(val);
	Atom aval = func->operator()(apar);
	return aval;
}

void FunctionAtomSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(actsc != 0);

	actsc->setNumericValue(vt,id,var_id);
	for(std::set<AtomVariable>::iterator it = atomvars.begin(); it != atomvars.end(); ++it)
	{
		static_cast<AtomVariable>(*it).setNumericValue(vt,id,var_id);
	}
	for(std::set<TypeVariable>::iterator it = types.begin(); it != types.end(); ++it)
	{
		static_cast<TypeVariable>(*it).setNumericValue(vt,id,var_id);
	}
}

Domain FunctionAtomSchema::getType() const
{
	assert(func != 0);
	
	return func->getFunctionType();
}

std::string FunctionAtomSchema::toString(const Context& c) const
{
	assert(func != 0 && actsc != 0);
	
	std::string str(func->toString(c));
	str.append(actsc->toString(c));
	return str;
}

z3::expr FunctionAtomSchema::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(false);
	return c.bool_val(false);
}

AtomSchemaTuple::AtomSchemaTuple() : std::vector<AtomSchema*>(), varsdefined(true), typesdefined(true), atomvars(std::set<AtomVariable>()), types(std::set<TypeVariable>())
{
}

AtomSchemaTuple::AtomSchemaTuple(const AtomSchemaTuple& act) : std::vector<AtomSchema*>(), typesdefined(false), varsdefined(false), atomvars(), types()
{
	std::vector<AtomSchema*>::reserve(act.size());

	for(int i = 0; i < act.size(); i++)
	{
		std::vector<AtomSchema*>::push_back(act[i]->clone());
	}
}

AtomSchemaTuple::~AtomSchemaTuple()
{
	for(int i = 0; i < size(); i++)
	{
		delete operator[](i);
	}
}

AtomSchemaTuple* AtomSchemaTuple::createPrimedVersion(const AtomVariable& av) const
{
	AtomSchemaTuple* ast = new AtomSchemaTuple();
	for(int i = 0; i < size(); i++)
	{
		ast->push_back(operator[](i)->createPrimedVersion(av));
	}
	return ast;
}

AtomSchemaTuple& AtomSchemaTuple::operator=(const AtomSchemaTuple& act)
{
	if(this == &act) return *this;
	AtomSchemaTuple a(act);
	std::vector<AtomSchema*>::swap(a);
	std::swap(varsdefined,a.varsdefined);
	std::swap(typesdefined,a.typesdefined);
	std::swap(atomvars,a.atomvars);
	std::swap(types,a.types);
	return *this;
}

bool AtomSchemaTuple::operator<(const AtomSchemaTuple& ast) const
{
	if(size() != ast.size()) return (size() < ast.size());
	for(int i = 0; i < size(); i++)
	{
		if(operator[](i)->getType() != ast[i]->getType()) return (operator[](i)->getType() < ast[i]->getType());
	}
	return false;
}

bool AtomSchemaTuple::operator==(const AtomSchemaTuple& ast) const
{
	if(size() != ast.size()) return false;
	for(int i = 0; i < size(); i++)
	{
		if(!operator[](i)->equals(ast[i])) return false;
	}
	return true;
}

bool AtomSchemaTuple::equalsInSet(const AtomSchemaTuple& ast, const std::set<AtomVariable>& set) const
{
	if(size() != ast.size()) return false;
	for(int i = 0; i < size(); i++)
	{
		const AtomVariableSchema* avs1 = dynamic_cast<const AtomVariableSchema*>(operator[](i));
		const AtomVariableSchema* avs2 = dynamic_cast<const AtomVariableSchema*>(ast[i]);
		if(avs1 != 0)
		{
			if(set.find(avs1->getAtomVariable()) != set.end())
			{
				if(!operator[](i)->equals(ast[i])) return false;
			}
		}
		if(avs2 != 0)
		{
			if(set.find(avs2->getAtomVariable()) != set.end())
			{
				if(!operator[](i)->equals(ast[i])) return false;
			}
		}
	}
	return true;
}

bool AtomSchemaTuple::equalsAtVariable(const AtomSchemaTuple& ast, const AtomVariable& av) const
{
	if(size() != ast.size()) return false;
	for(int i = 0; i < size(); i++)
	{
		const AtomVariableSchema* avs1 = dynamic_cast<const AtomVariableSchema*>(operator[](i));
		const AtomVariableSchema* avs2 = dynamic_cast<const AtomVariableSchema*>(ast[i]);
		if(avs1 != 0)
		{
			if(avs1->getAtomVariable() == av)
			{
				if(!operator[](i)->equals(ast[i])) return false;
			}
		}
		if(avs2 != 0)
		{
			if(avs2->getAtomVariable() == av)
			{
				if(!operator[](i)->equals(ast[i])) return false;
			}
		}
	}
	return true;
}

const AtomSchema* AtomSchemaTuple::operator[](int pos) const
{
	return std::vector<AtomSchema*>::operator[](pos);
}

AtomTuple AtomSchemaTuple::instance(const Valuation& val) const
{
	AtomTuple act;
	act.reserve(size());
	for(int i = 0; i < size(); i++)
	{
		act.push_back(std::vector<AtomSchema*>::operator[](i)->instance(val));
	}	
	return act;
}

AtomSchemaTuple& AtomSchemaTuple::push_back(AtomSchema* a)
{
	assert(a != 0);

	std::vector<AtomSchema*>::push_back(a);
	varsdefined = false;
	typesdefined = false;
	return *this;
}

int AtomSchemaTuple::size() const
{
	return std::vector<AtomSchema*>::size();
}

void AtomSchemaTuple::reserve(int cap)
{
	std::vector<AtomSchema*>::reserve(cap);
}

void AtomSchemaTuple::setNumericValue(VarType vt, int id, int var_id)
{
	for(int i = 0; i < std::vector<AtomSchema*>::size(); i++)
	{
		std::vector<AtomSchema*>::operator[](i)->setNumericValue(vt,id,var_id);
	}
	for(std::set<AtomVariable>::iterator it = atomvars.begin(); it != atomvars.end(); ++it)
	{
		static_cast<AtomVariable>(*it).setNumericValue(vt,id,var_id);
	}
	for(std::set<TypeVariable>::iterator it = types.begin(); it != types.end(); ++it)
	{
		static_cast<TypeVariable>(*it).setNumericValue(vt,id,var_id);
	}
}

const std::set<AtomVariable>& AtomSchemaTuple::getAtomVariables() const
{
	if(!varsdefined)
	{
		for(int i = 0; i < size(); i++)
		{
			const_cast<std::set<AtomVariable>&>(atomvars).insert(operator[](i)->getAtomVariables().begin(), operator[](i)->getAtomVariables().end());
		}
		const_cast<bool&>(varsdefined) = true;
	}
	return atomvars;
}

const std::set<TypeVariable>& AtomSchemaTuple::getTypeVariables() const
{
	if(!typesdefined)
	{
		for(int i = 0; i < size(); i++)
		{
			const_cast<std::set<TypeVariable>&>(types).insert(operator[](i)->getTypeVariables().begin(), operator[](i)->getTypeVariables().end());
		}
		const_cast<bool&>(typesdefined) = true;
	}
	return types;
}

int AtomSchemaTuple::numOfOccurrences(const Domain& d) const
{
	int num = 0;
	
	getAtomVariables();
	
	for(std::set<AtomVariable>::iterator it = atomvars.begin(); it != atomvars.end(); ++it)
	{
		if(it->second == d) num++;
	}
	
	return num;	
}

int AtomSchemaTuple::numOfOccurrences(const AtomVariable& av) const
{
	int num = 0;
	
	for(int i = 0; i < size(); i++)
	{
		const AtomVariableSchema* avs = dynamic_cast<const AtomVariableSchema*>(operator[](i));
		if(avs != 0)
		{
			if(avs->getAtomVariable() == av) num++;
		}
	}
	
	return num;	
}

bool AtomSchemaTuple::isElement(const AtomVariable& av) const
{
	return (numOfOccurrences(av) > 0);
}

bool AtomSchemaTuple::isEquallyTyped(const AtomSchemaTuple& ast) const
{
	if(size() != ast.size()) return false;
	for(int i = 0; i < size(); i++)
	{
		if(operator[](i)->getType() != ast[i]->getType()) return false;
	}
	return true;
}

bool AtomSchemaTuple::isEquallyTyped(const RelationVariable& rv) const
{
	if(size() != std::get<1>(rv).size()) return false;
	for(int i = 0; i < size(); i++)
	{
		if(operator[](i)->getType() != std::get<1>(rv)[i]) return false;
	}
	return true;
}

int AtomSchemaTuple::removeVariable(const AtomVariable& av) 
{
	int removedcount = 0;
	int i = 0;
	while(i < size())
	{
		const AtomVariableSchema* avs = dynamic_cast<const AtomVariableSchema*>(operator[](i));
		if(avs != 0)
		{
			if(avs->getAtomVariable() == av)
			{
				erase(begin()+i);
				delete avs;
				i--;
				varsdefined = false;
				typesdefined = false;
				atomvars.clear();
				types.clear();
				removedcount++;
			}
		}
		i++;
	}
	return removedcount;
}

int AtomSchemaTuple::removeVariables(const Domain& d) 
{
	int removedcount = 0;
	int i = 0;
	while(i < size())
	{
		const AtomVariableSchema* avs = dynamic_cast<const AtomVariableSchema*>(operator[](i));
		if(avs != 0)
		{
			if(avs->getAtomVariable().second == d)
			{
				erase(begin()+i);
				delete avs;
				i--;
				varsdefined = false;
				typesdefined = false;
				atomvars.clear();
				types.clear();
				removedcount++;
			}
		}
		i++;
	}
	return removedcount;
}

std::string AtomSchemaTuple::toString(const Context& c) const
{
	std::string str;
	std::vector<AtomSchema*>::const_iterator cit = std::vector<AtomSchema*>::begin();
	if(cit != std::vector<AtomSchema*>::end())
	{
		str.append((*cit)->toString(c));
		++cit;
	}
	for(; cit != std::vector<AtomSchema*>::end(); ++cit)
	{
		str.push_back(',');
		str.append((*cit)->toString(c));
	}
	return str;
}

ActionSchema::ActionSchema(Channel c, bool i) : AtomSchemaTuple(), chan(c), isInput(i)
{
}

ActionSchema::ActionSchema(const ActionSchema& as) : AtomSchemaTuple(as), chan(as.chan), isInput(as.isInput)
{
}

bool ActionSchema::operator<(const ActionSchema& as2) const
{
	if(chan != as2.chan) return chan < as2.chan;
//	if(isInput != as2.isInput) return isInput < as2.isInput;
	return (static_cast<const AtomSchemaTuple&>(*this) < static_cast<const AtomSchemaTuple&>(as2));
}

bool ActionSchema::equalsAtVariable(const ActionSchema& as, const AtomVariable& av) const
{
	if(chan != as.chan) return false;
	return AtomSchemaTuple::equalsAtVariable(as,av);
}

ActionSchema* ActionSchema::createPrimedVersion(const AtomVariable& av) const
{
	ActionSchema* newas = new ActionSchema(chan, isInput);
	for(int i = 0; i < size(); i++)
	{
		newas->push_back(operator[](i)->createPrimedVersion(av));
	}
	return newas;	
}

Action ActionSchema::instance(const Valuation& val) const
{
	Action act(chan,isInput);
	act.reserve(size());
	for(int i = 0; i < size(); i++)
	{
		act.push_back(std::vector<AtomSchema*>::operator[](i)->instance(val));
	}	
	return act;
}

std::string ActionSchema::toString(const Context& c) const
{
	std::string str;
	if(isInput) str.push_back('?');
	str.append(c.channelToString(chan));
	str.push_back('(');
	str.append(AtomSchemaTuple::toString(c));
	str.push_back(')');
	return str;
}

StateSchema::StateSchema(int p, int s) : ControlState(p,s), AtomSchemaTuple()
{
}

StateSchema::StateSchema(const ControlState& c) : ControlState(c), AtomSchemaTuple()
{
}

StateSchema::StateSchema(const StateSchema& as) : ControlState(as), AtomSchemaTuple(as)
{
}

bool StateSchema::operator<(const StateSchema& ss2) const
{
	if(static_cast<const ControlState&>(*this) != static_cast<const ControlState&>(ss2)) return (static_cast<const ControlState&>(*this) < static_cast<const ControlState&>(ss2));
	return (static_cast<const AtomSchemaTuple&>(*this) < static_cast<const AtomSchemaTuple&>(ss2));
}

bool StateSchema::operator==(const StateSchema& ss2) const
{
	if(static_cast<const ControlState&>(*this) != static_cast<const ControlState&>(ss2)) return false;
	return (static_cast<const AtomSchemaTuple&>(*this) == static_cast<const AtomSchemaTuple&>(ss2));
}

State StateSchema::instance(const Valuation& val) const
{
	State st(*this);
	st.reserve(size());
	for(int i = 0; i < size(); i++)
	{
		st.push_back(std::vector<AtomSchema*>::operator[](i)->instance(val));
	}	
	return st;
}

std::string StateSchema::toString(const Context& c) const
{
	std::string str(c.controlStateToString(*this));
	str.push_back('(');
	str.append(AtomSchemaTuple::toString(c));
	str.push_back(')');
	return str;
}

const AtomVariableSchema* ParameterList::operator[](int pos) const
{
	return static_cast<AtomVariableSchema*>(std::vector<AtomSchema*>::operator[](pos));
}

ParameterList& ParameterList::push_back(AtomSchema* a)
{
	assert(dynamic_cast<AtomVariableSchema*>(a));

	AtomSchemaTuple::push_back(a);
	return *this;
}

RelationSchema::RelationSchema(AtomSchemaTuple* ast, const std::vector<int>& ts) : std::set<AtomSchemaTuple*>(), types(std::vector<Domain>()), typesizes(ts), avars(std::set<AtomVariable>()), guard(new TopValuationFormula())
{
	assert(typesizes.size() == ast->size());
		
	for(int i = 0; i < ast->size(); i++)
	{
		types.push_back(ast->operator[](i)->getType());
	}
	std::set<AtomSchemaTuple*>::insert(ast);
	avars.insert(ast->getAtomVariables().begin(), ast->getAtomVariables().end());
}

RelationSchema::RelationSchema(const RelationSchema& rs) : std::set<AtomSchemaTuple*>(), types(rs.types), typesizes(rs.typesizes), avars(rs.avars), guard(rs.guard->clone())
{
	for(std::set<AtomSchemaTuple*>::const_iterator cit = rs.begin(); cit != rs.end(); cit++)
	{
		std::set<AtomSchemaTuple*>::insert(new AtomSchemaTuple(**cit));
	}
}

RelationSchema::~RelationSchema()
{
	for(std::set<AtomSchemaTuple*>::iterator cit = begin(); cit != end(); cit++)
	{
		delete *cit;
	}
	std::set<AtomSchemaTuple*>::clear();
	delete guard;
}

RelationSchema& RelationSchema::operator=(const RelationSchema& rs)
{
	if(this == &rs) return *this;
	RelationSchema nrs(rs);
	std::set<AtomSchemaTuple*>::swap(nrs);
	std::swap(types, nrs.types);
	std::swap(typesizes, nrs.typesizes);
	std::swap(avars, nrs.avars);
	std::swap(guard, nrs.guard);
	return *this;
}

void RelationSchema::insert(AtomSchemaTuple* ast)
{
	std::set<AtomSchemaTuple*>::insert(ast);
	avars.insert(ast->getAtomVariables().begin(), ast->getAtomVariables().end());
}

bool RelationSchema::isEquallyTyped(const AtomSchemaTuple& ast)
{
	if(types.size() != ast.size()) return false;
	for(int i = 0; i < ast.size(); i++)
	{
		if(types[i] != ast[i]->getType()) return false;
	}
	return true;
}

void RelationSchema::setGuard(ValuationFormula* vf)
{
	delete guard;
	guard = vf;
	avars.insert(guard->getAtomVariables().begin(), guard->getAtomVariables().end());
}

AtomRelation RelationSchema::instance() const
{
	AtomRelation ar(types, typesizes);
	std::list<Valuation*>* vals = new std::list<Valuation*>();
	vals->push_back(new Valuation());
	int numval = 0;
	for(std::set<AtomVariable>::const_iterator cavit = avars.begin(); cavit != avars.end(); cavit++)
	{
		for(std::set<AtomSchemaTuple*>::const_iterator castit = begin(); castit != end(); castit++)
		{
			(*castit)->setNumericValue(ATOMVAR,cavit->first,numval);
		}
		guard->setNumericValue(ATOMVAR,cavit->first,numval);
		std::list<Valuation*>* tempvals = new std::list<Valuation*>();
		std::swap(tempvals,vals);
		for(std::list<Valuation*>::const_iterator cvit = tempvals->begin(); cvit != tempvals->end(); ++cvit)
		{
			std::list<Valuation*> vals2 = (*cvit)->addAtomVariable(cavit->second);
			vals->splice(vals->end(), vals2);
			delete *cvit;
		}
		delete tempvals;
		numval++;
	}
	for(std::list<Valuation*>::const_iterator cvit = vals->begin(); cvit != vals->end(); ++cvit)
	{
		if(guard->instance(**cvit))
		{
			for(std::set<AtomSchemaTuple*>::const_iterator cit = begin(); cit != end(); cit++)
			{
				ar.insert((*cit)->instance(**cvit));
			}
		}
	}
	
	for(std::list<Valuation*>::const_iterator cvit = vals->begin(); cvit != vals->end(); ++cvit)
	{
		delete *cvit;
	}
	delete vals;
	return ar;
}

TransitionSchema::TransitionSchema() : from(new StateSchema()), to(new StateSchema()), actsc(new ActionSchema()), boundparams(new ParameterList()), guard(new TopValuationFormula()), atomvars(std::set<AtomVariable>()), types(std::set<TypeVariable>()), relvars(std::set<RelationVariable>()), qfvars(std::set<QuorumFunctionVariable>()), isMust(false)
{
}

TransitionSchema::TransitionSchema(StateSchema* s1, ParameterList* bp, ValuationFormula* gs, ActionSchema* a, StateSchema* s2, bool must) : from(s1), boundparams(bp), guard(gs), actsc(a), to(s2), atomvars(std::set<AtomVariable>()), types(std::set<TypeVariable>()), relvars(std::set<RelationVariable>()), qfvars(std::set<QuorumFunctionVariable>()), isMust(must)
{
//	assert(from != 0 && to != 0 && actsc != 0);

	if(from == 0) from = new StateSchema();
	if(to == 0) to = new StateSchema();
	if(actsc == 0) actsc = new ActionSchema();	
	if(boundparams == 0) boundparams = new ParameterList();
	if(guard == 0) guard = new TopValuationFormula();

	atomvars.insert(guard->getAtomVariables().begin(), guard->getAtomVariables().end());
	atomvars.insert(actsc->getAtomVariables().begin(), actsc->getAtomVariables().end());
	atomvars.insert(to->getAtomVariables().begin(), to->getAtomVariables().end());
	for(std::set<AtomVariable>::const_iterator cit = boundparams->getAtomVariables().begin(); cit != boundparams->getAtomVariables().end(); cit++)
	{
		std::set<AtomVariable>::iterator it = atomvars.find(*cit);
		if(it != atomvars.end()) atomvars.erase(it);
	}
	for(std::set<AtomVariable>::const_iterator cit = from->getAtomVariables().begin(); cit != from->getAtomVariables().end(); cit++)
	{
		std::set<AtomVariable>::iterator it = atomvars.find(*cit);
		if(it != atomvars.end()) atomvars.erase(it);
	}
	
	types.insert(guard->getTypeVariables().begin(), guard->getTypeVariables().end());
	types.insert(actsc->getTypeVariables().begin(), actsc->getTypeVariables().end());
	types.insert(to->getTypeVariables().begin(), to->getTypeVariables().end());
	types.insert(boundparams->getTypeVariables().begin(), boundparams->getTypeVariables().end());
	types.insert(from->getTypeVariables().begin(), from->getTypeVariables().end());	

	relvars.insert(guard->getRelationVariables().begin(), guard->getRelationVariables().end());
	qfvars.insert(guard->getQuorumFunctionVariables().begin(), guard->getQuorumFunctionVariables().end());
}

TransitionSchema::TransitionSchema(const TransitionSchema& ts) : from(new StateSchema(*(ts.from))), boundparams(new ParameterList(*(ts.boundparams))), guard(ts.guard->clone()), actsc(new ActionSchema(*(ts.actsc))), to(new StateSchema(*(ts.to))), atomvars(ts.atomvars), types(ts.types), relvars(ts.relvars), qfvars(ts.qfvars), isMust(ts.isMust)
{
}

TransitionSchema::~TransitionSchema()
{
	delete from;
	delete boundparams;
	delete guard;
	delete actsc;
	delete to;
}

TransitionSchema& TransitionSchema::operator=(const TransitionSchema& ts)
{
	if(this == &ts) return *this;
	TransitionSchema nts(ts);
	std::swap(from, nts.from);
	std::swap(boundparams, nts.boundparams);
	std::swap(guard, nts.guard);
	std::swap(actsc, nts.actsc);
	std::swap(to, nts.to);
	std::swap(atomvars, nts.atomvars);
	std::swap(types, nts.types);
	std::swap(relvars, nts.relvars);
	std::swap(qfvars, nts.qfvars);
	std::swap(isMust, nts.isMust);
	
	return *this;
}

bool TransitionSchema::operator<(const TransitionSchema& ts2) const
{
	if(*from < *(ts2.from)) return true;
	return false;
}

TransitionSet* TransitionSchema::instance(const Valuation& val) const
{
	TransitionSet* ts = new TransitionSet();
	std::list<Valuation*>* vals = new std::list<Valuation*>();
	vals->push_back(new Valuation(val));
	for(int i= 0; i < from->size(); i++)
	{
		assert(dynamic_cast<const AtomVariableSchema*>(from->operator[](i)));
		
		const AtomVariableSchema* avs = static_cast<const AtomVariableSchema*>(from->operator[](i));
		const AtomVariable& av = avs->getAtomVariable();
		
		std::list<Valuation*>* tempvals = new std::list<Valuation*>();
		std::swap(tempvals,vals);
		for(std::list<Valuation*>::const_iterator cvit = tempvals->begin(); cvit != tempvals->end(); ++cvit)
		{
			std::list<Valuation*> vals2 = (*cvit)->addAtomVariable(av.second);
			vals->splice(vals->end(), vals2);
			delete *cvit;
		}
		delete tempvals;
	}
	for(int i= 0; i < boundparams->size(); i++)
	{
		const AtomVariableSchema* avs = boundparams->operator[](i);
		const AtomVariable& av = avs->getAtomVariable();
		
		std::list<Valuation*>* tempvals = new std::list<Valuation*>();
		std::swap(tempvals,vals);
		for(std::list<Valuation*>::const_iterator cvit = tempvals->begin(); cvit != tempvals->end(); ++cvit)
		{
			std::list<Valuation*> vals2 = (*cvit)->addAtomVariable(av.second);
			vals->splice(vals->end(), vals2);
			delete *cvit;
		}
		delete tempvals;
	}

	for(std::list<Valuation*>::const_iterator cvit = vals->begin(); cvit != vals->end(); ++cvit)
	{
		if(guard->instance(**cvit))
		{
			State s1 = from->instance(**cvit);
			Action act = actsc->instance(**cvit);
			State s2 = to->instance(**cvit);
			ts->insert(Transition(s1, act, s2, isMust));
		}
	}
	
	for(std::list<Valuation*>::const_iterator cvit = vals->begin(); cvit != vals->end(); ++cvit)
	{
		delete *cvit;
	}
	delete vals;
	return ts;
	
}

const StateSchema& TransitionSchema::getFromState() const
{	
	assert(from != 0);
	
	return *from;
}

const StateSchema& TransitionSchema::getToState() const
{	
	assert(to != 0);
	
	return *to;
}

const ParameterList& TransitionSchema::getBoundParameters() const
{
	assert(boundparams != 0);
	
	return *boundparams;
}

const ValuationFormula& TransitionSchema::getGuard() const
{
	assert(guard != 0);
	
	return *guard;
}

const ActionSchema& TransitionSchema::getActionSchema() const
{
	assert(actsc != 0);
	
	return *actsc;
}

void TransitionSchema::setFromState(StateSchema* s)
{
	assert(from != 0);
	assert(from->size() == 0);

	delete from;
	from = ((s != 0) ? s : new StateSchema());
	for(std::set<AtomVariable>::const_iterator cit = from->getAtomVariables().begin(); cit != from->getAtomVariables().end(); cit++)
	{
		std::set<AtomVariable>::iterator it = atomvars.find(*cit);
		if(it != atomvars.end()) atomvars.erase(it);
	}
	types.insert(from->getTypeVariables().begin(), from->getTypeVariables().end());
}

void TransitionSchema::setToState(StateSchema* s)
{
	assert(to != 0);
	assert(to->size() == 0);

	delete to;
	to = ((s != 0) ? s : new StateSchema());
	
	atomvars.insert(to->getAtomVariables().begin(), to->getAtomVariables().end());
	for(std::set<AtomVariable>::const_iterator cit = from->getAtomVariables().begin(); cit != from->getAtomVariables().end(); cit++)
	{
		std::set<AtomVariable>::iterator it = atomvars.find(*cit);
		if(it != atomvars.end()) atomvars.erase(it);
	}
	for(std::set<AtomVariable>::const_iterator cit = boundparams->getAtomVariables().begin(); cit != boundparams->getAtomVariables().end(); cit++)
	{
		std::set<AtomVariable>::iterator it = atomvars.find(*cit);
		if(it != atomvars.end()) atomvars.erase(it);
	}
	types.insert(to->getTypeVariables().begin(), to->getTypeVariables().end());
}

void TransitionSchema::setBoundParameters(ParameterList* bpl)
{
	assert(boundparams != 0);
	assert(boundparams->size() == 0);
	
	delete boundparams;
	boundparams = ((bpl != 0) ? bpl : new ParameterList());
	for(std::set<AtomVariable>::const_iterator cit = boundparams->getAtomVariables().begin(); cit != boundparams->getAtomVariables().end(); cit++)
	{
		std::set<AtomVariable>::iterator it = atomvars.find(*cit);
		if(it != atomvars.end()) atomvars.erase(it);
	}
	types.insert(boundparams->getTypeVariables().begin(), boundparams->getTypeVariables().end());
}


void TransitionSchema::setGuard(ValuationFormula* gs)
{
	assert(dynamic_cast<TopValuationFormula*>(guard));

	delete guard;
	guard = ((gs != 0) ? gs : new TopValuationFormula());
	atomvars.insert(guard->getAtomVariables().begin(), guard->getAtomVariables().end());
	for(std::set<AtomVariable>::const_iterator cit = from->getAtomVariables().begin(); cit != from->getAtomVariables().end(); cit++)
	{
		std::set<AtomVariable>::iterator it = atomvars.find(*cit);
		if(it != atomvars.end()) atomvars.erase(it);
	}
	for(std::set<AtomVariable>::const_iterator cit = boundparams->getAtomVariables().begin(); cit != boundparams->getAtomVariables().end(); cit++)
	{
		std::set<AtomVariable>::iterator it = atomvars.find(*cit);
		if(it != atomvars.end()) atomvars.erase(it);
	}
	types.insert(guard->getTypeVariables().begin(), guard->getTypeVariables().end());
	relvars.insert(guard->getRelationVariables().begin(), guard->getRelationVariables().end());
	qfvars.insert(guard->getQuorumFunctionVariables().begin(), guard->getQuorumFunctionVariables().end());
}

void TransitionSchema::setActionSchema(ActionSchema* a)
{
	assert(actsc->size() == 0 && a != 0);
	
	delete actsc;

	actsc = a;
	atomvars.insert(actsc->getAtomVariables().begin(), actsc->getAtomVariables().end());
	for(std::set<AtomVariable>::const_iterator cit = from->getAtomVariables().begin(); cit != from->getAtomVariables().end(); cit++)
	{
		std::set<AtomVariable>::iterator it = atomvars.find(*cit);
		if(it != atomvars.end()) atomvars.erase(it);
	}
	for(std::set<AtomVariable>::const_iterator cit = boundparams->getAtomVariables().begin(); cit != boundparams->getAtomVariables().end(); cit++)
	{
		std::set<AtomVariable>::iterator it = atomvars.find(*cit);
		if(it != atomvars.end()) atomvars.erase(it);
	}
	types.insert(actsc->getTypeVariables().begin(), actsc->getTypeVariables().end());
}

bool TransitionSchema::hasVisibleGuard() const
{
	assert(from != 0 && to != 0 && actsc != 0);
	
	bool has = true;
	for(std::set<AtomVariable>::const_iterator cait = guard->getAtomVariables().begin(); cait != guard->getAtomVariables().end(); cait++)
	{
		std::set<AtomVariable>::const_iterator caitbnd = boundparams->getAtomVariables().find(*cait);
		std::set<AtomVariable>::const_iterator caitact = actsc->getAtomVariables().find(*cait);
		std::set<AtomVariable>::const_iterator caitout = to->getAtomVariables().find(*cait);
		if(caitbnd != boundparams->getAtomVariables().end() && caitout == to->getAtomVariables().end() && caitact == actsc->getAtomVariables().end())
		{
			has = false;
			break;
		}
	}
	return has;
}

const std::set<AtomVariable>& TransitionSchema::getAtomVariables() const
{
	return atomvars;
}

const std::set<TypeVariable>& TransitionSchema::getTypeVariables() const
{
	return types;
}

const std::set<RelationVariable>& TransitionSchema::getRelationVariables() const
{
	return relvars;
}

const std::set<QuorumFunctionVariable>& TransitionSchema::getQuorumFunctionVariables() const
{
	return qfvars;
}

void TransitionSchema::setNumericValue(VarType vt, int id, int var_id)
{
	bool isfree = true;
	for(int i = 0; i < boundparams->size(); i++)
	{
		if(vt == ATOMVAR && boundparams->operator[](i)->getAtomVariable().first == id)
		{
			isfree = false;
		}
		else
		{
			const_cast<AtomVariableSchema*>(boundparams->operator[](i))->setNumericValue(vt, id, var_id);
		}
	}
	for(int i = 0; i < from->size(); i++)
	{
		assert(dynamic_cast<const AtomVariableSchema*>(from->operator[](i)));
		
		if(vt == ATOMVAR && static_cast<const AtomVariableSchema*>(from->operator[](i))->getAtomVariable().first == id)
		{
			isfree = false;
		}
		else
		{
			const_cast<AtomSchema*>(from->operator[](i))->setNumericValue(vt, id, var_id);
		}
	}
	if(isfree)
	{
		guard->setNumericValue(vt, id, var_id);
		actsc->setNumericValue(vt, id, var_id);
		to->setNumericValue(vt, id, var_id);
	}
	for(std::set<AtomVariable>::iterator it = atomvars.begin(); it != atomvars.end(); ++it)
	{
		static_cast<AtomVariable>(*it).setNumericValue(vt, id, var_id);
	}
	for(std::set<TypeVariable>::iterator it = types.begin(); it != types.end(); ++it)
	{
		static_cast<TypeVariable>(*it).setNumericValue(vt, id, var_id);
	}
	for(std::set<RelationVariable>::iterator it = relvars.begin(); it != relvars.end(); ++it)
	{
		static_cast<RelationVariable>(*it).setNumericValue(vt, id, var_id);
	}
	for(auto& qfv : qfvars)
		const_cast<QuorumFunctionVariable&>(qfv).setNumericValue(vt, id, var_id);
}

void TransitionSchema::setNumericValuesToBoundVariables(int val)
{
	assert(from != 0 && boundparams != 0 && guard != 0 && actsc != 0 && to != 0);

	for(int i = 0; i < from->size(); i++)
	{
		assert(dynamic_cast<const AtomVariableSchema*>(from->operator[](i)));
		
		AtomVariableSchema* avs = const_cast<AtomVariableSchema*>(static_cast<const AtomVariableSchema*>(from->operator[](i)));
		const AtomVariable& av = avs->getAtomVariable();
		avs->setNumericValue(ATOMVAR, av.first, val);
		guard->setNumericValue(ATOMVAR, av.first, val);
		actsc->setNumericValue(ATOMVAR, av.first, val);
		to->setNumericValue(ATOMVAR, av.first, val);
		val++;
	}
	for(int i = 0; i < boundparams->size(); i++)
	{
		AtomVariableSchema* avs = const_cast<AtomVariableSchema*>(boundparams->operator[](i));
		const AtomVariable& av = avs->getAtomVariable();
		avs->setNumericValue(ATOMVAR, av.first, val);
		guard->setNumericValue(ATOMVAR, av.first, val);
		actsc->setNumericValue(ATOMVAR, av.first, val);
		to->setNumericValue(ATOMVAR, av.first, val);
		val++;
	}
}

int TransitionSchema::numOfNestedQFVs(const Domain& tv) const
{
	assert(guard != 0);

	return guard->numOfNestedQFVs(tv);
}

int TransitionSchema::numOfBoundOccurrences(const Domain& tv) const
{
	assert(from != 0 && boundparams != 0);
	
	int num = 0;
	for(int i = 0; i < from->size(); i++)
	{
		assert(dynamic_cast<const AtomVariableSchema*>(from->operator[](i)));
		
		if(tv == static_cast<const AtomVariableSchema*>(from->operator[](i))->getAtomVariable().second) num++;
	}
	for(int i = 0; i < boundparams->size(); i++)
	{
		if(tv == boundparams->operator[](i)->getAtomVariable().second) num++;
	}
	return num;
}

int TransitionSchema::numOfBoundOccurrencesInStates(const Domain& tv) const
{
	assert(from != 0 && boundparams != 0 && to != 0);

	int num = 0;
	const std::set<AtomVariable>& avars = to->getAtomVariables();
	
	for(int i = 0; i < from->size(); i++)
	{
		const AtomVariableSchema* avs = dynamic_cast<const AtomVariableSchema*>(from->operator[](i));
		
		assert(avs != 0);
		
		const TypeVariable& type = avs->getAtomVariable().second;
		if(tv == type) num++;
	}
	for(int i = 0; i < boundparams->size(); i++)
	{
		const AtomVariable& avar = boundparams->operator[](i)->getAtomVariable();
		if(tv == avar.second && avars.find(avar) != avars.end()) num++;
	}
	return num;
}

std::string TransitionSchema::toString(const Context& c) const
{
	assert(from != 0 && to != 0 && guard != 0 && actsc != 0 && boundparams != 0);
	
	std::string str(from->toString(c));
	str.append(" = ");

	if(boundparams->size() != 0)
	{
		str.append("[]");
		str.append(boundparams->toString(c));
		str.push_back(':');
	}
	
	str.push_back('[');
	str.append(guard->toString(c));
	str.push_back(']');
	
	str.append(actsc->toString(c));
	if(isMust) str.append(" => ");
	else str.append(" -> ");
	
	str.append(to->toString(c));
	
	return str;
}

TransitionSetSchema::iterator::iterator(const std::vector<TransitionSchema*>::iterator& it) : std::vector<TransitionSchema*>::iterator(it)
{
}

TransitionSetSchema::const_iterator::const_iterator(const std::vector<TransitionSchema*>::const_iterator& it) : std::vector<TransitionSchema*>::const_iterator(it)
{
}

TransitionSetSchema::const_iterator::const_iterator(const TransitionSetSchema::iterator& it) : std::vector<TransitionSchema*>::const_iterator(it)
{
}

TransitionSetSchema::reverse_iterator::reverse_iterator(const std::vector<TransitionSchema*>::reverse_iterator& it) : std::vector<TransitionSchema*>::reverse_iterator(it)
{
}

TransitionSetSchema::const_reverse_iterator::const_reverse_iterator(const std::vector<TransitionSchema*>::const_reverse_iterator& it) : std::vector<TransitionSchema*>::const_reverse_iterator(it)
{
}

TransitionSetSchema::const_reverse_iterator::const_reverse_iterator(const TransitionSetSchema::reverse_iterator& it) : std::vector<TransitionSchema*>::const_reverse_iterator(it)
{
}

TransitionSetSchema::TransitionSetSchema() : std::vector<TransitionSchema*>(), atomvars(std::set<AtomVariable>()), types(std::set<TypeVariable>()), relvars(std::set<RelationVariable>()), qfvars(std::set<QuorumFunctionVariable>()), inputchans(std::set<Channel>()), outputchans(std::set<Channel>())
{
}

TransitionSetSchema::TransitionSetSchema(const TransitionSetSchema& ts) : std::vector<TransitionSchema*>(), atomvars(ts.getAtomVariables()), types(ts.getTypeVariables()), relvars(ts.getRelationVariables()), qfvars(ts.getQuorumFunctionVariables()), inputchans(ts.getInputChannels()), outputchans(ts.getOutputChannels())
{
	for(TransitionSetSchema::const_iterator cit = ts.begin(); cit != ts.end(); ++cit)
	{
		insert(**cit);
	}
}

TransitionSetSchema::~TransitionSetSchema()
{
	for(TransitionSetSchema::iterator it = begin(); it != end(); ++it)
	{
		delete *it;
	}	
}

TransitionSetSchema& TransitionSetSchema::operator=(const TransitionSetSchema& ts)
{
	if(this == &ts) return *this;
	TransitionSetSchema t(ts);
	std::swap(static_cast<std::vector<TransitionSchema*>&>(*this), static_cast<std::vector<TransitionSchema*>&>(t));
	std::swap(atomvars, t.atomvars);
	std::swap(types, t.types);
	std::swap(relvars, t.relvars);
	std::swap(qfvars, t.qfvars);
	std::swap(inputchans, t.inputchans);
	std::swap(outputchans, t.outputchans);
	return *this;	
}

TransitionSet* TransitionSetSchema::instance(const Valuation& val) const
{
	TransitionSet* ts = new TransitionSet();
	for(std::vector<TransitionSchema*>::const_iterator clit = begin(); clit != end(); ++clit)
	{
//		std::cout << "Instantiating transition schema " << (*clit)->toString() << std::endl;
		TransitionSet* trans = (*clit)->instance(val);
//		std::cout << trans->toString() << std::endl;
		ts->insert(trans->begin(), trans->end());
		delete trans;
	}
	return ts;
}

void TransitionSetSchema::setNumericValue(VarType vt, int id, int var_id)
{
	for(std::vector<TransitionSchema*>::iterator it = begin(); it != end(); ++it)
	{
		(*it)->setNumericValue(vt,id,var_id);
	}
	for(std::set<AtomVariable>::iterator it = atomvars.begin(); it != atomvars.end(); ++it)
	{
		static_cast<AtomVariable>(*it).setNumericValue(vt,id,var_id);
	}
	for(std::set<TypeVariable>::iterator it = types.begin(); it != types.end(); ++it)
	{
		static_cast<TypeVariable>(*it).setNumericValue(vt,id,var_id);
	}
	for(std::set<RelationVariable>::iterator it = relvars.begin(); it != relvars.end(); ++it)
	{
		static_cast<RelationVariable>(*it).setNumericValue(vt,id,var_id);
	}
	for(auto& qfv : qfvars)
		const_cast<QuorumFunctionVariable&>(qfv).setNumericValue(vt, id, var_id);
}

void TransitionSetSchema::setNumericValuesToBoundVariables(int val)
{
	for(std::vector<TransitionSchema*>::const_iterator clit = begin(); clit != end(); ++clit)
	{
		(*clit)->setNumericValuesToBoundVariables(val);
	}	
}

TransitionSetSchema& TransitionSetSchema::insert(const TransitionSchema& t)
{
	int first = 0;
	int last = size();
	if(last == 0)
	{
		push_back(new TransitionSchema(t));
	}
	else
	{
		while(last - first > 1)
		{
			int middle = first + ((last - first) / 2);
			if(t < *operator[](middle))
			{
				last = middle;
			}
			else
			{
				first = middle;
			}
		}
		if(t < *operator[](first))
		{
			std::vector<TransitionSchema*>::insert(std::vector<TransitionSchema*>::begin() + first, new TransitionSchema(t));
		}
		else
		{
			std::vector<TransitionSchema*>::insert(std::vector<TransitionSchema*>::begin() + last, new TransitionSchema(t));
		}
	}

	atomvars.insert(t.getAtomVariables().begin(), t.getAtomVariables().end());
	types.insert(t.getTypeVariables().begin(), t.getTypeVariables().end());
	relvars.insert(t.getRelationVariables().begin(), t.getRelationVariables().end());
	qfvars.insert(t.getQuorumFunctionVariables().begin(), t.getQuorumFunctionVariables().end());
	if(t.getActionSchema().isInput)
	{
		inputchans.insert(t.getActionSchema().chan);
	}
	else if(t.getActionSchema().chan != 0)
	{
		outputchans.insert(t.getActionSchema().chan);
	}
	return *this;
}

TransitionSetSchema& TransitionSetSchema::insert(TransitionSchema* t)
{
	int first = 0;
	int last = size();
	if(last == 0)
	{
		push_back(t);
	}
	else
	{
		while(last - first > 1)
		{
			int middle = first + ((last - first) / 2);
			if(*t < *operator[](middle))
			{
				last = middle;
			}
			else
			{
				first = middle;
			}
		}
		if(*t < *operator[](first))
		{
			std::vector<TransitionSchema*>::insert(std::vector<TransitionSchema*>::begin() + first, t);
		}
		else
		{
			std::vector<TransitionSchema*>::insert(std::vector<TransitionSchema*>::begin() + last, t);
		}
	}

	atomvars.insert(t->getAtomVariables().begin(), t->getAtomVariables().end());
	types.insert(t->getTypeVariables().begin(), t->getTypeVariables().end());
	relvars.insert(t->getRelationVariables().begin(), t->getRelationVariables().end());
	qfvars.insert(t->getQuorumFunctionVariables().begin(), t->getQuorumFunctionVariables().end());
	if(t->getActionSchema().isInput)
	{
		inputchans.insert(t->getActionSchema().chan);
	}
	else if(t->getActionSchema().chan != 0)
	{
		outputchans.insert(t->getActionSchema().chan);
	}
	return *this;
}

const std::set<AtomVariable>& TransitionSetSchema::getAtomVariables() const
{
	return atomvars;
}

const std::set<TypeVariable>& TransitionSetSchema::getTypeVariables() const
{
	return types;
}

const std::set<RelationVariable>& TransitionSetSchema::getRelationVariables() const
{
	return relvars;
}

const std::set<QuorumFunctionVariable>& TransitionSetSchema::getQuorumFunctionVariables() const
{
	return qfvars;
}

const std::set<Channel>& TransitionSetSchema::getInputChannels() const
{
	return inputchans;
}

const std::set<Channel>& TransitionSetSchema::getOutputChannels() const
{
	return outputchans;
}

bool TransitionSetSchema::hasDisjointInputOutputChannels() const
{
	return disjoint<Channel>(inputchans, outputchans);
}

std::pair<TransitionSetSchema::const_iterator, TransitionSetSchema::const_iterator> TransitionSetSchema::getSuccessors(const StateSchema& ss) const
{
	TransitionSchema tr;
	tr.setFromState(new StateSchema(ss));
	return std::equal_range(std::vector<TransitionSchema*>::begin(), std::vector<TransitionSchema*>::end(), &tr, PointerCompare<TransitionSchema>());
}

int TransitionSetSchema::size() const
{
	return std::vector<TransitionSchema*>::size();
}

std::string TransitionSetSchema::toString(const Context& c) const
{
	std::string str("");
	std::vector<TransitionSchema*>::const_iterator cit = begin();
	if(cit != end())
	{
		str.append((*cit)->toString(c));
		cit++;
	}
	for(; cit != end(); cit++)
	{
		str.push_back('\n');
		str.append((*cit)->toString(c));
	}
	return str;
}

TransitionSetSchema::iterator TransitionSetSchema::begin() 
{
	return TransitionSetSchema::iterator(std::vector<TransitionSchema*>::begin());
}

TransitionSetSchema::iterator TransitionSetSchema::end() 
{
	return TransitionSetSchema::iterator(std::vector<TransitionSchema*>::end());
}

TransitionSetSchema::const_iterator TransitionSetSchema::begin() const
{
	return TransitionSetSchema::const_iterator(std::vector<TransitionSchema*>::begin());
}

TransitionSetSchema::const_iterator TransitionSetSchema::end() const
{
	return TransitionSetSchema::const_iterator(std::vector<TransitionSchema*>::end());
}

TransitionSetSchema::reverse_iterator TransitionSetSchema::rbegin() 
{
	return TransitionSetSchema::reverse_iterator(std::vector<TransitionSchema*>::rbegin());
}

TransitionSetSchema::reverse_iterator TransitionSetSchema::rend() 
{
	return TransitionSetSchema::reverse_iterator(std::vector<TransitionSchema*>::rend());
}

TransitionSetSchema::const_reverse_iterator TransitionSetSchema::rbegin() const
{
	return TransitionSetSchema::const_reverse_iterator(std::vector<TransitionSchema*>::rbegin());
}

TransitionSetSchema::const_reverse_iterator TransitionSetSchema::rend() const
{
	return TransitionSetSchema::const_reverse_iterator(std::vector<TransitionSchema*>::rend());
}

