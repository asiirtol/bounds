#include "ltsschema.h"
#include <sstream>
#include <iostream>
#include <limits>
#include "template.h"
#include <list>
#include "context.h"
#include <iomanip>
#include "reduce.h"
#include <algorithm>
#include <new>

#ifdef _OPENMP
	#include "omp.h"
#endif

LTSSchema::LTSSchema() : types(std::set<TypeVariable>()), relvars(std::set<RelationVariable>()), atomvars(std::set<AtomVariable>()), qfvars(std::set<QuorumFunctionVariable>()), inputchans(std::set<Channel>()), outputchans(std::set<Channel>())
{
}

/*
LTSSchema::LTSSchema(const std::set<TypeVariable>& t, const std::set<RelationVariable>& r, const std::set<AtomVariable>& a) : types(t), relvars(r), atomvars(a)
{
}
*/

LTSSchema::LTSSchema(const LTSSchema& l) : types(l.types), relvars(l.relvars), atomvars(l.atomvars), qfvars(l.qfvars), inputchans(l.inputchans), outputchans(l.outputchans)
{
}

const std::set<TypeVariable>& LTSSchema::getTypeVariables() const
{
	return types;
}

const std::set<RelationVariable>& LTSSchema::getRelationVariables() const
{
	return relvars;
}

const std::set<AtomVariable>& LTSSchema::getAtomVariables() const
{
	return atomvars;
}

const std::set<QuorumFunctionVariable>& LTSSchema::getQuorumFunctionVariables() const
{
	return qfvars;
}

const std::set<Channel>& LTSSchema::getInputChannels() const
{
	return inputchans;
}

const std::set<Channel>& LTSSchema::getOutputChannels() const
{
	return outputchans;
}

bool LTSSchema::hasDisjointInputOutputChannels() const
{
	return disjoint<Channel>(inputchans, outputchans);
}

LTSSchema::~LTSSchema()
{
}

void LTSSchema::setNumericValue(VarType vt, int id, int var_id)
{
	for(std::set<TypeVariable>::iterator sit = types.begin(); sit != types.end(); ++sit)
	{
		const_cast<TypeVariable&>(*sit).setNumericValue(vt,id,var_id);
	}
	for(std::set<RelationVariable>::iterator sit = relvars.begin(); sit != relvars.end(); ++sit)
	{
		const_cast<RelationVariable&>(*sit).setNumericValue(vt,id,var_id);
	}
	for(std::set<AtomVariable>::iterator sit = atomvars.begin(); sit != atomvars.end(); ++sit)
	{
		const_cast<AtomVariable&>(*sit).setNumericValue(vt,id,var_id);
	}
	for(auto& qfv : qfvars)
		const_cast<QuorumFunctionVariable&>(qfv).setNumericValue(vt,id,var_id);
}

ValuationFormula* LTSSchema::prependValuationFormula(ValuationFormula*, Context&) const
{
	assert(false);

	return nullptr;
}

ElementaryLTSSchema::ElementaryLTSSchema(TransitionSetSchema* tss, StateSchema* is) : LTSSchema(), trsc(tss), initstate(is)
{
	assert(trsc != 0 && initstate != 0);

	atomvars.insert(initstate->getAtomVariables().begin(), initstate->getAtomVariables().end());
	atomvars.insert(trsc->getAtomVariables().begin(), trsc->getAtomVariables().end());
	types.insert(initstate->getTypeVariables().begin(), initstate->getTypeVariables().end());
	types.insert(trsc->getTypeVariables().begin(), trsc->getTypeVariables().end());
	relvars.insert(trsc->getRelationVariables().begin(), trsc->getRelationVariables().end());
	qfvars.insert(trsc->getQuorumFunctionVariables().begin(), trsc->getQuorumFunctionVariables().end());
	inputchans.insert(trsc->getInputChannels().begin(), trsc->getInputChannels().end());
	outputchans.insert(trsc->getOutputChannels().begin(), trsc->getOutputChannels().end());
}

ElementaryLTSSchema::ElementaryLTSSchema(const ElementaryLTSSchema& ltss) : LTSSchema(ltss), trsc(new TransitionSetSchema(*(ltss.trsc))), initstate(new StateSchema(*(ltss.initstate)))
{
}

ElementaryLTSSchema& ElementaryLTSSchema::operator=(const ElementaryLTSSchema& l)
{
	if(this == &l) return *this;
	ElementaryLTSSchema nl(l);
	std::swap(types, nl.types);
	std::swap(relvars, nl.relvars);
	std::swap(atomvars, nl.atomvars);
	std::swap(qfvars, nl.qfvars);
	std::swap(inputchans, nl.inputchans);
	std::swap(outputchans, nl.outputchans);
	std::swap(trsc, nl.trsc);
	std::swap(initstate, nl.initstate);
	return *this;
}

ElementaryLTSSchema::~ElementaryLTSSchema()
{
	delete trsc;
	delete initstate;
}

LTSSchema* ElementaryLTSSchema::clone() const
{
	assert(trsc != 0 && initstate != 0);

	return new ElementaryLTSSchema(*this);
}

const TransitionSetSchema& ElementaryLTSSchema::getTransitionSetSchema() const
{
	assert(trsc != 0);

	return *trsc;
}

const StateSchema& ElementaryLTSSchema::getStateSchema() const
{
	assert(initstate != 0);

	return *initstate;
}

/*
InstanceOfLTSSchema* ElementaryLTSSchema::instance(const Valuation& val) const
{
	return new ElementaryInstanceOfLTSSchema(this, val);
}
*/

PartialInstanceOfLTSSchema* ElementaryLTSSchema::partialInstance(const Valuation& val) const
{
	return new PartialInstanceOfElementaryLTSSchema(this, val);
}

bool ElementaryLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const AtomSet& subvals) const
{
	return (subvals.size() != 0);
}

bool ElementaryLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const TypeVariable& tv, const std::set<AtomSet>& subvals) const
{
	std::set<Atom> atoms;
	for(auto& avar : atomvars)
	{
		if(avar.second == tv)
		{
			atoms.insert(val.getValue(avar));
		}
	}
	for(auto& subval : subvals)
	{
		bool iscovered = true;
		for(auto& atom : atoms)
		{
			iscovered = iscovered && (subval.isElement(atom) == true);
		}
		if(iscovered) return true;
	}
	return false;
}

int ElementaryLTSSchema::numOfNestedQFVs(const Domain& tv) const
{
	assert(trsc != 0);

	int num = 0;
	for(auto& tr : *trsc)
		num += tr->numOfNestedQFVs(tv);
	return std::min(num, MAX_NESTED_QFVS);
}

int ElementaryLTSSchema::numOfNestedOccurrences(const Domain& tv) const
{
	assert(trsc != 0);

	int num = 0;
	for(TransitionSetSchema::const_iterator cit = trsc->begin(); cit != trsc->end(); cit++)
	{
		if((*cit)->numOfBoundOccurrences(tv) > 0) num = MAX_NESTED_OCCURRENCES;
	}
	return num;
}

std::set<std::pair<int,int>> ElementaryLTSSchema::occurrencesInBranches(const Domain& tv) const
{
	std::set<std::pair<int,int>> ocount;
	ocount.insert(std::make_pair(numOfNestedQFVs(tv), numOfNestedOccurrences(tv)));
	return std::move(ocount);
}

int ElementaryLTSSchema::numOfParallelOccurrences(const Domain& tv, const LTSSchema&, const std::map<TypeVariable,int>& co) const
{
	assert(trsc != 0);

	int num = 0;
	for(TransitionSetSchema::const_iterator cit = trsc->begin(); cit != trsc->end(); cit++)
	{
		num = std::max(num, (*cit)->numOfBoundOccurrences(tv));
	}
	return num;
}

int ElementaryLTSSchema::numOfParallelOccurrencesInAdjacentStates(const Domain& tv, const LTSSchema&, const std::map<TypeVariable,int>& co) const
{
	assert(trsc != 0);

	int num = 0;
	for(TransitionSetSchema::const_iterator cit = trsc->begin(); cit != trsc->end(); cit++)
	{
		num = std::max(num, (*cit)->numOfBoundOccurrencesInStates(tv));
	}
	return num;
}

bool ElementaryLTSSchema::isDataTypeVariable(const TypeVariable& tv) const
{
	return true;
}

bool ElementaryLTSSchema::isProcessTypeVariable(const TypeVariable& tv) const
{
	return (numOfNestedOccurrences(tv) == 0);
}

bool ElementaryLTSSchema::isHidingFree() const
{
	return true;
}

bool ElementaryLTSSchema::involvesConditionalHiding() const
{
	return false;
}

bool ElementaryLTSSchema::involvesInvisibleActions() const
{
	assert(trsc != 0);
	for(TransitionSetSchema::const_iterator ctit = trsc->begin(); ctit != trsc->end(); ctit++)
	{
		if((*ctit)->getActionSchema().chan == 0) return true;
	}
	return false;
}

/*
std::map<std::string, const ElementaryLTSSchema*> ElementaryLTSSchema::getNamedElementaryLTSSchemata() const
{
	return std::map<std::string, const ElementaryLTSSchema*>();
}
*/

void ElementaryLTSSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(trsc != 0 && initstate != 0);

	LTSSchema::setNumericValue(vt,id,var_id);
	trsc->setNumericValue(vt,id,var_id);
	initstate->setNumericValue(vt,id,var_id);
}

void ElementaryLTSSchema::setNumericValuesToBoundVariables(int val)
{
	assert(trsc != 0);

	trsc->setNumericValuesToBoundVariables(val);
}

// try to convert replicated choices related to the given type to replicated parallel compositions
LTSSchema* ElementaryLTSSchema::convertChoiceToParallelComposition(const TypeVariable& tv, bool abstract) const
{
	assert(trsc != 0 && initstate != 0);

	if(trsc->size() == 0)
	{
		return new ElementaryLTSSchema(*this);
	}
	std::set<AtomVariable> freevars = getAtomVariables();
	std::set<AtomVariable> choicevars;
	std::set<AtomVariable> primedchoicevars;
	TransitionSetSchema* newtrans = new TransitionSetSchema();
	// go through the transitions in order to find replicated choices related to the given type
	for(TransitionSetSchema::const_iterator cit = trsc->begin(); cit != trsc->end(); cit++)
	{
		// invisible actions are only allowed in the abstraction mode
		const ActionSchema& startact = (*cit)->getActionSchema();
		if(!abstract && startact.chan == 0)
		{
			delete newtrans;
			return new ElementaryLTSSchema(*this);
		}
		const StateSchema& loopstart = (*cit)->getFromState();
		const ParameterList& bndloopvars = (*cit)->getBoundParameters();
		const std::set<AtomVariable>& loopvars = loopstart.getAtomVariables();
		// search for bound variables of the given type
		bool avarfound = false;
		for(int i = 0; i < bndloopvars.size(); i++)
		{
			const AtomVariable& avar = bndloopvars[i]->getAtomVariable();
			if(avar.second == tv)
			{
				avarfound = true;
				// the atom variable must not occur free in the LTS schema
				if(getAtomVariables().find(avar) != getAtomVariables().end())
				{
					delete newtrans;
					return new ElementaryLTSSchema(*this);
				}
				choicevars.insert(avar);
				freevars.insert(avar);
				std::vector<const ActionSchema*> loopacts;
				if(startact.chan != 0) loopacts.push_back(&startact);

				// the atom variable must occur in the action schema precisely once
				if(!abstract && (*cit)->getActionSchema().numOfOccurrences(avar) != 1)
				{
					delete newtrans;
					return new ElementaryLTSSchema(*this);
				}
				// the atom variables of the start state of the loop must be preserved
				if(!abstract && !disjoint(bndloopvars.getAtomVariables(),loopvars))
				{
					delete newtrans;
					return new ElementaryLTSSchema(*this);
				}
				// create a new transition where the atom variable is not bound
				StateSchema* newfrom = new StateSchema(loopstart);
				ValuationFormula* newguard = (*cit)->getGuard().clone();
				ActionSchema* newact = new ActionSchema(startact);
				StateSchema* newto = new StateSchema((*cit)->getToState());
				newto->removeVariable(avar);
				ParameterList* newboundparams = new ParameterList(bndloopvars);
				newboundparams->removeVariable(avar);
				TransitionSchema* loopstarttr = new TransitionSchema(newfrom, newboundparams, newguard, newact, newto);
				newtrans->insert(loopstarttr);
				// unless the loop is trivial, create another transition where the atom variable is primed
				TransitionSchema* loopstarttr2 = 0;
				if((!(loopstart == *newto)) && (*cit)->getToState().isElement(avar))
				{
					StateSchema* newfrom = new StateSchema(loopstart);
					AtomVariable* primedvar = new AtomVariable(avar);
					primedvar->first = -(primedvar->first)-1;
					ValuationFormula* newconjunct = new NegatedValuationFormula(new EquivalenceValuationFormula(new AtomVariableSchema(new AtomVariable(avar)), new AtomVariableSchema(primedvar)));
					newguard = new ConjunctiveValuationFormula(newconjunct, (*cit)->getGuard().createPrimedVersion(avar));
					ActionSchema* newact = startact.createPrimedVersion(avar);
					StateSchema* newto = new StateSchema(loopstart);
					newto->removeVariable(avar);
					ParameterList* newboundparams = new ParameterList(bndloopvars);
					newboundparams->removeVariable(avar);
					loopstarttr2 = new TransitionSchema(newfrom, newboundparams, newguard, newact, newto);
					newtrans->insert(loopstarttr2);
					primedchoicevars.insert(avar);
					freevars.insert(avar.getPrimedVersion());
				}
				// search for all reachable states and actions
				std::vector<const StateSchema*> seen;
				seen.push_back(&loopstart);
				std::list<const StateSchema*> unvisited;
				unvisited.push_front(&((*cit)->getToState()));
				while(!unvisited.empty())
				{
					const StateSchema* from = unvisited.back();
					unvisited.pop_back();
					bool precisematch = false;
					bool checksuccessors = false;
					// check whether a type equivalent state is seen before
					std::pair<std::vector<const StateSchema*>::const_iterator, std::vector<const StateSchema*>::const_iterator> ssrange = std::equal_range(seen.begin(), seen.end(), from, PointerCompare<StateSchema>());
					if(ssrange.first != ssrange.second)
					{
						for(std::vector<const StateSchema*>::const_iterator cit3 = ssrange.first; cit3 != ssrange.second; cit3++)
						{
							// check for precise match
							if(*from == **cit3)
							{
								precisematch = true;
								break;
							}
							// check that the states match for the loop variables and the atom variable
							else if(!from->equalsInSet(**cit3,loopvars) || !from->equalsAtVariable(**cit3,avar))
							{
								delete newtrans;
								return new ElementaryLTSSchema(*this);
							}
						}
					}
					// if this state is not seen before
					const std::set<AtomVariable>& fromvars = from->getAtomVariables();
					if(!precisematch)
					{
						// check that the variables of the start state are preserved
						if(fromvars.find(avar) == fromvars.end())
						{
							if(abstract)
							{
								StateSchema* newfrom = new StateSchema(loopstart);
								ParameterList* newboundparams = new ParameterList();
								for(std::set<AtomVariable>::const_iterator cait = fromvars.begin(); cait != fromvars.end(); cait++)
								{
									if(loopvars.find(*cait) == loopvars.end())
									{
										newboundparams->push_back(new AtomVariableSchema(new AtomVariable(*cait)));
									}
								}
								ValuationFormula* newguard = new TopValuationFormula();
								ActionSchema* newact = new ActionSchema(0);
								StateSchema* newto = new StateSchema(*from);
								TransitionSchema* newtr = new TransitionSchema(newfrom, newboundparams, newguard, newact, newto);
								newtrans->insert(newtr);
							}
							else
							{
								delete newtrans;
								return new ElementaryLTSSchema(*this);
							}
						}
						else if(!subset(loopvars,fromvars))
						{
							delete newtrans;
							return new ElementaryLTSSchema(*this);
						}
						else
						{
							checksuccessors = true;
						}
						// insert to seen states
						std::vector<const StateSchema*>::iterator vit = std::partition(seen.begin(), seen.end(), PointerCompareAgainst<const StateSchema>(from));
						seen.insert(vit, from);
					}
					if(checksuccessors)
					{
						// go through the successors
						std::pair<TransitionSetSchema::const_iterator, TransitionSetSchema::const_iterator> range = trsc->getSuccessors(*from);
						for(TransitionSetSchema::const_iterator cit4 = range.first; cit4 != range.second; cit4++)
						{
							// the atom variable must occur in the action of the transition precisely once
							const ActionSchema& actsc = (*cit4)->getActionSchema();
							const std::set<AtomVariable>& actvars = actsc.getAtomVariables();
							if(actsc.numOfOccurrences(avar) != 1)
							{
								delete newtrans;
								return new ElementaryLTSSchema(*this);
							}
							// if the channel is seen before, it must match with the seen action for the atom variable
							std::pair<std::vector<const ActionSchema*>::const_iterator, std::vector<const ActionSchema*>::const_iterator> asrange = std::equal_range(loopacts.begin(), loopacts.end(), &actsc, PointerCompare<ActionSchema>());
							for(std::vector<const ActionSchema*>::const_iterator asit = asrange.first; asit != asrange.second; asit++)
							{
								if(!actsc.equalsAtVariable(**asit,avar))
								{
									delete newtrans;
									return new ElementaryLTSSchema(*this);
								}
							}
							// if the channel is not seen before, insert the action into the seen actions
							if(asrange.first == asrange.second)
							{
								// insert to seen actions
								std::vector<const ActionSchema*>::iterator vit = std::partition(loopacts.begin(), loopacts.end(), PointerCompareAgainst<const ActionSchema>(&actsc));
								loopacts.insert(vit, &actsc);
							}
							// the atom variables of the start state of the loop must be preserved
							const StateSchema& from2 = (*cit4)->getFromState();
							const std::set<AtomVariable>& fromvars2 = from2.getAtomVariables();
							if(!from->equalsInSet(from2, fromvars))
							{
								delete newtrans;
								return new ElementaryLTSSchema(*this);
							}
							if(!from->equalsAtVariable(from2, avar))
							{
								delete newtrans;
								return new ElementaryLTSSchema(*this);
							}
							const ParameterList& boundparams = (*cit4)->getBoundParameters();
							const std::set<AtomVariable>& boundvars = boundparams.getAtomVariables();
							if(!disjoint(fromvars,boundvars))
							{
								delete newtrans;
								return new ElementaryLTSSchema(*this);
							}
							const StateSchema& to = (*cit4)->getToState();
							const std::set<AtomVariable>& tovars = to.getAtomVariables();
							if(!subset(loopvars,tovars))
							{
								delete newtrans;
								return new ElementaryLTSSchema(*this);
							}
							// mark the state to be unvisited
							unvisited.push_front(&to);
						}
					}
				}
				// create new transitions
				std::pair<TransitionSetSchema::const_iterator, TransitionSetSchema::const_iterator> range = trsc->getSuccessors(loopstart);
				for(TransitionSetSchema::const_iterator tit = trsc->begin(); tit != trsc->end(); tit++)
				{
					const StateSchema& from2 = (*tit)->getFromState();
					// skip, if the transition starts from the init state of loop
					if(!(from2 == loopstart))
					{
						// check whether the action is a part of the loop
						const ActionSchema& actsc2 = (*tit)->getActionSchema();
						std::pair<std::vector<const ActionSchema*>::const_iterator, std::vector<const ActionSchema*>::const_iterator> asrange = std::equal_range(loopacts.begin(), loopacts.end(), &actsc2, PointerCompare<ActionSchema>());
						if(asrange.first != asrange.second)
						{
							// find the corresponding atom variable
							const AtomVariable* unbound;
							const ActionSchema& actsc = **(asrange.first);
							for(int k = 0; k < actsc.size(); k++)
							{
								const AtomVariableSchema* avsc = dynamic_cast<const AtomVariableSchema*>(actsc[k]);
								if(avsc != 0)
								{
									if(avsc->getAtomVariable() == avar)
									{
										unbound = &(static_cast<const AtomVariableSchema*>(actsc2[k])->getAtomVariable());
										if(actsc2.numOfOccurrences(*unbound) != 1)
										{
											delete newtrans;
											return new ElementaryLTSSchema(*this);
										}
									}
								}
							}
							const ParameterList& boundparams2 = ((*tit)->getBoundParameters());
							bool unboundprimed = (from2.numOfOccurrences(*unbound) + boundparams2.numOfOccurrences(*unbound) > 0);
							if(unboundprimed)
							{
								if((*tit)->getFromState() == (*tit)->getToState())
								{
									unboundprimed = false;
								}
								else if(abstract && (*tit)->getBoundParameters().isElement(*unbound) && !(*tit)->getToState().isElement(*unbound))
								{
									unboundprimed = false;
								}
								else
								{
									primedchoicevars.insert(*unbound);
								}
								choicevars.insert(*unbound);
							}
							bool createtwotrans = unboundprimed;
							do
							{
								createtwotrans = unboundprimed;
								StateSchema* newfrom = new StateSchema(loopstart);
								ParameterList* newboundparams = new ParameterList();
								for(int k = 0; k < from2.size(); k++)
								{
									if(static_cast<const AtomVariableSchema*>(from2[k])->getAtomVariable() != *unbound)
									{
										newboundparams->push_back(from2[k]->clone());
									}
								}
								for(int k = 0; k < boundparams2.size(); k++)
								{
									if(boundparams2[k]->getAtomVariable() != *unbound)
									{
										newboundparams->push_back(boundparams2[k]->clone());
									}
								}
								ValuationFormula* newguard;
								ActionSchema* newact;
								AtomVariable* thevar = new AtomVariable(*unbound);
								ValuationFormula* newconjunct = new NegatedValuationFormula(new EquivalenceValuationFormula(new AtomVariableSchema(new AtomVariable(avar)), new AtomVariableSchema(thevar)));
								if(unboundprimed)
								{
									thevar->first = -(thevar->first)-1;
									newguard = new ConjunctiveValuationFormula(newconjunct, (*tit)->getGuard().createPrimedVersion(*unbound));
									newact = (*tit)->getActionSchema().createPrimedVersion(*unbound);
								}
								else
								{
									newguard = new ConjunctiveValuationFormula(newconjunct, (*tit)->getGuard().clone());
									newact = new ActionSchema((*tit)->getActionSchema());
								}
								// if the action is already enabled in the init state of the loop, strengthen the guard
								for(TransitionSetSchema::const_iterator enit = range.first; enit != range.second; enit++)
								{
									if((*enit)->getActionSchema().equalsAtVariable(*newact, *thevar))
									{
										ValuationFormula* newconjunct = new TopValuationFormula();
										std::list<ValuationFormula*> cons = (*enit)->getGuard().conjuncts();
										for(std::list<ValuationFormula*>::const_iterator clit = cons.begin(); clit != cons.end(); clit++)
										{
											if(subset((*clit)->getAtomVariables(),freevars))
											{
												newconjunct = new ConjunctiveValuationFormula(newconjunct, *clit);
											}
											else
											{
												delete *clit;
											}
										}
										newguard = new ConjunctiveValuationFormula(new NegatedValuationFormula(newconjunct), newguard);
									}
								}
								if(startact.equalsAtVariable(*newact, *thevar))
								{
									ValuationFormula* newconjunct = new TopValuationFormula();
									std::list<ValuationFormula*> cons = (*cit)->getGuard().conjuncts();
									for(std::list<ValuationFormula*>::const_iterator clit = cons.begin(); clit != cons.end(); clit++)
									{
										if(subset((*clit)->getAtomVariables(),freevars))
										{
											newconjunct = new ConjunctiveValuationFormula(newconjunct, *clit);
										}
										else
										{
											delete *clit;
										}
									}
									newguard = new ConjunctiveValuationFormula(new NegatedValuationFormula(newconjunct), newguard);
								}
								if(loopstarttr2 != 0)
								{
									if(loopstarttr2->getActionSchema().equalsAtVariable(*newact, *thevar))
									{
										ValuationFormula* newconjunct = new TopValuationFormula();
										std::list<ValuationFormula*> cons = loopstarttr2->getGuard().conjuncts();
										for(std::list<ValuationFormula*>::const_iterator clit = cons.begin(); clit != cons.end(); clit++)
										{
											if(subset((*clit)->getAtomVariables(),freevars))
											{
												newconjunct = new ConjunctiveValuationFormula(newconjunct, *clit);
											}
											else
											{
												delete *clit;
											}
										}
										newguard = new ConjunctiveValuationFormula(new NegatedValuationFormula(newconjunct), newguard);
									}
								}
								StateSchema* newto = new StateSchema(loopstart);
								TransitionSchema* newtr = new TransitionSchema(newfrom, newboundparams, newguard, newact, newto);
								newtrans->insert(newtr);
								unboundprimed = false;
							}
							while(createtwotrans);
						}
					}
				}
			}
		}
		if(!avarfound)
		{
			StateSchema* newfrom = new StateSchema((*cit)->getFromState());
			newfrom->removeVariables(tv);
			StateSchema* newto = new StateSchema((*cit)->getToState());
			newto->removeVariables(tv);
			ParameterList* newparams = new ParameterList((*cit)->getBoundParameters());
			ValuationFormula* newguard = (*cit)->getGuard().clone();
			ActionSchema* newact = new ActionSchema((*cit)->getActionSchema());
			TransitionSchema* trsc = new TransitionSchema(newfrom, newparams, newguard, newact, newto);
			newtrans->insert(trsc);
		}
	}
	LTSSchema* ltsc = new ElementaryLTSSchema(newtrans, new StateSchema(this->getStateSchema()));
	for(std::set<AtomVariable>::const_iterator cait = choicevars.begin(); cait != choicevars.end(); cait++)
	{
		ltsc = new ReplicatedLTSSchema(new AtomVariable(*cait), ltsc);
	}
	for(std::set<AtomVariable>::const_iterator cait = primedchoicevars.begin(); cait != primedchoicevars.end(); cait++)
	{
		AtomVariable* primedvar = new AtomVariable(*cait);
		primedvar->first = -(primedvar->first)-1;
		ltsc = new ReplicatedLTSSchema(primedvar, ltsc);
	}
	return ltsc;
}

std::list<SyntaxBranchOfLTSSchema> ElementaryLTSSchema::getSyntaxBranches() const
{
	SyntaxBranchOfLTSSchema new_branch;
	new_branch.push_front(this);
	std::list<SyntaxBranchOfLTSSchema> new_set;
	new_set.push_front(std::move(new_branch));
	return new_set;
}

ValuationFormula* ElementaryLTSSchema::prependValuationFormula(ValuationFormula* vf, Context&) const
{
/*
	for(auto& av : atomvars)
	{
		auto avp = av.getPrimedVersion();
		vf = new ConjunctiveValuationFormula(new EquivalenceValuationFormula(new AtomVariableSchema(new AtomVariable(avp)), new AtomVariableSchema(new AtomVariable(av))), vf);
	}
*/
	return vf;
}

std::set<RelationVariable> ElementaryLTSSchema::getPredicates(bool mode) const
{
	return {};
}

std::string ElementaryLTSSchema::toString(const Context& c) const
{
	assert(trsc != 0 && initstate != 0);

	std::string str("(LTS \n");
	str.append(trsc->toString(c));
	str.append(" from ");
	str.append(initstate->toString(c));
	str.append(")\n");
	return str;
}

ParallelLTSSchema::ParallelLTSSchema(const LTSSchema& ls1, const LTSSchema& ls2) : ParallelLTSSchema(ls1.clone(), ls2.clone())
{
}

ParallelLTSSchema::ParallelLTSSchema(LTSSchema* ls1, LTSSchema* ls2) : LTSSchema(), ltss1(ls1), ltss2(ls2)
{
	assert(ltss1 != 0 && ltss2 != 0);

	types.insert(ltss1->getTypeVariables().begin(), ltss1->getTypeVariables().end());
	relvars.insert(ltss1->getRelationVariables().begin(), ltss1->getRelationVariables().end());
	atomvars.insert(ltss1->getAtomVariables().begin(), ltss1->getAtomVariables().end());
	qfvars.insert(ltss1->getQuorumFunctionVariables().begin(), ltss1->getQuorumFunctionVariables().end());
	inputchans.insert(ltss1->getInputChannels().begin(), ltss1->getInputChannels().end());
	outputchans.insert(ltss1->getOutputChannels().begin(), ltss1->getOutputChannels().end());
	types.insert(ltss2->getTypeVariables().begin(), ltss2->getTypeVariables().end());
	relvars.insert(ltss2->getRelationVariables().begin(), ltss2->getRelationVariables().end());
	atomvars.insert(ltss2->getAtomVariables().begin(), ltss2->getAtomVariables().end());
	qfvars.insert(ltss2->getQuorumFunctionVariables().begin(), ltss2->getQuorumFunctionVariables().end());
	inputchans.insert(ltss2->getInputChannels().begin(), ltss2->getInputChannels().end());
	outputchans.insert(ltss2->getOutputChannels().begin(), ltss2->getOutputChannels().end());
}

ParallelLTSSchema::ParallelLTSSchema(const ParallelLTSSchema& pltss) : LTSSchema(pltss), ltss1(pltss.ltss1->clone()), ltss2(pltss.ltss2->clone())
{
}

ParallelLTSSchema& ParallelLTSSchema::operator=(const ParallelLTSSchema& pltss)
{
	ParallelLTSSchema pls(pltss);
	std::swap(types, pls.types);
	std::swap(relvars, pls.relvars);
	std::swap(atomvars, pls.atomvars);
	std::swap(qfvars, pls.qfvars);
	std::swap(inputchans, pls.inputchans);
	std::swap(outputchans, pls.outputchans);
	std::swap(ltss1, pls.ltss1);
	std::swap(ltss2, pls.ltss2);
	return *this;
}

LTSSchema* ParallelLTSSchema::clone() const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return new ParallelLTSSchema(*this);
}

ParallelLTSSchema::~ParallelLTSSchema()
{
	delete ltss1;
	delete ltss2;
}

const LTSSchema& ParallelLTSSchema::oneLTSSchema() const
{
	assert(ltss1 != 0);

	return *ltss1;
}

const LTSSchema& ParallelLTSSchema::theOtherLTSSchema() const
{
	assert(ltss2 != 0);

	return *ltss2;
}

/*
InstanceOfLTSSchema* ParallelLTSSchema::instance(const Valuation& val) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return new ParallelInstanceOfLTSSchema(ltss1->instance(val), ltss2->instance(val));
}
*/

PartialInstanceOfLTSSchema* ParallelLTSSchema::partialInstance(const Valuation& val) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	PartialInstanceOfLTSSchema* pi1 = ltss1->partialInstance(val);
	PartialInstanceOfLTSSchema* pi2 = ltss2->partialInstance(val);

	PartialInstanceOfGeneralisedParallelLTSSchema* gpi1 = dynamic_cast<PartialInstanceOfGeneralisedParallelLTSSchema*>(pi1);
	PartialInstanceOfGeneralisedParallelLTSSchema* gpi2 = dynamic_cast<PartialInstanceOfGeneralisedParallelLTSSchema*>(pi2);

	// check for associativity
	const std::set<Channel>& inchan1 = ltss1->getInputChannels();
	const std::set<Channel>& outchan1 = ltss1->getOutputChannels();
	const std::set<Channel>& inchan2 = ltss2->getInputChannels();
	const std::set<Channel>& outchan2 = ltss2->getOutputChannels();
	if(ltss1->hasDisjointInputOutputChannels() && ltss2->hasDisjointInputOutputChannels() && disjoint<Channel>(inchan1, outchan2) && disjoint<Channel>(inchan2,outchan2))
	{
		if(gpi1 != 0 && gpi2 != 0)
		{
			gpi1->insert(gpi1->end(), gpi2->begin(), gpi2->end());
			delete gpi2;
			return gpi1;
		}
		else if(gpi1 != 0)
		{
			gpi1->push_back(pi2);
			return gpi1;
		}
		else if(gpi2 != 0)
		{
			gpi2->push_back(pi1);
			return gpi2;
		}
	}
	PartialInstanceOfGeneralisedParallelLTSSchema* pi = new PartialInstanceOfGeneralisedParallelLTSSchema();
	pi->push_back(pi1);
	pi->push_back(pi2);
	return pi;
}

bool ParallelLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const AtomSet& subvals) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return (ltss1->instanceCoveredBySmallerOnes(val,subvals) && ltss2->instanceCoveredBySmallerOnes(val,subvals));
}

bool ParallelLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const TypeVariable& tv, const std::set<AtomSet>& subvals) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return (ltss1->instanceCoveredBySmallerOnes(val,tv,subvals) && ltss2->instanceCoveredBySmallerOnes(val,tv,subvals));
}

int ParallelLTSSchema::numOfNestedQFVs(const Domain& tv) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return std::max(ltss1->numOfNestedQFVs(tv), ltss2->numOfNestedQFVs(tv));
}

int ParallelLTSSchema::numOfNestedOccurrences(const Domain& tv) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return std::max(ltss1->numOfNestedOccurrences(tv), ltss2->numOfNestedOccurrences(tv));
}

std::set<std::pair<int,int>> ParallelLTSSchema::occurrencesInBranches(const Domain& tv) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	auto ocount1 = ltss1->occurrencesInBranches(tv);
	auto ocount2 = ltss2->occurrencesInBranches(tv);
	ocount1.insert(ocount2.begin(), ocount2.end());
	return std::move(ocount1);
}

int ParallelLTSSchema::numOfParallelOccurrences(const Domain& tv, const LTSSchema& ltsc, const std::map<TypeVariable,int>& co) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return std::max(ltss1->numOfParallelOccurrences(tv, ltsc, co) + ltss2->numOfParallelOccurrencesInAdjacentStates(tv, ltsc, co), ltss1->numOfParallelOccurrencesInAdjacentStates(tv, ltsc, co) + ltss2->numOfParallelOccurrences(tv, ltsc, co));
}

int ParallelLTSSchema::numOfParallelOccurrencesInAdjacentStates(const Domain& tv, const LTSSchema& ltsc, const std::map<TypeVariable,int>& co) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return ltss1->numOfParallelOccurrencesInAdjacentStates(tv, ltsc, co) + ltss2->numOfParallelOccurrencesInAdjacentStates(tv, ltsc, co);
}

bool ParallelLTSSchema::isProcessTypeVariable(const TypeVariable& tv) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return (ltss1->isProcessTypeVariable(tv) && ltss2->isProcessTypeVariable(tv));
}

bool ParallelLTSSchema::isDataTypeVariable(const TypeVariable& tv) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return (ltss1->isDataTypeVariable(tv) && ltss2->isDataTypeVariable(tv));
}

bool ParallelLTSSchema::isHidingFree() const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return (ltss1->isHidingFree() && ltss2->isHidingFree());
}

bool ParallelLTSSchema::involvesConditionalHiding() const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return (ltss1->involvesConditionalHiding() || ltss2->involvesConditionalHiding());
}

bool ParallelLTSSchema::involvesInvisibleActions() const
{
	assert(ltss1 != 0 && ltss2 != 0);

	return (ltss1->involvesInvisibleActions() || ltss2->involvesInvisibleActions());
}

/*
std::map<std::string, const ElementaryLTSSchema*> ParallelLTSSchema::getNamedElementaryLTSSchemata() const
{
	std::map<std::string, const ElementaryLTSSchema*> map1 = ltss1->getNamedElementaryLTSSchemata();
	const std::map<std::string, const ElementaryLTSSchema*>& map2 = ltss2->getNamedElementaryLTSSchemata();
	for(std::map<std::string, const ElementaryLTSSchema*>::const_reverse_iterator cit = map2.rbegin(); cit != map2.rend(); ++cit)
	{
		map1.insert(*cit);
	}
	return map1;
}
*/

void ParallelLTSSchema::setNumericValue(VarType vt, int id, int var_id)
{
	LTSSchema::setNumericValue(vt,id,var_id);
	ltss1->setNumericValue(vt,id,var_id);
	ltss2->setNumericValue(vt,id,var_id);
}

void ParallelLTSSchema::setNumericValuesToBoundVariables(int val)
{
	ltss1->setNumericValuesToBoundVariables(val);
	ltss2->setNumericValuesToBoundVariables(val);
}

LTSSchema* ParallelLTSSchema::convertChoiceToParallelComposition(const TypeVariable& tv, bool abs) const
{
	assert(ltss1 != 0 && ltss2 != 0);
	return new ParallelLTSSchema(ltss1->convertChoiceToParallelComposition(tv,abs), ltss2->convertChoiceToParallelComposition(tv,abs));
}

std::list<SyntaxBranchOfLTSSchema> ParallelLTSSchema::getSyntaxBranches() const
{
	assert(ltss1 != 0 && ltss2 != 0);

	std::list<SyntaxBranchOfLTSSchema> branches1 = ltss1->getSyntaxBranches();
	std::list<SyntaxBranchOfLTSSchema> branches2 = ltss2->getSyntaxBranches();
	branches1.splice(branches1.end(), branches2);
	return branches1;
}

std::set<RelationVariable> ParallelLTSSchema::getPredicates(bool mode) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	auto preds1 = ltss1->getPredicates(mode);
	auto preds2 = ltss2->getPredicates(mode);
	preds1.insert(preds2.begin(), preds2.end());
	return preds1;
}

std::string ParallelLTSSchema::toString(const Context& c) const
{
	assert(ltss1 != 0 && ltss2 != 0);

	std::string str(1,'(');
	str.append(ltss1->toString(c));
	str.push_back('|');
	str.push_back('|');
	str.append(ltss2->toString(c));
	str.push_back(')');
	return str;
}

ReplicatedLTSSchema::ReplicatedLTSSchema(const AtomVariable& a, const LTSSchema& l) : ReplicatedLTSSchema(new AtomVariable(a), l.clone())
{
}

ReplicatedLTSSchema::ReplicatedLTSSchema(AtomVariable* a, LTSSchema* l) : LTSSchema(), avar(a), ltss(l)
{
	assert(a != 0 && ltss != 0);

	types.insert(ltss->getTypeVariables().begin(), ltss->getTypeVariables().end());
	relvars.insert(ltss->getRelationVariables().begin(), ltss->getRelationVariables().end());
	atomvars.insert(ltss->getAtomVariables().begin(), ltss->getAtomVariables().end());
	qfvars.insert(ltss->getQuorumFunctionVariables().begin(), ltss->getQuorumFunctionVariables().end());
	inputchans.insert(ltss->getInputChannels().begin(), ltss->getInputChannels().end());
	outputchans.insert(ltss->getOutputChannels().begin(), ltss->getOutputChannels().end());
	if(avar->second.second == TYPEVAR)
	{
		types.insert(avar->second);
	}
	std::set<AtomVariable>::iterator it = atomvars.find(*avar);
	if(it != atomvars.end()) atomvars.erase(it);
}

ReplicatedLTSSchema::ReplicatedLTSSchema(const ReplicatedLTSSchema& l) : LTSSchema(l), avar(new AtomVariable(*(l.avar))), ltss(l.ltss->clone())
{
}

ReplicatedLTSSchema& ReplicatedLTSSchema::operator=(const ReplicatedLTSSchema& l)
{
	ReplicatedLTSSchema rpls(l);
	std::swap(types, rpls.types);
	std::swap(relvars, rpls.relvars);
	std::swap(atomvars, rpls.atomvars);
	std::swap(qfvars, rpls.qfvars);
	std::swap(inputchans, rpls.inputchans);
	std::swap(outputchans, rpls.outputchans);
	std::swap(avar, rpls.avar);
	std::swap(ltss, rpls.ltss);
	return *this;
}

LTSSchema* ReplicatedLTSSchema::clone() const
{
	assert(avar != 0 && ltss != 0);

	return new ReplicatedLTSSchema(*this);
}

ReplicatedLTSSchema::~ReplicatedLTSSchema()
{
	delete ltss;
	delete avar;
}

const LTSSchema& ReplicatedLTSSchema::getLTSSchema() const
{
	assert(ltss != 0);

	return *ltss;
}

const AtomVariable& ReplicatedLTSSchema::getAtomVariable() const
{
	assert(avar != 0);

	return *avar;
}

/*
InstanceOfLTSSchema* ReplicatedLTSSchema::instance(const Valuation& val) const
{
	assert(avar != 0 && ltss != 0);

	InstanceOfLTSSchema* inst = new UnitLTS();
	std::list<Valuation*> vals = val.addAtomVariable(avar->second);
	for(std::list<Valuation*>::iterator it = vals.begin(); it != vals.end(); ++it)
	{
		inst = new ParallelInstanceOfLTSSchema(inst, ltss->instance(**it));
		delete *it;
	}
	return inst;
}
*/

PartialInstanceOfLTSSchema* ReplicatedLTSSchema::partialInstance(const Valuation& val) const
{
	assert(avar != 0 && ltss != 0);

	PartialInstanceOfGeneralisedParallelLTSSchema* pinst = new PartialInstanceOfGeneralisedParallelLTSSchema();
	std::list<Valuation*> vals = val.addAtomVariable(avar->second);
	for(std::list<Valuation*>::iterator it = vals.begin(); it != vals.end(); ++it)
	{
		PartialInstanceOfLTSSchema* inst2 = ltss->partialInstance(**it);
		PartialInstanceOfGeneralisedParallelLTSSchema* pinst2 = dynamic_cast<PartialInstanceOfGeneralisedParallelLTSSchema*>(inst2);
		if(pinst2 == 0)
		{
			pinst->push_back(inst2);
		}
		else
		{
			pinst->insert(pinst->end(), pinst2->begin(), pinst2->end());
			delete pinst2;
		}
	}
	return pinst;
}

bool ReplicatedLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const AtomSet& subvals) const
{
	assert(avar != 0 && ltss != 0);

	bool iscovered = true;
	std::list<Valuation*> vals = val.addAtomVariable(avar->second);
	for(std::list<Valuation*>::iterator it = vals.begin(); it != vals.end(); ++it)
	{
		AtomSet extsubvals(subvals);
		Atom a = (*it)->getValue(*avar);
		extsubvals.erase(a);
		iscovered = iscovered && ltss->instanceCoveredBySmallerOnes(**it,extsubvals);
		delete *it;
	}
	return iscovered;
}

bool ReplicatedLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const TypeVariable& tv, const std::set<AtomSet>& subvals) const
{
	assert(avar != 0 && ltss != 0);

	bool iscovered = true;
	auto extvals = val.addAtomVariable(avar->second);
	for(auto& extval : extvals)
	{
		if(avar->second == tv)
		{
			Atom a = extval->getValue(*avar);
			std::set<AtomSet> extsubvals;
			for(auto& subval : subvals)
			{
				if(subval.isElement(a))
				{
					extsubvals.insert(subval);
				}
			}
			iscovered = iscovered && ltss->instanceCoveredBySmallerOnes(*extval,tv,extsubvals);
		}
		else
		{
			iscovered = iscovered && ltss->instanceCoveredBySmallerOnes(*extval,tv,subvals);
		}
		delete extval;
	}
	return iscovered;
}

int ReplicatedLTSSchema::numOfNestedQFVs(const Domain& tv) const
{
	assert(ltss != 0);

	return ltss->numOfNestedQFVs(tv);
}

int ReplicatedLTSSchema::numOfNestedOccurrences(const Domain& tv) const
{
	assert(avar != 0 && ltss != 0);

	if(tv == avar->second)
	{
		return (1 + ltss->numOfNestedOccurrences(tv));
	}
	else
	{
		return ltss->numOfNestedOccurrences(tv);
	}
}

std::set<std::pair<int,int>> ReplicatedLTSSchema::occurrencesInBranches(const Domain& tv) const
{
	assert(avar != 0 && ltss != 0);

	auto ocount = ltss->occurrencesInBranches(tv);
	if(tv == avar->second)
	{
		std::set<std::pair<int,int>> ocount2;
		for(auto& cpair : ocount)
		{
			ocount2.insert(std::make_pair(cpair.first, cpair.second + 1));
		}
		std::swap(ocount, ocount2);
	}
	return std::move(ocount);
}

int ReplicatedLTSSchema::numOfParallelOccurrences(const Domain& tv, const LTSSchema& ltsc, const std::map<TypeVariable,int>& co) const
{
	assert(avar != 0 && ltss != 0);

	if(tv == avar->second)
	{
		if(co.find(avar->second) == co.end())
		{
			return CUTOFFMAX + 1;
		}
		else
		{
			return ((co.at(avar->second) - 1) * ltss->numOfParallelOccurrencesInAdjacentStates(tv,ltsc,co) + ltss->numOfParallelOccurrences(tv,ltsc,co));
		}
	}
	else if(avar->second.second == TYPEVAR)
	{
		return ((ltsc.numOfNestedOccurrences(avar->second) - 1) * ltss->numOfParallelOccurrencesInAdjacentStates(tv,ltsc,co) + ltss->numOfParallelOccurrences(tv,ltsc,co));
	}
	else /* if(avar->second.second == FINTYPE) */
	{
		return ((avar->second.id - 1) * ltss->numOfParallelOccurrencesInAdjacentStates(tv,ltsc,co) + ltss->numOfParallelOccurrences(tv,ltsc,co));
	}
}

int ReplicatedLTSSchema::numOfParallelOccurrencesInAdjacentStates(const Domain& tv, const LTSSchema& ltsc, const std::map<TypeVariable,int>& co) const
{
	assert(avar != 0 && ltss != 0);

	if(tv == avar->second)
	{
		if(co.find(avar->second) == co.end())
		{
			return CUTOFFMAX + 1;
		}
		else
		{
			return (co.at(avar->second) * ltss->numOfParallelOccurrencesInAdjacentStates(tv,ltsc,co));
		}
	}
	else if(avar->second.second == TYPEVAR)
	{
		return (ltsc.numOfNestedOccurrences(avar->second) * ltss->numOfParallelOccurrencesInAdjacentStates(tv,ltsc,co));
	}
	else /* if(avar->second.second == FINTYPE) */
	{
		return (avar->second.id * ltss->numOfParallelOccurrencesInAdjacentStates(tv,ltsc,co));
	}
}

bool ReplicatedLTSSchema::isProcessTypeVariable(const TypeVariable& tv) const
{
	assert(ltss != 0);

	return ltss->isProcessTypeVariable(tv);
}

bool ReplicatedLTSSchema::isDataTypeVariable(const TypeVariable& tv) const
{
	assert(ltss != 0);

	if(tv == avar->second)
	{
		return false;
	}
	else
	{
		return ltss->isDataTypeVariable(tv);
	}
}

bool ReplicatedLTSSchema::isHidingFree() const
{
	assert(ltss != 0);

	return ltss->isHidingFree();
}

bool ReplicatedLTSSchema::involvesConditionalHiding() const
{
	assert(ltss != 0);

	return ltss->involvesConditionalHiding();
}

bool ReplicatedLTSSchema::involvesInvisibleActions() const
{
	assert(ltss != 0);

	return ltss->involvesInvisibleActions();
}

/*
std::map<std::string, const ElementaryLTSSchema*> ReplicatedLTSSchema::getNamedElementaryLTSSchemata() const
{
	assert(ltss != 0);

	return ltss->getNamedElementaryLTSSchemata();
}
*/

void ReplicatedLTSSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(avar != 0 && ltss != 0);

	LTSSchema::setNumericValue(vt, id, var_id);
	if(vt != ATOMVAR || avar->first != id)
	{
		avar->setNumericValue(vt,id,var_id);
		ltss->setNumericValue(vt,id,var_id);
	}
}

void ReplicatedLTSSchema::setNumericValuesToBoundVariables(int val)
{
	assert(avar != 0 && ltss != 0);

	avar->id = val;
	ltss->setNumericValue(ATOMVAR, avar->first, val);
	ltss->setNumericValuesToBoundVariables(val + 1);
}

LTSSchema* ReplicatedLTSSchema::convertChoiceToParallelComposition(const TypeVariable& tv, bool abs) const
{
	assert(ltss != 0 && avar != 0);
	return new ReplicatedLTSSchema(new AtomVariable(*avar), ltss->convertChoiceToParallelComposition(tv,abs));
}

std::list<SyntaxBranchOfLTSSchema> ReplicatedLTSSchema::getSyntaxBranches() const
{
	assert(ltss != 0);

	std::list<SyntaxBranchOfLTSSchema> branches = ltss->getSyntaxBranches();
	for(auto& b : branches)
	{
		b.push_front(this);
	}
	return branches;
}

ValuationFormula* ReplicatedLTSSchema::prependValuationFormula(ValuationFormula* vf, Context& c) const
{
	AtomVariable* temp_av = c.createAtomVariable(avar->second);
	ValuationFormula* freevar = new EquivalenceValuationFormula(new AtomVariableSchema(temp_av), new AtomVariableSchema(new AtomVariable(*avar)));
	return new ExistentialValuationFormula(new AtomVariable(*avar), new ConjunctiveValuationFormula(freevar, vf));
}

std::set<RelationVariable> ReplicatedLTSSchema::getPredicates(bool mode) const
{
	assert(ltss != 0);

	return ltss->getPredicates(mode);
}

std::string ReplicatedLTSSchema::toString(const Context& c) const
{
	assert(avar != 0 && ltss != 0);

	std::string str(1,'(');
	str.push_back('|');
	str.push_back('|');
	str.append(avar->toString(c));
	str.push_back(':');
	str.append(ltss->toString(c));
	str.push_back(')');
	return str;
}

ConditionLTSSchema::ConditionLTSSchema(const LTSSchema& l, const ValuationFormula& c) : ConditionLTSSchema(l.clone(), c.clone())
{
}

ConditionLTSSchema::ConditionLTSSchema(LTSSchema* l, ValuationFormula* c) : LTSSchema(), ltss(l), cond(c)
{
	assert(ltss != 0 && cond != 0);

	types.insert(ltss->getTypeVariables().begin(), ltss->getTypeVariables().end());
	relvars.insert(ltss->getRelationVariables().begin(), ltss->getRelationVariables().end());
	atomvars.insert(ltss->getAtomVariables().begin(), ltss->getAtomVariables().end());
	qfvars.insert(ltss->getQuorumFunctionVariables().begin(), ltss->getQuorumFunctionVariables().end());
	inputchans.insert(ltss->getInputChannels().begin(), ltss->getInputChannels().end());
	outputchans.insert(ltss->getOutputChannels().begin(), ltss->getOutputChannels().end());
	types.insert(cond->getTypeVariables().begin(), cond->getTypeVariables().end());
	relvars.insert(cond->getRelationVariables().begin(), cond->getRelationVariables().end());
	atomvars.insert(cond->getAtomVariables().begin(), cond->getAtomVariables().end());
	qfvars.insert(cond->getQuorumFunctionVariables().begin(), cond->getQuorumFunctionVariables().end());
}

ConditionLTSSchema::ConditionLTSSchema(const ConditionLTSSchema& l) : LTSSchema(l), ltss(l.ltss->clone()), cond(l.cond->clone())
{
}

ConditionLTSSchema& ConditionLTSSchema::operator=(const ConditionLTSSchema& l)
{
	ConditionLTSSchema cls(l);
	std::swap(types, cls.types);
	std::swap(relvars, cls.relvars);
	std::swap(atomvars, cls.atomvars);
	std::swap(qfvars, cls.qfvars);
	std::swap(inputchans, cls.inputchans);
	std::swap(outputchans, cls.outputchans);
	std::swap(cond, cls.cond);
	std::swap(ltss, cls.ltss);
	return *this;
}

LTSSchema* ConditionLTSSchema::clone() const
{
	assert(cond != 0 && ltss != 0);

	return new ConditionLTSSchema(*this);
}

ConditionLTSSchema::~ConditionLTSSchema()
{
	delete ltss;
	delete cond;
}

const LTSSchema& ConditionLTSSchema::getLTSSchema() const
{
	assert(ltss != 0);

	return *ltss;
}

const ValuationFormula& ConditionLTSSchema::valuationFormula() const
{
	assert(cond != 0);

	return *cond;
}

PartialInstanceOfLTSSchema* ConditionLTSSchema::partialInstance(const Valuation& val) const
{
	assert(cond != 0 && ltss != 0);

	if(cond->instance(val))
	{
		return ltss->partialInstance(val);
	}
	else
	{
		return new PartialInstanceOfGeneralisedParallelLTSSchema();
	}
}

bool ConditionLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const AtomSet& subvals) const
{
	assert(cond != 0 && ltss != 0);

	if(cond->instance(val))
	{
		return ltss->instanceCoveredBySmallerOnes(val,subvals);
	}
	else
	{
		return true;
	}
}

bool ConditionLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const TypeVariable& tv, const std::set<AtomSet>& subvals) const
{
	assert(cond != 0 && ltss != 0);

	if(cond->instance(val))
	{
		std::set<AtomSet> truesubvals;
		for(auto& subval : subvals)
		{
			if(cond->instanceCoveredBySmallerOne(val, tv, subval))
			{
				truesubvals.insert(subval);
			}
		}
		return ltss->instanceCoveredBySmallerOnes(val,tv,truesubvals);
	}
	else
	{
		return true;
	}
}

int ConditionLTSSchema::numOfNestedQFVs(const Domain& tv) const
{
	assert(cond != 0 && ltss != 0);

	return cond->numOfNestedQFVs(tv) + ltss->numOfNestedQFVs(tv);
}

int ConditionLTSSchema::numOfNestedOccurrences(const Domain& tv) const
{
	assert(ltss != 0);

	return ltss->numOfNestedOccurrences(tv);
}

std::set<std::pair<int,int>> ConditionLTSSchema::occurrencesInBranches(const Domain& tv) const
{
	assert(cond != 0 && ltss != 0);

	auto ocount = ltss->occurrencesInBranches(tv);
	std::set<std::pair<int,int>> ocount2;
	int nestedqfvs = cond->numOfNestedQFVs(tv);
	for(auto& cpair : ocount)
	{
		ocount2.insert(std::make_pair(cpair.first + nestedqfvs, cpair.second));
	}
	std::swap(ocount, ocount2);
	return std::move(ocount);
}

int ConditionLTSSchema::numOfParallelOccurrences(const Domain& tv, const LTSSchema& ltsc, const std::map<TypeVariable,int>& co) const
{
	assert(ltss != 0);

	return ltss->numOfParallelOccurrences(tv, ltsc, co);
}

int ConditionLTSSchema::numOfParallelOccurrencesInAdjacentStates(const Domain& tv, const LTSSchema& ltsc, const std::map<TypeVariable,int>& co) const
{
	assert(ltss != 0);

	return ltss->numOfParallelOccurrencesInAdjacentStates(tv, ltsc, co);
}

bool ConditionLTSSchema::isProcessTypeVariable(const TypeVariable& tv) const
{
	assert(ltss != 0);

	return ltss->isProcessTypeVariable(tv);
}

bool ConditionLTSSchema::isDataTypeVariable(const TypeVariable& tv) const
{
	assert(ltss != 0);

	return ltss->isDataTypeVariable(tv);
}

bool ConditionLTSSchema::isHidingFree() const
{
	assert(ltss != 0);

	return ltss->isHidingFree();
}

bool ConditionLTSSchema::involvesConditionalHiding() const
{
	assert(ltss != 0);

	return ltss->involvesConditionalHiding();
}

bool ConditionLTSSchema::involvesInvisibleActions() const
{
	assert(ltss != 0);

	return ltss->involvesInvisibleActions();
}

void ConditionLTSSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(ltss != 0 && cond != 0);

	LTSSchema::setNumericValue(vt,id,var_id);
	cond->setNumericValue(vt,id,var_id);
	ltss->setNumericValue(vt,id,var_id);
}

void ConditionLTSSchema::setNumericValuesToBoundVariables(int val)
{
	assert(ltss != 0 && cond != 0);

	cond->setNumericValuesToBoundVariables(val);
	ltss->setNumericValuesToBoundVariables(val);
}

LTSSchema* ConditionLTSSchema::convertChoiceToParallelComposition(const TypeVariable& tv, bool abs) const
{
	assert(ltss != 0 && cond != 0);
	return new ConditionLTSSchema(ltss->convertChoiceToParallelComposition(tv,abs), cond->clone());
}

std::list<SyntaxBranchOfLTSSchema> ConditionLTSSchema::getSyntaxBranches() const
{
	assert(ltss != 0);

	std::list<SyntaxBranchOfLTSSchema> newbranches{};
	std::list<SyntaxBranchOfLTSSchema> branches = ltss->getSyntaxBranches();
	std::list<ValuationFormula*> disj = cond->disjuncts();
	for(auto& b : branches)
	{
		for(auto c : disj)
		{
			ConditionLTSSchema clts = ConditionLTSSchema(ltss->clone(), c->clone());
			SyntaxBranchOfLTSSchema newb{b};
			newb.push_front(&clts);
			newbranches.push_back(std::move(newb));
		}
	}
	for(auto c : disj) delete c;
	return newbranches;
}

ValuationFormula* ConditionLTSSchema::prependValuationFormula(ValuationFormula* vf, Context& av_num) const
{
	return new ConjunctiveValuationFormula(cond->clone(), vf);
}

std::set<RelationVariable> ConditionLTSSchema::getPredicates(bool mode) const
{
	assert(ltss != 0 && cond != 0);

	auto preds1 = ltss->getPredicates(mode);
	auto preds2 = cond->getPredicates(mode);
	preds1.insert(preds2.begin(), preds2.end());
	return preds1;
}

std::string ConditionLTSSchema::toString(const Context& c) const
{
	assert(ltss != 0 && cond != 0);

	std::string str(1,'(');
	str.push_back('[');
	str.append(cond->toString(c));
	str.push_back(']');
	str.append(ltss->toString(c));
	str.push_back(')');
	return str;
}

HidingLTSSchema::HidingLTSSchema(const LTSSchema& l, const SetSchema& s) : HidingLTSSchema(l.clone(), s.clone())
{
}

HidingLTSSchema::HidingLTSSchema(LTSSchema* l, SetSchema* s) : LTSSchema(), ltss(l), hideset(s)
{
	assert(ltss != 0 && hideset != 0);

	types.insert(ltss->getTypeVariables().begin(), ltss->getTypeVariables().end());
	relvars.insert(ltss->getRelationVariables().begin(), ltss->getRelationVariables().end());
	atomvars.insert(ltss->getAtomVariables().begin(), ltss->getAtomVariables().end());
	qfvars.insert(ltss->getQuorumFunctionVariables().begin(), ltss->getQuorumFunctionVariables().end());
	inputchans.insert(ltss->getInputChannels().begin(), ltss->getInputChannels().end());
	outputchans.insert(ltss->getOutputChannels().begin(), ltss->getOutputChannels().end());
	types.insert(hideset->getTypeVariables().begin(), hideset->getTypeVariables().end());
	relvars.insert(hideset->getRelationVariables().begin(), hideset->getRelationVariables().end());
	atomvars.insert(hideset->getAtomVariables().begin(), hideset->getAtomVariables().end());
	qfvars.insert(hideset->getQuorumFunctionVariables().begin(), hideset->getQuorumFunctionVariables().end());
}

HidingLTSSchema::HidingLTSSchema(const HidingLTSSchema& l) : LTSSchema(l), ltss(l.ltss->clone()), hideset(l.hideset->clone())
{
}

HidingLTSSchema& HidingLTSSchema::operator=(const HidingLTSSchema& l)
{
	HidingLTSSchema hls(l);
	std::swap(types, hls.types);
	std::swap(relvars, hls.relvars);
	std::swap(atomvars, hls.atomvars);
	std::swap(qfvars, hls.qfvars);
	std::swap(inputchans, hls.inputchans);
	std::swap(outputchans, hls.outputchans);
	std::swap(hideset, hls.hideset);
	std::swap(ltss, hls.ltss);
	return *this;
}

LTSSchema* HidingLTSSchema::clone() const
{
	assert(ltss != 0 && hideset != 0);

	return new HidingLTSSchema(*this);
}

HidingLTSSchema::~HidingLTSSchema()
{
	delete ltss;
	delete hideset;
}

const LTSSchema& HidingLTSSchema::getLTSSchema() const
{
	assert(ltss != 0);

	return *ltss;
}

const SetSchema& HidingLTSSchema::setSchema() const
{
	assert(hideset != 0);

	return *hideset;
}

PartialInstanceOfLTSSchema* HidingLTSSchema::partialInstance(const Valuation& val) const
{
	assert(ltss != 0 && hideset != 0);

	PartialInstanceOfLTSSchema* inst = ltss->partialInstance(val);
	PartialInstanceOfHidingLTSSchema* hinst = dynamic_cast<PartialInstanceOfHidingLTSSchema*>(inst);
	if(hinst == 0)
	{
		hinst = new PartialInstanceOfHidingLTSSchema(inst);
	}
	hinst->push_back(std::pair<SetSchema*,Valuation>(hideset,val));
	return hinst;
}

bool HidingLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const AtomSet& subvals) const
{
	assert(ltss != 0);

	return ltss->instanceCoveredBySmallerOnes(val, subvals);
}

bool HidingLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const TypeVariable& tv, const std::set<AtomSet>& subvals) const
{
	assert(ltss != 0);

	return ltss->instanceCoveredBySmallerOnes(val,tv,subvals);
}

int HidingLTSSchema::numOfNestedQFVs(const Domain& tv) const
{
	assert(hideset != 0 && ltss != 0);

	return hideset->numOfNestedQFVs(tv) + ltss->numOfNestedQFVs(tv);
}

int HidingLTSSchema::numOfNestedOccurrences(const Domain& tv) const
{
	assert(ltss != 0);

	return ltss->numOfNestedOccurrences(tv);
}

std::set<std::pair<int,int>> HidingLTSSchema::occurrencesInBranches(const Domain& tv) const
{
	assert(hideset != 0 && ltss != 0);

	auto ocount = ltss->occurrencesInBranches(tv);
	std::set<std::pair<int,int>> ocount2;
	int nestedqfvs = hideset->numOfNestedQFVs(tv);
	for(auto& cpair : ocount)
	{
		ocount2.insert(std::make_pair(cpair.first + nestedqfvs, cpair.second));
	}
	std::swap(ocount, ocount2);
	return std::move(ocount);
}

int HidingLTSSchema::numOfParallelOccurrences(const Domain& tv, const LTSSchema& ltsc, const std::map<TypeVariable,int>& co) const
{
	assert(ltss != 0);

	return ltss->numOfParallelOccurrences(tv, ltsc, co);
}

int HidingLTSSchema::numOfParallelOccurrencesInAdjacentStates(const Domain& tv, const LTSSchema& ltsc, const std::map<TypeVariable,int>& co) const
{
	assert(ltss != 0);

	return ltss->numOfParallelOccurrencesInAdjacentStates(tv, ltsc, co);
}

bool HidingLTSSchema::isProcessTypeVariable(const TypeVariable& tv) const
{
	assert(ltss != 0);

	return ltss->isProcessTypeVariable(tv);
}

bool HidingLTSSchema::isDataTypeVariable(const TypeVariable& tv) const
{
	assert(ltss != 0);

	return ltss->isDataTypeVariable(tv);
}

bool HidingLTSSchema::isHidingFree() const
{
	return false;
}

bool HidingLTSSchema::involvesConditionalHiding() const
{
	assert(ltss != 0 && hideset != 0);

	return (ltss->involvesConditionalHiding() || !hideset->isConditionFree());
}

bool HidingLTSSchema::involvesInvisibleActions() const
{
	return true;
}

void HidingLTSSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(ltss != 0 && hideset != 0);

	LTSSchema::setNumericValue(vt,id,var_id);
	hideset->setNumericValue(vt,id,var_id);
	ltss->setNumericValue(vt,id,var_id);
}

void HidingLTSSchema::setNumericValuesToBoundVariables(int val)
{
	assert(ltss != 0 && hideset != 0);

	hideset->setNumericValuesToBoundVariables(val);
	ltss->setNumericValuesToBoundVariables(val);
}

LTSSchema* HidingLTSSchema::convertChoiceToParallelComposition(const TypeVariable& tv, bool abs) const
{
	assert(ltss != 0 && hideset != 0);
	return new HidingLTSSchema(ltss->convertChoiceToParallelComposition(tv,abs), hideset->clone());
}

std::list<SyntaxBranchOfLTSSchema> HidingLTSSchema::getSyntaxBranches() const
{
	assert(ltss != 0);

	return ltss->getSyntaxBranches();
}

std::set<RelationVariable> HidingLTSSchema::getPredicates(bool mode) const
{
	assert(ltss != 0);

	return ltss->getPredicates(mode);
}

std::string HidingLTSSchema::toString(const Context& c) const
{
	assert(ltss != 0 && hideset != 0);

	std::string str(1,'(');
	str.append(ltss->toString(c));
	str.push_back('\\');
	str.append(hideset->toString(c));
	str.push_back(')');
	return str;
}

NamedLTSSchema::NamedLTSSchema(const std::string& n, const LTSSchema& l) : LTSSchema(l), name(new std::string(n)), ltss(l.clone())
{
}

NamedLTSSchema::NamedLTSSchema(const std::string* n, LTSSchema* l) : LTSSchema(*l), name(n), ltss(l)
{
}

NamedLTSSchema::NamedLTSSchema(const NamedLTSSchema& l) : LTSSchema(l), name(new std::string(*(l.name))), ltss(l.ltss->clone())
{
}

NamedLTSSchema& NamedLTSSchema::operator=(const NamedLTSSchema& l)
{
	NamedLTSSchema nls(l);
	std::swap(types, nls.types);
	std::swap(relvars, nls.relvars);
	std::swap(atomvars, nls.atomvars);
	std::swap(qfvars, nls.qfvars);
	std::swap(inputchans, nls.inputchans);
	std::swap(outputchans, nls.outputchans);
	std::swap(name, nls.name);
	std::swap(ltss, nls.ltss);
	return *this;
}

LTSSchema* NamedLTSSchema::clone() const
{
	assert(name != 0 && ltss != 0);

	return new NamedLTSSchema(*this);
}

NamedLTSSchema::~NamedLTSSchema()
{
	delete ltss;
	delete name;
}

const std::string& NamedLTSSchema::getName() const
{
	assert(name != 0);

	return *name;
}

const LTSSchema& NamedLTSSchema::getLTSSchema() const
{
	assert(ltss != 0);

	return *ltss;
}

PartialInstanceOfLTSSchema* NamedLTSSchema::partialInstance(const Valuation& val) const
{
	assert(ltss != 0);

	return ltss->partialInstance(val);
}

bool NamedLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const AtomSet& subvals) const
{
	assert(ltss != 0);

	return ltss->instanceCoveredBySmallerOnes(val, subvals);
}

bool NamedLTSSchema::instanceCoveredBySmallerOnes(const Valuation& val, const TypeVariable& tv, const std::set<AtomSet>& subvals) const
{
	assert(ltss != 0);

	return ltss->instanceCoveredBySmallerOnes(val, tv, subvals);
}

int NamedLTSSchema::numOfNestedQFVs(const Domain& tv) const
{
	assert(ltss != 0);

	return ltss->numOfNestedQFVs(tv);
}

int NamedLTSSchema::numOfNestedOccurrences(const Domain& tv) const
{
	assert(ltss != 0);

	return (ltss->numOfNestedOccurrences(tv));
}

std::set<std::pair<int,int>> NamedLTSSchema::occurrencesInBranches(const Domain& tv) const
{
	assert(ltss != 0);

	return ltss->occurrencesInBranches(tv);
}

int NamedLTSSchema::numOfParallelOccurrences(const Domain& tv, const LTSSchema& ltsc, const std::map<TypeVariable,int>& co) const
{
	assert(ltss != 0);

	return ltss->numOfParallelOccurrences(tv, ltsc, co);
}

int NamedLTSSchema::numOfParallelOccurrencesInAdjacentStates(const Domain& tv, const LTSSchema& ltsc, const std::map<TypeVariable,int>& co) const
{
	assert(ltss != 0);

	return ltss->numOfParallelOccurrencesInAdjacentStates(tv, ltsc, co);
}

bool NamedLTSSchema::isProcessTypeVariable(const TypeVariable& tv) const
{
	assert(ltss != 0);

	return ltss->isProcessTypeVariable(tv);
}

bool NamedLTSSchema::isDataTypeVariable(const TypeVariable& tv) const
{
	assert(ltss != 0);

	return ltss->isDataTypeVariable(tv);
}

bool NamedLTSSchema::isHidingFree() const
{
	assert(ltss != 0);

	return ltss->isHidingFree();
}

bool NamedLTSSchema::involvesConditionalHiding() const
{
	assert(ltss != 0);

	return ltss->involvesConditionalHiding();
}

bool NamedLTSSchema::involvesInvisibleActions() const
{
	assert(ltss != 0);

	return ltss->involvesInvisibleActions();
}

void NamedLTSSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(ltss != 0);

	LTSSchema::setNumericValue(vt,id,var_id);
	ltss->setNumericValue(vt,id,var_id);
}

void NamedLTSSchema::setNumericValuesToBoundVariables(int val)
{
	assert(ltss != 0);

	ltss->setNumericValuesToBoundVariables(val);
}

LTSSchema* NamedLTSSchema::convertChoiceToParallelComposition(const TypeVariable& tv, bool abs) const
{
	assert(ltss != 0);

	return ltss->convertChoiceToParallelComposition(tv,abs);
}

std::list<SyntaxBranchOfLTSSchema> NamedLTSSchema::getSyntaxBranches() const
{
	assert(ltss != 0);

	return ltss->getSyntaxBranches();
}

std::set<RelationVariable> NamedLTSSchema::getPredicates(bool mode) const
{
	assert(ltss != 0);

	return ltss->getPredicates(mode);
}

std::string NamedLTSSchema::toString(const Context& c) const
{
	assert(name != 0);

	return *name;
}

SyntaxBranchOfLTSSchema::SyntaxBranchOfLTSSchema(const SyntaxBranchOfLTSSchema& b) : std::list<const LTSSchema*>{}, bavs{b.bavs}
{
	for(auto l : static_cast<std::list<const LTSSchema*>>(b))
	{
		push_back(l->clone());
	}
}

SyntaxBranchOfLTSSchema::SyntaxBranchOfLTSSchema(SyntaxBranchOfLTSSchema&& b) : std::list<const LTSSchema*>{std::move(b)}, bavs{std::move(b.bavs)}
{
}

SyntaxBranchOfLTSSchema::~SyntaxBranchOfLTSSchema()
{
	for(auto l : static_cast<std::list<const LTSSchema*>>(*this))
	{
		delete l;
	}
}

void SyntaxBranchOfLTSSchema::push_front(const LTSSchema* l)
{
	std::list<const LTSSchema*>::push_front(l->clone());
	if(auto rplts = dynamic_cast<const ReplicatedLTSSchema*>(l))
	{
		auto& av = rplts->getAtomVariable();
		auto it = getAtomVariables().find(av);
		if(it != getAtomVariables().end())
		{
			bavs.insert(av);			
		}
	}
}

ValuationFormula* SyntaxBranchOfLTSSchema::toValuationFormula(Context& c) const
{
	ValuationFormula* vf = new TopValuationFormula();
	for(auto rit = rbegin(); rit != rend(); rit++)
	{
		vf = (*rit)->prependValuationFormula(vf, c);
	}
	return vf;
}

const std::set<AtomVariable>& SyntaxBranchOfLTSSchema::getAtomVariables() const
{
	return back()->getAtomVariables();
}

const std::set<AtomVariable>& SyntaxBranchOfLTSSchema::getBoundAtomVariables() const
{
	return bavs;
}

PartialInstanceOfLTSSchema::~PartialInstanceOfLTSSchema()
{
}

PartialInstanceOfElementaryLTSSchema::PartialInstanceOfElementaryLTSSchema(const ElementaryLTSSchema* l, const Valuation& v) : ltsc(l), val(v)
{
	assert(l != 0);
}

PartialInstanceOfElementaryLTSSchema::PartialInstanceOfElementaryLTSSchema(const PartialInstanceOfElementaryLTSSchema& pi) : ltsc(pi.ltsc), val(pi.val)
{
}

PartialInstanceOfElementaryLTSSchema& PartialInstanceOfElementaryLTSSchema::operator=(const PartialInstanceOfElementaryLTSSchema& pi)
{
	if(&pi == this) return *this;
	PartialInstanceOfElementaryLTSSchema npi(pi);
	std::swap(npi.ltsc, ltsc);
	std::swap(npi.val, val);
	return *this;
}

PartialInstanceOfElementaryLTSSchema::~PartialInstanceOfElementaryLTSSchema()
{
}

const ElementaryLTSSchema& PartialInstanceOfElementaryLTSSchema::getElementaryLTSSchema() const
{
	assert(ltsc != 0);

	return *ltsc;
}

const Valuation& PartialInstanceOfElementaryLTSSchema::getValuation() const
{
	return val;
}

int PartialInstanceOfElementaryLTSSchema::numOfOccurrencesInStates(const AtomSet& as) const
{
	if(as.size() == 0)
	{
		return 0;
	}
	else
	{
		assert(ltsc != 0);

		const Domain& tv = as.begin()->second;
		const TransitionSetSchema& trsc = ltsc->getTransitionSetSchema();
		int num = 0;
		for(TransitionSetSchema::const_iterator cit = trsc.begin(); cit != trsc.end(); cit++)
		{
			num = std::max(num, (*cit)->numOfBoundOccurrencesInStates(tv));
		}
		return num;
	}
}

int PartialInstanceOfElementaryLTSSchema::numOfOccurrencesInTransitions(const AtomSet& as) const
{
	if(as.size() == 0)
	{
		return 0;
	}
	else
	{
		assert(ltsc != 0);

		const Domain& tv = as.begin()->second;
		const TransitionSetSchema& trsc = ltsc->getTransitionSetSchema();
		int num = 0;
		for(TransitionSetSchema::const_iterator cit = trsc.begin(); cit != trsc.end(); cit++)
		{
			num = std::max(num, (*cit)->numOfBoundOccurrences(tv));
		}
		return num;
	}
}

int PartialInstanceOfElementaryLTSSchema::numOfOccurrencesInAdjacentStates(const AtomSet& as) const
{
	if(as.size() == 0)
	{
		return 0;
	}
	else
	{
		assert(ltsc != 0);

		const Domain& tv = as.begin()->second;
		const TransitionSetSchema& trsc = ltsc->getTransitionSetSchema();
		int num = 0;
		for(TransitionSetSchema::const_iterator cit = trsc.begin(); cit != trsc.end(); cit++)
		{
			num = std::max(num, (*cit)->numOfBoundOccurrencesInStates(tv));
		}
		return num;
	}
}

PartialInstanceOfLTSSchema* PartialInstanceOfElementaryLTSSchema::clone() const
{
	return new PartialInstanceOfElementaryLTSSchema(*this);
}

bool PartialInstanceOfElementaryLTSSchema::composeLeaves(const Context&, int)
{
	return false;
}

bool PartialInstanceOfElementaryLTSSchema::isInstance() const
{
	return false;
}

bool PartialInstanceOfElementaryLTSSchema::involvesInvisibleActions() const
{
	assert(ltsc != 0);

	return ltsc->involvesInvisibleActions();
}

std::string PartialInstanceOfElementaryLTSSchema::toString(const Context& ctxt) const
{
	assert(ltsc != 0);

	std::string str("(Instance of ");
	str.append(ltsc->toString(ctxt));
	str.append(" generated by ");

	bool delimiter = false;
	const std::set<TypeVariable>& types = ltsc->getTypeVariables();
	for(std::set<TypeVariable>::const_iterator ctit = types.begin(); ctit != types.end(); ++ctit)
	{
		AtomSet* as = val.getValue(*ctit);
		if(delimiter) str.append(", ");
		str.append(ctit->toString(ctxt));
		str.append(" -> ");
		str.append(as->toString(ctxt));
		delete as;
		delimiter = true;
	}

	const std::set<RelationVariable>& relvars = ltsc->getRelationVariables();
	for(std::set<RelationVariable>::const_iterator crit = relvars.begin(); crit != relvars.end(); ++crit)
	{
		AtomTupleSet* ats = val.getValue(*crit);
		str.append(", ");
		str.append(crit->toString(ctxt));
		str.append(" -> ");
		str.append(ats->toString(ctxt));
		delete ats;
	}

	const std::set<AtomVariable>& atomvars = ltsc->getAtomVariables();
	for(std::set<AtomVariable>::const_iterator cait = atomvars.begin(); cait != atomvars.end(); ++cait)
	{
		str.append(", ");
		str.append(cait->toString(ctxt));
		str.append(" -> ");
		str.append(ctxt.atomToString(val.getValue(*cait)));
	}

	str.push_back(')');
	return str;
}

InstanceOfLTSSchema* PartialInstanceOfElementaryLTSSchema::instance() const
{
	return new ElementaryInstanceOfLTSSchema(ltsc,val);
}

PartialInstanceOfGeneralisedParallelLTSSchema::PartialInstanceOfGeneralisedParallelLTSSchema() : PartialInstanceOfLTSSchema(), std::vector<PartialInstanceOfLTSSchema*>()
{
}

PartialInstanceOfGeneralisedParallelLTSSchema::PartialInstanceOfGeneralisedParallelLTSSchema(const PartialInstanceOfGeneralisedParallelLTSSchema& pi) : PartialInstanceOfLTSSchema(), std::vector<PartialInstanceOfLTSSchema*>(pi)
{
}

PartialInstanceOfGeneralisedParallelLTSSchema& PartialInstanceOfGeneralisedParallelLTSSchema::operator=(const PartialInstanceOfGeneralisedParallelLTSSchema& pi)
{
	if(&pi == this) return *this;
	PartialInstanceOfGeneralisedParallelLTSSchema npi(pi);
	std::swap(static_cast<std::vector<PartialInstanceOfLTSSchema*>&>(npi), static_cast<std::vector<PartialInstanceOfLTSSchema*>&>(*this));
	return *this;
}

PartialInstanceOfGeneralisedParallelLTSSchema::~PartialInstanceOfGeneralisedParallelLTSSchema()
{
}

int PartialInstanceOfGeneralisedParallelLTSSchema::numOfOccurrencesInStates(const AtomSet& as) const
{
	int num = 0;
	for(std::vector<PartialInstanceOfLTSSchema*>::const_iterator clit = begin(); clit != end(); clit++)
	{
		num += (*clit)->numOfOccurrencesInStates(as);
	}
	return num;
}

int PartialInstanceOfGeneralisedParallelLTSSchema::numOfOccurrencesInTransitions(const AtomSet& as) const
{
	int num = 0;
	// the maximum number of occurrences in an action only
	int numactonly = 0;
	for(std::vector<PartialInstanceOfLTSSchema*>::const_iterator clit = begin(); clit != end(); clit++)
	{
		int numtr = (*clit)->numOfOccurrencesInTransitions(as);
		int numst = (*clit)->numOfOccurrencesInAdjacentStates(as);
		numactonly = std::max(numactonly, numtr-numst);
		num += numst;
	}
	return num + numactonly;
}

int PartialInstanceOfGeneralisedParallelLTSSchema::numOfOccurrencesInAdjacentStates(const AtomSet& as) const
{
	int numstates = 0;
	for(std::vector<PartialInstanceOfLTSSchema*>::const_iterator clit = begin(); clit != end(); clit++)
	{
		numstates += (*clit)->numOfOccurrencesInAdjacentStates(as);
	}
	return numstates;
}

PartialInstanceOfLTSSchema* PartialInstanceOfGeneralisedParallelLTSSchema::clone() const
{
	return new PartialInstanceOfGeneralisedParallelLTSSchema(*this);
}

std::string PartialInstanceOfGeneralisedParallelLTSSchema::toString(const Context& c) const
{
	if(empty())
	{
		return "UnitLTS";
	}
	else if(size() == 1)
	{
		return (*begin())->toString(c);
	}
	else
	{
		std::string str(1,'(');
		std::vector<PartialInstanceOfLTSSchema*>::const_iterator clit = begin();
		str.append((*clit)->toString(c));
		for(;clit != end();clit++)
		{
			str.push_back('|');
			str.push_back('|');
			str.append((*clit)->toString(c));
		}
		str.push_back(')');
		return str;
	}
}

bool PartialInstanceOfGeneralisedParallelLTSSchema::composeLeaves(const Context& ctxt, int max_product_size)
{
	bool composed = false;

	// call recursively
	for(int i = 0; i < size(); i++)
	{
		if(PartialInstanceOfGeneralisedParallelLTSSchema* gpi = dynamic_cast<PartialInstanceOfGeneralisedParallelLTSSchema*>(operator[](i)))
		{
			composed = gpi->composeLeaves(ctxt);
			if(composed) return true;
		}
		else if(PartialInstanceOfHidingLTSSchema* hpi = dynamic_cast<PartialInstanceOfHidingLTSSchema*>(operator[](i)))
		{
			composed = hpi->composeLeaves(ctxt);
			if(composed) return true;
		}
	}

	// if recursive calls failed, try to pick two partial instances and compose them
	if(size() <= 1) return false;
	int match[size()][size()];
	for(int i = 0; i < size(); i++)
	{
		for(int j = 0; j < size(); j++)
		{
			match[i][j] = std::numeric_limits<int>::max();
		}
	}

	int bestmatchi = 0;
	int bestmatchj = size()-1;
	for(int i = 0; i < size(); i++)
	{
		for(int j = 0; j < size(); j++)
		{
			if(i < j)
			{
				PartialInstanceOfElementaryLTSSchema* pi = dynamic_cast<PartialInstanceOfElementaryLTSSchema*>(operator[](i));
				PartialInstanceOfElementaryLTSSchema* pj = dynamic_cast<PartialInstanceOfElementaryLTSSchema*>(operator[](j));
				if(pi != 0)
				{
					if(!pi->isInstance())
					{
						operator[](i) = pi->instance();
						delete pi;
					}
				}
				if(pj != 0)
				{
					if(!pj->isInstance())
					{
						operator[](j) = pj->instance();
						delete pj;
					}
				}
				ElementaryInstanceOfLTSSchema* ei = dynamic_cast<ElementaryInstanceOfLTSSchema*>(operator[](i));
				ElementaryInstanceOfLTSSchema* ej = dynamic_cast<ElementaryInstanceOfLTSSchema*>(operator[](j));

				if(ei != 0 && ej != 0)
				{
					#pragma omp critical
					{
						int score = 0;
						try
						{
							long amount = ei->getTransitions().size() * (sizeof(Transition) + sizeof(int) * ei->getTransitions().width()) * ej->getTransitions().size() * (sizeof(Transition) + sizeof(int) * ei->getTransitions().width());
							const ActionSet& asi = ei->getActionSet();
							const ActionSet& asj = ej->getActionSet();
							int jc = jointCount(asi,asj);
							score = ((ei->involvesInvisibleActions() ? 1 : 0) + disjointCount(asi,asj))*((ej->involvesInvisibleActions() ? 1 : 0) + disjointCount(asj,asi)) - (jc * jc);
							if(score <= (int)asi.size()-(int)asj.size()) match[i][j] = score;
							match[i][j] = score;
							int* memtest = new int[amount];
							delete[] memtest;
						}
						catch(std::bad_alloc& ba)
						{
//							std::cout << "LTSs with score " << score << " not composed due to the probably huge size of the composition." << std::endl;
							match[i][j] = std::numeric_limits<int>::max();
						}
					}
/*					if(ei->getTransitions().size() * ej->getTransitions().size() <= max_product_size)
					{
						const ActionSet& asi = ei->getActionSet();
						const ActionSet& asj = ej->getActionSet();
						match[i][j] = (ei->involvesInvisibleActions() + disjointCount(asi,asj))*(ej->involvesInvisibleActions() + disjointCount(asj,asi));
					}
*/
				}
/*				else if(pi != 0 && ej != 0)
				{
					const ElementaryLTSSchema& li = pi->getElementaryLTSSchema();
					const std::set<Channel>& liic = li.getInputChannels();
					const std::set<Channel>& lioc = li.getOutputChannels();
					const ActionSet& asj = ej->getActionSet();
					std::set<Channel> ljic = asj.getInputChannels();
					std::set<Channel> ljoc = asj.getOutputChannels();
					match[i][j] = (li.involvesInvisibleActions() + disjointCount(liic,ljic) + disjoint(lioc,ljoc))*(ej->involvesInvisibleActions() + disjointCount(ljic,liic) + disjoint(ljoc,lioc));
					AtomSet* asi = pi->getValuation().getValue(li.getTypeVariables());
					AtomSet atsj = asj.getAtoms();
					match[i][j] += disjointCount(*asi,atsj) * disjointCount(atsj,*asi);
					delete asi;
				}
				else if(ei != 0 && pj != 0)
				{
					const ActionSet& asi = ei->getActionSet();
					std::set<Channel> liic = asi.getInputChannels();
					std::set<Channel> lioc = asi.getOutputChannels();
					const ElementaryLTSSchema& lj = pj->getElementaryLTSSchema();
					const std::set<Channel>& ljic = lj.getInputChannels();
					const std::set<Channel>& ljoc = lj.getOutputChannels();
					match[i][j] = (ei->involvesInvisibleActions() + disjointCount(liic,ljic) + disjoint(lioc,ljoc))*(lj.involvesInvisibleActions() + disjointCount(ljic,liic) + disjoint(ljoc,lioc));
					AtomSet atsi = asi.getAtoms();
					AtomSet* asj = pj->getValuation().getValue(lj.getTypeVariables());
					match[i][j] += disjointCount(atsi,*asj) * disjointCount(*asj,atsi);
					delete asj;
				}
				else if(pi != 0 && pj != 0)
				{
					const ElementaryLTSSchema& li = pi->getElementaryLTSSchema();
					const std::set<Channel>& liic = li.getInputChannels();
					const std::set<Channel>& lioc = li.getOutputChannels();
					const ElementaryLTSSchema& lj = pj->getElementaryLTSSchema();
					const std::set<Channel>& ljic = lj.getInputChannels();
					const std::set<Channel>& ljoc = lj.getOutputChannels();
					match[i][j] = (li.involvesInvisibleActions() + disjointCount(liic,ljic) + disjoint(lioc,ljoc))*(lj.involvesInvisibleActions() + disjointCount(ljic,liic) + disjoint(ljoc,lioc));
					if(pi->getValuation() == pj->getValuation())
					{
						match[i][j]-=2;
					}
					else if(pi->getValuation().almostEquals(pj->getValuation()))
					{
						match[i][j]--;
					}
					else
					{
						AtomSet* asi = pi->getValuation().getValue(li.getTypeVariables());
						AtomSet* asj = pj->getValuation().getValue(lj.getTypeVariables());
						match[i][j] += disjointCount(*asi,*asj) * disjointCount(*asj,*asi);
						delete asi;
						delete asj;
					}
				}
*/
				if(match[bestmatchi][bestmatchj] > match[i][j])
				{
					bestmatchi = i;
					bestmatchj = j;
				}
			}
		}
	}

	if(match[bestmatchi][bestmatchj] == std::numeric_limits<int>::max())
	{
		return false;
	}
	else
	{
		PartialInstanceOfLTSSchema* pi = operator[](bestmatchi);
		if(!pi->isInstance())
		{
			operator[](bestmatchi) = pi->instance();
			delete pi;
		}
		PartialInstanceOfLTSSchema* pj = operator[](bestmatchj);
		if(!pj->isInstance())
		{
			operator[](bestmatchj) = pj->instance();
			delete pj;
		}

		InstanceOfLTSSchema* ii = dynamic_cast<InstanceOfLTSSchema*>(operator[](bestmatchi));
		InstanceOfLTSSchema* ij = dynamic_cast<InstanceOfLTSSchema*>(operator[](bestmatchj));
/*
		std::cout << "Composing LTSs with score " << match[bestmatchi][bestmatchj] << " and alphabet" << std::endl;
		std::cout << ii->getActionSet().toString(ctxt) << std::endl;
		std::cout << ij->getActionSet().toString(ctxt) << std::endl;
*/
		ParallelInstanceOfLTSSchema* p = new ParallelInstanceOfLTSSchema(ii,ij);
		operator[](bestmatchi) = p->evaluate();
		operator[](bestmatchj) = operator[](size()-1);
		pop_back();
		delete p;
		return true;
	}
}

bool PartialInstanceOfGeneralisedParallelLTSSchema::isInstance() const
{
	return false;
}

bool PartialInstanceOfGeneralisedParallelLTSSchema::involvesInvisibleActions() const
{
	for(int i = 0; i < size(); i++)
	{
		if(operator[](i)->involvesInvisibleActions()) return true;
	}
	return false;
}

InstanceOfLTSSchema* PartialInstanceOfGeneralisedParallelLTSSchema::instance() const
{
	return instance(0,size());
}

InstanceOfLTSSchema* PartialInstanceOfGeneralisedParallelLTSSchema::instance(int first, int last) const
{
	if(first == last)
	{
		return new UnitLTS();
	}
	else if(last == first + 1)
	{
		return operator[](first)->instance();
	}
	else
	{
		int mid = first + (last - first)/2;
		return new ParallelInstanceOfLTSSchema(instance(first,mid), instance(mid,last));
	}
}

PartialInstanceOfHidingLTSSchema::PartialInstanceOfHidingLTSSchema(PartialInstanceOfLTSSchema* l) : PartialInstanceOfLTSSchema(), std::vector<std::pair<SetSchema*,Valuation> >(), ltsc(l)
{
}

PartialInstanceOfHidingLTSSchema::PartialInstanceOfHidingLTSSchema(const PartialInstanceOfHidingLTSSchema& pi) : PartialInstanceOfLTSSchema(), std::vector<std::pair<SetSchema*,Valuation> >(pi), ltsc(pi.ltsc)
{
}

PartialInstanceOfHidingLTSSchema& PartialInstanceOfHidingLTSSchema::operator=(const PartialInstanceOfHidingLTSSchema& pi)
{
	if(&pi == this) return *this;
	PartialInstanceOfHidingLTSSchema npi(pi);
	std::swap(static_cast<std::vector<std::pair<SetSchema*,Valuation> >&>(npi), static_cast<std::vector<std::pair<SetSchema*,Valuation> >&>(*this));
	std::swap(npi.ltsc, ltsc);
	return *this;
}

PartialInstanceOfHidingLTSSchema::~PartialInstanceOfHidingLTSSchema()
{
}

int PartialInstanceOfHidingLTSSchema::numOfOccurrencesInStates(const AtomSet& as) const
{
	return ltsc->numOfOccurrencesInStates(as);
}

int PartialInstanceOfHidingLTSSchema::numOfOccurrencesInTransitions(const AtomSet& as) const
{
	return ltsc->numOfOccurrencesInTransitions(as);
}

int PartialInstanceOfHidingLTSSchema::numOfOccurrencesInAdjacentStates(const AtomSet& as) const
{
	return ltsc->numOfOccurrencesInAdjacentStates(as);
}

PartialInstanceOfLTSSchema* PartialInstanceOfHidingLTSSchema::clone() const
{
	return new PartialInstanceOfHidingLTSSchema(*this);
}

std::string PartialInstanceOfHidingLTSSchema::toString(const Context& ctxt) const
{
	if(empty())
	{
		return ltsc->toString(ctxt);
	}
	else
	{
		std::string str(1,'(');
		str.append(ltsc->toString(ctxt));
		for(std::vector<std::pair<SetSchema*,Valuation> >::const_iterator clit = begin(); clit != end(); clit++)
		{
			str.append("\(instance of ");
			str.append(clit->first->toString(ctxt));
			str.append(" generated by ");

			bool delimiter = false;
			const std::set<TypeVariable>& types = clit->first->getTypeVariables();
			for(std::set<TypeVariable>::const_iterator ctit = types.begin(); ctit != types.end(); ++ctit)
			{
				AtomSet* as = clit->second.getValue(*ctit);
				if(delimiter) str.append(", ");
				str.append(ctit->toString(ctxt));
				str.append(" -> ");
				str.append(as->toString(ctxt));
				delete as;
				delimiter = true;
			}

			const std::set<RelationVariable>& relvars = clit->first->getRelationVariables();
			for(std::set<RelationVariable>::const_iterator crit = relvars.begin(); crit != relvars.end(); ++crit)
			{
				AtomTupleSet* ats = clit->second.getValue(*crit);
				str.append(", ");
				str.append(crit->toString(ctxt));
				str.append(" -> ");
				str.append(ats->toString(ctxt));
				delete ats;
			}

			const std::set<AtomVariable>& atomvars = clit->first->getAtomVariables();
			for(std::set<AtomVariable>::const_iterator cait = atomvars.begin(); cait != atomvars.end(); ++cait)
			{
				str.append(", ");
				str.append(cait->toString(ctxt));
				str.append(" -> ");
				str.append(ctxt.atomToString(clit->second.getValue(*cait)));
			}

			str.push_back(')');
		}
		str.push_back(')');
		return str;
	}
}

bool PartialInstanceOfHidingLTSSchema::composeLeaves(const Context& ctxt, int max_product_size)
{
	assert(ltsc != 0);

	return ltsc->composeLeaves(ctxt, max_product_size);
}

bool PartialInstanceOfHidingLTSSchema::isInstance() const
{
	return false;
}

bool PartialInstanceOfHidingLTSSchema::involvesInvisibleActions() const
{
	return (size() != 0);
}

InstanceOfLTSSchema* PartialInstanceOfHidingLTSSchema::instance() const
{
	if(empty())
	{
		return ltsc->instance();
	}
	else
	{
		ActionSet* as = operator[](0).first->instance(operator[](0).second);
		for(int i = 1; i != size(); i++)
		{
			ActionSet* as2 = operator[](i).first->instance(operator[](i).second);
			as->insert(as2->begin(), as2->end());
			delete as2;
		}
		return new HidingInstanceOfLTSSchema(ltsc->instance(), as);
	}
}

InstanceOfLTSSchema::InstanceOfLTSSchema() : PartialInstanceOfLTSSchema(), acts(new ActionSet())
{
}

InstanceOfLTSSchema::InstanceOfLTSSchema(ActionSet* as2) : PartialInstanceOfLTSSchema(), acts(as2)
{
}

InstanceOfLTSSchema::InstanceOfLTSSchema(const InstanceOfLTSSchema& ils) : PartialInstanceOfLTSSchema(), acts(new ActionSet(*(ils.acts)))
{
}

InstanceOfLTSSchema::~InstanceOfLTSSchema()
{
//	delete alph;
	delete acts;
}

/*
const ActionSet& InstanceOfLTSSchema::getAlphabet() const
{
	return *alph;
}
*/

const ActionSet& InstanceOfLTSSchema::getActionSet() const
{
	return *acts;
}

bool InstanceOfLTSSchema::composeLeaves(const Context&, int)
{
	return false;
}

bool InstanceOfLTSSchema::isInstance() const
{
	return true;
}

InstanceOfLTSSchema* InstanceOfLTSSchema::instance() const
{
	return clone();
}

InstanceOfLTSSchema* InstanceOfLTSSchema::evaluate() const
{
	return clone();
}

int UnitLTS::numOfOccurrencesInStates(const AtomSet& as) const
{
	return 0;
}

int UnitLTS::numOfOccurrencesInTransitions(const AtomSet& as) const
{
	return 0;
}

int UnitLTS::numOfOccurrencesInAdjacentStates(const AtomSet& as) const
{
	return 0;
}

bool UnitLTS::isDeterministic() const
{
	return true;
}

bool UnitLTS::isComponentWiseDeterministic() const
{
	return true;
}

bool UnitLTS::isComponentWiseMustDeterministic() const
{
	return true;
}

bool UnitLTS::involvesInvisibleActions() const
{
	return false;
}

InstanceOfLTSSchema* UnitLTS::clone() const
{
	return new UnitLTS(*this);
}

InstanceOfLTSSchema* UnitLTS::evaluate() const
{
	return new UnitLTS(*this);
}

std::string UnitLTS::toString(const Context&) const
{
	return "UnitLTS";
}

ElementaryInstanceOfLTSSchema::ElementaryInstanceOfLTSSchema(TransitionSet* ts, State* is)
{
	assert(ts != 0 && is != 0);

	transet = ts;
	initstate = is;
	acts = new ActionSet(transet->getActionSet());
	inputs = new ActionSet(transet->getInputActionSet());
	outputs = new ActionSet(transet->getOutputActionSet());
}

ElementaryInstanceOfLTSSchema::ElementaryInstanceOfLTSSchema(const ElementaryLTSSchema* l, const Valuation& v) : InstanceOfLTSSchema()
{
	transet = l->getTransitionSetSchema().instance(v);
	acts = new ActionSet(transet->getActionSet());
	inputs = new ActionSet(transet->getInputActionSet());
	outputs = new ActionSet(transet->getOutputActionSet());
	initstate = new State(l->getStateSchema().instance(v));
}

ElementaryInstanceOfLTSSchema::ElementaryInstanceOfLTSSchema(const ElementaryInstanceOfLTSSchema& l) : InstanceOfLTSSchema(l), transet(new TransitionSet(*l.transet)), initstate(new State(*l.initstate)), inputs(new ActionSet(*l.inputs)), outputs(new ActionSet(*l.outputs))
{
}

ElementaryInstanceOfLTSSchema& ElementaryInstanceOfLTSSchema::operator=(const ElementaryInstanceOfLTSSchema& ei)
{
	if(this == &ei) return *this;
	ElementaryInstanceOfLTSSchema nei(ei);
	std::swap(transet, nei.transet);
	std::swap(initstate, nei.initstate);
	std::swap(acts, nei.acts);
	std::swap(inputs, nei.inputs);
	std::swap(outputs, nei.outputs);
	return *this;
}

int ElementaryInstanceOfLTSSchema::numOfOccurrencesInStates(const AtomSet& as) const
{
	assert(transet != 0 && initstate != 0);

	int maxcount = 0;
	AtomSet atoms = initstate->getAtoms();
	for(AtomSet::const_iterator cait = as.begin(); cait != as.end(); cait++)
	{
		if(atoms.isElement(*cait))
		{
			maxcount++;
		}
	}
	for(TransitionSet::const_iterator ctit = transet->begin(); ctit != transet->end(); ctit++)
	{
		atoms = ctit->first.getAtoms();
		int count = 0;
		for(AtomSet::const_iterator cait = as.begin(); cait != as.end(); cait++)
		{
			if(atoms.isElement(*cait))
			{
				count++;
			}
		}
		maxcount = std::max(maxcount,count);

		atoms = ctit->third.getAtoms();
		count = 0;
		for(AtomSet::const_iterator cait = as.begin(); cait != as.end(); cait++)
		{
			if(atoms.isElement(*cait))
			{
				count++;
			}
		}
		maxcount = std::max(maxcount,count);
	}
	return maxcount;
}

int ElementaryInstanceOfLTSSchema::numOfOccurrencesInTransitions(const AtomSet& as) const
{
	assert(transet != 0);

	int maxcount = 0;
	for(TransitionSet::const_iterator ctit = transet->begin(); ctit != transet->end(); ctit++)
	{
		AtomSet atoms1 = ctit->first.getAtoms();
		AtomSet atoms2 = ctit->second.getAtoms();
		AtomSet atoms3 = ctit->third.getAtoms();
		int count = 0;
		for(AtomSet::const_iterator cait = as.begin(); cait != as.end(); cait++)
		{
			if(atoms1.isElement(*cait) || atoms2.isElement(*cait) || atoms3.isElement(*cait))
			{
				count++;
			}
		}
		maxcount = std::max(maxcount,count);
	}
	return maxcount;
}

int ElementaryInstanceOfLTSSchema::numOfOccurrencesInAdjacentStates(const AtomSet& as) const
{
	assert(transet != 0);

	int maxcount = 0;
	for(TransitionSet::const_iterator ctit = transet->begin(); ctit != transet->end(); ctit++)
	{
		AtomSet atoms1 = ctit->first.getAtoms();
		AtomSet atoms3 = ctit->third.getAtoms();
		int count = 0;
		for(AtomSet::const_iterator cait = as.begin(); cait != as.end(); cait++)
		{
			if(atoms1.isElement(*cait) || atoms3.isElement(*cait))
			{
				count++;
			}
		}
		maxcount = std::max(maxcount,count);
	}
	return maxcount;
}

bool ElementaryInstanceOfLTSSchema::isDeterministic() const
{
	assert(transet != 0);

	int progress = 0;

//	#pragma omp critical
//	std::cout << "Checking the determinism of an LTS..." << std::endl;

	ParallelAnalysisDisplay::addTask(transet->size());

	for(TransitionSet::const_iterator ctit = transet->begin(); ctit != transet->end(); ctit++)
	{
		if(ctit->second.chan == 0) return false;

		for(TransitionSet::const_iterator ctit2 = transet->begin(); ctit2 != transet->end(); ctit2++)
		{
			if(ctit != ctit2 && ctit->first == ctit2->first && ctit->second == ctit2->second && ctit->third != ctit2->third)
			{
//				#pragma omp critical
//				std::cout << "...No. The LTS is not deterministic." << std::endl << std::endl;
				ParallelAnalysisDisplay::removeTask();
				return false;
			}
		}

		progress++;
		ParallelAnalysisDisplay::setProgress(progress);
	}
//	#pragma omp critical
//	std::cout << "...Yes. The LTS is deterministic." << std::endl << std::endl;
	ParallelAnalysisDisplay::removeTask();
	return true;
}

bool ElementaryInstanceOfLTSSchema::isComponentWiseDeterministic() const
{
	assert(transet != 0);

	for(TransitionSet::const_iterator ctit = transet->begin(); ctit != transet->end(); ctit++)
	{
		for(TransitionSet::const_iterator ctit2 = transet->begin(); ctit2 != transet->end(); ctit2++)
		{
			if(ctit != ctit2 && ctit->first == ctit2->first && ctit->second == ctit2->second && ctit->third != ctit2->third)
			{
				return false;
			}
		}
	}
	return true;
}

bool ElementaryInstanceOfLTSSchema::isComponentWiseMustDeterministic() const
{
	assert(transet != 0);

	for(TransitionSet::const_iterator ctit = transet->begin(); ctit != transet->end(); ctit++)
	{
		if(ctit->isMust || ctit->second.isInput)
		{
			for(TransitionSet::const_iterator ctit2 = transet->begin(); ctit2 != transet->end(); ctit2++)
			{
				if(ctit != ctit2 && ctit->first == ctit2->first && ctit->second == ctit2->second && ctit->third != ctit2->third)
				{
					return false;
				}
			}
		}
	}
	return true;
}

bool ElementaryInstanceOfLTSSchema::involvesInvisibleActions() const
{
	assert(transet != 0);

	for(TransitionSet::const_iterator ctit = transet->begin(); ctit != transet->end(); ctit++)
	{
		if(ctit->second.chan == 0) return true;
	}
	return false;
}

const ActionSet& ElementaryInstanceOfLTSSchema::getInputActionSet() const
{
	assert(inputs != 0);
	return *inputs;
}

const ActionSet& ElementaryInstanceOfLTSSchema::getOutputActionSet() const
{
	assert(outputs != 0);
	return *outputs;
}

InstanceOfLTSSchema* ElementaryInstanceOfLTSSchema::clone() const
{
	return new ElementaryInstanceOfLTSSchema(*this);
}

InstanceOfLTSSchema* ElementaryInstanceOfLTSSchema::evaluate() const
{
	return new ElementaryInstanceOfLTSSchema(*this);
}

std::string ElementaryInstanceOfLTSSchema::toString(const Context& c) const
{
	assert(transet != 0 && initstate != 0);

	std::string str("(LTS ");
	str.append(transet->toString(c));
	str.append(" from ");
	str.append(initstate->toString(c));
	str.push_back(')');
	return str;
}

const TransitionSet& ElementaryInstanceOfLTSSchema::getTransitions() const
{
	assert(transet != 0);

	return *transet;
}

const State& ElementaryInstanceOfLTSSchema::getInitialState() const
{
	assert(initstate != 0);

	return *initstate;
}

ParallelInstanceOfLTSSchema::ParallelInstanceOfLTSSchema(const InstanceOfLTSSchema* i1, const InstanceOfLTSSchema* i2) : InstanceOfLTSSchema(*i1), iltsc1(i1), iltsc2(i2)
{
	const ActionSet& set2 = iltsc2->getActionSet();
	for(ActionSet::const_reverse_iterator cit = set2.rbegin(); cit != set2.rend(); ++cit)
	{
//		alph->insert(**cit);
		acts->insert(*cit);
	}
}

ParallelInstanceOfLTSSchema::ParallelInstanceOfLTSSchema(const ParallelInstanceOfLTSSchema& i) : InstanceOfLTSSchema(i), iltsc1(i.iltsc1->clone()), iltsc2(i.iltsc2->clone())
{
}

ParallelInstanceOfLTSSchema::~ParallelInstanceOfLTSSchema()
{
	delete iltsc1;
	delete iltsc2;
}

ParallelInstanceOfLTSSchema& ParallelInstanceOfLTSSchema::operator=(const ParallelInstanceOfLTSSchema& pi)
{
	if(this == &pi) return *this;
	ParallelInstanceOfLTSSchema npi(pi);
	std::swap(iltsc1, npi.iltsc1);
	std::swap(iltsc2, npi.iltsc2);
//	std::swap(alph, npi.alph);
	std::swap(acts, npi.acts);
	return *this;
}

const InstanceOfLTSSchema& ParallelInstanceOfLTSSchema::getOneInstance() const
{
	assert(iltsc1 != 0);

	return *iltsc1;
}

const InstanceOfLTSSchema& ParallelInstanceOfLTSSchema::getOtherInstance() const
{
	assert(iltsc2 != 0);

	return *iltsc2;
}

int ParallelInstanceOfLTSSchema::numOfOccurrencesInStates(const AtomSet& as) const
{
	assert(iltsc1 != 0 && iltsc2 != 0);

	return iltsc1->numOfOccurrencesInStates(as) + iltsc2->numOfOccurrencesInStates(as);
}

int ParallelInstanceOfLTSSchema::numOfOccurrencesInTransitions(const AtomSet& as) const
{
	assert(iltsc1 != 0 && iltsc2 != 0);

	return std::max(iltsc1->numOfOccurrencesInTransitions(as) + iltsc2->numOfOccurrencesInAdjacentStates(as), iltsc1->numOfOccurrencesInAdjacentStates(as) + iltsc2->numOfOccurrencesInTransitions(as));
}

int ParallelInstanceOfLTSSchema::numOfOccurrencesInAdjacentStates(const AtomSet& as) const
{
	assert(iltsc1 != 0 && iltsc2 != 0);

	return iltsc1->numOfOccurrencesInAdjacentStates(as) + iltsc2->numOfOccurrencesInAdjacentStates(as);
}

bool ParallelInstanceOfLTSSchema::isDeterministic() const
{
	assert(iltsc1 != 0 && iltsc2 != 0);

	return iltsc1->isDeterministic() && iltsc2->isDeterministic();
}

bool ParallelInstanceOfLTSSchema::isComponentWiseDeterministic() const
{
	assert(iltsc1 != 0 && iltsc2 != 0);

	return iltsc1->isComponentWiseDeterministic() && iltsc2->isComponentWiseDeterministic();
}

bool ParallelInstanceOfLTSSchema::isComponentWiseMustDeterministic() const
{
	assert(iltsc1 != 0 && iltsc2 != 0);

	return iltsc1->isComponentWiseMustDeterministic() && iltsc2->isComponentWiseMustDeterministic();
}

bool ParallelInstanceOfLTSSchema::involvesInvisibleActions() const
{
	assert(iltsc1 != 0 && iltsc2 != 0);

	return iltsc1->involvesInvisibleActions() && iltsc2->involvesInvisibleActions();
}

InstanceOfLTSSchema* ParallelInstanceOfLTSSchema::clone() const
{
	return new ParallelInstanceOfLTSSchema(*this);
}

InstanceOfLTSSchema* ParallelInstanceOfLTSSchema::evaluate() const
{
	assert(iltsc1 != 0 && iltsc2 != 0);

	InstanceOfLTSSchema* inst1 = iltsc1->evaluate();
	InstanceOfLTSSchema* inst2 = iltsc2->evaluate();
	if(dynamic_cast<UnitLTS*>(inst1) != 0)
	{
		delete inst1;
		return inst2;
	}
	else if(dynamic_cast<UnitLTS*>(inst2) != 0)
	{
		delete inst2;
		return inst1;
	}
	ElementaryInstanceOfLTSSchema* elinst1 = dynamic_cast<ElementaryInstanceOfLTSSchema*>(inst1);
	ElementaryInstanceOfLTSSchema* elinst2 = dynamic_cast<ElementaryInstanceOfLTSSchema*>(inst2);
	if(elinst1 != 0 && elinst2 != 0)
	{
		const TransitionSet& trans1 = elinst1->getTransitions();
		const TransitionSet& trans2 = elinst2->getTransitions();
		const State& initstate1 = elinst1->getInitialState();
		const State& initstate2 = elinst2->getInitialState();
		ActionSet alph1 = trans1.getActionSet();
		ActionSet alph2 = trans2.getActionSet();
		ActionSet alph(alph1);
		alph.insert(alph2.begin(), alph2.end());
		StateSet states1 = trans1.getStateSet();
		states1.insert(elinst1->getInitialState());
		StateSet states2 = trans2.getStateSet();
		states2.insert(elinst2->getInitialState());
		TransitionSet* reach = new TransitionSet();
		std::map<std::pair<State,State>,State> statemap;

		int totalwork = states1.size() * states2.size() + alph.size();
		int progress = 0;

//		#pragma omp critical
//		std::cout << "Computing the parallel composition of LTSs..." << std::endl;

		ParallelAnalysisDisplay::addTask(totalwork);

		State* initstate = new State(ControlState(0,0));
		initstate->insert(initstate->end(), initstate1.begin(), initstate1.end());
		initstate->insert(initstate->end(), initstate2.begin(), initstate2.end());
		statemap.insert(std::pair<std::pair<State,State>,State>(std::pair<State,State>(initstate1, initstate2), *initstate));

		std::set<std::pair<State,State> > seen;
		std::list<std::pair<State,State> > unvisited;
		unvisited.push_front(std::pair<State,State>(initstate1,initstate2));
		seen.insert(std::pair<State,State>(initstate1,initstate2));
		while(!unvisited.empty())
		{
			std::pair<State,State> frompair = unvisited.back();
			unvisited.pop_back();
			std::pair<TransitionSet::const_iterator, TransitionSet::const_iterator> range1 = trans1.getSuccessors(frompair.first);
			std::pair<TransitionSet::const_iterator, TransitionSet::const_iterator> range2 = trans2.getSuccessors(frompair.second);
			std::map<std::pair<State,State>,State>::const_iterator from = statemap.find(frompair);

			assert(from != statemap.end());

			for(TransitionSet::const_iterator ctit1 = range1.first; ctit1 != range1.second; ctit1++)
			{
				if(alph2.find(ctit1->second) == alph2.end())
				{
					std::pair<State,State> topair(ctit1->third, frompair.second);
					std::map<std::pair<State,State>,State>::const_iterator to = statemap.find(topair);
					if(to == statemap.end())
					{
						State tost(ControlState(0,statemap.size()));
						tost.insert(tost.end(), ctit1->third.begin(), ctit1->third.end());
						tost.insert(tost.end(), frompair.second.begin(), frompair.second.end());
						to = statemap.insert(statemap.begin(), std::pair<std::pair<State,State>,State>(topair, tost));
					}
					reach->insert(Transition(from->second, ctit1->second, to->second, ctit1->isMust));
					if(seen.find(topair) == seen.end())
					{
						unvisited.push_front(topair);
						seen.insert(topair);
					}
				}
				else
				{
					for(TransitionSet::const_iterator ctit2 = range2.first; ctit2 != range2.second; ctit2++)
					{
						if(ctit1->second == ctit2->second)
						{
							std::pair<State,State> topair(ctit1->third, ctit2->third);
							std::map<std::pair<State,State>,State>::const_iterator to = statemap.find(topair);
							if(to == statemap.end())
							{
								State tost(ControlState(0,statemap.size()));
								tost.insert(tost.end(), ctit1->third.begin(), ctit1->third.end());
								tost.insert(tost.end(), ctit2->third.begin(), ctit2->third.end());
								to = statemap.insert(statemap.begin(), std::pair<std::pair<State,State>,State>(topair, tost));
							}
							reach->insert(Transition(from->second, ctit1->second, to->second, ctit1->isMust && ctit2->isMust));
							if(seen.find(topair) == seen.end())
							{
								unvisited.push_front(topair);
								seen.insert(topair);
							}

						}
					}
				}
			}
			for(TransitionSet::const_iterator ctit2 = range2.first; ctit2 != range2.second; ctit2++)
			{
				if(alph1.find(ctit2->second) == alph1.end())
				{
					std::pair<State,State> topair(frompair.first, ctit2->third);
					std::map<std::pair<State,State>,State>::const_iterator to = statemap.find(topair);
					if(to == statemap.end())
					{
						State tost(ControlState(0,statemap.size()));
						tost.insert(tost.end(), frompair.first.begin(), frompair.first.end());
						tost.insert(tost.end(), ctit2->third.begin(), ctit2->third.end());
						to = statemap.insert(statemap.begin(), std::pair<std::pair<State,State>,State>(topair, tost));
					}
					reach->insert(Transition(from->second, ctit2->second, to->second, ctit2->isMust));
					if(seen.find(topair) == seen.end())
					{
						unvisited.push_front(topair);
						seen.insert(topair);
					}
				}
			}

			progress++;
			ParallelAnalysisDisplay::setProgress(progress);
		}

		// make sure that the alphabet is preserved
		const_cast<TransitionSet&>(trans1).sortByAction();
		const_cast<TransitionSet&>(trans2).sortByAction();
		ActionSet newacts = reach->getActionSet();
		for(ActionSet::const_iterator cait = alph.begin(); cait != alph.end(); cait++)
		{
			if(newacts.find(*cait) == newacts.end())
			{
				TransitionSet extratr;
				int atomcount = std::numeric_limits<int>::max();
				std::pair<TransitionSet::const_iterator, TransitionSet::const_iterator> range1 = trans1.getTransitions(*cait);
				std::pair<TransitionSet::const_iterator, TransitionSet::const_iterator> range2 = trans2.getTransitions(*cait);
				if(range1.first != range1.second && range2.first != range2.second)
				{
					for(TransitionSet::const_iterator ctit1 = range1.first; ctit1 != range1.second; ctit1++)
					{
						for(TransitionSet::const_iterator ctit2 = range2.first; ctit2 != range2.second; ctit2++)
						{
							std::pair<State,State> frompair(ctit1->first, ctit2->first);
							std::map<std::pair<State,State>,State>::const_iterator from = statemap.find(frompair);
							if(from == statemap.end())
							{
								State fromst(ControlState(0,statemap.size()));
								fromst.insert(fromst.end(), ctit1->first.begin(), ctit1->first.end());
								fromst.insert(fromst.end(), ctit2->first.begin(), ctit2->first.end());
								from = statemap.insert(statemap.begin(), std::pair<std::pair<State,State>,State>(frompair, fromst));
							}
							std::pair<State,State> topair(ctit1->third, ctit2->third);
							std::map<std::pair<State,State>,State>::const_iterator to = statemap.find(topair);
							if(to == statemap.end())
							{
								State tost(ControlState(0,statemap.size()));
								tost.insert(tost.end(), ctit1->third.begin(), ctit1->third.end());
								tost.insert(tost.end(), ctit2->third.begin(), ctit2->third.end());
								to = statemap.insert(statemap.begin(), std::pair<std::pair<State,State>,State>(topair, tost));
							}
							Transition tr(from->second, *cait, to->second);
							int atomcount2 = tr.getAtoms().size();
							if(atomcount2 < atomcount)
							{
								extratr.clear();
								atomcount = atomcount2;
							}
							if(atomcount == atomcount2)
							{
								extratr.insert(tr);
							}
						}
					}
				}
				else if(range1.first != range1.second)
				{
					for(TransitionSet::const_iterator ctit1 = range1.first; ctit1 != range1.second; ctit1++)
					{
						for(StateSet::const_iterator csit2 = states2.begin(); csit2 != states2.end(); csit2++)
						{
							std::pair<State,State> frompair(ctit1->first, *csit2);
							std::map<std::pair<State,State>,State>::const_iterator from = statemap.find(frompair);
							if(from == statemap.end())
							{
								State fromst(ControlState(0,statemap.size()));
								fromst.insert(fromst.end(), ctit1->first.begin(), ctit1->first.end());
								fromst.insert(fromst.end(), csit2->begin(), csit2->end());
								from = statemap.insert(statemap.begin(), std::pair<std::pair<State,State>,State>(frompair, fromst));
							}
							std::pair<State,State> topair(ctit1->third, *csit2);
							std::map<std::pair<State,State>,State>::const_iterator to = statemap.find(topair);
							if(to == statemap.end())
							{
								State tost(ControlState(0,statemap.size()));
								tost.insert(tost.end(), ctit1->third.begin(), ctit1->third.end());
								tost.insert(tost.end(), csit2->begin(), csit2->end());
								to = statemap.insert(statemap.begin(), std::pair<std::pair<State,State>,State>(topair, tost));
							}
							Transition tr(from->second, *cait, to->second);
							int atomcount2 = tr.getAtoms().size();
							if(atomcount2 < atomcount)
							{
								extratr.clear();
								atomcount = atomcount2;
							}
							if(atomcount == atomcount2)
							{
								extratr.insert(tr);
							}
						}
					}
				}
				else if(range2.first != range2.second)
				{
					for(TransitionSet::const_iterator ctit2 = range2.first; ctit2 != range2.second; ctit2++)
					{
						for(StateSet::const_iterator csit1 = states1.begin(); csit1 != states1.end(); csit1++)
						{
							std::pair<State,State> frompair(*csit1, ctit2->first);
							std::map<std::pair<State,State>,State>::const_iterator from = statemap.find(frompair);
							if(from == statemap.end())
							{
								State fromst(ControlState(0,statemap.size()));
								fromst.insert(fromst.end(), csit1->begin(), csit1->end());
								fromst.insert(fromst.end(), ctit2->first.begin(), ctit2->first.end());
								from = statemap.insert(statemap.begin(), std::pair<std::pair<State,State>,State>(frompair, fromst));
							}
							std::pair<State,State> topair(*csit1, ctit2->third);
							std::map<std::pair<State,State>,State>::const_iterator to = statemap.find(topair);
							if(to == statemap.end())
							{
								State tost(ControlState(0,statemap.size()));
								tost.insert(tost.end(), csit1->begin(), csit1->end());
								tost.insert(tost.end(), ctit2->third.begin(), ctit2->third.end());
								to = statemap.insert(statemap.begin(), std::pair<std::pair<State,State>,State>(topair, tost));
							}
							Transition tr(from->second, *cait, to->second);
							int atomcount2 = tr.getAtoms().size();
							if(atomcount2 < atomcount)
							{
								extratr.clear();
								atomcount = atomcount2;
							}
							if(atomcount == atomcount2)
							{
								extratr.insert(tr);
							}
						}
					}
				}
				reach->insert(extratr.begin(), extratr.end());
			}

			progress++;
			ParallelAnalysisDisplay::setProgress(progress);
		}

//		std::cout << "...Done. The reachable part has " << reach->size() << " transitions." << std::endl << std::endl;
		ParallelAnalysisDisplay::removeTask();
		return new ElementaryInstanceOfLTSSchema(reach, initstate);
	}
	else
	{
		delete inst2;
		assert(false);
		return inst1;
	}
}

std::string ParallelInstanceOfLTSSchema::toString(const Context& c) const
{
	assert(iltsc1 != 0 && iltsc2 != 0);

	std::string str(1,'(');
	str.append(iltsc1->toString(c));
	str.push_back('|');
	str.push_back('|');
	str.append(iltsc2->toString(c));
	str.push_back(')');
	return str;
}

HidingInstanceOfLTSSchema::HidingInstanceOfLTSSchema(const InstanceOfLTSSchema* i, const ActionSet* s) : InstanceOfLTSSchema(*i), iltsc(i), issc(s)
{
	assert(i != 0 && s != 0);

	for(ActionSet::const_iterator cait = issc->begin(); cait != issc->end(); ++cait)
	{
//		alph->remove(*cait);
		acts->insert(*cait);
	}
}

HidingInstanceOfLTSSchema::HidingInstanceOfLTSSchema(const HidingInstanceOfLTSSchema& i) : InstanceOfLTSSchema(i), iltsc(i.iltsc->clone()), issc(new ActionSet(*(i.issc)))
{
}

HidingInstanceOfLTSSchema::~HidingInstanceOfLTSSchema()
{
	delete iltsc;
	delete issc;
}

HidingInstanceOfLTSSchema& HidingInstanceOfLTSSchema::operator=(const HidingInstanceOfLTSSchema& hi)
{
	if(this == &hi) return *this;
	HidingInstanceOfLTSSchema nhi(hi);
	std::swap(iltsc, nhi.iltsc);
	std::swap(issc, nhi.issc);
//	std::swap(alph, nhi.alph);
	std::swap(acts, nhi.acts);
	return *this;
}

const InstanceOfLTSSchema& HidingInstanceOfLTSSchema::getInstanceOfLTSSchema() const
{
	assert(iltsc != 0);

	return *iltsc;
}

const ActionSet& HidingInstanceOfLTSSchema::getHidingSet() const
{
	assert(issc != 0);

	return *issc;
}

int HidingInstanceOfLTSSchema::numOfOccurrencesInStates(const AtomSet& as) const
{
	assert(iltsc != 0);

	return iltsc->numOfOccurrencesInStates(as);
}

int HidingInstanceOfLTSSchema::numOfOccurrencesInTransitions(const AtomSet& as) const
{
	assert(iltsc != 0);

	return iltsc->numOfOccurrencesInTransitions(as);
}

int HidingInstanceOfLTSSchema::numOfOccurrencesInAdjacentStates(const AtomSet& as) const
{
	assert(iltsc != 0);

	return iltsc->numOfOccurrencesInAdjacentStates(as);
}

bool HidingInstanceOfLTSSchema::isDeterministic() const
{
	return false;
}

bool HidingInstanceOfLTSSchema::isComponentWiseDeterministic() const
{
	return false;
}

bool HidingInstanceOfLTSSchema::isComponentWiseMustDeterministic() const
{
	return false;
}

bool HidingInstanceOfLTSSchema::involvesInvisibleActions() const
{
	return true;
}

InstanceOfLTSSchema* HidingInstanceOfLTSSchema::clone() const
{
	return new HidingInstanceOfLTSSchema(*this);
}

InstanceOfLTSSchema* HidingInstanceOfLTSSchema::evaluate() const
{
	assert(iltsc != 0 && issc != 0);

	InstanceOfLTSSchema* inst = iltsc->evaluate();
	if(dynamic_cast<UnitLTS*>(inst))
	{
		return inst;
	}
	else if(ElementaryInstanceOfLTSSchema* elinst = dynamic_cast<ElementaryInstanceOfLTSSchema*>(inst))
	{
		const TransitionSet& trans = elinst->getTransitions();
		TransitionSet* newtrans = new TransitionSet();
		State* initstate = new State(elinst->getInitialState());
		for(TransitionSet::const_iterator tit = trans.begin(); tit != trans.end(); tit++)
		{
			if(issc->find(tit->second) != issc->end())
			{
				Transition tr = *tit;
				tr.second.chan = 0;
				newtrans->insert(*tit);
			}
			else
			{
				newtrans->insert(*tit);
			}
		}
		delete inst;
		return new ElementaryInstanceOfLTSSchema(newtrans, initstate);
	}
	else
	{
		assert(false);
		return inst;
	}
}

std::string HidingInstanceOfLTSSchema::toString(const Context& c) const
{
	assert(iltsc != 0);

	std::string str(1,'(');
	str.append(iltsc->toString(c));
	str.push_back('\\');
	str.append(issc->toString(c));
	str.push_back(')');
	return str;
}

#ifdef _OPENMP
std::vector<int> ParallelAnalysisDisplay::progress = std::vector<int>(omp_get_max_threads(),0);
std::vector<int> ParallelAnalysisDisplay::printedprogress = std::vector<int>(omp_get_max_threads(),0);
std::vector<int> ParallelAnalysisDisplay::maxprogress = std::vector<int>(omp_get_max_threads(),0);
#else
std::vector<int> ParallelAnalysisDisplay::progress = std::vector<int>(1,0);
std::vector<int> ParallelAnalysisDisplay::printedprogress = std::vector<int>(1,0);
std::vector<int> ParallelAnalysisDisplay::maxprogress = std::vector<int>(1,0);
#endif

void ParallelAnalysisDisplay::addTask(int max)
{
	int thread = 0;
	#ifdef _OPENMP
	thread = omp_get_thread_num();
	#endif
	progress[thread] = 0;
	printedprogress[thread] = 0;
	maxprogress[thread] = max;
}

void ParallelAnalysisDisplay::updateProgress(int delta)
{
	int thread = 0;
	#ifdef _OPENMP
	thread = omp_get_thread_num();
	#endif
	int skipped = 0;
	progress[thread] += delta;
	if(100*(progress[thread] - printedprogress[thread])/maxprogress[thread] > 0)
	{
		#pragma omp critical
		{
			std::cout << "Analysing:";
			for(int i = 0; i < progress.size(); i++)
			{
				if(maxprogress[i] != 0)
				{
					std::cout << std::setw(6) << std::right << 100*progress[i]/maxprogress[i] << '%';
					printedprogress[i] = progress[i];
				}
				else
				{
					skipped+=7;
				}
			}
			std::cout << std::setw(skipped) << " " << '\r';
		}
	}
}

void ParallelAnalysisDisplay::setProgress(int newprogress)
{
	int thread = 0;
	#ifdef _OPENMP
	thread = omp_get_thread_num();
	#endif
	int skipped = 0;
	progress[thread] = newprogress;
	if(100*(progress[thread] - printedprogress[thread])/maxprogress[thread] > 0)
	{
		#pragma omp critical
		{
			std::cout << "Analysing:";
			for(int i = 0; i < progress.size(); i++)
			{
				if(maxprogress[i] != 0)
				{
					std::cout << std::setw(6) << std::right << 100*progress[i]/maxprogress[i] << '%';
					printedprogress[i] = progress[i];
				}
				else
				{
					skipped+=7;
				}
			}
			std::cout << std::setw(skipped) << " " << '\r';
		}
	}
}

void ParallelAnalysisDisplay::removeTask()
{
	int thread = 0;
	#ifdef _OPENMP
	thread = omp_get_thread_num();
	#endif
	progress[thread] = 0;
	printedprogress[thread] = 0;
	maxprogress[thread] = 0;
}
