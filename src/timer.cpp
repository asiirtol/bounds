#include "timer.h"

Timer::Timer(int numoftimers) : timerCount(numoftimers), activeTimer(0), timers(0), running(false)
{
	timers = new long unsigned int[numoftimers];
	for(int i = 0; i < numoftimers; i++) timers[i] = 0;
	startTime.tv_sec = 0;
	startTime.tv_usec = 0;
	switchTime.tv_sec = 0;
	switchTime.tv_usec = 0;
	stopTime.tv_sec = 0;
	stopTime.tv_usec = 0;
}

Timer::~Timer()
{
	delete[] timers;
}

void Timer::start()
{
	gettimeofday(&startTime, 0);
	switchTime = startTime;
	running = true;
}

void Timer::stop()
{
	if(running)
	{
		gettimeofday(&stopTime, 0);
		timers[activeTimer] += (1000*(stopTime.tv_sec - switchTime.tv_sec) + (stopTime.tv_usec - switchTime.tv_usec)/1000);
	}
	running = false;
}

int Timer::switchToTimer(int timer)
{
	if(!running) return -1;
	if(timer < 0 || timer >= timerCount) return -1;
	struct timeval currentTime;
	gettimeofday(&currentTime, 0);
	timers[activeTimer] += (1000*(currentTime.tv_sec - switchTime.tv_sec) + (currentTime.tv_usec - switchTime.tv_usec)/1000);
	switchTime = currentTime;
	activeTimer = timer;
	return timer;
}

double Timer::getValue(int timer) const
{
	return (timer >= 0 && timer < timerCount) ? double(timers[timer])/1000 : 0;
}

double Timer::getTotalTime() const
{
	return (stopTime.tv_sec - startTime.tv_sec) + double(stopTime.tv_usec - startTime.tv_usec)/1000000;
}

