#include "fdr2output4altsim.h"
#include <fstream>
#include <sstream>
#include <list>
#include <set>
#include <map>
#include <iomanip>
#include <ctime>
#include <cstdio>
#include <future>
#include <mutex>
#include "reduce.h"

int AlternatingSimulationOutput::generateInstances(const Context& ctxt, const LTSSchema* spec, const LTSSchema* sys, const ValuationFormula* vf, std::set<Valuation*,PointerCompare<Valuation> >* vals, const std::string& filenameprefix, const std::map<TypeVariable,bool>& cutoffisexact, bool sound, bool complete)
{
	std::set<TypeVariable> types = sys->getTypeVariables();
	types.insert(spec->getTypeVariables().begin(), spec->getTypeVariables().end());
	types.insert(vf->getTypeVariables().begin(), vf->getTypeVariables().end());
	std::set<RelationVariable> relvars = sys->getRelationVariables();
	relvars.insert(spec->getRelationVariables().begin(), spec->getRelationVariables().end());
	relvars.insert(vf->getRelationVariables().begin(), vf->getRelationVariables().end());
	std::set<AtomVariable> atomvars = sys->getAtomVariables();
	atomvars.insert(spec->getAtomVariables().begin(), spec->getAtomVariables().end());
	atomvars.insert(vf->getAtomVariables().begin(), vf->getAtomVariables().end());
	std::set<QuorumFunctionVariable> qfvars = sys->getQuorumFunctionVariables();
	qfvars.insert(spec->getQuorumFunctionVariables().begin(), spec->getQuorumFunctionVariables().end());
	qfvars.insert(vf->getQuorumFunctionVariables().begin(), vf->getQuorumFunctionVariables().end());
	std::set<TypeVariable> datatypes;
	std::set<TypeVariable> proctypes;
	for(std::set<TypeVariable>::const_iterator ctit = types.begin(); ctit != types.end(); ctit++)
	{
		if(spec->isDataTypeVariable(*ctit) && sys->isDataTypeVariable(*ctit)) datatypes.insert(*ctit);
		if(spec->isProcessTypeVariable(*ctit) && sys->isProcessTypeVariable(*ctit)) proctypes.insert(*ctit);
	}
//	std::list<triplet<InstanceOfLTSSchema*, InstanceOfLTSSchema*, int> > instances;
	std::atomic<bool> isDeterministic{true};
	std::atomic<bool> isCorrect{true};
	std::atomic<int> preservedInstCount{0};
	int instCount = 0;

	bool isDeleted = true;
	int delCount = 0;
	while(isDeleted)
	{
		std::ostringstream filename;
		filename << filenameprefix << "_instance_" << delCount++ << ".csp";
		isDeleted = isDeleted && (std::remove(filename.str().c_str()) == 0);
	}

	int thread_count = std::max(1, static_cast<int>(std::thread::hardware_concurrency()));
	std::set<Valuation*,PointerCompare<Valuation> >* vals_part[thread_count];
	int partindex;
	for(partindex = 0; partindex < thread_count; partindex++)
	{
		vals_part[partindex] = new std::set<Valuation*,PointerCompare<Valuation> >();
	}
	partindex = 0;
	for(std::set<Valuation*,PointerCompare<Valuation> >::iterator sit = vals->begin(); sit != vals->end(); sit++)
	{
		vals_part[partindex]->insert(*sit);
		partindex = (partindex + 1) % thread_count;
	}
	vals->clear();

	std::vector<std::future<std::pair<bool,bool>>> futures;
	for(partindex = 0; partindex < thread_count; partindex++)
	{
		auto fut = std::async(std::launch::async, generatePartOfInstances, std::ref(ctxt), spec, sys, vf, vals_part[partindex], std::cref(proctypes), std::cref(datatypes), std::cref(relvars), std::cref(atomvars), std::cref(qfvars), std::cref(filenameprefix), instCount, std::cref(cutoffisexact), sound, complete, std::ref(preservedInstCount));
		futures.push_back(std::move(fut));
		instCount+=vals_part[partindex]->size();
	}
	for(partindex = 0; partindex < thread_count; partindex++)
	{
		auto res = futures[partindex].get();
		delete vals_part[partindex];
		isCorrect = isCorrect && res.first;
		isDeterministic = isDeterministic && res.second;
	}
	
	if(sound && !isDeterministic)
	{
		std::stringstream output;
		output << "NOTE! The specification is not deterministic. The reduction is not sound." << std::endl << std::endl;
		printString(output.str());
		sound = false;
	}
	
	std::ostringstream filename;
	filename << filenameprefix << "_instance_" << preservedInstCount << ".csp";
	std::ofstream file(filename.str().c_str(), std::fstream::out | std::fstream::trunc);
	if(file.is_open())
	{
		file << "-- Bounds2.3 end of alternating simulation check --" << std::endl;
		file << "-- The model is " << (complete ? "" : "*NOT* ") << "complete --" << std::endl;
		file << "-- The model is " << (sound ? "" : "*NOT* ") << "sound --" << std::endl << std::endl;
		file.close();
	}
	else
	{
		std::stringstream output;
		output << "Error in opening file " << filename.str() << std::endl << std::endl;
		printString(output.str());
	}
	
	return preservedInstCount;
}

std::pair<bool,bool> AlternatingSimulationOutput::generatePartOfInstances(const Context& ctxt, const LTSSchema* spec, const LTSSchema* sys, const ValuationFormula* vf, std::set<Valuation*,PointerCompare<Valuation>>* vals, const std::set<TypeVariable>& proctypes, const std::set<TypeVariable>& datatypes, const std::set<RelationVariable>& relvars, const std::set<AtomVariable>& atomvars, const std::set<QuorumFunctionVariable>& qfvars, const std::string& filenameprefix, int instCount, const std::map<TypeVariable,bool>& cutoffisexact, bool sound, bool complete, std::atomic<int>& preservedInstCount)
{
	bool isCorrect = true;
	bool isDeterministic = true;
	for(std::set<Valuation*,PointerCompare<Valuation> >::iterator sit = vals->begin(); sit != vals->end(); sit++)
	{
		{
			std::stringstream output;
			output << "Generating Instance " << instCount << " generated by valuation" << std::endl;
			for(std::set<TypeVariable>::const_iterator ctit = proctypes.begin(); ctit != proctypes.end(); ++ctit)
			{
				AtomSet* as = (*sit)->getValue(*ctit);
				output << ctit->toString(ctxt) << " -> " << as->toString(ctxt) << std::endl;
				delete as;
			}
			for(std::set<TypeVariable>::const_iterator ctit = datatypes.begin(); ctit != datatypes.end(); ++ctit)
			{
				AtomSet* as = (*sit)->getValue(*ctit);
				output << ctit->toString(ctxt) << " -> " << as->toString(ctxt) << std::endl;
				delete as;
			}
			for(std::set<RelationVariable>::const_iterator crit = relvars.begin(); crit != relvars.end(); ++crit)
			{
				AtomTupleSet* ats = (*sit)->getValue(*crit);
				output << crit->toString(ctxt) << " -> " << ats->toString(ctxt) << std::endl;
				delete ats;
			}
			for(std::set<AtomVariable>::const_iterator cait = atomvars.begin(); cait != atomvars.end(); ++cait)
			{
				output << cait->toString(ctxt) << " -> " << ctxt.atomToString((*sit)->getValue(*cait)) << std::endl;
			}
			for(auto& qfv : qfvars)
			{
				auto quorumsets = (*sit)->getValue(qfv);
				output << qfv.toString(ctxt) << " -> {";
				for(auto asit = quorumsets.begin(); asit != quorumsets.end();)
				{
					output << '(' << asit->first.toString(ctxt) << ")->" << asit->second.toString(ctxt);
					if(++asit != quorumsets.end()) output << ',';
				}
				output << '}' << std::endl;
			}
			output << std::endl;
			printString(output.str());

			bool keep = true;

			for(std::set<TypeVariable>::const_iterator ctit = proctypes.begin(); ctit != proctypes.end(); ++ctit)
			{	
				if(cutoffisexact.at(*ctit))
				{
					AtomSet* typeval = (*sit)->getValue(*ctit);
					for(std::set<AtomVariable>::const_iterator cavit = atomvars.begin(); cavit != atomvars.end(); ++cavit)
					{
						typeval->erase((*sit)->getValue(*cavit));
					}
					if(sys->instanceCoveredBySmallerOnes(**sit,*typeval) && spec->instanceCoveredBySmallerOnes(**sit,*typeval))
					{
						delete typeval;
						output.str("");
						output << "Discarding Instance " << instCount << ". The instance has no behaviour or is already covered by smaller ones." << std::endl << std::endl;
						printString(output.str());
						keep = false;
						break;
					}
					delete typeval;
				}
			}

			if(keep)
			{
				PartialInstanceOfLTSSchema* syspinst = sys->partialInstance(**sit);
				PartialInstanceOfLTSSchema* specpinst = spec->partialInstance(**sit);

				for(std::set<TypeVariable>::const_iterator ctit = datatypes.begin(); ctit != datatypes.end(); ++ctit)
				{	
					if(cutoffisexact.at(*ctit))
					{
						AtomSet* typeval = (*sit)->getValue(*ctit);
						int typesize = typeval->size();
						AtomSet* freeatoms = new AtomSet();
						for(std::set<AtomVariable>::const_iterator cavit = atomvars.begin(); cavit != atomvars.end(); ++cavit)
						{
							Atom a = (*sit)->getValue(*cavit);
							if(typeval->erase(a))
							{
								freeatoms->insert(a);
							}
						}
						int freecount = freeatoms->size();
						int sysidst, sysidtr, specidst, specidtr;
						int sysidst2 = syspinst->numOfOccurrencesInStates(*typeval);
						int sysidtr2 = syspinst->numOfOccurrencesInTransitions(*typeval);
						int specidst2 = specpinst->numOfOccurrencesInStates(*typeval);
						int specidtr2 = specpinst->numOfOccurrencesInTransitions(*typeval);
						delete freeatoms;
						do
						{
							sysidst = sysidst2;
							sysidtr = sysidtr2;
							specidst = specidst2;
							specidtr = specidtr2;
							if(typesize > 1 && typesize > freecount + std::max(specidtr + sysidst, sysidtr + specidst))
							{	
								output.str("");
								output << "Discarding Instance " << instCount << ". The instance is already covered by smaller ones." << std::endl << std::endl;
								printString(output.str());
								keep = false;
								break;
							}
							else
							{
								syspinst->composeLeaves(ctxt, MAX_SYS_COMP_SIZE);
								specpinst->composeLeaves(ctxt, MAX_SPEC_COMP_SIZE);
								sysidst2 = syspinst->numOfOccurrencesInStates(*typeval);
								sysidtr2 = syspinst->numOfOccurrencesInTransitions(*typeval);
								specidst2 = specpinst->numOfOccurrencesInStates(*typeval);
								specidtr2 = specpinst->numOfOccurrencesInTransitions(*typeval);							
							}
						}
						while(keep && (sysidst2 < sysidst || sysidtr2 < sysidtr || specidst2 < specidst || specidtr2 < specidtr));
						delete typeval;
					}
				}
			
				if(keep)
				{
					InstanceOfLTSSchema* sysinst = syspinst->instance();
					InstanceOfLTSSchema* specinst = specpinst->instance();

					if(datatypes.size() > 0 && isDeterministic)
					{
						bool thisisdet = (specinst->isComponentWiseDeterministic()) & (sysinst->isComponentWiseMustDeterministic());
						#pragma omp atomic
						isDeterministic &= thisisdet;
					}

					std::ostringstream filename;
					filename << filenameprefix << "_instance_" << preservedInstCount << ".csp";
					preservedInstCount++;
					bool isCorrect1 = writeInstance(ctxt, specinst, sysinst, instCount, filename.str(), sound, complete);
					isCorrect &= isCorrect1;
					
					delete sysinst;
					delete specinst;
				}
				delete syspinst;
				delete specpinst;
			}
			
			delete *sit;
		}
		instCount++;
	}
	return std::make_pair(isCorrect, isDeterministic);
}

bool AlternatingSimulationOutput::writeInstance(const Context& ctxt, const InstanceOfLTSSchema* specinst, const InstanceOfLTSSchema* sysinst, int num, const std::string& filename, bool sound, bool complete)
{
	bool isCorrect = false;
	std::stringstream output;

	std::ofstream file(filename.c_str(), std::fstream::out | std::fstream::trunc);
//	std::stringstream file(std::fstream::out | std::fstream::trunc);
	if(file.is_open())
	{
		file << "-- Bounds2.3 alternating simulation check --" << std::endl;
		file << "-- The model is " << (complete ? "" : "*NOT* ") << "complete --" << std::endl;
		file << "-- The model is " << (sound ? "" : "*NOT* ") << "sound --" << std::endl << std::endl;

		file << "transparent sbisim" << std::endl << std::endl;

		ActionSet chans = sysinst->getActionSet();
		ActionSet chans2 = specinst->getActionSet();
		chans.insert(chans2.begin(), chans2.end());
		chans2.clear();
		for(ActionSet::const_iterator cit = chans.begin(); cit != chans.end(); ++cit)
		{
			file << "channel " << cit->toString(ctxt,'_','_','_',' ',' ') << std::endl;
			file << "channel Must_" << cit->toString(ctxt,'_','_','_',' ',' ') << std::endl;
		}
		file << "channel err__" << std::endl;
		file << "channel tau__" << std::endl;
		file << std::endl;

		toFDRFormattedStream(ctxt, file, sysinst, "Impl");
		toFDRFormattedStream(ctxt, file, specinst, "Spec");

		file << "InAlph_Spec = CHAOS(inalph_Spec_Proc_1)" << std::endl;
		file << "MustInAlph_Spec = CHAOS(mustinalph_Spec_Proc_1)" << std::endl;
		file << "OutAlph_Spec = CHAOS(union(outalph_Spec_Proc_1,{err__}))" << std::endl;
		file << "MustOutAlph_Spec = CHAOS(mustoutalph_Spec_Proc_1)" << std::endl;
		file << "InAlph_Impl = CHAOS(inalph_Impl_Proc_1)" << std::endl;
		file << "MustInAlph_Impl = CHAOS(mustinalph_Impl_Proc_1)" << std::endl;
		file << "OutAlph_Impl = CHAOS(union(outalph_Impl_Proc_1,{err__}))" << std::endl;
		file << "MustOutAlph_Impl = CHAOS(mustoutalph_Impl_Proc_1)" << std::endl;
		file << "assert InAlph_Spec [T= InAlph_Impl" << std::endl;
		file << "assert InAlph_Impl [T= InAlph_Spec" << std::endl;
		file << "assert OutAlph_Spec [T= OutAlph_Impl" << std::endl;
		file << "assert OutAlph_Impl [T= OutAlph_Spec" << std::endl;
		
		if(file.good())
		{
			output << "Instance " << num << " written successfully to file " << filename << std::endl << std::endl;
			printString(output.str());
			isCorrect = true;
		}
		else
		{
			output << "An error has occurred while writing Instance " << num << " to a file." << std::endl << std::endl;
			printString(output.str());
		}
		file.close();
	}
	else
	{
		output << "Error in opening file " << filename << std::endl << std::endl;
		printString(output.str());
	}

	return isCorrect;
}


void AlternatingSimulationOutput::printString(const std::string& ss)
{
	#pragma omp critical
	std::cout << ss;
};


std::ostream& AlternatingSimulationOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const InstanceOfLTSSchema* i, const std::string& name, bool compress, int depth)
{
	if(const ParallelInstanceOfLTSSchema* pi = dynamic_cast<const ParallelInstanceOfLTSSchema*>(i))
	{
		toFDRFormattedStream(ctxt, os, *pi, name, compress, depth);
	}
/*	else if(const NamedInstanceOfLTSSchema* ni = dynamic_cast<const NamedInstanceOfLTSSchema*>(i))
	{
		toFDRFormattedStream(ctxt, os, *ni, name, compress, depth);
	}
*/	else if(const UnitLTS* id = dynamic_cast<const UnitLTS*>(i))
	{
		toFDRFormattedStream(ctxt, os, *id, name, compress, depth);
	}
	else if(const HidingInstanceOfLTSSchema* hi = dynamic_cast<const HidingInstanceOfLTSSchema*>(i))
	{
		assert(false);
	}
	else if(const ElementaryInstanceOfLTSSchema* ei = dynamic_cast<const ElementaryInstanceOfLTSSchema*>(i))
	{
		toFDRFormattedStream(ctxt, os, *ei, name, compress, depth);
	}
	return os;
}

std::ostream& AlternatingSimulationOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const UnitLTS& i, const std::string& name, bool compress, int depth)
{
	os << name << "_Proc_" << depth << " = STOP" << std::endl;
	os << "inalph_" << name << "_Proc_" << depth << " = {}" << std::endl;
	os << "outalph_" << name << "_Proc_" << depth << " = {}" << std::endl << std::endl;
	os << "mustinalph_" << name << "_Proc_" << depth << " = {}" << std::endl << std::endl;
	os << "mustoutalph_" << name << "_Proc_" << depth << " = {}" << std::endl << std::endl;
	os << "alph_" << name << "_Proc_" << depth << " = {}" << std::endl << std::endl;
	return os;
}

std::ostream& AlternatingSimulationOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const ElementaryInstanceOfLTSSchema& i, const std::string& name, bool compress, int depth)
{
	std::ostringstream procname;
	procname << name << "_Proc_" << depth;
	os << procname.str() << " =" << std::endl;
	os << "\tlet" << std::endl;
	const TransitionSet& tran = i.getTransitions();
	StateSet visited;
	State prevstate(ControlState(-1,-1));
	ActionSet missinginputs;
	for(TransitionSet::const_iterator ctit = tran.begin(); ctit != tran.end(); ++ctit)
	{
		visited.insert(ctit->first);
		if(ctit->first != prevstate)
		{
			for(ActionSet::const_iterator cait = missinginputs.begin(); cait != missinginputs.end(); cait++)
			{
				os << "\t\t\t[] (" << cait->toString(ctxt,'_','_','_',' ',' ') << " -> ERR)" << std::endl;
			}
			missinginputs = tran.getInputActionSet();
			missinginputs.erase(ctit->second);
			os << "\t\t" << ctit->first.toString(ctxt,'_','_','_') << " =\t(" << ctit->second.toString(ctxt,'_','_','_',' ',' ') << " -> " << ctit->third.toString(ctxt,'_','_','_') << ")" << std::endl;
			if(ctit->isMust || ctit->second.isInput) os << "\t\t\t[] (" << "Must_" << ctit->second.toString(ctxt,'_','_','_',' ',' ') << " -> " << ctit->third.toString(ctxt,'_','_','_') << ")" << std::endl;
			prevstate = ctit->first;
		}
		else
		{
			missinginputs.erase(ctit->second);
			os << "\t\t\t[] (" << ctit->second.toString(ctxt,'_','_','_',' ',' ') << " -> " << ctit->third.toString(ctxt,'_','_','_') << ")" << std::endl;
			if(ctit->isMust || ctit->second.isInput) os << "\t\t\t[] (" << "Must_" << ctit->second.toString(ctxt,'_','_','_',' ',' ') << " -> " << ctit->third.toString(ctxt,'_','_','_') << ")" << std::endl;
		}
	}
	if(static_cast<ControlState&>(prevstate) != ControlState(-1,-1))
	{
		for(ActionSet::const_iterator cait = missinginputs.begin(); cait != missinginputs.end(); cait++)
		{
			os << "\t\t\t[] (" << cait->toString(ctxt,'_','_','_',' ',' ') << " -> ERR)" << std::endl;
		}
	}
	const StateSet& states = tran.getStateSet();
	const ActionSet& inputs = tran.getInputActionSet();
	for(StateSet::const_iterator csit = states.begin(); csit != states.end(); ++csit)
	{
		if(visited.find(*csit)==visited.end())
		{
			if(inputs.size() == 0)
			{
				os << "\t\t" << csit->toString(ctxt,'_','_','_') << " = STOP" << std::endl;
			}
			else
			{
				for(ActionSet::const_iterator cait = inputs.begin(); cait != inputs.end(); cait++)
				{
					if(cait == inputs.begin())
					{
						os << "\t\t" << csit->toString(ctxt,'_','_','_') << " =\t(" << cait->toString(ctxt,'_','_','_',' ',' ') << " -> ERR)" << std::endl;
					}
					else
					{
						os << "\t\t\t[] (" << cait->toString(ctxt,'_','_','_',' ',' ') << " -> ERR)" << std::endl;
					}
				}
			}
		}
	}
	const ActionSet& events = tran.getActionSet();
	os << "\t\tERR =\t(err__ -> ERR)" << std::endl;
	for(ActionSet::const_iterator cait = events.begin(); cait != events.end(); ++cait)
	{
		os << "\t\t\t[] (" << cait->toString(ctxt,'_','_','_',' ',' ') << " -> ERR)" << std::endl;
	}
	os << "\twithin (" << i.getInitialState().toString(ctxt,'_','_','_') << " \\ {tau__})" << std::endl << std::endl;
	os << "inalph_" << procname.str() << " = ";
	toFDRFormattedStream(ctxt, os, i.getInputActionSet());
	os << std::endl << "mustinalph_" << procname.str() << " = ";
	toFDRFormattedStream(ctxt, os, i.getInputActionSet(), "Must_");
	os << std::endl << "outalph_" << procname.str() << " = ";
	toFDRFormattedStream(ctxt, os, i.getOutputActionSet());
	os << std::endl << "mustoutalph_" << procname.str() << " = ";
	toFDRFormattedStream(ctxt, os, i.getOutputActionSet(), "Must_");
	os << std::endl << "alph_" << procname.str() << " = union(union(inalph_" << procname.str() << ", outalph_" << procname.str() << "),union(mustinalph_" << procname.str() << ", mustoutalph_" << procname.str() << "))" << std::endl << std::endl;
	return os;
}

std::ostream& AlternatingSimulationOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const ParallelInstanceOfLTSSchema& i, const std::string& name, bool compress, int depth)
{
	std::string normalise;
	if(compress) normalise = "sbisim";
	std::ostringstream procname;
	procname << name << "_Proc_" << depth;
	int leftdepth = 2*depth;
	int rightdepth = leftdepth + 1;
	std::ostringstream leftname;
	leftname << name << "_Proc_" << leftdepth;
	std::ostringstream rightname;
	rightname << name << "_Proc_" << rightdepth;
	toFDRFormattedStream(ctxt, os, &(i.getOneInstance()), name, compress, leftdepth);
	toFDRFormattedStream(ctxt, os, &(i.getOtherInstance()), name, compress, rightdepth);
	os << procname.str() << "' = " << '(' << leftname.str() << " [|inter(alph_" << leftname.str() << ",alph_" << rightname.str() << ")|] " << rightname.str() << ')' << std::endl;
	os << procname.str() << " = " << normalise << '(' << procname.str() << "' \\ union(inter(inalph_" << leftname.str() << ",outalph_" << rightname.str() << "),inter(inalph_" << rightname.str() << ",outalph_" << leftname.str() << ")))" << std::endl;
	os << "inalph_" << procname.str() << " = union(diff(inalph_" << leftname.str() << ", outalph_" << rightname.str() << "),diff(inalph_" << rightname.str() << ", outalph_" << leftname.str() << "))" << std::endl;
	os << "mustinalph_" << procname.str() << " = union(diff(mustinalph_" << leftname.str() << ", mustoutalph_" << rightname.str() << "),diff(mustinalph_" << rightname.str() << ", mustoutalph_" << leftname.str() << "))" << std::endl;
	os << "outalph_" << procname.str() << " = union(diff(outalph_" << leftname.str() << ", inalph_" << rightname.str() << "),diff(outalph_" << rightname.str() << ", inalph_" << leftname.str() << "))" << std::endl;
	os << "mustoutalph_" << procname.str() << " = union(diff(mustoutalph_" << leftname.str() << ", mustinalph_" << rightname.str() << "),diff(mustoutalph_" << rightname.str() << ", mustinalph_" << leftname.str() << "))" << std::endl;
	os << "alph_" << procname.str() << " = union(union(inalph_" << procname.str() << ", outalph_" << procname.str() << "),union(mustinalph_" << procname.str() << ", mustoutalph_" << procname.str() << "))" << std::endl << std::endl;
	return os;
}

/*
std::ostream& AlternatingSimulationOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const NamedInstanceOfLTSSchema& i, const std::string& name, bool compress, int depth)
{
	toFDRFormattedStream(ctxt, os, &(i.getInstanceOfLTSSchema()), name, compress, depth);
	return os;
}
*/

std::ostream& AlternatingSimulationOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const ActionSet& as, const std::string& mod)
{
	ActionSet::const_iterator cait = as.begin();
	os << '{';
	if(cait != as.end())
	{
		os << mod << cait->toString(ctxt,'_','_','_',' ',' ');
		cait++;
	}
	for(; cait != as.end(); cait++)
	{
		os << ',' << mod << cait->toString(ctxt,'_','_','_',' ',' ');
	}
	os << '}';
	return os;
}


