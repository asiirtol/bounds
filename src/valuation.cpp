#include "valuation.h"
#include "elementschema.h"
#include <iostream>
#include <sstream>
#include <assert.h>
#include <algorithm>

Valuation::Valuation() : typeCount(0), relvarCount(0), avarCount(0), qfvCount(0), typeSizes(std::vector<int>()), relType(std::vector<std::vector<int>>{}), relTypeCumSizes(std::vector<std::pair<int, int>>{}), relTypeOffsets(std::vector<int>(1,0)), qfvType(std::vector<std::pair<int,std::vector<int>>>()), qfvTypeCumSizes(std::vector<std::pair<int,int>>(1, std::make_pair(0,0))), avarType(std::vector<int>()), avarVal(std::vector<int>()), colourCount(std::vector<int>()), colours(std::vector<std::vector<int> >()), greatestNonemptyRelvar(-1), greatestNonemptyRow(0), greatestNonemptyCol(0), greatestNonemptySet(0), greatestNonemptyAtom(0), valg(0), cang(0), n(0), m(0)
{
	valg = new graph[0];
	cang = new graph[0];
}

/*
Valuation::Valuation(const std::vector<int>& ts, const std::vector<std::pair<int, int> >& rvs) : typeCount(ts.size()), relvarCount(rvs.size()), avarCount(0), qfvCount(0), typeSizes(ts), relType(rvs), relTypeCumSizes(std::vector<std::pair<int, int> >(rvs.size()+1, std::pair<int,int>(0,0))), avarType(std::vector<int>()), avarVal(std::vector<int>()), colourCount(std::vector<int>(ts.size(), 1)), colours(std::vector<std::vector<int> >(ts.size())), greatestNonemptyRelvar(-1), greatestNonemptyRow(0), greatestNonemptyCol(0), greatestNonemptySet(0), greatestNonemptyAtom(0), valg(0), cang(0), n(0), m(0)
{
	int i;
	for(int i = 0; i < typeCount; i++)
	{
		colours[i].assign(typeSizes[i], 1);
	}
	for(int i = 0; i < relvarCount; i++)
	{
		if(relType[i].first < 0 || relType[i].second < 0 || relType[i].first >= typeCount || relType[i].second >= typeCount)
		{
			std::cout << "Every type in domain must be listed as a type variable!" << std::endl;
			exit(1);
		}

		relTypeCumSizes[i + 1].first = relTypeCumSizes[i].second + typeSizes[relType[i].first];
		if(relType[i].first != relType[i].second)
		{
			relTypeCumSizes[i + 1].second = relTypeCumSizes[i + 1].first + typeSizes[relType[i].second];
		}
		else
		{
			relTypeCumSizes[i + 1].second = relTypeCumSizes[i + 1].first;
		}
	}
	n = relTypeCumSizes[relvarCount].second;
	m = ((n - 1) / WORDSIZE) + 1;
	valg = new graph[n*m];
	cang = new graph[n*m];
	for(i = 0; i < n; ++i)
	{
		EMPTYSET(GRAPHROW(valg, i, m), m);
	}
	initialiseGraphs();
}
*/

void Valuation::initialiseGraphs()
{
	for(int i = 0; i < relvarCount; ++i)
	{
		initialiseRelationVariable(i);
		for(int j = i; j < relvarCount; ++j)
		{
			for(int it = 0; it < relType[i].size(); ++it)
			{
				int itoffset = baseTypeOffset(i, it);
				for(int jt = 0; jt < relType[j].size(); ++jt)
				{
					int jtoffset = baseTypeOffset(j, jt);
					if((i != j || itoffset != jtoffset) && relType[i][it] == relType[j][jt])
					{
						for(int k = 0; k < typeSizes[relType[i][it]]; ++k)
						{
							ADDELEMENT(GRAPHROW(valg, relTypeOffsets[i] + itoffset + k, m), relTypeOffsets[j] + jtoffset + k);
							ADDELEMENT(GRAPHROW(valg, relTypeOffsets[j] + jtoffset + k, m), relTypeOffsets[i] + itoffset + k);
						}
					}
				}
			}
/*
			if(relType[i][0] == relType[j][0])
			{
				for(int k = 0; k < typeSizes[relType[i][0]]; ++k)
				{
					ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[i].first + k, m), relTypeCumSizes[j].first + k);
					ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[j].first + k, m), relTypeCumSizes[i].first + k);
				}
			}
			if(relType[i][0] == relType[j][1])
			{
				for(int k = 0; k < typeSizes[relType[i][0]]; ++k)
				{
					ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[i].first + k, m), relTypeCumSizes[j].second + k);
					ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[j].second + k, m), relTypeCumSizes[i].first + k);
				}
			}
			if(relType[i][1] == relType[j][0])
			{
				for(int k = 0; k < typeSizes[relType[i][1]]; ++k)
				{
					ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[i].second + k, m), relTypeCumSizes[j].first + k);
					ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[j].first + k, m), relTypeCumSizes[i].second + k);
				}
			}
			if(relType[i][1] == relType[j][1])
			{
				for(int k = 0; k < typeSizes[relType[i][1]]; ++k)
				{
					ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[i].second + k, m), relTypeCumSizes[j].second + k);
					ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[j].second + k, m), relTypeCumSizes[i].second + k);
				}
			}
*/
		}
	}
	for(int i = 0; i < qfvCount; ++i)
	{
		int offset = relTypeOffsets[relvarCount] + qfvTypeCumSizes[i].second;
		for(int j = 0; j < typeSizes[qfvType[i].first]; ++j)
		{
			for(int k = 0; k < relvarCount; k++)
			{
				if(qfvType[i].first == relType[k][0])
				{
					ADDELEMENT(GRAPHROW(valg, offset + j, m), relTypeCumSizes[k].first + j);
					ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[k].first + j, m), offset + j);
				}
				if(relType[k][1] != relType[k][0] && qfvType[i].first == relType[k][1])
				{
					ADDELEMENT(GRAPHROW(valg, offset + j, m), relTypeCumSizes[k].second + j);
					ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[k].second + j, m), offset + j);
				}
			}
		}
		offset += typeSizes[qfvType[i].first];
		for(int l = 0; l < qfvType[i].second.size(); ++l)
		{
			for(int k = 0; k < relvarCount; k++)
			{
				for(int j = 0; j < typeSizes[qfvType[i].second[l]]; ++j)
				{
					if(qfvType[i].second[l] == relType[k][0])
					{
						ADDELEMENT(GRAPHROW(valg, offset + j, m), relTypeCumSizes[k].first + j);
						ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[k].first + j, m), offset + j);
					}
					if(relType[k][1] != relType[k][0] && qfvType[i].second[l] == relType[k][1])
					{
						ADDELEMENT(GRAPHROW(valg, offset + j, m), relTypeCumSizes[k].second + j);
						ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[k].second + j, m), offset + j);
					}
				}
			}
			offset += typeSizes[qfvType[i].second[l]];
		}
	}
	for(int i = 0; i < qfvCount; ++i)
	{
		for(int j = i; j < qfvCount; ++j)
		{
			int offseti = relTypeOffsets[relvarCount] + qfvTypeCumSizes[i].second;
			int offsetj = relTypeOffsets[relvarCount] + qfvTypeCumSizes[j].second;
			if(i != j && qfvType[i].first == qfvType[j].first)
			{
				for(int k = 0; k < typeSizes[qfvType[i].first]; ++k)
				{
					ADDELEMENT(GRAPHROW(valg, offsetj + k, m), offseti + k);
					ADDELEMENT(GRAPHROW(valg, offseti + k, m), offsetj + k);
				}			
			}
			offsetj += typeSizes[qfvType[j].first];
			for(int l = 0; l < qfvType[j].second.size(); ++l)
			{
				if(qfvType[i].first == qfvType[j].second[l])
				{
					for(int k = 0; k < typeSizes[qfvType[i].first]; ++k)
					{
						ADDELEMENT(GRAPHROW(valg, offsetj + k, m), offseti + k);
						ADDELEMENT(GRAPHROW(valg, offseti + k, m), offsetj + k);
					}			
				}
				offsetj += typeSizes[qfvType[j].second[l]];
			}
			offseti += typeSizes[qfvType[i].first];
			for(int l = 0; l < qfvType[i].second.size(); ++l)
			{
				offsetj = relTypeOffsets[relvarCount] + qfvTypeCumSizes[j].second + typeSizes[qfvType[j].first];
				for(int m = 0; m < qfvType[j].second.size(); ++m)
				{
					if((i != j || l != m) && qfvType[i].second[l] == qfvType[j].second[m])
					{
						for(int k = 0; k < typeSizes[qfvType[i].second[l]]; ++k)
						{
							ADDELEMENT(GRAPHROW(valg, offsetj + k, m), offseti + k);
							ADDELEMENT(GRAPHROW(valg, offseti + k, m), offsetj + k);
						}			
					}
					offsetj += typeSizes[qfvType[j].second[m]];
				}
				offseti += typeSizes[qfvType[i].second[l]];
			}
		}
	}
	for(int i = 0; i < qfvCount; ++i)
	{
		int offsetj = relTypeOffsets[relvarCount];
		for(int j = qfvTypeCumSizes[i+1].first; j < qfvTypeCumSizes[i+1].second; ++j)
		{
			int offseta = offsetj + qfvTypeCumSizes[i].second + typeSizes[qfvType[i].first];
			std::vector<int> atoms = int2vec(j - qfvTypeCumSizes[i+1].first, qfvType[i].second);
			for(int l = 0; l < atoms.size(); ++l)
			{
				ADDELEMENT(GRAPHROW(valg, offsetj + j, m), offseta + atoms[l]);
				ADDELEMENT(GRAPHROW(valg, offseta + atoms[l], m), offsetj + j);
				offseta += typeSizes[qfvType[i].second[l]];
			}
		}
	}
	for(int i = 0; i < n*m; ++i)
	{
		cang[i] = valg[i];
	}
}

void Valuation::initialiseRelationVariable(int rv, int offset, int first, int last)
{
	if(last == -1) last = relType[rv].size();

	assert(first < last);

	if(last - first <= 2) return;

	auto& types = relType[rv];
	int mid = (first + last)/2;
	initialiseRelationVariable(rv, offset, first, mid);
	int space1 = space(rv, first, mid);
	int quarter1 = (first + mid)/2;
	int quarter3 = (mid + last)/2;
	int type1size = typeSize(rv, first, mid);
	int type11size = typeSize(rv, first, quarter1);
	int type12size = typeSize(rv, quarter1, mid);
	int type2size = typeSize(rv, mid, last);
	int type21size = typeSize(rv, mid, quarter3);
	int type22size = typeSize(rv, quarter3, last);
	int offset11 = offset + typeOffset(rv, first, quarter1);
	int offset12 = offset + typeOffset(rv, quarter1, mid);
	if(!isSquareType(rv, first, mid))
	{
		offset12 += space(rv, first, quarter1);
	}
	int offset21 = offset + typeOffset(rv, mid, quarter3);
	int offset22 = offset + typeOffset(rv, quarter3, last);
	if(!isSquareType(rv, mid, last))
	{
		offset22 += space(rv, mid, quarter3);
	}
	int myoffset1 = offset + space1;
	int myoffset2 = myoffset1;
	if(!isSquareType(rv, first, last))
	{
		initialiseRelationVariable(rv, offset + space1, mid, last);
		int space2 = space(rv, mid, last);
		offset21 += space1;
		offset22 += space1;
		myoffset1 += space2;
		myoffset2 += space2 + type1size;
	}
	if(type11size == 0)
	{
		for(int j = 0; j < type12size; j++)
		{
			ADDELEMENT(GRAPHROW(valg, relTypeOffsets[rv] + offset12 + j, m), relTypeOffsets[rv] + myoffset1 + j);
			ADDELEMENT(GRAPHROW(valg, relTypeOffsets[rv] + myoffset1 + j, m), relTypeOffsets[rv] + offset12 + j);
		}
	}
	else
	{
		std::vector<int> val(relType[rv].size(),0);
		for(int i = 0; i < type1size; i++)
		{
			auto xy = coord(rv, val, first, mid);
			int p = vec2int(val, types, first, mid);
			ADDELEMENT(GRAPHROW(valg, relTypeOffsets[rv] + offset11 + xy.first, m), relTypeOffsets[rv] + myoffset1 + p);
			ADDELEMENT(GRAPHROW(valg, relTypeOffsets[rv] + myoffset1 + p, m), relTypeOffsets[rv] + offset12 + xy.second);
			val = modinc(val, types, first, mid);
		}
	}
	std::vector<int> val(relType[rv].size(),0);
	for(int i = 0; i < type2size; i++)
	{
		auto xy = coord(rv, val, mid, last);
		int p = vec2int(val, types, mid, last);
		ADDELEMENT(GRAPHROW(valg, relTypeOffsets[rv] + offset21 + xy.first, m), relTypeOffsets[rv] + myoffset2 + p);
		ADDELEMENT(GRAPHROW(valg, relTypeOffsets[rv] + myoffset2 + p, m), relTypeOffsets[rv] + offset22 + xy.second);
		val = modinc(val, types, mid, last);
	}
}

int Valuation::space(int rv, int first, int last) const
{
	if(first == last) return 0;
	if(last == -1) last = relType[rv].size();

	assert(first < last);

	if(last == first + 1) return typeSizes[relType[rv][first]];
	int mid = (first + last)/2;
	int myspace = space(rv, first, mid);
	int mysize = typeSize(rv, first, mid);
	if(!isSquareType(rv, first, last))
	{
		myspace += space(rv, mid, last);
		mysize += typeSize(rv, mid, last);
	}
	if(last - first > 2)
	{
		myspace += mysize;
	}
	return myspace;
}

int Valuation::type1Size(int rv) const
{
	int mid = static_cast<int>(relType[rv].size())/2;
	return typeSize(rv, 0, mid);
}

int Valuation::type2Size(int rv) const
{
	int mid = static_cast<int>(relType[rv].size())/2;
	return typeSize(rv, mid, relType[rv].size());
}

int Valuation::typeSize(int rv, int first, int last) const
{
	if(first == last) return 0;
	if(last == -1) last = relType[rv].size();

	assert(first < last);

	int s = 1;
	for(int i = first; i < last; i++)
	{
		s *= typeSizes[relType[rv][i]];
	}
	return s;
}

int Valuation::typeOffset(int rv, int first, int last) const
{
	if(first == last) return 0;
	if(last == -1) last = relType[rv].size();

	assert(first < last);

	if(last - first <= 2) return 0;
	int mid = (first + last)/2;
	int offset = space(rv, first, mid);
	if(!isSquareType(rv, first, last))
	{
		offset += space(rv, mid, last);
	}
	return offset;
}

int Valuation::baseTypeOffset(int rv, int type, int first, int last) const
{
	if(last == -1) last = relType[rv].size();

	assert(type >= first && type < last);

	if(type == first) return 0;
	int mid = (first + last)/2;
	if(type < mid) return baseTypeOffset(rv, type, first, mid);
	int offset = baseTypeOffset(rv, type, mid, last);
	if(!isSquareType(rv, first, last))
	{
		offset += space(rv, first, mid);
	}
	return offset;
}

int Valuation::pos(int rv, const std::vector<int>& p, int first, int last) const
{
	if(last == -1) last = relType[rv].size();
	if(last == first + 1) return p[first];	
	int mid = (first + last)/2;
	auto subcoord = coord(rv, p, first, last);
	int space1 = space(rv, first, mid);
	int pos = subcoord.first + subcoord.second * space1 + space1;
	if(!isSquareType(rv, first, last))
	{
		pos += space(rv, mid, last);
	}
	return  pos;
}

std::pair<int,int> Valuation::coord(int rv, const std::vector<int>& p, int first, int last) const
{
	if(last == -1) last = relType[rv].size();
	int mid = (first + last)/2;
	int xcoord = vec2int(p, relType[rv], first, mid);
	int ycoord = vec2int(p, relType[rv], mid, last);
	return std::make_pair(xcoord, ycoord);
}

bool Valuation::isSquareType(int rv, int first, int last) const
{
	if(last == -1) last = relType[rv].size();
	int mid = (first + last)/2;
	if(mid - first != last - mid) return false;
	for(int i = 0; i < mid - first; i++)
	{
		if(relType[rv][first + i] != relType[rv][mid + i]) return false;
	}
	return true;
}

std::vector<std::tuple<int,int,int>> Valuation::offsets(int rv, int first, int last) const
{
	if(last == -1) last = relType[rv].size();

	assert(first < last);

	if(last == first + 1) return {std::make_tuple(0, typeSizes[relType[rv][first]], relType[rv][first])};
	int mid = (first + last)/2;
	auto myoffsets = offsets(rv, first, mid);
	int myspace1 = space(rv, first, mid);
	int mytypesize = typeSize(rv, first, mid);
	int myoffset = myspace1;
	if(!isSquareType(rv, first, last))
	{
		auto offsets2 = offsets(rv, mid, last);
		for(auto& t : offsets2)
		{
			std::get<0>(t) += myspace1;
			myoffsets.push_back(t);
		}
		myoffset += space(rv, mid, last);
		mytypesize += typeSize(rv, mid, last);
	}
	if(last - first > 2)
	{
		myoffsets.push_back(std::make_tuple(myoffset, mytypesize, -1));
	}
	return myoffsets;
}

Valuation::~Valuation()
{
	delete[] valg;
	delete[] cang;
}

Valuation::Valuation(const Valuation& val) : typeCount(val.typeCount), relvarCount(val.relvarCount), avarCount(val.avarCount), qfvCount(val.qfvCount), typeSizes(val.typeSizes), relType(val.relType), relTypeCumSizes(val.relTypeCumSizes), relTypeOffsets(val.relTypeOffsets), qfvType(val.qfvType), qfvTypeCumSizes(val.qfvTypeCumSizes), avarType(val.avarType), avarVal(val.avarVal), colourCount(val.colourCount), colours(val.colours), greatestNonemptyRelvar(val.greatestNonemptyRelvar), greatestNonemptyRow(val.greatestNonemptyRow), greatestNonemptyCol(val.greatestNonemptyCol), greatestNonemptySet(val.greatestNonemptySet), greatestNonemptyAtom(val.greatestNonemptyAtom), valg(0), cang(0), n(val.n), m(val.m)
{
	valg = new graph[n*m];
	cang = new graph[n*m];
	for(int i = 0; i < n*m; ++i)
	{
		valg[i] = val.valg[i];
		cang[i] = val.cang[i];
	}
}

Valuation::Valuation(const Valuation& val, const std::vector<int>& types) : typeCount(val.typeCount), relvarCount(val.relvarCount + 1), avarCount(val.avarCount), qfvCount(val.qfvCount), typeSizes(val.typeSizes), relType(val.relType), relTypeCumSizes(val.relTypeCumSizes), relTypeOffsets(val.relTypeOffsets), qfvType(val.qfvType), qfvTypeCumSizes(val.qfvTypeCumSizes), avarType(val.avarType), avarVal(val.avarVal), colourCount(val.colourCount), colours(val.colours), greatestNonemptyRelvar(val.greatestNonemptyRelvar), greatestNonemptyRow(val.greatestNonemptyRow), greatestNonemptyCol(val.greatestNonemptyCol), greatestNonemptySet(val.greatestNonemptySet), greatestNonemptyAtom(val.greatestNonemptyAtom), valg(0), cang(0), n(0), m(0)
{
	for(auto& t : types)
	{
		if(t < 0 || t >= typeCount)
		{
			std::cout << "The domain types of the relation variable must be listed as type variables!" << std::endl;
			exit(1);
		}
	}
	relType.push_back(types);
	int cumoffset = relTypeOffsets[relvarCount-1] + typeOffset(relvarCount-1);
	int cumSize1 = cumoffset;
	int cumSize2 = cumSize1;
	// std::cout << "n: " << cumoffset << std::endl;
	cumoffset += type2Size(relvarCount-1);
	// std::cout << "n: " << cumoffset << std::endl;
	if(!isSquareType(relvarCount-1)) 
	{
		int t1size = type1Size(relvarCount-1);
		cumSize2 += t1size;
		cumoffset += t1size;
	}
	relTypeCumSizes.push_back(std::make_pair(cumSize1, cumSize2));
	relTypeOffsets.push_back(cumoffset);
	n = cumoffset + qfvTypeCumSizes[qfvCount].second;
	/*
	int cumSize1 = relTypeCumSizes[relvarCount-1].second + type1Size(relvarCount-1);//typeSizes[type1];
	int cumSize2 = cumSize1;
	if(!isSquareType(relvarCount-1)) cumSize2 += type2Size(relvarCount-1);//typeSizes[type2];
	relTypeCumSizes.push_back(std::pair<int, int>(cumSize1, cumSize2));
	n = cumSize2 + qfvTypeCumSizes[qfvCount].second;
	*/
	m = ((n - 1) / WORDSIZE) + 1;
	valg = new graph[n*m];
	cang = new graph[n*m];
	for(int i = 0; i < n; ++i)
	{
		EMPTYSET(GRAPHROW(valg, i, m), m);
	}
	for(int i = 0; i < relTypeOffsets[relvarCount-1]; ++i)
	{
		for(int j = 0; j < relTypeOffsets[relvarCount-1]; ++j)
		{
			if(ISELEMENT(GRAPHROW(val.valg, i, val.m), j))
			{
				ADDELEMENT(GRAPHROW(valg, i, m), j);
			}
		}
	}
	int offset = relTypeOffsets[relvarCount];
	int valoffset = val.relTypeOffsets[val.relvarCount];
	for(int i = 0; i < qfvTypeCumSizes[qfvCount].second; ++i)
	{
		for(int j = 0; j < qfvTypeCumSizes[qfvCount].second; ++j)
		{
			if(ISELEMENT(GRAPHROW(val.valg, valoffset + i, val.m), valoffset + j))
			{
				ADDELEMENT(GRAPHROW(valg, offset + i, m), offset + j);
			}
		}
	}
/*
	for(int i = 0; i < val.n; ++i)
	{
		for(int j = 0; j < val.m; ++j)
		{
			valg[m*i+j] = val.valg[(val.m)*i+j];
		}
	}
*/
	initialiseGraphs();
}

Valuation::Valuation(const Valuation& val, int type, std::vector<int>& domain) : typeCount(val.typeCount), relvarCount(val.relvarCount), avarCount(val.avarCount), qfvCount(val.qfvCount + 1), typeSizes(val.typeSizes), relType(val.relType), relTypeCumSizes(val.relTypeCumSizes), relTypeOffsets(val.relTypeOffsets), qfvType(val.qfvType), qfvTypeCumSizes(val.qfvTypeCumSizes), avarType(val.avarType), avarVal(val.avarVal), colourCount(val.colourCount), colours(val.colours), greatestNonemptyRelvar(val.greatestNonemptyRelvar), greatestNonemptyRow(val.greatestNonemptyRow), greatestNonemptyCol(val.greatestNonemptyCol), valg(0), cang(0), n(0), m(0)
{
	if(type < 0 || type >= typeCount)
	{
		std::cout << "The type of the quorum function variable must be listed as a type variable!" << std::endl;
		exit(1);
	}
	int cumSize1 = 0;
	int cumSize2 = 1;
	for(auto t : domain)
	{
		if(t < 0 || t >= typeCount)
		{
			std::cout << "The domain types of the quorum function variable must be listed as types variable!" << std::endl;
			exit(1);
		}
		cumSize1 += typeSizes[t];
		cumSize2 *= typeSizes[t];
	}
	greatestNonemptySet = cumSize2;
	greatestNonemptyAtom = typeSizes[type] - 1;
	qfvType.push_back(std::pair<int, std::vector<int>>(type, domain));
	cumSize1 += qfvTypeCumSizes[qfvCount-1].second + typeSizes[type];
	cumSize2 += cumSize1;
	qfvTypeCumSizes.push_back(std::make_pair(cumSize1, cumSize2));
	n = cumSize2 + relTypeOffsets[relvarCount];
	m = ((n - 1) / WORDSIZE) + 1;
	valg = new graph[n*m];
	cang = new graph[n*m];
	for(int i = 0; i < n; ++i)
	{
		EMPTYSET(GRAPHROW(valg, i, m), m);
	}
/*
	for(int i = 0; i < relTypeCumSizes[relvarCount-1].second; ++i)
	{
		for(int j = 0; j < relTypeCumSizes[relvarCount-1].second; ++j)
		{
			if(ISELEMENT(GRAPHROW(val.valg, i, val.m), j))
			{
				ADDELEMENT(GRAPHROW(valg, i, m), j);
			}
		}
	}
	int offset = relTypeCumSizes[relvarCount].second;
	int valoffset = val.relTypeCumSizes[val.relvarCount].second;
	for(int i = 0; i < qfvTypeCumSizes[qfvCount]; ++i)
	{
		for(int j = 0; j < qfvTypeCumSizes[qfvCount]; ++j)
		{
			if(ISELEMENT(GRAPHROW(val.valg, valoffset + i, val.m), valoffset + j))
			{
				ADDELEMENT(GRAPHROW(valg, offset + i, m), offset + j);
			}
		}
	}
*/

	for(int i = 0; i < val.n; ++i)
	{
		for(int j = 0; j < val.m; ++j)
		{
			valg[m*i+j] = val.valg[(val.m)*i+j];
		}
	}

	initialiseGraphs();

	int offset = relTypeOffsets[relvarCount] + qfvTypeCumSizes[qfvCount-1].second;
	for(int j = 0; j < typeSizes[qfvType[qfvCount-1].first]; ++j)
	{
		for(int k = qfvTypeCumSizes[qfvCount].first; k < qfvTypeCumSizes[qfvCount].second; ++k)
		{
			ADDELEMENT(GRAPHROW(valg, offset + j, m), k);
			ADDELEMENT(GRAPHROW(valg, k, m), offset + j);
		}
	}

}

Valuation&::Valuation::operator=(const Valuation& val)
{
	Valuation tempval(val);
	std::swap(typeCount, tempval.typeCount);
	std::swap(relvarCount, tempval.relvarCount);
	std::swap(avarCount, tempval.avarCount);
	std::swap(qfvCount, tempval.qfvCount);
	std::swap(typeSizes, tempval.typeSizes);
	std::swap(relType, tempval.relType);
	std::swap(relTypeCumSizes, tempval.relTypeCumSizes);
	std::swap(relTypeOffsets, tempval.relTypeOffsets);
	std::swap(qfvType, tempval.qfvType);
	std::swap(qfvTypeCumSizes, tempval.qfvTypeCumSizes);
	std::swap(avarType, tempval.avarType);
	std::swap(avarVal, tempval.avarVal);
	std::swap(colourCount, tempval.colourCount);
	std::swap(colours, tempval.colours);
	std::swap(greatestNonemptyRelvar, tempval.greatestNonemptyRelvar);
	std::swap(greatestNonemptyRow, tempval.greatestNonemptyRow);
	std::swap(greatestNonemptyCol, tempval.greatestNonemptyCol);
	std::swap(greatestNonemptySet, tempval.greatestNonemptySet);
	std::swap(greatestNonemptyAtom, tempval.greatestNonemptyAtom);
	std::swap(valg, tempval.valg);
	std::swap(cang, tempval.cang);
	std::swap(n, tempval.n);
	std::swap(m, tempval.m);
	return *this;
}

/*
std::set<TypeVariable> Valuation::getTypeVariables() const
{
	std::set<TypeVariable> types;
	for(std::map<TypeVariable, int>::const_reverse_iterator crit = type2int.rbegin(); crit != type2int.rend(); ++crit)
	{
		types.insert(crit->first);
	}
	return types;
}

std::set<RelationVariable> Valuation::getRelationVariables() const
{
	std::set<RelationVariable> relvars;
	for(std::map<RelationVariable, int>::const_reverse_iterator crit = relv2int.rbegin(); crit != relv2int.rend(); ++crit)
	{
		relvars.insert(crit->first);
	}
	return relvars;
}

std::set<AtomVariable> Valuation::getAtomVariables() const
{
	std::set<AtomVariable> atomvars;
	for(std::map<AtomVariable, int>::const_reverse_iterator crit = atomv2int.rbegin(); crit != atomv2int.rend(); ++crit)
	{
		atomvars.insert(crit->first);
	}
	return atomvars;
}
*/

AtomSet* Valuation::getValue(const TypeVariable& tv) const
{
	assert(tv.second == TYPEVAR);

	AtomSet* atoms = new AtomSet();
	if(tv.id >= 0 && tv.id < typeCount)
	{
		for(int i = typeSizes[tv.id]-1; i >= 0; i--)
		{
			atoms->insert(Atom(i,tv));
		}
	}
	return atoms;
}

AtomSet* Valuation::getValue(const std::set<TypeVariable>& types) const
{
	AtomSet* atoms = new AtomSet();

	for(std::set<TypeVariable>::const_iterator ctit = types.begin(); ctit != types.end(); ctit++)
	{
		if(ctit->second == TYPEVAR)
		{
			if(ctit->id >= 0 && ctit->id < typeCount)
			{
				for(int i = typeSizes[ctit->id]-1; i >= 0; i--)
				{
					atoms->insert(Atom(i,*ctit));
				}
			}
		}
	}
	return atoms;
}

Atom Valuation::getValue(const AtomVariable& av) const
{
	assert(av.id >= 0 && av.id < avarCount);
	return Atom(avarVal[av.id],av.second);
}

bool Valuation::isMember(int rv, const std::vector<int>& avs) const
{
	assert(rv >= 0 && rv < relvarCount);

	std::vector<int> p{};
	for(auto av : avs)
	{
		assert(av >= 0 && av < avarCount);
		p.push_back(avarVal[av]);
	}

	auto xy = coord(rv, p);
	//return ISELEMENT(GRAPHROW(valg, relTypeCumSizes[rv].first + avarVal[av1], m), relTypeCumSizes[rv].second + avarVal[av2]);
	return ISELEMENT(GRAPHROW(valg, relTypeCumSizes[rv].first + xy.first, m), relTypeCumSizes[rv].second + xy.second);
}

void Valuation::setMember(int rv, const std::vector<int>& p)
{
	assert(rv >= 0 && rv < relvarCount);

/*	std::vector<int> p{};
	for(auto av : avs)
	{
		assert(av >= 0 && av < avarCount);
		p.push_back(avarVal[av]);
	}
*/
	auto xy = coord(rv, p);
	ADDELEMENT(GRAPHROW(valg, relTypeCumSizes[rv].first + xy.first, m), relTypeCumSizes[rv].second + xy.second);
}

bool Valuation::isMember(int qfv, int av, const std::vector<int>& params) const
{
	if(qfv < 0 || qfv >= qfvCount) return false;
	if(av < 0 || av >= avarCount) return false;
	if(params.size() != qfvType[qfv].second.size()) return false;

	std::vector<int> avs;
	for(int i = 0; i < params.size(); i++)
	{
		if(params[i]< 0 || params[i] >= avarCount) return false;
		avs.push_back(avarVal[params[i]]);
	}
	int qset = vec2int(avs, qfvType[qfv].second);

	return ISELEMENT(GRAPHROW(valg, relTypeOffsets[relvarCount] + qfvTypeCumSizes[qfv].second + avarVal[av], m), relTypeOffsets[relvarCount] + qfvTypeCumSizes[qfv+1].first + qset);
}

bool Valuation::areEqual(int av1, int av2) const
{
	if(av1 >= 0 && av1 < avarCount && av2 >= 0 && av2 < avarCount)
	{
		return avarType[av1] == avarType[av2] && avarVal[av1] == avarVal[av2];
	}
	else
	{
		return false;
	}
}

AtomTupleSet* Valuation::getValue(const RelationVariable& rv) const
{
	assert(rv.id >= 0 && rv.id < relvarCount);

	AtomTupleSet* ats = new AtomTupleSet();

	const auto& typeids = relType[rv.id];
	const auto& typevars = std::get<1>(rv);
	for(auto& t : typevars)
	{
		assert(t.id >= 0 && t.id < typeCount);
	}

	std::vector<int> val(typeids.size(),0);
	int domsize = 1;
	for(auto& id : typeids)
	{
		domsize *= typeSizes[id];
	}
	for(int i = 0; i < domsize; i++)
	{
		auto c = coord(rv.id, val);
		if(ISELEMENT(GRAPHROW(valg, relTypeCumSizes[rv.id].first + c.first, m), relTypeCumSizes[rv.id].second + c.second))
		{
			AtomTuple at;
			for(int j = 0; j < typevars.size(); j++)
			{
				at.push_back(Atom(val[j],typevars[j]));
			}
			ats->insert(at);
		}
		val = modinc(val, typeids);
	}
/*
	for(int i = 0; i < type1size(rv.id); ++i)
	{
		for(int j = 0; j < type2size(rv.id); ++j)
		{
			if(ISELEMENT(GRAPHROW(valg, relTypeCumSizes[rv.id].first + i, m), relTypeCumSizes[rv.id].second + j))
			{
				AtomTuple at;
				at.push_back(Atom(i,std::get<1>(rv).at(0)));
				at.push_back(Atom(j,std::get<1>(rv).at(1)));
				ats->insert(at);
			}
		}
	}
*/
	return ats;
}

AtomTupleSet* Valuation::getComplementValue(const RelationVariable& rv) const
{
	assert(rv.id >= 0 && rv.id < relvarCount);

	AtomTupleSet* ats = new AtomTupleSet();

	const auto& typeids = relType[rv.id];
	const auto& typevars = std::get<1>(rv);
	for(auto& t : typevars)
	{
		assert(t.id >= 0 && t.id < typeCount);
	}

	std::vector<int> val(typeids.size(),0);
	int domsize = 1;
	for(auto& id : typeids)
	{
		domsize *= typeSizes[id];
	}
	for(int i = 0; i < domsize; i++)
	{
		auto c = coord(rv.id, val);
		if(!ISELEMENT(GRAPHROW(valg, relTypeCumSizes[rv.id].first + c.first, m), relTypeCumSizes[rv.id].second + c.second))
		{
			AtomTuple at;
			for(int j = 0; j < typevars.size(); j++)
			{
				at.push_back(Atom(val[j],typevars[j]));
			}
			ats->insert(at);
		}
		val = modinc(val, typeids);
	}
/*
	for(int i = 0; i < type1size(rv.id); ++i)
	{
		for(int j = 0; j < type2size(rv.id); ++j)
		{
			if(ISELEMENT(GRAPHROW(valg, relTypeCumSizes[rv.id].first + i, m), relTypeCumSizes[rv.id].second + j))
			{
				AtomTuple at;
				at.push_back(Atom(i,std::get<1>(rv).at(0)));
				at.push_back(Atom(j,std::get<1>(rv).at(1)));
				ats->insert(at);
			}
		}
	}
*/
	return ats;
}

std::vector<std::pair<AtomTuple,AtomSet>> Valuation::getValue(const QuorumFunctionVariable& qfv) const
{
	std::vector<std::pair<AtomTuple,AtomSet>> quorumsets;
	if(qfv.id >= 0 && qfv.id < qfvCount && std::get<1>(qfv).id >= 0 && std::get<1>(qfv).id < typeCount)
	{
		int offset = relTypeOffsets[relvarCount];
		for(int i = qfvTypeCumSizes[qfv.id + 1].first; i < qfvTypeCumSizes[qfv.id + 1].second; ++i)
		{
			std::vector<int> a = int2vec(i - qfvTypeCumSizes[qfv.id + 1].first, qfvType[qfv.id].second);
			AtomTuple at;
			for(int j = 0; j < a.size(); ++j)
			{
				at.push_back(Atom(a[j],std::get<2>(qfv)[j]));
			}
			AtomSet as;
			for(int j = 0; j < typeSizes[qfvType[qfv.id].first]; ++j)
			{
				if(ISELEMENT(GRAPHROW(valg, offset + i, m), offset + qfvTypeCumSizes[qfv.id].second + j))
				{
					as.insert(Atom(j,std::get<1>(qfv)));
				}
			}
			quorumsets.push_back(std::make_pair(std::move(at),std::move(as)));
		}
	}
	return std::move(quorumsets);
}

AtomSet Valuation::getValue(const QuorumFunctionVariable& qfv, const ParameterList& params) const
{
	std::vector<int> pvals;
	for(int i = 0; i < params.size(); ++i)
	{
		const AtomVariable& av = params[i]->getAtomVariable();
		pvals.push_back(avarVal[av.id]);
	}
	int qset = vec2int(pvals, qfvType[qfv.id].second);

	AtomSet as;
	int offset = relTypeOffsets[relvarCount];
	for(int j = 0; j < typeSizes[qfvType[qfv.id].first]; ++j)
	{
		if(ISELEMENT(GRAPHROW(valg, offset + qfvTypeCumSizes[qfv.id + 1].first + qset, m), offset + qfvTypeCumSizes[qfv.id].second + j))
		{
			as.insert(Atom(j,std::get<1>(qfv)));
		}
	}
	return as;
}

std::list<Valuation*> Valuation::children() const
{
	std::list<Valuation*> vals;
	if(greatestNonemptyRelvar >= 0)
	{
		int type1size = type1Size(greatestNonemptyRelvar);
		int type2size = type2Size(greatestNonemptyRelvar);
		for(int k = greatestNonemptyCol + 1; k < type2size; ++k)
		{
			Valuation* val = new Valuation(*this);
			ADDELEMENT(GRAPHROW(val->valg, relTypeCumSizes[greatestNonemptyRelvar].first + greatestNonemptyRow, m), relTypeCumSizes[greatestNonemptyRelvar].second + k);
			val->greatestNonemptyCol = k;
			for(int i = 0; i < n*m; ++i) val->cang[i] = val->valg[i];
			vals.push_back(val);
		}
		for(int j = greatestNonemptyRow + 1; j < type1size; ++j)
		{
			for(int k = 0; k < type2size; ++k)
			{
				Valuation* val = new Valuation(*this);
				ADDELEMENT(GRAPHROW(val->valg, relTypeCumSizes[greatestNonemptyRelvar].first + j, m), relTypeCumSizes[greatestNonemptyRelvar].second + k);
				val->greatestNonemptyRow = j;
				val->greatestNonemptyCol = k;
				for(int i = 0; i < n*m; ++i) val->cang[i] = val->valg[i];
				vals.push_back(val);
			}
		}
	}
	for(int i = greatestNonemptyRelvar + 1; i < relvarCount; ++i)
	{
		int type1size = type1Size(greatestNonemptyRelvar);
		int type2size = type2Size(greatestNonemptyRelvar);
		for(int j = 0; j < type1size; ++j)
		{
			for(int k = 0; k < type2size; ++k)
			{
				Valuation* val = new Valuation(*this);
				ADDELEMENT(GRAPHROW(val->valg, relTypeCumSizes[i].first + j, m), relTypeCumSizes[i].second + k);
				val->greatestNonemptyRelvar = i;
				val->greatestNonemptyRow = j;
				val->greatestNonemptyCol = k;
				for(int i = 0; i < n*m; ++i) val->cang[i] = val->valg[i];
				vals.push_back(val);
			}
		}
	}
	return vals;
}

std::list<Valuation*> Valuation::childrenQFV(bool decrementGreatestNonemptySet) const
{
	std::list<Valuation*> vals;
	if(decrementGreatestNonemptySet)
	{
		const_cast<Valuation&>(*this).greatestNonemptySet--;
		const_cast<Valuation&>(*this).greatestNonemptyAtom = typeSizes[qfvType[qfvCount-1].first] - 1;
	}
	if(greatestNonemptySet >= 0 && greatestNonemptyAtom >= 0)
	{
		int atomoffset = relTypeOffsets[relvarCount] + qfvTypeCumSizes[qfvCount-1].second;
		int setoffset = relTypeOffsets[relvarCount] + qfvTypeCumSizes[qfvCount].first;
		for(int i = 0; i <= greatestNonemptyAtom; ++i)
		{
			Valuation* val = new Valuation(*this);
			DELELEMENT(GRAPHROW(val->valg, atomoffset + i, m), setoffset + greatestNonemptySet);
			DELELEMENT(GRAPHROW(val->valg, setoffset + greatestNonemptySet, m), atomoffset + i);

			/* test whether the set is still a quorum set, if not, empty the set */
			int elsinset = 0;
			for(int k = 0; k < typeSizes[qfvType[qfvCount-1].first]; ++k)
			{
				elsinset += ISELEMENT(GRAPHROW(val->valg, atomoffset + k, m), setoffset + greatestNonemptySet);
			}
			if(2*elsinset <= typeSizes[qfvType[qfvCount-1].first])
			{
				for(int k = 0; k < typeSizes[qfvType[qfvCount-1].first]; ++k)
				{
					DELELEMENT(GRAPHROW(val->valg, atomoffset + k, m), setoffset + greatestNonemptySet);
					DELELEMENT(GRAPHROW(val->valg, setoffset + greatestNonemptySet, m), atomoffset + k);
				}
				val->greatestNonemptyAtom = -1;
			}
			else
			{
				val->greatestNonemptyAtom = i-1;
			}
			val->greatestNonemptySet = greatestNonemptySet;

			for(int i = 0; i < n*m; ++i) val->cang[i] = val->valg[i];
			vals.push_back(val);
		}		
	}
	return vals;
}

std::list<Valuation*> Valuation::addAtomVariable(const TypeVariable& type, int v) const
{
	assert(type.second == FINTYPE || (type.id >= 0 && type.id < typeCount));

	std::list<Valuation*> vals;

/*	if(type < 0 || type >= typeCount)
	{
			std::cout << "The type of the atom variable must be listed as a type variable!" << std::endl;
			exit(1);
	}
*/

	// std::cout << "adding an atom variable" << std::endl;
	if(type.second == TYPEVAR)
	{
		for(int a = 0; a < typeSizes[type.id]; a++)
		{
			if(v == -1 || v == a)
			{
				Valuation* val = new Valuation(*this);
				val->avarCount++;
				val->avarType.push_back(type.id);
				val->avarVal.push_back(a);
				if(val->colours[type.id][a] == 1)
				{
					val->colours[type.id][a] = val->colourCount[type.id] + 1;
					if(val->colourCount[type.id] < val->typeSizes[type.id]) val->colourCount[type.id]++;
				}
				vals.push_back(val);				
			}
		}
	}
	else
	{
		for(int a = 0; a < type.id; a++)
		{
			if(v == -1 || v == a)
			{
				Valuation* val = new Valuation(*this);
				val->avarCount++;
				val->avarType.push_back(-1);
				val->avarVal.push_back(a);
				vals.push_back(val);
			}
		}
	}

	return vals;
}

std::list<Valuation*> Valuation::addTypeVariable(int cutoff, int cutoffmin) const
{
	std::list<Valuation*> vals;
	for(int size = cutoffmin; size <= cutoff; size++)
	{
		Valuation* val = new Valuation(*this);
		val->typeCount++;
		val->typeSizes.push_back(size);
		val->colourCount.push_back(1);
		val->colours.push_back(std::vector<int>(size,1));
		vals.push_back(val);
	}
	return vals;
}

/*
Valuation* Valuation::addTypeVariable(int size) const
{
	Valuation* val = new Valuation(*this);
	val->typeCount++;
	val->typeSizes.push_back(size);
	val->colourCount.push_back(1);
	val->colours.push_back(std::vector<int>(size,1));
	return val;
}
*/

Valuation* Valuation::addRelationVariable(const std::vector<TypeVariable>& types) const
{
	std::vector<int> typeids{};
	for(auto& t : types)
	{
		assert(t.id >= 0 && t.id < typeCount);
		typeids.push_back(t.id);
	}

	Valuation* val = new Valuation(*this, typeids);
	val->greatestNonemptyRelvar = val->relvarCount - 1;
	val->greatestNonemptyCol = val->type2Size(val->relvarCount - 1);//val->typeSizes[type2.id];
	val->greatestNonemptyRow = -1;
	return val;
}

Valuation* Valuation::addQuorumFunctionVariable(const TypeVariable& type, const std::vector<TypeVariable>& domain) const
{
	assert(type.id >= 0 && type.id < typeCount);

	std::vector<int> domids;
	for(auto& t : domain)
	{
		assert(t.id >= 0 && t.id < typeCount);
		domids.push_back(t.id);
	}

	Valuation* val = new Valuation(*this, type.id, domids);
	return val;
}

bool Valuation::operator<(const Valuation& val) const
{
	if(typeCount < val.typeCount) return true;
	if(val.typeCount < typeCount) return false;

	if(avarCount < val.avarCount) return true;
	if(val.avarCount < avarCount) return false;

	if(relvarCount < val.relvarCount) return true;
	if(val.relvarCount < relvarCount) return false;

	if(qfvCount < val.qfvCount) return true;
	if(val.qfvCount < qfvCount) return false;

	for(int i = 0; i != typeCount; ++i)
	{
		if(typeSizes[i] < val.typeSizes[i]) return true;
		if(val.typeSizes[i] < typeSizes[i]) return false;
	}

	for(int i = 0; i != avarCount; i++)
	{
		if(avarType[i] < val.avarType[i]) return true;
		if(avarType[i] > val.avarType[i]) return false;
	}

	for(int i = 0; i != avarCount; ++i)
	{
		if(avarType[i] != -1)
		{
			if(colours[avarType[i]][avarVal[i]] < val.colours[val.avarType[i]][val.avarVal[i]]) return true;
			if(colours[avarType[i]][avarVal[i]] > val.colours[val.avarType[i]][val.avarVal[i]]) return false;
		}
	}

	for(int i = 0; i != relvarCount; i++)
	{
		if(relType[i] < val.relType[i]) return true;
		if(relType[i] > val.relType[i]) return false;
	}

	for(int i = 0; i != qfvCount; i++)
	{
		if(qfvType[i].second.size() < val.qfvType[i].second.size()) return true;
		if(qfvType[i].second.size() > val.qfvType[i].second.size()) return false;
		if(qfvType[i] < val.qfvType[i]) return true;
		if(qfvType[i] > val.qfvType[i]) return false;
	}

	for(int i = 0; i < n*m; ++i)
	{
		if(cang[i] < val.cang[i]) return true;
		if(cang[i] > val.cang[i]) return false;
	}

	return false;
}

bool Valuation::operator==(const Valuation& val) const
{
	if(typeCount != val.typeCount) return false;

	if(avarCount != val.avarCount) return false;

	if(val.relvarCount != relvarCount) return false;

	if(val.qfvCount != qfvCount) return false;

	for(int i = 0; i != typeCount; ++i)
	{
		if(val.typeSizes[i] != typeSizes[i]) return false;
	}


	for(int i = 0; i != avarCount; i++)
	{
		if(avarType[i] != val.avarType[i]) return false;
	}

	for(int i = 0; i != avarCount; ++i)
	{
		// if the type of the variable is not bounded
		if(avarType[i] != -1)
		{
			if(colours[avarType[i]][avarVal[i]] != val.colours[val.avarType[i]][val.avarVal[i]]) return false;
		}
	}

	for(int i = 0; i != relvarCount; i++)
	{
		if(relType[i] != val.relType[i]) return false;
	}

	for(int i = 0; i != qfvCount; i++)
	{
		if(qfvType[i] != val.qfvType[i]) return false;
	}

	for(int i = 0; i < n*m; ++i)
	{
		if(valg[i] != val.valg[i]) return false;
	}

	return true;
}

bool Valuation::almostEquals(const Valuation& val) const
{
	if(typeCount != val.typeCount) return false;

	if(val.relvarCount != relvarCount) return false;

	if(val.qfvCount != qfvCount) return false;

	for(int i = 0; i != typeCount; ++i)
	{
		if(val.typeSizes[i] != typeSizes[i]) return false;
	}

	for(int i = 0; i != avarCount; i++)
	{
		if(avarType[i] != val.avarType[i]) return false;
	}

	for(int i = 0; i != avarCount; ++i)
	{
		// if the type of the variable is not bounded
		if(avarType[i] != -1)
		{
			if(std::find(val.colours[val.avarType[i]].begin(), val.colours[val.avarType[i]].end(), colours[avarType[i]][avarVal[i]]) == val.colours[val.avarType[i]].end()) return false;
			if(std::find(colours[avarType[i]].begin(), colours[avarType[i]].end(), val.colours[val.avarType[i]][val.avarVal[i]]) == colours[avarType[i]].end()) return false;
		}
	}

	for(int i = 0; i != relvarCount; i++)
	{
		if(relType[i] != val.relType[i]) return false;
	}

	for(int i = 0; i != qfvCount; i++)
	{
		if(qfvType[i] != val.qfvType[i]) return false;
	}

	for(int i = 0; i < n*m; ++i)
	{
		if(valg[i] != val.valg[i]) return false;
	}

	return true;
}

void Valuation::computeCanonicalGraph()
{
	int* lab = new int[n];
	int* ptn = new int[n];
	int* orb = new int[n];
	set* ws = new set[100*m];
	statsblk stats;
	static DEFAULTOPTIONS_DIGRAPH(opt);
	opt.getcanon = TRUE;
	//opt.digraph = TRUE;
	opt.defaultptn = FALSE;

	//std::cout << "About to compute a canonical graph" << std::endl;

	for(int i = 0; i < relvarCount; ++i)
	{
		auto typeOffsets = offsets(i);
		for(auto& t : typeOffsets)
		{
			// std::cout << std::get<0>(t) << ", " << std::get<1>(t) << ", " << std::get<2>(t) << std::endl;
			// std::cout << "reltype offset: " << relTypeOffsets[i] << std::endl;
			int atom = 0;
			int offset = std::get<0>(t);
			int type = std::get<2>(t);
			if(type >= 0)
			{
				int atom = colourCount[type] - 1;
				for(int j = 0; j < std::get<1>(t); ++j)
				{
					if(colours[type][j] > 1)
					{
						lab[relTypeOffsets[i] + offset + colours[type][j] - 2] = relTypeOffsets[i] + offset + j;
						ptn[relTypeOffsets[i] + offset + colours[type][j] - 2] = 0;
					}
					else
					{
						lab[relTypeOffsets[i] + offset + atom] = relTypeOffsets[i] + offset + j;
						ptn[relTypeOffsets[i] + offset + atom] = 1;
						atom++;
					}
				}
			}
			else
			{
				for(int j = 0; j < std::get<1>(t); ++j)
				{
					lab[relTypeOffsets[i] + offset + j] = relTypeOffsets[i] + offset + j;
					ptn[relTypeOffsets[i] + offset + j] = 1;
				}				
			}
			ptn[relTypeOffsets[i] + offset + std::get<1>(t) - 1] = 0;
		}
/*
		int atom = colourCount[relType[i][0]] - 1;
		for(int j = 0; j < typeSizes[relType[i][0]]; ++j)
		{
			if(colours[relType[i][0]][j] > 1)
			{
				lab[relTypeCumSizes[i].first + colours[relType[i][0]][j] - 2] = relTypeCumSizes[i].first + j;
				ptn[relTypeCumSizes[i].first + colours[relType[i][0]][j] - 2] = 0;
			}
			else
			{
				lab[relTypeCumSizes[i].first + atom] = relTypeCumSizes[i].first + j;
				ptn[relTypeCumSizes[i].first + atom] = 1;
				atom++;
			}
		}
		ptn[relTypeCumSizes[i].first + typeSizes[relType[i][0]] - 1] = 0;
		if(relType[i][0] != relType[i][1])
		{
			atom = colourCount[relType[i][1]] - 1;
			for(int j = 0; j < typeSizes[relType[i][1]]; ++j)
			{
				if(colours[relType[i][1]][j] > 1)
				{
					lab[relTypeCumSizes[i].second + colours[relType[i][1]][j] - 2] = relTypeCumSizes[i].second + j;
					ptn[relTypeCumSizes[i].second + colours[relType[i][1]][j] - 2] = 0;
				}
				else
				{
					lab[relTypeCumSizes[i].second + atom] = relTypeCumSizes[i].second + j;
					ptn[relTypeCumSizes[i].second + atom] = 1;
					atom++;
				}
			}
			ptn[relTypeCumSizes[i].second + typeSizes[relType[i][1]] - 1] = 0;
		}
*/
	}
	for(int i = 0; i < qfvCount; ++i)
	{
		int offset = relTypeOffsets[relvarCount] + qfvTypeCumSizes[i].second;
		int j = 0;
		for(; j < typeSizes[qfvType[i].first]; ++j)
		{
			lab[offset + j] = offset + j;
			ptn[offset + j] = 1;
		}
		offset += j;
		ptn[offset - 1] = 0;
		for(int k = 0; k < qfvType[i].second.size(); ++k)
		{
			for(j = 0; j < typeSizes[qfvType[i].second[k]]; ++j)
			{
				lab[offset + j] = offset + j;
				ptn[offset + j] = 1;
			}
			offset += j;
			ptn[offset - 1] = 0;
		}
		for(j = 0; j < qfvTypeCumSizes[i+1].second - qfvTypeCumSizes[i+1].first; ++j)
		{
			lab[offset + j] = offset + j;
			ptn[offset + j] = 1;
		}
		offset += j;
		ptn[offset - 1] = 0;
	}

	//std::cout << "valg: " << valg << " lab: " << lab << " ptn: " << ptn << " orb: " << orb << " ws: " << ws << " m: " << m << " n: " << n << " cang: " << cang << std::endl;
	//std::cout << "lab[0]: " << lab[0] << " ptn[0]: " << ptn[0] << std::endl;
/*
	for(int i = 0; i < n; i++)
	{
		std::cout << lab[i] << ", " << ptn[i] << std::endl;
	}
	std::cout << std::endl;
*/
//	std::cout << "Computing a anonical graph." << std::endl;

	nauty_check(WORDSIZE, m, n,NAUTYVERSIONID);
	nauty(valg, lab, ptn, NULL, orb, &opt, &stats, ws, 100*m, m, n, cang);
//	Nauty::getInstance().canonicalGraph(valg, m, n, lab, ptn, cang);

	//std::cout << "Canonical graph computed." << std::endl;

	delete[] lab;
	delete[] ptn;
	delete[] orb;
	delete[] ws;
}

void Valuation::clearRelationVariable(const RelationVariable& rv)
{
	assert(rv.id >= 0 && rv.id < relvarCount);

	for(int i = relTypeOffsets[rv.id]; i < relTypeOffsets[rv.id+1]; i++)
	{
		for(int j =0; j < n; j++)
		{
			DELELEMENT(GRAPHROW(valg, i, m), j);
			DELELEMENT(GRAPHROW(valg, j, m), i);
		}
	}
}

int Valuation::getTypeVariableCount() const
{
	return typeCount;
}

int Valuation::getRelationVariableCount() const
{
	return relvarCount;
}

int Valuation::getAtomVariableCount() const
{
	return avarCount;
}

int Valuation::getQuorumFunctionVariableCount() const
{
	return qfvCount;
}

int Valuation::getGreatestNonemptySet() const
{
	return greatestNonemptySet;
}


std::vector<int>& Valuation::modinc(std::vector<int>& vec, const std::vector<int>& types, int first, int last) const
{
	if(last == -1) last = types.size();

	assert(vec.size() == types.size());

	for(int i = first; i < last; i++)
	{
		vec[i] = (vec[i] + 1) % typeSizes[types[i]];
		if(vec[i] != 0) return vec;
	}
	return vec;
}

int Valuation::vec2int(const std::vector<int>& vec, const std::vector<int>& types, int first, int last) const
{
	if(last == -1) last = types.size();

	assert(vec.size() == types.size());

	int base = 1;
	int val = 0;
	for(int i = first; i < last; i++)
	{
		val += base*vec[i];
		base *= typeSizes[types[i]];
	}
	return val;
}

std::vector<int> Valuation::int2vec(int val, const std::vector<int>& types) const
{
	std::vector<int> vec;

	for(int i = 0; i < types.size(); i++)
	{
		int q = val % typeSizes[types[i]];
		val = val / typeSizes[types[i]];
		vec.push_back(q);
	}
	return std::move(vec);
}

