#ifndef ELEMENT_H
#define ELEMENT_H

#include <string>
#include <vector>
#include <utility>
#include <iostream>
#include <list>
#include <set>
#include <map>
#include <sstream>
#include <z3++.h>
#include "template.h"

class Context;
class AtomSchemaTuple;

bool operator<(const std::string&, const std::string&);
bool operator==(const std::string&, const std::string&);
bool operator!=(const std::string&, const std::string&);

enum VarType {TYPEVAR = 0, FINTYPE = 1, ATOMVAR = 2, RELVAR = 3, FATOMVAR=4, QFVAR=5};

typedef std::pair<int,bool> Domain;
typedef int Variable;

class TypeVariable : public Domain
{
public:
	/*
	 * 	for finite types (FINTYPE), id stores the size of the type
	 */
	Variable id;
	TypeVariable(int, int=-1, bool=TYPEVAR);
	TypeVariable(const TypeVariable&);
	void setNumericValue(VarType, int, int);
	const std::string& toString(const Context&) const;
	std::pair<z3::sort,z3::func_decl_vector> toZ3Sort(z3::context& c, const std::map<TypeVariable,int>& = std::map<TypeVariable,int>{}) const;
};

class AtomVariable : public std::pair<int,TypeVariable>
{
public:
	Variable id;
	AtomVariable(int, const TypeVariable&, int=-1);
	AtomVariable(const AtomVariable&);
	void setNumericValue(VarType, int, int);
	AtomVariable getPrimedVersion() const;
	const std::string& toString(const Context&) const;
	z3::expr toZ3Expr(z3::context& c, unsigned int = 0, const std::map<TypeVariable,int>& = std::map<TypeVariable,int>{}) const;
	z3::expr toZ3Expr(z3::context& c, const std::string&, const std::map<TypeVariable,int>& = std::map<TypeVariable,int>{}) const;
};

class RelationVariable : public std::tuple<int,std::vector<TypeVariable>>
{
public:
	Variable id;
	RelationVariable(int, const TypeVariable&, const TypeVariable&, int=-1);
	RelationVariable(int, const std::vector<TypeVariable>&, int=-1);
	RelationVariable(const RelationVariable&);
	void setNumericValue(VarType, int, int);
	const std::string& toString(const Context&) const;
	z3::func_decl toZ3Func(z3::context& c, unsigned int = 0, const std::map<TypeVariable,int>& = std::map<TypeVariable,int>{}) const;
};

class QuorumFunctionVariable : public std::tuple<int, TypeVariable, std::vector<TypeVariable>>
{
public:
	Variable id;
	QuorumFunctionVariable(int, const TypeVariable&, const std::vector<TypeVariable>&, int=-1);
	QuorumFunctionVariable(const QuorumFunctionVariable&) = default;
	void setNumericValue(VarType, int, int);
	const std::string& toString(const Context&) const;
};

class Atom : public std::pair<int,Domain>
{
public:
	Atom(int c, Domain d) : std::pair<int,Domain>(c, d) {};
	z3::expr toZ3Expr(z3::context& c, const std::map<TypeVariable,int>&) const;
};

class AtomSet : public std::set<Atom>
{
public:
	Variable id;
	AtomSet(int = -1);
	AtomSet(const AtomSet&);
	bool isElement(const Atom&) const;
	std::string toString(const Context&) const;
};

typedef std::pair<Atom,Atom> AtomPair;

typedef std::pair<int,int> ControlState;

class AtomTuple : public std::vector<Atom>
{
public:
	AtomSet getAtoms() const;
	virtual std::string toString(const Context&, char = '(', char = ')', char = ',') const;
};

class AtomRelation : protected std::vector<bool>
{
	std::vector<Domain> types;
	std::vector<int> typesizes;
	std::vector<int> cumsizes;
public:
	AtomRelation(const std::vector<Domain>&, const std::vector<int>&);
	AtomRelation(const AtomRelation&);
	bool operator==(const AtomRelation&);
	void plus(const AtomRelation&);
	void diff(const AtomRelation&);
	void insert(const AtomTuple&);
	bool isElement(const AtomTuple&) const;
	bool isEquallyTyped(const AtomRelation&) const;
	bool isEquallyTyped(const AtomTuple&) const;
	bool isEquallyTyped(const AtomSchemaTuple&) const;
	Atom operator()(const AtomTuple&) const;
	AtomTuple get(int) const;
	const Domain& getFunctionType() const;
	int getDimension() const;
	std::string toString(const Context&) const;
};

class AtomTupleSet : public std::set<AtomTuple>
{
public:
	std::string toString(const Context&) const;
};

class State : public ControlState, public AtomTuple
{
public:
	State(const ControlState&);
	State(const State&);
	std::string toString(const Context&, char = '(', char = ')', char = ',') const;
};

bool operator<(const State&, const State&);
bool operator==(const State&, const State&);
bool operator!=(const State&, const State&);

class StateSet : public std::set<State>
{
public:
	std::string toString(const Context&) const;
};

typedef int Channel;

class Action : public AtomTuple
{
public:
	Channel chan;
	bool isInput;
	Action(Channel chan=-1, bool=false);
	Action(const Action&);
	std::string toString(const Context&, char = '(', char = ')', char = ',', char = '!', char = '?') const;
	bool operator<(const Action&) const;
	bool operator==(const Action&) const;
	bool operator!=(const Action&) const;
};

class ActionSet : public std::set<Action>
{
public:
	int id;
	ActionSet(int = -1);
	ActionSet(const ActionSet&);
	Atom operator()(const Action&) const;
	bool operator()(const Action&, const Atom&) const;
	bool isElement(const Action&) const;
	std::string toString(const Context&) const;
	std::set<Channel> getInputChannels() const;
	std::set<Channel> getOutputChannels() const;
	AtomSet getAtoms() const;
};

class Transition : public triplet<State, Action, State>
{
public:
	bool isMust;
	Transition(const State&, const Action&, const State&, bool=false);
	Transition(const Transition&);
	std::string toString(const Context&) const;
	AtomSet getAtoms() const;
};

class TransitionSet : protected std::vector<Transition>
{
public:
	class iterator : public std::vector<Transition>::iterator
	{
	public:
		iterator(const std::vector<Transition>::iterator&);
	};
	class const_iterator : public std::vector<Transition>::const_iterator
	{
	public:
		const_iterator(const std::vector<Transition>::const_iterator&);
		const_iterator(const TransitionSet::iterator&);
	};
	StateSet getStateSet() const;
	ActionSet getActionSet() const;
	ActionSet getInputActionSet() const;
	ActionSet getOutputActionSet() const;
	bool insert(const Transition&);
	void insert(const TransitionSet::const_iterator&, const TransitionSet::const_iterator&);
	bool isElement(const Transition&) const;
	std::pair<TransitionSet::const_iterator, TransitionSet::const_iterator> getSuccessors(const State&) const;
	std::pair<TransitionSet::const_iterator, TransitionSet::const_iterator> getTransitions(const Action&) const;
	std::string toString(const Context&) const;
	int width() const;
	int size() const;
	void clear();
	const_iterator begin() const;
	const_iterator end() const;
	iterator begin();
	iterator end();
	void sortByAction();
};



#endif
