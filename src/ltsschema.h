#ifndef LTSSCHEMA_H
#define LTSSCHEMA_H

#include <string>
#include <list>
#include <set>
#include <map>
#include <assert.h>
#include <limits>
#include <z3++.h>
#include "element.h"
#include "elementschema.h"
#include "valuation.h"
#include "valuationformula.h"
#include "setschema.h"

#define MAX_NESTED_OCCURRENCES 1000
#define MAX_NESTED_QFVS 1000

class LTSSchema;
class ElementaryLTSSchema;
class ParallelLTSSchema;
class ReplicatedLTSSchema;
class ConditionLTSSchema;
class HidingLTSSchema;
class NamedLTSSchema;

class SyntaxBranchOfLTSSchema;

class PartialInstanceOfLTSSchema;
class PartialInstanceOfElementaryLTSSchema;
class PartialInstanceOfGeneralisedParallelLTSSchema;
class PartialInstanceOfHidingLTSSchema;

class InstanceOfLTSSchema;
class UnitLTS;
class ElementaryInstanceOfLTSSchema;
class ParallelInstanceOfLTSSchema;
class HidingInstanceOfLTSSchema;
class NamedInstanceOfLTSSchema;

class ParallelAnalysisDisplay;

class LTSSchema
{
protected:
	std::set<TypeVariable> types;
	std::set<RelationVariable> relvars;
	std::set<AtomVariable> atomvars;
	std::set<QuorumFunctionVariable> qfvars;
	std::set<Channel> inputchans;
	std::set<Channel> outputchans;
	LTSSchema();
//	LTSSchema(const std::set<TypeVariable>&, const std::set<RelationVariable>&, const std::set<AtomVariable>&);
	LTSSchema(const LTSSchema&);
public:
	virtual ~LTSSchema();
//	virtual InstanceOfLTSSchema* instance(const Valuation&) const = 0;
	virtual PartialInstanceOfLTSSchema* partialInstance(const Valuation&) const = 0;
	virtual bool instanceCoveredBySmallerOnes(const Valuation&, const AtomSet&) const = 0;
	virtual bool instanceCoveredBySmallerOnes(const Valuation&, const TypeVariable&, const std::set<AtomSet>&) const = 0;
	virtual int numOfNestedOccurrences(const Domain&) const = 0;
	virtual int numOfNestedQFVs(const Domain&) const = 0;
	virtual std::set<std::pair<int,int>> occurrencesInBranches(const Domain&) const = 0;
	virtual int numOfParallelOccurrences(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const = 0;
	virtual int numOfParallelOccurrencesInAdjacentStates(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const = 0;
//	virtual int numOfParallelOccurrences(const Domain&, const Valuation&) const = 0;
	virtual bool isProcessTypeVariable(const TypeVariable&) const = 0;
	virtual bool isDataTypeVariable(const TypeVariable&) const = 0;
	virtual bool isHidingFree() const = 0;
	virtual bool involvesConditionalHiding() const = 0;
	virtual bool involvesInvisibleActions() const = 0;
//	virtual std::map<std::string, const ElementaryLTSSchema*> getNamedElementaryLTSSchemata() const = 0;
	virtual LTSSchema* clone() const = 0;
	virtual std::string toString(const Context& c) const = 0;
	virtual void setNumericValue(VarType, int, int);
	virtual void setNumericValuesToBoundVariables(int) = 0;
	virtual std::set<RelationVariable> getPredicates(bool mode) const = 0;
	const std::set<TypeVariable>& getTypeVariables() const;
	const std::set<RelationVariable>& getRelationVariables() const;
	const std::set<AtomVariable>& getAtomVariables() const;
	const std::set<QuorumFunctionVariable>& getQuorumFunctionVariables() const;
	const std::set<Channel>& getInputChannels() const;
	const std::set<Channel>& getOutputChannels() const;
	bool hasDisjointInputOutputChannels() const;
	virtual LTSSchema* convertChoiceToParallelComposition(const TypeVariable&, bool = false) const = 0;
	virtual std::list<SyntaxBranchOfLTSSchema> getSyntaxBranches() const = 0;
	virtual ValuationFormula* prependValuationFormula(ValuationFormula*, Context&) const;
};

class ElementaryLTSSchema : public LTSSchema
{
	TransitionSetSchema* trsc;
	StateSchema* initstate;
public:
	ElementaryLTSSchema(TransitionSetSchema*, StateSchema*);
	ElementaryLTSSchema(const ElementaryLTSSchema&);
	ElementaryLTSSchema& operator=(const ElementaryLTSSchema&);
	~ElementaryLTSSchema();
	const TransitionSetSchema& getTransitionSetSchema() const;
	const StateSchema& getStateSchema() const;
//	InstanceOfLTSSchema* instance(const Valuation&) const;
	PartialInstanceOfLTSSchema* partialInstance(const Valuation&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const AtomSet&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const TypeVariable&, const std::set<AtomSet>&) const;
	int numOfNestedQFVs(const Domain&) const;
	int numOfNestedOccurrences(const Domain&) const;
	std::set<std::pair<int,int>> occurrencesInBranches(const Domain&) const;
	int numOfParallelOccurrences(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
	int numOfParallelOccurrencesInAdjacentStates(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
//	int numOfParallelOccurrences(const Domain&, const Valuation&) const;
	bool isProcessTypeVariable(const TypeVariable&) const;
	bool isDataTypeVariable(const TypeVariable&) const;
	bool isHidingFree() const;
	bool involvesConditionalHiding() const;
	bool involvesInvisibleActions() const;
//	std::map<std::string, const ElementaryLTSSchema*> getNamedElementaryLTSSchemata() const;
	LTSSchema* clone() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	LTSSchema* convertChoiceToParallelComposition(const TypeVariable&, bool = false) const;
	std::list<SyntaxBranchOfLTSSchema> getSyntaxBranches() const override;
	ValuationFormula* prependValuationFormula(ValuationFormula*, Context&) const override;
	std::set<RelationVariable> getPredicates(bool mode) const override;
};

class ParallelLTSSchema : public LTSSchema
{
	LTSSchema* ltss1;
	LTSSchema* ltss2;
public:
	ParallelLTSSchema(const LTSSchema&, const LTSSchema&);
	ParallelLTSSchema(LTSSchema*, LTSSchema*);
	ParallelLTSSchema(const ParallelLTSSchema&);
	ParallelLTSSchema& operator=(const ParallelLTSSchema&);
	~ParallelLTSSchema();
	const LTSSchema& oneLTSSchema() const;
	const LTSSchema& theOtherLTSSchema() const;
//	InstanceOfLTSSchema* instance(const Valuation&) const;
	PartialInstanceOfLTSSchema* partialInstance(const Valuation&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const AtomSet&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const TypeVariable&, const std::set<AtomSet>&) const;
	int numOfNestedQFVs(const Domain&) const;
	int numOfNestedOccurrences(const Domain&) const;
	std::set<std::pair<int,int>> occurrencesInBranches(const Domain&) const;
	int numOfParallelOccurrences(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
	int numOfParallelOccurrencesInAdjacentStates(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
//	int numOfParallelOccurrences(const Domain&, const Valuation&) const;
	bool isProcessTypeVariable(const TypeVariable&) const;
	bool isDataTypeVariable(const TypeVariable&) const;
	bool isHidingFree() const;
	bool involvesConditionalHiding() const;
	bool involvesInvisibleActions() const;
//	std::map<std::string, const ElementaryLTSSchema*> getNamedElementaryLTSSchemata() const;
	LTSSchema* clone() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	LTSSchema* convertChoiceToParallelComposition(const TypeVariable&, bool = false) const;
	std::list<SyntaxBranchOfLTSSchema> getSyntaxBranches() const override;
	std::set<RelationVariable> getPredicates(bool mode) const override;
};

class ReplicatedLTSSchema : public LTSSchema
{
	AtomVariable* avar;
	LTSSchema* ltss;
public:
	ReplicatedLTSSchema(const AtomVariable&, const LTSSchema&);
	ReplicatedLTSSchema(AtomVariable*, LTSSchema*);
	ReplicatedLTSSchema(const ReplicatedLTSSchema&);
	ReplicatedLTSSchema& operator=(const ReplicatedLTSSchema&);
	~ReplicatedLTSSchema();
	const LTSSchema& getLTSSchema() const;
	const AtomVariable& getAtomVariable() const;
//	InstanceOfLTSSchema* instance(const Valuation&) const;
	PartialInstanceOfLTSSchema* partialInstance(const Valuation&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const AtomSet&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const TypeVariable&, const std::set<AtomSet>&) const;
	int numOfNestedQFVs(const Domain&) const;
	int numOfNestedOccurrences(const Domain&) const;
	std::set<std::pair<int,int>> occurrencesInBranches(const Domain&) const;
	int numOfParallelOccurrences(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
	int numOfParallelOccurrencesInAdjacentStates(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
//	int numOfParallelOccurrences(const Domain&, const Valuation&) const;
	bool isProcessTypeVariable(const TypeVariable&) const;
	bool isDataTypeVariable(const TypeVariable&) const;
	bool isHidingFree() const;
	bool involvesConditionalHiding() const;
	bool involvesInvisibleActions() const;
//	std::map<std::string, const ElementaryLTSSchema*> getNamedElementaryLTSSchemata() const;
	LTSSchema* clone() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	LTSSchema* convertChoiceToParallelComposition(const TypeVariable&, bool = false) const;
	std::list<SyntaxBranchOfLTSSchema> getSyntaxBranches() const override;
	ValuationFormula* prependValuationFormula(ValuationFormula*, Context&) const override;
	std::set<RelationVariable> getPredicates(bool mode) const override;
};

class ConditionLTSSchema : public LTSSchema 
{
	LTSSchema* ltss;
	ValuationFormula* cond;
public:
	ConditionLTSSchema(const LTSSchema&, const ValuationFormula&);
	ConditionLTSSchema(LTSSchema*, ValuationFormula*);
	ConditionLTSSchema(const ConditionLTSSchema&);
	ConditionLTSSchema& operator=(const ConditionLTSSchema&);
	~ConditionLTSSchema();
	const LTSSchema& getLTSSchema() const;
	const ValuationFormula& valuationFormula() const;
//	InstanceOfLTSSchema* instance(const Valuation&) const;
	PartialInstanceOfLTSSchema* partialInstance(const Valuation&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const AtomSet&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const TypeVariable&, const std::set<AtomSet>&) const;
	int numOfNestedQFVs(const Domain&) const;
	int numOfNestedOccurrences(const Domain&) const;
	std::set<std::pair<int,int>> occurrencesInBranches(const Domain&) const;
	int numOfParallelOccurrences(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
	int numOfParallelOccurrencesInAdjacentStates(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
//	int numOfParallelOccurrences(const Domain&, const Valuation&) const;
	bool isProcessTypeVariable(const TypeVariable&) const;
	bool isDataTypeVariable(const TypeVariable&) const;
	bool isHidingFree() const;
	bool involvesConditionalHiding() const;
	bool involvesInvisibleActions() const;
//	std::map<std::string, const ElementaryLTSSchema*> getNamedElementaryLTSSchemata() const;
	LTSSchema* clone() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	LTSSchema* convertChoiceToParallelComposition(const TypeVariable&, bool = false) const;
	std::list<SyntaxBranchOfLTSSchema> getSyntaxBranches() const override;
	ValuationFormula* prependValuationFormula(ValuationFormula*, Context&) const override;
	std::set<RelationVariable> getPredicates(bool mode) const override;
};

class HidingLTSSchema : public LTSSchema
{
	LTSSchema* ltss;
	SetSchema* hideset;
public:
	HidingLTSSchema(const LTSSchema&, const SetSchema&);
	HidingLTSSchema(LTSSchema*, SetSchema*);
	HidingLTSSchema(const HidingLTSSchema&);
	HidingLTSSchema& operator=(const HidingLTSSchema&);
	~HidingLTSSchema();
	const LTSSchema& getLTSSchema() const;
	const SetSchema& setSchema() const;
//	InstanceOfLTSSchema* instance(const Valuation&) const;
	PartialInstanceOfLTSSchema* partialInstance(const Valuation&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const AtomSet&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const TypeVariable&, const std::set<AtomSet>&) const;
	int numOfNestedQFVs(const Domain&) const;
	int numOfNestedOccurrences(const Domain&) const;
	std::set<std::pair<int,int>> occurrencesInBranches(const Domain&) const;
	int numOfParallelOccurrences(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
	int numOfParallelOccurrencesInAdjacentStates(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
//	int numOfParallelOccurrences(const Domain&, const Valuation&) const;
	bool isProcessTypeVariable(const TypeVariable&) const;
	bool isDataTypeVariable(const TypeVariable&) const;
	bool isHidingFree() const;
	bool involvesConditionalHiding() const;
	bool involvesInvisibleActions() const;
//	std::map<std::string, const ElementaryLTSSchema*> getNamedElementaryLTSSchemata() const;
	LTSSchema* clone() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	LTSSchema* convertChoiceToParallelComposition(const TypeVariable&, bool = false) const;
	std::list<SyntaxBranchOfLTSSchema> getSyntaxBranches() const override;
	std::set<RelationVariable> getPredicates(bool mode) const override;
};

class NamedLTSSchema : public LTSSchema
{
	const std::string* name;
	LTSSchema* ltss;
public:
	NamedLTSSchema(const std::string&, const LTSSchema&);
	NamedLTSSchema(const std::string*, LTSSchema*);
	NamedLTSSchema(const NamedLTSSchema&);
	NamedLTSSchema& operator=(const NamedLTSSchema&);
	~NamedLTSSchema();
	const std::string& getName() const;
	const LTSSchema& getLTSSchema() const;
//	InstanceOfLTSSchema* instance(const Valuation&) const;
	PartialInstanceOfLTSSchema* partialInstance(const Valuation&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const AtomSet&) const;
	bool instanceCoveredBySmallerOnes(const Valuation&, const TypeVariable&, const std::set<AtomSet>&) const;
	int numOfNestedQFVs(const Domain&) const;
	int numOfNestedOccurrences(const Domain&) const;
	std::set<std::pair<int,int>> occurrencesInBranches(const Domain&) const;
	int numOfParallelOccurrences(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
	int numOfParallelOccurrencesInAdjacentStates(const Domain&, const LTSSchema&, const std::map<TypeVariable,int>&) const;
//	int numOfParallelOccurrences(const Domain&, const Valuation&) const;
	bool isProcessTypeVariable(const TypeVariable&) const;
	bool isDataTypeVariable(const TypeVariable&) const;
	bool isHidingFree() const;
	bool involvesConditionalHiding() const;
	bool involvesInvisibleActions() const;
//	std::map<std::string, const ElementaryLTSSchema*> getNamedElementaryLTSSchemata() const;
	LTSSchema* clone() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
	LTSSchema* convertChoiceToParallelComposition(const TypeVariable&, bool = false) const;
	std::list<SyntaxBranchOfLTSSchema> getSyntaxBranches() const override;
	std::set<RelationVariable> getPredicates(bool mode) const override;
};

class SyntaxBranchOfLTSSchema : protected std::list<const LTSSchema*>
{
	std::set<AtomVariable> bavs;
public:
	SyntaxBranchOfLTSSchema() = default;
	~SyntaxBranchOfLTSSchema();
	SyntaxBranchOfLTSSchema(const SyntaxBranchOfLTSSchema&);
	SyntaxBranchOfLTSSchema(SyntaxBranchOfLTSSchema&&);
	void push_front(const LTSSchema*);
	using std::list<const LTSSchema*>::back;
	ValuationFormula* toValuationFormula(Context&) const;
	const std::set<AtomVariable>& getAtomVariables() const;
	const std::set<AtomVariable>& getBoundAtomVariables() const;
};

class PartialInstanceOfLTSSchema
{
public:
	virtual ~PartialInstanceOfLTSSchema();
	virtual int numOfOccurrencesInStates(const AtomSet&) const = 0;
	virtual int numOfOccurrencesInTransitions(const AtomSet&) const = 0;
	virtual int numOfOccurrencesInAdjacentStates(const AtomSet&) const = 0;
	virtual PartialInstanceOfLTSSchema* clone() const = 0;
	virtual std::string toString(const Context& c) const = 0;
	virtual bool composeLeaves(const Context&, int) = 0;
	virtual bool isInstance() const = 0;
	virtual bool involvesInvisibleActions() const = 0;
	virtual InstanceOfLTSSchema* instance() const = 0;
};

class PartialInstanceOfElementaryLTSSchema : public PartialInstanceOfLTSSchema
{
	const ElementaryLTSSchema* ltsc;
	Valuation val;
public:	
	PartialInstanceOfElementaryLTSSchema(const ElementaryLTSSchema*, const Valuation&);
	PartialInstanceOfElementaryLTSSchema(const PartialInstanceOfElementaryLTSSchema&);
	PartialInstanceOfElementaryLTSSchema& operator=(const PartialInstanceOfElementaryLTSSchema&);
	~PartialInstanceOfElementaryLTSSchema();
	const ElementaryLTSSchema& getElementaryLTSSchema() const;
	const Valuation& getValuation() const;
	int numOfOccurrencesInStates(const AtomSet&) const;
	int numOfOccurrencesInTransitions(const AtomSet&) const;
	int numOfOccurrencesInAdjacentStates(const AtomSet&) const;
	PartialInstanceOfLTSSchema* clone() const;
	std::string toString(const Context& c) const;
	bool composeLeaves(const Context&, int = std::numeric_limits<int>::max());
	bool isInstance() const;
	bool involvesInvisibleActions() const;
	InstanceOfLTSSchema* instance() const;
};

class PartialInstanceOfGeneralisedParallelLTSSchema : public PartialInstanceOfLTSSchema, public std::vector<PartialInstanceOfLTSSchema*>
{
public:	
	PartialInstanceOfGeneralisedParallelLTSSchema();
	PartialInstanceOfGeneralisedParallelLTSSchema(const PartialInstanceOfGeneralisedParallelLTSSchema&);
	PartialInstanceOfGeneralisedParallelLTSSchema& operator=(const PartialInstanceOfGeneralisedParallelLTSSchema&);
	~PartialInstanceOfGeneralisedParallelLTSSchema();
	int numOfOccurrencesInStates(const AtomSet&) const;
	int numOfOccurrencesInTransitions(const AtomSet&) const;
	int numOfOccurrencesInAdjacentStates(const AtomSet&) const;
	PartialInstanceOfLTSSchema* clone() const;
	std::string toString(const Context& c) const;
	bool composeLeaves(const Context&, int = std::numeric_limits<int>::max());
	bool isInstance() const;
	bool involvesInvisibleActions() const;
	InstanceOfLTSSchema* instance() const;
	InstanceOfLTSSchema* instance(int, int) const;
};

class PartialInstanceOfHidingLTSSchema : public PartialInstanceOfLTSSchema, public std::vector<std::pair<SetSchema*,Valuation> >
{
	PartialInstanceOfLTSSchema* ltsc;
public:	
	PartialInstanceOfHidingLTSSchema(PartialInstanceOfLTSSchema*);
	PartialInstanceOfHidingLTSSchema(const PartialInstanceOfHidingLTSSchema&);
	PartialInstanceOfHidingLTSSchema& operator=(const PartialInstanceOfHidingLTSSchema&);
	~PartialInstanceOfHidingLTSSchema();
	int numOfOccurrencesInStates(const AtomSet&) const;
	int numOfOccurrencesInTransitions(const AtomSet&) const;
	int numOfOccurrencesInAdjacentStates(const AtomSet&) const;
	PartialInstanceOfLTSSchema* clone() const;
	std::string toString(const Context& c) const;
	bool composeLeaves(const Context&, int = std::numeric_limits<int>::max());
	bool involvesInvisibleActions() const;
	bool isInstance() const;
	InstanceOfLTSSchema* instance() const;
};

class InstanceOfLTSSchema : public PartialInstanceOfLTSSchema
{
protected:
	ActionSet* acts;
	InstanceOfLTSSchema();
	InstanceOfLTSSchema(ActionSet*);
	InstanceOfLTSSchema(const InstanceOfLTSSchema&);
public:
	virtual ~InstanceOfLTSSchema();
	virtual bool isDeterministic() const = 0;
	virtual bool isComponentWiseMustDeterministic() const = 0;
	virtual bool isComponentWiseDeterministic() const = 0;
	virtual InstanceOfLTSSchema* clone() const = 0;
	virtual InstanceOfLTSSchema* evaluate() const = 0;
	const ActionSet& getActionSet() const;
	bool composeLeaves(const Context&, int = std::numeric_limits<int>::max());
	bool isInstance() const;
	InstanceOfLTSSchema* instance() const;
};

class UnitLTS : public InstanceOfLTSSchema
{
public:
	int numOfOccurrencesInStates(const AtomSet&) const;
	int numOfOccurrencesInTransitions(const AtomSet&) const;
	int numOfOccurrencesInAdjacentStates(const AtomSet&) const;
	bool isDeterministic() const;
	bool isComponentWiseMustDeterministic() const;
	bool isComponentWiseDeterministic() const;
	bool involvesInvisibleActions() const;
	InstanceOfLTSSchema* clone() const;
	InstanceOfLTSSchema* evaluate() const;
	std::string toString(const Context& c) const;
};

class ElementaryInstanceOfLTSSchema : public InstanceOfLTSSchema
{
	TransitionSet* transet;
	State* initstate;
	ActionSet* inputs;
	ActionSet* outputs;
public:
	ElementaryInstanceOfLTSSchema(TransitionSet*, State*);
	ElementaryInstanceOfLTSSchema(const ElementaryLTSSchema*, const Valuation&);
	ElementaryInstanceOfLTSSchema(const ElementaryInstanceOfLTSSchema&);
	ElementaryInstanceOfLTSSchema& operator=(const ElementaryInstanceOfLTSSchema&);
	const TransitionSet& getTransitions() const;
	const State& getInitialState() const;
	int numOfOccurrencesInStates(const AtomSet&) const;
	int numOfOccurrencesInTransitions(const AtomSet&) const;
	int numOfOccurrencesInAdjacentStates(const AtomSet&) const;
	bool isDeterministic() const;
	bool isComponentWiseMustDeterministic() const;
	bool isComponentWiseDeterministic() const;
	bool involvesInvisibleActions() const;
	InstanceOfLTSSchema* clone() const;
	InstanceOfLTSSchema* evaluate() const;
	std::string toString(const Context& c) const;
	const ActionSet& getInputActionSet() const;
	const ActionSet& getOutputActionSet() const;
};

class ParallelInstanceOfLTSSchema : public InstanceOfLTSSchema
{
	const InstanceOfLTSSchema* iltsc1;
	const InstanceOfLTSSchema* iltsc2;
public:
	ParallelInstanceOfLTSSchema(const InstanceOfLTSSchema*, const InstanceOfLTSSchema*);
	ParallelInstanceOfLTSSchema(const ParallelInstanceOfLTSSchema&);
	~ParallelInstanceOfLTSSchema();
	ParallelInstanceOfLTSSchema& operator=(const ParallelInstanceOfLTSSchema&);
	const InstanceOfLTSSchema& getOneInstance() const;
	const InstanceOfLTSSchema& getOtherInstance() const;
	int numOfOccurrencesInStates(const AtomSet&) const;
	int numOfOccurrencesInTransitions(const AtomSet&) const;
	int numOfOccurrencesInAdjacentStates(const AtomSet&) const;
	bool isDeterministic() const;
	bool isComponentWiseMustDeterministic() const;
	bool isComponentWiseDeterministic() const;
	bool involvesInvisibleActions() const;
	InstanceOfLTSSchema* clone() const;
	InstanceOfLTSSchema* evaluate() const;
	std::string toString(const Context& c) const;
};

/*
class GeneralisedParallelInstanceOfLTSSchema : public InstanceOfLTSSchema, public std::list<InstanceOfLTSSchema*>
{
public:
	GeneralisedParallelInstanceOfLTSSchema();
	GeneralisedParallelInstanceOfLTSSchema(const GeneralisedParallelInstanceOfLTSSchema&);
	~GeneralisedParallelInstanceOfLTSSchema();
	GeneralisedParallelInstanceOfLTSSchema& operator=(const GeneralisedParallelInstanceOfLTSSchema&);
	int numOfOccurrencesInStates(const AtomSet&) const;
	int numOfOccurrencesInTransitions(const AtomSet&) const;
	bool isDeterministic() const;
	bool isComponentWiseInputDeterministic() const;
	bool isComponentWiseOutputDeterministic() const;
	InstanceOfLTSSchema* clone() const;
	std::string toString(const Context& c) const;
};
*/

class HidingInstanceOfLTSSchema : public InstanceOfLTSSchema
{
	const InstanceOfLTSSchema* iltsc;
	const ActionSet* issc;
public:
	HidingInstanceOfLTSSchema(const InstanceOfLTSSchema*, const ActionSet*);
	HidingInstanceOfLTSSchema(const HidingInstanceOfLTSSchema&);
	~HidingInstanceOfLTSSchema();
	HidingInstanceOfLTSSchema& operator=(const HidingInstanceOfLTSSchema&);
	const InstanceOfLTSSchema& getInstanceOfLTSSchema() const;
	const ActionSet& getHidingSet() const;
	std::list<const InstanceOfLTSSchema*> getElementaryInstances() const;
	std::list<const InstanceOfLTSSchema*> getInstancesInParallel() const;
	int numOfOccurrencesInStates(const AtomSet&) const;
	int numOfOccurrencesInTransitions(const AtomSet&) const;
	int numOfOccurrencesInAdjacentStates(const AtomSet&) const;
	bool isDeterministic() const;
	bool isComponentWiseMustDeterministic() const;
	bool isComponentWiseDeterministic() const;
	bool involvesInvisibleActions() const;
	InstanceOfLTSSchema* clone() const;
	InstanceOfLTSSchema* evaluate() const;
	std::string toString(const Context& c) const;
};

class ParallelAnalysisDisplay
{
	static std::vector<int> progress;
	static std::vector<int> printedprogress;
	static std::vector<int> maxprogress;
public:
	static void addTask(int);
	static void updateProgress(int);
	static void setProgress(int);
	static void removeTask();
};

#endif
