#ifndef SETSCHEMA_H
#define SETSCHEMA_H

#include <string>
#include <list>
#include <set>
#include <map>
#include "element.h"
#include "valuation.h"
#include "valuationformula.h"

class SetSchema;
class ElementarySetSchema;
class UnionSetSchema;
class ReplicatedSetSchema;
class NamedSetSchema;

/*
class InstanceOfSetSchema;
class EmptyInstanceOfSetSchema;
class ElementaryInstanceOfSetSchema;
class UnionInstanceOfSetSchema;
*/

class SetSchema
{
protected:
	std::set<TypeVariable> types;
	std::set<AtomVariable> avars;
	std::set<RelationVariable> relvars;
	std::set<QuorumFunctionVariable> qfvars;
	SetSchema();
	SetSchema(const std::set<TypeVariable>&, const std::set<AtomVariable>&, const std::set<RelationVariable>&, const std::set<QuorumFunctionVariable>&);
	SetSchema(const SetSchema&);
public:
	virtual ~SetSchema();
	virtual ActionSet* instance(const Valuation&) const = 0;
	virtual SetSchema* clone() const = 0;
	virtual std::string toString(const Context& c) const = 0;
	virtual void setNumericValue(VarType, int, int);
	virtual void setNumericValuesToBoundVariables(int) = 0;
	virtual int numOfNestedQFVs(const Domain&) const = 0;
	virtual bool isConditionFree() const = 0;
	virtual bool isVisible() const = 0;
	virtual bool isInvisible() const = 0;
	bool isNotParametrised() const;
	bool isClosed() const;
	const std::set<TypeVariable>& getTypeVariables() const;
	const std::set<AtomVariable>& getAtomVariables() const;
	const std::set<RelationVariable>& getRelationVariables() const;
	const std::set<QuorumFunctionVariable>& getQuorumFunctionVariables() const;
};

class ElementarySetSchema : public SetSchema
{
private:
	ActionSetSchema* as;
public:
	ElementarySetSchema(const ActionSetSchema&);
	ElementarySetSchema(ActionSetSchema*);
	ElementarySetSchema(const ElementarySetSchema&);
	~ElementarySetSchema();
	ElementarySetSchema& operator=(const ElementarySetSchema&);
	const ActionSetSchema& getActionSetSchema() const;
	int numOfNestedQFVs(const Domain&) const;
	bool isVisible() const;
	bool isInvisible() const;
	bool isConditionFree() const;
	ActionSet* instance(const Valuation&) const;
	SetSchema* clone() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
};

class UnionSetSchema : public SetSchema
{
private:
	SetSchema* set1;
	SetSchema* set2;
public:
	UnionSetSchema(const SetSchema&, const SetSchema&);
	UnionSetSchema(SetSchema*, SetSchema*);
	UnionSetSchema(const UnionSetSchema&);
	~UnionSetSchema();
	UnionSetSchema& operator=(const UnionSetSchema&);
	const SetSchema& getOneSetSchema() const;
	const SetSchema& getOtherSetSchema() const;
	int numOfNestedQFVs(const Domain&) const;
	bool isVisible() const;
	bool isInvisible() const;
	bool isConditionFree() const;
	ActionSet* instance(const Valuation&) const;
	SetSchema* clone() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
};

class ConditionSetSchema : public SetSchema
{
private:
	SetSchema* set;
	ValuationFormula* guard;
public:
	ConditionSetSchema(const SetSchema&, const ValuationFormula&);
	ConditionSetSchema(SetSchema*, ValuationFormula*);
	ConditionSetSchema(const ConditionSetSchema&);
	~ConditionSetSchema();
	ConditionSetSchema& operator=(const ConditionSetSchema&);
	const SetSchema& getSetSchema() const;
	const ValuationFormula& getValuationFormula() const;
	int numOfNestedQFVs(const Domain&) const;
	bool isVisible() const;
	bool isInvisible() const;
	bool isConditionFree() const;
	ActionSet* instance(const Valuation&) const;
	SetSchema* clone() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
};

class ReplicatedSetSchema : public SetSchema
{
private:
	SetSchema* set;
	AtomVariable* a;
public:
	ReplicatedSetSchema(const AtomVariable&, const SetSchema&);
	ReplicatedSetSchema(AtomVariable*, SetSchema*);
	ReplicatedSetSchema(const ReplicatedSetSchema&);
	~ReplicatedSetSchema();
	ReplicatedSetSchema& operator=(const ReplicatedSetSchema&);
	const SetSchema& getSetSchema() const;
	const AtomVariable& getAtomVariable() const;
	int numOfNestedQFVs(const Domain&) const;
	bool isVisible() const;
	bool isInvisible() const;
	bool isConditionFree() const;
	ActionSet* instance(const Valuation&) const;
	SetSchema* clone() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
};

class NamedSetSchema : public SetSchema
{
private:
	const std::string* name;
	SetSchema* set;
public:
	NamedSetSchema(const std::string&, const SetSchema&);
	NamedSetSchema(const std::string*, SetSchema*);
	NamedSetSchema(const NamedSetSchema&);
	~NamedSetSchema();
	NamedSetSchema& operator=(const NamedSetSchema&);
	const std::string& getName() const;
	const SetSchema& getSetSchema() const;
	int numOfNestedQFVs(const Domain&) const;
	bool isVisible() const;
	bool isInvisible() const;
	bool isConditionFree() const;
	ActionSet* instance(const Valuation&) const;
	SetSchema* clone() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	std::string toString(const Context& c) const;
};

/*
class InstanceOfSetSchema
{
protected:
	ActionSet* acts;
	InstanceOfSetSchema();
	InstanceOfSetSchema(ActionSet*);
	InstanceOfSetSchema(const InstanceOfSetSchema&);
public:
	virtual ~InstanceOfSetSchema();
	virtual InstanceOfSetSchema* clone() const = 0;
	virtual std::string toString() const = 0;
	const ActionSet& getActionSet() const;
};

class ElementaryInstanceOfSetSchema : public InstanceOfSetSchema
{
	const ElementarySetSchema* ssc;
	Valuation val;
public:
	ElementaryInstanceOfSetSchema(const ElementarySetSchema*, const Valuation&);
	ElementaryInstanceOfSetSchema(const ElementaryInstanceOfSetSchema&);
	ElementaryInstanceOfSetSchema& operator=(const ElementaryInstanceOfSetSchema&);
	const ElementarySetSchema& getSetSchema() const;
	const Valuation& getValuation() const;
	InstanceOfSetSchema* clone() const;
	std::string toString() const;
};

class EmptyInstanceOfSetSchema : public InstanceOfSetSchema
{
public:
	InstanceOfSetSchema* clone() const;
	std::string toString() const;
};

class UnionInstanceOfSetSchema : public InstanceOfSetSchema
{
	const InstanceOfSetSchema* ssc1;
	const InstanceOfSetSchema* ssc2;
public:
	UnionInstanceOfSetSchema(const InstanceOfSetSchema*, const InstanceOfSetSchema*);
	UnionInstanceOfSetSchema(const UnionInstanceOfSetSchema&);
	~UnionInstanceOfSetSchema();
	UnionInstanceOfSetSchema& operator=(const UnionInstanceOfSetSchema&);
	const InstanceOfSetSchema& getOneInstance() const;
	const InstanceOfSetSchema& getOtherInstance() const;

	InstanceOfSetSchema* clone() const;
	std::string toString() const;
};
*/

#endif