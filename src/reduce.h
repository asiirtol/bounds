#ifndef REDUCE_H
#define REDUCE_H

#include <set>
#include <list>
#include <map>
#include <functional>
#include <atomic>
#include <limits>
#include "template.h"
#include "element.h"
#include "valuation.h"
#include "valuationformula.h"
#include "ltsschema.h"
#include "context.h"

#define CUTOFFMAX 255
#define MAX_SYS_COMP_SIZE 2500000
#define MAX_SPEC_COMP_SIZE 200000
#define INST_OUTPUT_MAX std::numeric_limits<int>::max()

enum VerType {NOVERIFY, TRACEREF, COMPAT, ALTSIM};

std::tuple<std::set<Valuation*,PointerCompare<Valuation>>*, int, int, int, std::map<TypeVariable,bool>*, bool, bool> computeCutoffSet(const Context&, ValuationFormula*&, LTSSchema*&, LTSSchema*&, VerType);
std::tuple<std::set<Valuation*,PointerCompare<Valuation> >*, int, int, int> computeValuations(const Context&, ValuationFormula*&, LTSSchema*&, LTSSchema*&, const std::map<TypeVariable,int>&, TypeVariable* = nullptr);
void updateStoredCount(std::atomic<int>&, int, std::atomic<int>&);
void updateCompleted(std::atomic<double>&, double);
void printProgress(std::atomic<double>&, double, std::atomic<int>&, std::atomic<int>&, std::atomic<int>&);
std::set<Valuation*,PointerCompare<Valuation>>* computeExtendedValuations(std::set<TypeVariable, VariableCompare<TypeVariable> >, std::set<RelationVariable, VariableCompare<RelationVariable> >, std::set<AtomVariable, VariableCompare<AtomVariable> >, std::set<QuorumFunctionVariable, VariableCompare<QuorumFunctionVariable>>, const std::map<TypeVariable,int>&, ValuationFormula*, ValuationFormula*, std::list<ValuationFormula*>, std::list<ValuationFormula*>, std::atomic<double>&, double, std::atomic<int>&, std::atomic<int>&, std::atomic<int>&, std::atomic<int>&, std::atomic<int>&, std::atomic<int>&, std::set<Valuation*,PointerCompare<Valuation> >*, bool, TypeVariable* = nullptr);
int search(Valuation*, std::list<Valuation*>*, const ValuationFormula*, std::atomic<int>&);
int search(Valuation*, std::list<Valuation*>*, bool = false);
std::list<ValuationFormula*> cloneConjuncts(const std::list<ValuationFormula*>&);
void deleteConjuncts(std::list<ValuationFormula*>&);
int quorumBound(int,int);
std::tuple<std::set<Valuation*,PointerCompare<Valuation> >*, int, int, int> computeDynamicCutoff(Context, LTSSchema*, LTSSchema*, ValuationFormula*, const std::map<TypeVariable, int>&);

#endif
