#ifndef CONTEXT_H
#define CONTEXT_H

#include <string>
#include <set>
#include <map>
#include <utility>
#include <tuple>
#include "template.h"
#include "element.h"
#include "elementschema.h"
#include "valuationformula.h"
#include "setschema.h"
#include "ltsschema.h"

class Context
{
/*	std::set<std::string> atoms;
	std::map<std::string, int> typevars;
	std::map<std::string, std::pair<Domain*, int> > atomvars;
	std::map<std::string, triplet<TypeVariable, TypeVariable, int> > relvars;
	std::map<std::string, const AtomSet*> sets;
	std::map<std::string, std::vector<const Domain*> > chans;
	std::vector<std::set<ControlState> > cstates;
*/
	std::map<std::string, const ValuationFormula*> valfs;
	std::map<std::string, const SetSchema*> sscs;
	std::map<std::string, const LTSSchema*> ltscs;
	std::map<std::string, const AtomRelation*> rels;
/*	std::map<std::string, const AtomSet*> sets;
	std::vector<std::string> int2rel;
	std::vector<std::string> int2set;
*/	std::map<std::string, std::pair<int,int> > type2intpair;
	std::vector<std::string> int2type;
	std::map<std::string, AtomVariable> avar2inttriplet;
	std::vector<std::string> int2avar;
	std::vector<std::string> int2primedavar;
	std::map<std::string, RelationVariable> relv2intquartet;
	std::vector<std::string> int2relv;
	std::map<std::string, std::tuple<int,int,int>> str2qfv;
	std::vector<std::string> int2qfvname;
	std::vector<std::vector<TypeVariable>> qfvdom;
	std::map<std::string, int> ftype2int;
	std::vector<int> ftypesizes;
	std::vector<std::string> int2ftype;
	std::map<std::string, int> chan2int;
	std::vector<std::string> int2chan;
	std::vector<std::vector<Domain> > chantypes;
	std::vector<std::map<std::string, int> > cstate2int;
	std::vector<std::vector<std::string> > int2cstate;
	std::map<std::string, std::pair<int,int> > atom2intpair;
	std::vector<std::vector<std::string> > int2atom;
public:
	Context() = default;
	Context(const Context& c);
	~Context();
	bool isNotDefined(const std::string*,bool=true) const;
	bool isAtom(const std::string*) const;
	bool isTypeVariable(const std::string*) const;
	bool isAtomVariable(const std::string*) const;
	bool isRelationVariable(const std::string*) const;
	bool isQuorumFunctionVariable(const std::string*) const;
	bool isValuationFormula(const std::string*) const;
	bool isSetSchema(const std::string*) const;
	bool isLTSSchema(const std::string*) const;
//	bool isAtomSet(const std::string*) const;
	bool isFiniteType(const std::string*) const;
	bool isAtomRelation(const std::string*) const;
	bool isChannel(const std::string*) const;
	bool isCorrectlyTyped(const ActionSchema*) const;
	std::vector<int> getTypeSizes(const AtomSchemaTuple*) const;
	Atom* getAtom(const std::string*) const;
	TypeVariable* getTypeVariable(const std::string*) const;
	TypeVariable* getFiniteType(const std::string*) const;
	AtomVariable* getAtomVariable(const std::string*) const;
	AtomVariable* createAtomVariable(const TypeVariable&);
	RelationVariable* getRelationVariable(const std::string*) const;
	QuorumFunctionVariable* getQuorumFunctionVariable(const std::string*) const;
	ValuationFormula* getValuationFormula(const std::string*) const;
	SetSchema* getSetSchema(const std::string*) const;
	const LTSSchema* getLTSSchema(const std::string*) const;
//	AtomSet* getAtomSet(const std::string*) const;
	AtomRelation* getAtomRelation(const std::string*) const;
	Channel getChannel(const std::string*) const;
	std::string atomToString(const Atom&) const;
	const std::string& typeVariableToString(const TypeVariable&) const;
	const std::string& atomVariableToString(const AtomVariable&) const;
	const std::string& relationVariableToString(const RelationVariable&) const;
	const std::string& quorumFunctionVariableToString(const QuorumFunctionVariable&) const;
	const std::string& channelToString(const Channel&) const;
	const std::string& controlStateToString(const ControlState&) const;
/*
	const std::string& relationToString(const AtomRelation&) const;
	const std::string& setToString(const AtomSet&) const;
*/
	void insertNewProcess();
	ControlState insertControlState(const std::string*);
	void insertFiniteType(const std::string*);
	void finiteTypeInserted();
	void insertAtom(const std::string*);
	/* insert TypeVariable */
	void insertTypeVariable(const std::string*, int = -1);
	/* insert AtomVariable */
	void insertAtomVariable(const std::string*, const std::string*, int = -1);
	/* insert RelationVariable */
	void insertRelationVariable(const std::string*, const std::string*, const std::string*, int = -1);
	void insertRelationVariable(const std::string*, const std::vector<std::string>*, int = -1);
	/* insert QuorumFunctionVariable */
	void insertQuorumFunctionVariable(const std::string*, const std::string*, int = -1);
	void appendQuorumFunctionType(const std::string*);
	/* insert ValuationFormula */
	void insertValuationFormula(const std::string*, const ValuationFormula*);
	/* insert ValuationFormula */
	void insertSetSchema(const std::string*, const SetSchema*);
	/* insert ValuationFormula */
	void insertLTSSchema(const std::string*, const LTSSchema*);
//	void insertAtomSet(const std::string*, const AtomSet*);
	void insertAtomRelation(const std::string*, const AtomRelation*);
	void insertChannel(const std::string*);
	void appendChannelType(const std::string*);
	void clear();
};

#endif

