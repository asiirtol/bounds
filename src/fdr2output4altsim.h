#ifndef FDR2OUTPUT4ALTSIM_H
#define FDR2OUTPUT4ALTSIM_H

#include <string>
#include <iostream>
#include <atomic>
#include "template.h"
#include "element.h"
#include "valuation.h"
#include "ltsschema.h"
#include "context.h"

class AlternatingSimulationOutput
{
private:
	static std::pair<bool,bool> generatePartOfInstances(const Context&, const LTSSchema*, const LTSSchema*, const ValuationFormula*, std::set<Valuation*,PointerCompare<Valuation> >*, const std::set<TypeVariable>&, const std::set<TypeVariable>&, const std::set<RelationVariable>&, const std::set<AtomVariable>&, const std::set<QuorumFunctionVariable>&, const std::string&, int instCount, const std::map<TypeVariable,bool>&, bool, bool, std::atomic<int>&);
	static bool writeInstance(const Context&, const InstanceOfLTSSchema*, const InstanceOfLTSSchema*, int, const std::string&, bool, bool);
	static void printString(const std::string&);
	static std::ostream& toFDRFormattedStream(const Context&, std::ostream&, const InstanceOfLTSSchema*, const std::string&, bool = false, int = 1);
	static std::ostream& toFDRFormattedStream(const Context&, std::ostream&, const UnitLTS&, const std::string&, bool, int);
	static std::ostream& toFDRFormattedStream(const Context&, std::ostream&, const ElementaryInstanceOfLTSSchema&, const std::string&, bool, int);
	static std::ostream& toFDRFormattedStream(const Context&, std::ostream&, const ParallelInstanceOfLTSSchema&, const std::string&, bool, int);
//	static std::ostream& toFDRFormattedStream(const Context&, std::ostream&, const NamedInstanceOfLTSSchema&, const std::string&, bool, int);
	static std::ostream& toFDRFormattedStream(const Context&, std::ostream&, const ActionSet&, const std::string& = "");
public:
	static int generateInstances(const Context&, const LTSSchema*, const LTSSchema*, const ValuationFormula*, std::set<Valuation*,PointerCompare<Valuation> >*, const std::string&, const std::map<TypeVariable,bool>&, bool, bool);
};

#endif 
