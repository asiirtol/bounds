#ifndef ELEMENTSCHEMA_H
#define ELEMENTSCHEMA_H

#include <string>
#include <vector>
#include <utility>
#include <iostream>
#include <list>
#include <set>
#include <map>
#include <sstream>
#include <z3++.h>
#include "element.h"
#include "valuation.h"

class ValuationFormula;

class AtomSchema
{
protected:
	std::set<AtomVariable> atomvars;
	std::set<TypeVariable> types;
public:
	AtomSchema();
	AtomSchema(const AtomSchema&);
	virtual ~AtomSchema();
	virtual AtomSchema* clone() const = 0;
	virtual AtomSchema* createPrimedVersion(const AtomVariable&) const = 0;
	virtual bool equals(const AtomSchema*) const = 0;
	virtual Atom instance(const Valuation&) const = 0;
	virtual void setNumericValue(VarType, int, int) = 0;
	virtual Domain getType() const = 0;
	const std::set<AtomVariable>& getAtomVariables() const;
	const std::set<TypeVariable>& getTypeVariables() const;
	virtual std::string toString(const Context& c) const = 0;
	virtual z3::expr toZ3Expr(z3::context& c, unsigned int = 0, const std::map<TypeVariable,int>& = std::map<TypeVariable,int>{}) const = 0;
};

class AtomSchemaTuple : protected std::vector<AtomSchema*>
{
	bool varsdefined;
	bool typesdefined;
	std::set<AtomVariable> atomvars;
	std::set<TypeVariable> types;
public:
	AtomSchemaTuple();
	AtomSchemaTuple(const AtomSchemaTuple&);
	~AtomSchemaTuple();
	AtomSchemaTuple* createPrimedVersion(const AtomVariable&) const;
	AtomSchemaTuple& operator=(const AtomSchemaTuple&);
	const AtomSchema* operator[](int) const;
	bool operator<(const AtomSchemaTuple&) const;
	bool operator==(const AtomSchemaTuple&) const;
	bool equalsInSet(const AtomSchemaTuple&, const std::set<AtomVariable>&) const;
	bool equalsAtVariable(const AtomSchemaTuple&, const AtomVariable&) const;
	virtual AtomSchemaTuple& push_back(AtomSchema*);
	AtomTuple instance(const Valuation&) const;
	int size() const;
	void reserve(int);
	void setNumericValue(VarType, int, int);
	const std::set<AtomVariable>& getAtomVariables() const;
	const std::set<TypeVariable>& getTypeVariables() const;
	int numOfOccurrences(const Domain&) const;
	int numOfOccurrences(const AtomVariable&) const;
	bool isElement(const AtomVariable&) const;
	bool isEquallyTyped(const AtomSchemaTuple&) const;
	bool isEquallyTyped(const RelationVariable&) const;
	int removeVariable(const AtomVariable&);
	int removeVariables(const Domain&);
	virtual std::string toString(const Context& c) const;
	using std::vector<AtomSchema*>::const_iterator;
	using std::vector<AtomSchema*>::begin;
	using std::vector<AtomSchema*>::end;
};

class ElementaryAtomSchema : public AtomSchema
{
	Atom* atom;
public:
	ElementaryAtomSchema(Atom*);
	ElementaryAtomSchema(const ElementaryAtomSchema&);
	~ElementaryAtomSchema();
	ElementaryAtomSchema& operator=(const ElementaryAtomSchema&);
	bool equals(const AtomSchema*) const;
	AtomSchema* clone() const;
	AtomSchema* createPrimedVersion(const AtomVariable&) const;
	Atom instance(const Valuation&) const;
	void setNumericValue(VarType, int, int);
	Domain getType() const;
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context& c, unsigned int, const std::map<TypeVariable,int>& = std::map<TypeVariable,int>{}) const override;
};

class AtomVariableSchema : public AtomSchema
{
	AtomVariable* avar;
public:
	AtomVariableSchema(AtomVariable*);
	AtomVariableSchema(const AtomVariableSchema&);
	~AtomVariableSchema();
	AtomVariableSchema& operator=(const AtomVariableSchema&);
	bool equals(const AtomSchema*) const;
	AtomSchema* clone() const;
	AtomSchema* createPrimedVersion(const AtomVariable&) const;
	Atom instance(const Valuation&) const;
	const AtomVariable& getAtomVariable() const;
	void setNumericValue(VarType, int, int);
	Domain getType() const;
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context& c, unsigned int, const std::map<TypeVariable,int>& = std::map<TypeVariable,int>{}) const override;
};

class ActionSchema : public AtomSchemaTuple
{
public:
	Channel chan;
	bool isInput;
	ActionSchema(Channel = -1, bool=false);
	ActionSchema(const ActionSchema&);
	bool operator<(const ActionSchema&) const;
	bool equalsAtVariable(const ActionSchema&, const AtomVariable&) const;
	ActionSchema* createPrimedVersion(const AtomVariable&) const;
	Action instance(const Valuation&) const;
	std::string toString(const Context& c) const;
};

class FunctionAtomSchema : public AtomSchema
{
	AtomSchemaTuple* actsc;
	AtomRelation* func;
public:
	FunctionAtomSchema(AtomRelation*, AtomSchemaTuple*);
	FunctionAtomSchema(const FunctionAtomSchema&);
	~FunctionAtomSchema();
	FunctionAtomSchema& operator=(const FunctionAtomSchema&);
	bool equals(const AtomSchema*) const;
	AtomSchema* clone() const;
	AtomSchema* createPrimedVersion(const AtomVariable&) const;
	Atom instance(const Valuation&) const;
	void setNumericValue(VarType, int, int);
	Domain getType() const;
	std::string toString(const Context& c) const;
	z3::expr toZ3Expr(z3::context& c, unsigned int, const std::map<TypeVariable,int>& = std::map<TypeVariable,int>{}) const override;
};

class RelationSchema : protected std::set<AtomSchemaTuple*>
{
	std::vector<Domain> types;
	std::vector<int> typesizes;
	std::set<AtomVariable> avars;
	ValuationFormula* guard;
public:
	RelationSchema(AtomSchemaTuple*, const std::vector<int>&);
	RelationSchema(const RelationSchema&);
	~RelationSchema();
	RelationSchema& operator=(const RelationSchema&);
	void insert(AtomSchemaTuple*);
	bool isEquallyTyped(const AtomSchemaTuple&);
	void setGuard(ValuationFormula*);
	AtomRelation instance() const;
};

class StateSchema : public ControlState, public AtomSchemaTuple
{
public:
	StateSchema(int=-1,int=-1);
	StateSchema(const ControlState&);
	StateSchema(const StateSchema&);
	bool operator<(const StateSchema&) const;
	bool operator==(const StateSchema&) const;
	State instance(const Valuation&) const;
	std::string toString(const Context& c) const;
};

class ParameterList : public AtomSchemaTuple
{
public:
	ParameterList& push_back(AtomSchema*);
	const AtomVariableSchema* operator[](int) const;
};

std::ostream& operator<<(std::ostream&, const ActionSchema*&);

typedef std::list<ActionSchema*> ActionSetSchema;

class TransitionSchema
{
	StateSchema* from;
	ParameterList* boundparams;
	ValuationFormula* guard;
	ActionSchema* actsc;
	StateSchema* to;
	std::set<AtomVariable> atomvars;
	std::set<TypeVariable> types;
	std::set<RelationVariable> relvars;
	std::set<QuorumFunctionVariable> qfvars;
public:
	bool isMust;
	TransitionSchema();
	TransitionSchema(StateSchema*, ParameterList*, ValuationFormula*, ActionSchema*, StateSchema*,bool=false);
	TransitionSchema(const TransitionSchema&);
	~TransitionSchema();
	TransitionSchema& operator=(const TransitionSchema&);
	bool operator<(const TransitionSchema&) const;
	TransitionSet* instance(const Valuation&) const;
	void setFromState(StateSchema*);
	void setToState(StateSchema*);
	void setBoundParameters(ParameterList*);
	void setGuard(ValuationFormula*);
	void setActionSchema(ActionSchema*);
	const StateSchema& getFromState() const;
	const StateSchema& getToState() const;
	const ParameterList& getBoundParameters() const;
	const ValuationFormula& getGuard() const;
	const ActionSchema& getActionSchema() const;
	bool hasVisibleGuard() const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	const std::set<AtomVariable>& getAtomVariables() const;
	const std::set<TypeVariable>& getTypeVariables() const;
	const std::set<RelationVariable>& getRelationVariables() const;
	const std::set<QuorumFunctionVariable>& getQuorumFunctionVariables() const;
	int numOfNestedQFVs(const Domain&) const;
	int numOfBoundOccurrences(const Domain&) const;
	int numOfBoundOccurrencesInStates(const Domain&) const;
	std::string toString(const Context& c) const;
};

class TransitionSetSchema : protected std::vector<TransitionSchema*>
{
	std::set<AtomVariable> atomvars;
	std::set<TypeVariable> types;
	std::set<RelationVariable> relvars;
	std::set<QuorumFunctionVariable> qfvars;
	std::set<Channel> inputchans;
	std::set<Channel> outputchans;
public:
	class iterator : public std::vector<TransitionSchema*>::iterator
	{
	public:
		iterator(const std::vector<TransitionSchema*>::iterator&);
	};
	class const_iterator : public std::vector<TransitionSchema*>::const_iterator
	{
	public:
		const_iterator(const std::vector<TransitionSchema*>::const_iterator&);
		const_iterator(const TransitionSetSchema::iterator&);
	};
	class reverse_iterator : public std::vector<TransitionSchema*>::reverse_iterator 
	{
	public:
		reverse_iterator(const std::vector<TransitionSchema*>::reverse_iterator&);
	};
	class const_reverse_iterator : public std::vector<TransitionSchema*>::const_reverse_iterator 
	{
	public:
		const_reverse_iterator(const std::vector<TransitionSchema*>::const_reverse_iterator&);
		const_reverse_iterator(const TransitionSetSchema::reverse_iterator&);
	};

	TransitionSetSchema();
	TransitionSetSchema(const TransitionSetSchema&);
	~TransitionSetSchema();
	TransitionSetSchema& operator=(const TransitionSetSchema&);
	TransitionSetSchema& insert(TransitionSchema*);
	TransitionSetSchema& insert(const TransitionSchema&);
	TransitionSet* instance(const Valuation&) const;
	void setNumericValue(VarType, int, int);
	void setNumericValuesToBoundVariables(int);
	const std::set<AtomVariable>& getAtomVariables() const;
	const std::set<TypeVariable>& getTypeVariables() const;
	const std::set<RelationVariable>& getRelationVariables() const;
	const std::set<QuorumFunctionVariable>& getQuorumFunctionVariables() const;
	const std::set<Channel>& getInputChannels() const;
	const std::set<Channel>& getOutputChannels() const;
	bool hasDisjointInputOutputChannels() const;
	std::pair<TransitionSetSchema::const_iterator, TransitionSetSchema::const_iterator> getSuccessors(const StateSchema&) const;
	int size() const;
	std::string toString(const Context& c) const;
	iterator begin();
	iterator end();
	const_iterator begin() const;
	const_iterator end() const;
	reverse_iterator rbegin();
	reverse_iterator rend();
	const_reverse_iterator rbegin() const;
	const_reverse_iterator rend() const;
};

#endif
