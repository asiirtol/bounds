#include "valuationformula.h"
#include <iostream>
#include <assert.h>
#include "context.h"

ValuationFormula::ValuationFormula() : types(std::set<TypeVariable>()), relvars(std::set<RelationVariable>()), atomvars(std::set<AtomVariable>()), qfvars(std::set<QuorumFunctionVariable>())
{
}

ValuationFormula::ValuationFormula(const std::set<TypeVariable>& tvs, const std::set<RelationVariable>& rvs, const std::set<AtomVariable>& avs, const std::set<QuorumFunctionVariable>& qfvs) : types(tvs), relvars(rvs), atomvars(avs), qfvars(qfvs)
{
}

ValuationFormula::ValuationFormula(const ValuationFormula& vf) : types(vf.types), relvars(vf.relvars), atomvars(vf.atomvars), qfvars(vf.qfvars)
{
}

ValuationFormula::~ValuationFormula()
{
}

const std::set<TypeVariable>& ValuationFormula::getTypeVariables() const
{
	return types;
}

const std::set<RelationVariable>& ValuationFormula::getRelationVariables() const
{
	return relvars;
}

const std::set<AtomVariable>& ValuationFormula::getAtomVariables() const
{
	return atomvars;
}

const std::set<QuorumFunctionVariable>& ValuationFormula::getQuorumFunctionVariables() const
{
	return qfvars;
}

void ValuationFormula::setNumericValue(VarType vt, int id, int var_id)
{
	for(std::set<TypeVariable>::iterator sit = types.begin(); sit != types.end(); sit++)
	{
		const_cast<TypeVariable&>(*sit).setNumericValue(vt,id,var_id);
	}
	for(std::set<RelationVariable>::iterator sit = relvars.begin(); sit != relvars.end(); sit++)
	{
		const_cast<RelationVariable&>(*sit).setNumericValue(vt,id,var_id);
	}
	for(std::set<AtomVariable>::iterator sit = atomvars.begin(); sit != atomvars.end(); sit++)
	{
		const_cast<AtomVariable&>(*sit).setNumericValue(vt,id,var_id);
	}
	for(auto& qfv : qfvars)
		const_cast<QuorumFunctionVariable&>(qfv).setNumericValue(vt,id,var_id);
}

std::list<ValuationFormula*> ValuationFormula::conjuncts() const
{
	std::list<ValuationFormula*> cons;
	cons.push_back(clone());
	return cons;
}

std::list<ValuationFormula*> ValuationFormula::disjuncts() const
{
	std::list<ValuationFormula*> cons;
	cons.push_back(clone());
	return cons;
}

ConjunctiveValuationFormula::ConjunctiveValuationFormula(const ValuationFormula& f1, const ValuationFormula& f2) : ConjunctiveValuationFormula(f1.clone(), f2.clone())
{
}

ConjunctiveValuationFormula::ConjunctiveValuationFormula(ValuationFormula* f1, ValuationFormula* f2) : ValuationFormula(), vf1(f1), vf2(f2)
{
	assert(vf1 != 0 && vf2 != 0);

	types.insert(vf1->getTypeVariables().begin(), vf1->getTypeVariables().end());
	relvars.insert(vf1->getRelationVariables().begin(), vf1->getRelationVariables().end());
	atomvars.insert(vf1->getAtomVariables().begin(), vf1->getAtomVariables().end());
	qfvars.insert(vf1->getQuorumFunctionVariables().begin(), vf1->getQuorumFunctionVariables().end());
	types.insert(vf2->getTypeVariables().begin(), vf2->getTypeVariables().end());
	relvars.insert(vf2->getRelationVariables().begin(), vf2->getRelationVariables().end());
	atomvars.insert(vf2->getAtomVariables().begin(), vf2->getAtomVariables().end());
	qfvars.insert(vf2->getQuorumFunctionVariables().begin(), vf2->getQuorumFunctionVariables().end());
}

ConjunctiveValuationFormula::ConjunctiveValuationFormula(const ConjunctiveValuationFormula& f) : ValuationFormula(f), vf1(f.vf1->clone()), vf2(f.vf2->clone())
{
}

ConjunctiveValuationFormula& ConjunctiveValuationFormula::operator=(const ConjunctiveValuationFormula& f)
{
	if(&f == this) return *this;
	ConjunctiveValuationFormula cvf(f);
	std::swap(types, cvf.types);
	std::swap(relvars, cvf.relvars);
	std::swap(atomvars, cvf.atomvars);
	std::swap(qfvars, cvf.qfvars);
	std::swap(vf1, cvf.vf1);
	std::swap(vf2, cvf.vf2);
	return *this;
}

ValuationFormula* ConjunctiveValuationFormula::clone() const
{
	assert(vf1 != 0 && vf2 != 0);

	return new ConjunctiveValuationFormula(*this);
}

ValuationFormula* ConjunctiveValuationFormula::createPrimedVersion(const AtomVariable& av) const
{
	assert(vf1 != 0 && vf2 != 0);

	return new ConjunctiveValuationFormula(vf1->createPrimedVersion(av), vf2->createPrimedVersion(av));
}

ConjunctiveValuationFormula::~ConjunctiveValuationFormula()
{
	delete vf1;
	delete vf2;
}

const ValuationFormula& ConjunctiveValuationFormula::oneValuationFormula() const
{
	assert(vf1 != 0);

	return *vf1;
}

const ValuationFormula& ConjunctiveValuationFormula::theOtherValuationFormula() const
{
	assert(vf2 != 0);

	return *vf2;
}

bool ConjunctiveValuationFormula::instance(const Valuation& val) const
{
	assert(vf1 != 0 && vf2 != 0);

	return (vf1->instance(val) && vf2->instance(val));
}

bool ConjunctiveValuationFormula::instanceCoveredBySmallerOne(const Valuation& val, const TypeVariable& tv, const AtomSet& subval) const
{
	assert(vf1 != 0 && vf2 != 0);

	return (vf1->instanceCoveredBySmallerOne(val, tv, subval) && vf2->instanceCoveredBySmallerOne(val, tv, subval));	
}

int ConjunctiveValuationFormula::numOfNestedQFVs(const Domain& tv) const
{
	assert(vf1 != 0 && vf2 != 0);

	return vf1->numOfNestedQFVs(tv) + vf2->numOfNestedQFVs(tv);
}

std::list<ValuationFormula*> ConjunctiveValuationFormula::conjuncts() const
{
	assert(vf1 != 0 && vf2 != 0);

	std::list<ValuationFormula*> cons(vf1->conjuncts());
	std::list<ValuationFormula*> cons2(vf2->conjuncts());
	cons.splice(cons.end(), cons2);
	return cons;
}

std::list<ValuationFormula*> ConjunctiveValuationFormula::disjuncts() const
{
	assert(vf1 != 0 && vf2 != 0);

	std::list<ValuationFormula*> cons{};
	std::list<ValuationFormula*> cons1(vf1->disjuncts());
	std::list<ValuationFormula*> cons2(vf2->disjuncts());
	for(auto c1 : cons1)
	{
		for(auto c2: cons2)
		{
			cons.push_back(new ConjunctiveValuationFormula(c1->clone(), c2->clone()));
		}
	}
	for(auto c1 : cons1) delete c1;
	for(auto c2 : cons2) delete c2;
	return cons;
}

bool ConjunctiveValuationFormula::isExistentialFree(int numOfNegations) const
{
	assert(vf1 != 0 && vf2 != 0);

	return (vf1->isExistentialFree(numOfNegations) && vf2->isExistentialFree(numOfNegations));
}

bool ConjunctiveValuationFormula::isMembershipNegated(int numOfNegations) const
{
	assert(vf1 != 0 && vf2 != 0);

	return (vf1->isMembershipNegated(numOfNegations) && vf2->isMembershipNegated(numOfNegations));
}

bool ConjunctiveValuationFormula::isUnquantified() const
{
	assert(vf1 != 0 && vf2 != 0);

	return (vf1->isUnquantified() && vf2->isUnquantified());
}

void ConjunctiveValuationFormula::setNumericValue(VarType vt, int id, int var_id)
{
	assert(vf1 != 0 && vf2 != 0);

	ValuationFormula::setNumericValue(vt,id,var_id);
	vf1->setNumericValue(vt,id,var_id);
	vf2->setNumericValue(vt,id,var_id);
}

void ConjunctiveValuationFormula::setNumericValuesToBoundVariables(int val)
{
	assert(vf1 != 0 && vf2 != 0);

	vf1->setNumericValuesToBoundVariables(val);
	vf2->setNumericValuesToBoundVariables(val);
}

std::string ConjunctiveValuationFormula::toString(const Context& c) const
{
	assert(vf1 != 0 && vf2 != 0);

	std::string str(1, '(');
	str.append(vf1->toString(c));
	str.append(")&(");
	str.append(vf2->toString(c));
	str.push_back(')');
	return str;
}

z3::expr ConjunctiveValuationFormula::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(vf1 != 0 && vf2 != 0);

	return vf1->toZ3Expr(c, inst_num, cutoffs) && vf2->toZ3Expr(c, inst_num, cutoffs);	
}

std::set<RelationVariable> ConjunctiveValuationFormula::getPredicates(bool mode, bool within) const
{
	assert(vf1 != 0 && vf2 != 0);
	
	auto preds1 = vf1->getPredicates(mode, within);	
	auto preds2 = vf2->getPredicates(mode, within);
	preds1.insert(preds2.begin(), preds2.end());

	return std::move(preds1);
}

DisjunctiveValuationFormula::DisjunctiveValuationFormula(const ValuationFormula& f1, const ValuationFormula& f2) : DisjunctiveValuationFormula(f1.clone(), f2.clone())
{
}

DisjunctiveValuationFormula::DisjunctiveValuationFormula(ValuationFormula* f1, ValuationFormula* f2) : ValuationFormula(), vf1(f1), vf2(f2)
{
	assert(vf1 != 0 && vf2 != 0);

	types.insert(vf1->getTypeVariables().begin(), vf1->getTypeVariables().end());
	relvars.insert(vf1->getRelationVariables().begin(), vf1->getRelationVariables().end());
	atomvars.insert(vf1->getAtomVariables().begin(), vf1->getAtomVariables().end());
	qfvars.insert(vf1->getQuorumFunctionVariables().begin(), vf1->getQuorumFunctionVariables().end());
	types.insert(vf2->getTypeVariables().begin(), vf2->getTypeVariables().end());
	relvars.insert(vf2->getRelationVariables().begin(), vf2->getRelationVariables().end());
	atomvars.insert(vf2->getAtomVariables().begin(), vf2->getAtomVariables().end());
	qfvars.insert(vf2->getQuorumFunctionVariables().begin(), vf2->getQuorumFunctionVariables().end());
}

DisjunctiveValuationFormula::DisjunctiveValuationFormula(const DisjunctiveValuationFormula& f) : ValuationFormula(f), vf1(f.vf1->clone()), vf2(f.vf2->clone())
{
}

DisjunctiveValuationFormula& DisjunctiveValuationFormula::operator=(const DisjunctiveValuationFormula& f)
{
	if(&f == this) return *this;
	DisjunctiveValuationFormula cvf(f);
	std::swap(types, cvf.types);
	std::swap(relvars, cvf.relvars);
	std::swap(atomvars, cvf.atomvars);
	std::swap(qfvars, cvf.qfvars);
	std::swap(vf1, cvf.vf1);
	std::swap(vf2, cvf.vf2);
	return *this;
}

ValuationFormula* DisjunctiveValuationFormula::clone() const
{
	assert(vf1 != 0 && vf2 != 0);

	return new DisjunctiveValuationFormula(*this);
}

ValuationFormula* DisjunctiveValuationFormula::createPrimedVersion(const AtomVariable& av) const
{
	assert(vf1 != 0 && vf2 != 0);

	return new DisjunctiveValuationFormula(vf1->createPrimedVersion(av), vf2->createPrimedVersion(av));
}

DisjunctiveValuationFormula::~DisjunctiveValuationFormula()
{
	delete vf1;
	delete vf2;
}

const ValuationFormula& DisjunctiveValuationFormula::oneValuationFormula() const
{
	assert(vf1 != 0);

	return *vf1;
}

const ValuationFormula& DisjunctiveValuationFormula::theOtherValuationFormula() const
{
	assert(vf2 != 0);

	return *vf2;
}

bool DisjunctiveValuationFormula::instance(const Valuation& val) const
{
	assert(vf1 != 0 && vf2 != 0);

	return (vf1->instance(val) || vf2->instance(val));
}

bool DisjunctiveValuationFormula::instanceCoveredBySmallerOne(const Valuation& val, const TypeVariable& tv, const AtomSet& subval) const
{
	assert(vf1 != 0 && vf2 != 0);

	return (vf1->instanceCoveredBySmallerOne(val, tv, subval) && vf2->instanceCoveredBySmallerOne(val, tv, subval));	
}

int DisjunctiveValuationFormula::numOfNestedQFVs(const Domain& tv) const
{
	assert(vf1 != 0 && vf2 != 0);

	return vf1->numOfNestedQFVs(tv) + vf2->numOfNestedQFVs(tv);
}

std::list<ValuationFormula*> DisjunctiveValuationFormula::disjuncts() const
{
	assert(vf1 != 0 && vf2 != 0);

	std::list<ValuationFormula*> cons(vf1->disjuncts());
	std::list<ValuationFormula*> cons2(vf2->disjuncts());
	cons.splice(cons.end(), cons2);
	return cons;
}

bool DisjunctiveValuationFormula::isExistentialFree(int numOfNegations) const
{
	assert(vf1 != 0 && vf2 != 0);

	return (vf1->isExistentialFree(numOfNegations) && vf2->isExistentialFree(numOfNegations));
}

bool DisjunctiveValuationFormula::isMembershipNegated(int numOfNegations) const
{
	assert(vf1 != 0 && vf2 != 0);

	return (vf1->isMembershipNegated(numOfNegations) && vf2->isMembershipNegated(numOfNegations));
}

bool DisjunctiveValuationFormula::isUnquantified() const
{
	assert(vf1 != 0 && vf2 != 0);

	return (vf1->isUnquantified() && vf2->isUnquantified());
}

void DisjunctiveValuationFormula::setNumericValue(VarType vt, int id, int var_id)
{
	assert(vf1 != 0 && vf2 != 0);

	ValuationFormula::setNumericValue(vt,id,var_id);
	vf1->setNumericValue(vt,id,var_id);
	vf2->setNumericValue(vt,id,var_id);
}

void DisjunctiveValuationFormula::setNumericValuesToBoundVariables(int val)
{
	assert(vf1 != 0 && vf2 != 0);

	vf1->setNumericValuesToBoundVariables(val);
	vf2->setNumericValuesToBoundVariables(val);
}

std::string DisjunctiveValuationFormula::toString(const Context& c) const
{
	assert(vf1 != 0 && vf2 != 0);

	std::string str(1, '(');
	str.append(vf1->toString(c));
	str.append(")|(");
	str.append(vf2->toString(c));
	str.push_back(')');
	return str;
}

z3::expr DisjunctiveValuationFormula::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(vf1 != 0 && vf2 != 0);

	return vf1->toZ3Expr(c, inst_num, cutoffs) || vf2->toZ3Expr(c, inst_num, cutoffs);	
}

std::set<RelationVariable> DisjunctiveValuationFormula::getPredicates(bool mode, bool within) const
{
	assert(vf1 != 0 && vf2 != 0);
	
	auto preds1 = vf1->getPredicates(mode, within);	
	auto preds2 = vf2->getPredicates(mode, within);
	preds1.insert(preds2.begin(), preds2.end());

	return std::move(preds1);
}

NegatedValuationFormula::NegatedValuationFormula(const ValuationFormula& f, int) : ValuationFormula(f), vf(f.clone())
{
}

NegatedValuationFormula::NegatedValuationFormula(ValuationFormula* f) : ValuationFormula(), vf(f)
{
	assert(vf != 0);

	types.insert(vf->getTypeVariables().begin(), vf->getTypeVariables().end());
	relvars.insert(vf->getRelationVariables().begin(), vf->getRelationVariables().end());
	atomvars.insert(vf->getAtomVariables().begin(), vf->getAtomVariables().end());
	qfvars.insert(vf->getQuorumFunctionVariables().begin(), vf->getQuorumFunctionVariables().end());
}

NegatedValuationFormula::NegatedValuationFormula(const NegatedValuationFormula& f) : ValuationFormula(f), vf(f.vf->clone())
{
}

NegatedValuationFormula& NegatedValuationFormula::operator=(const NegatedValuationFormula& f)
{
	if(&f == this) return *this;
	NegatedValuationFormula nvf(f);
	std::swap(types, nvf.types);
	std::swap(relvars, nvf.relvars);
	std::swap(atomvars, nvf.atomvars);
	std::swap(qfvars, nvf.qfvars);
	std::swap(vf, nvf.vf);
	return *this;
}

ValuationFormula* NegatedValuationFormula::clone() const
{
	assert(vf != 0);

	return new NegatedValuationFormula(*this);
}

ValuationFormula* NegatedValuationFormula::createPrimedVersion(const AtomVariable& av) const
{
	assert(vf != 0);

	return new NegatedValuationFormula(vf->createPrimedVersion(av));
}

NegatedValuationFormula::~NegatedValuationFormula()
{
	delete vf;
}

const ValuationFormula& NegatedValuationFormula::valuationFormula() const
{
	assert(vf != 0);

	return *vf;
}

bool NegatedValuationFormula::instance(const Valuation& val) const
{
	assert(vf != 0);

	return (!vf->instance(val));
}

bool NegatedValuationFormula::instanceCoveredBySmallerOne(const Valuation& val, const TypeVariable& tv, const AtomSet& subval) const
{
	assert(vf != 0);

	return vf->instanceCoveredBySmallerOne(val, tv, subval);
}

int NegatedValuationFormula::numOfNestedQFVs(const Domain& tv) const
{
	assert(vf != 0);

	return vf->numOfNestedQFVs(tv);
}

bool NegatedValuationFormula::isExistentialFree(int numOfNegations) const
{
	assert(vf != 0);

	return (vf->isExistentialFree(numOfNegations+1));
}

bool NegatedValuationFormula::isMembershipNegated(int numOfNegations) const
{
	assert(vf != 0);

	return (vf->isMembershipNegated(numOfNegations+1));
}

bool NegatedValuationFormula::isUnquantified() const
{
	assert(vf != 0);

	return vf->isUnquantified();
}

void NegatedValuationFormula::setNumericValue(VarType vt, int id, int var_id)
{
	assert(vf != 0);

	ValuationFormula::setNumericValue(vt,id,var_id);
	vf->setNumericValue(vt,id,var_id);
}

void NegatedValuationFormula::setNumericValuesToBoundVariables(int val)
{
	assert(vf != 0);

	vf->setNumericValuesToBoundVariables(val);
}

std::string NegatedValuationFormula::toString(const Context& c) const
{
	assert(vf != 0);

	std::string str{"!("};
	str.append(vf->toString(c));
	str.push_back(')');
	return str;
}

z3::expr NegatedValuationFormula::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(vf != 0);

	return !(vf->toZ3Expr(c, inst_num, cutoffs));
}

std::set<RelationVariable> NegatedValuationFormula::getPredicates(bool mode, bool within) const
{
	assert(vf != 0);

	return vf->getPredicates(mode, within ^ true);
}

UniversalValuationFormula::UniversalValuationFormula(const AtomVariable& a, const ValuationFormula& f) : UniversalValuationFormula(new AtomVariable(a), f.clone())
{
}

UniversalValuationFormula::UniversalValuationFormula(AtomVariable* a, ValuationFormula* f) : ValuationFormula(), av(a), vf(f)
{
	assert(vf != 0 && av != 0);

	types.insert(vf->getTypeVariables().begin(), vf->getTypeVariables().end());
	relvars.insert(vf->getRelationVariables().begin(), vf->getRelationVariables().end());
	atomvars.insert(vf->getAtomVariables().begin(), vf->getAtomVariables().end());
	qfvars.insert(vf->getQuorumFunctionVariables().begin(), vf->getQuorumFunctionVariables().end());
	if(av->second.second == TYPEVAR)
	{
		types.insert(av->second);
	}
	std::set<AtomVariable>::iterator it = atomvars.find(*av);
	if(it != atomvars.end()) atomvars.erase(it);
}

UniversalValuationFormula::UniversalValuationFormula(const UniversalValuationFormula& f) : ValuationFormula(f), av(new AtomVariable(*(f.av))), vf(f.vf->clone())
{
}

ValuationFormula* UniversalValuationFormula::clone() const
{
	assert(vf != 0 && av != 0);

	return new UniversalValuationFormula(*this);
}

ValuationFormula* UniversalValuationFormula::createPrimedVersion(const AtomVariable& pv) const
{
	assert(vf != 0 && av != 0);

	if(pv == *av)
	{
		return new UniversalValuationFormula(*this);
	}
	else
	{
		return new UniversalValuationFormula(new AtomVariable(*av), vf->createPrimedVersion(pv));
	}
}

UniversalValuationFormula& UniversalValuationFormula::operator=(const UniversalValuationFormula& f)
{
	if(&f == this) return *this;
	UniversalValuationFormula uvf(f);
	std::swap(types, uvf.types);
	std::swap(relvars, uvf.relvars);
	std::swap(atomvars, uvf.atomvars);
	std::swap(qfvars, uvf.qfvars);
	std::swap(av, uvf.av);
	std::swap(vf, uvf.vf);
	return *this;
}

UniversalValuationFormula::~UniversalValuationFormula()
{
	delete vf;
	delete av;
}

const ValuationFormula& UniversalValuationFormula::valuationFormula() const
{
	assert(vf != 0);

	return *vf;
}

const AtomVariable& UniversalValuationFormula::getAtomVariable() const
{
	assert(av != 0);

	return *av;
}

bool UniversalValuationFormula::instance(const Valuation& val) const
{
	assert(vf != 0 && av != 0);

	bool inst = true;
	std::list<Valuation*> vals = val.addAtomVariable(av->second);
	for(std::list<Valuation*>::const_iterator it = vals.begin(); it != vals.end(); ++it)
	{
		inst = inst && vf->instance(**it);
		delete *it;
	}
	return inst;
}

bool UniversalValuationFormula::instanceCoveredBySmallerOne(const Valuation& val, const TypeVariable& tv, const AtomSet& subval) const
{
	return false;
}

int UniversalValuationFormula::numOfNestedQFVs(const Domain& tv) const
{
	assert(vf != 0);

	return vf->numOfNestedQFVs(tv);
}

bool UniversalValuationFormula::isExistentialFree(int numOfNegations) const
{
	assert(vf != 0);

	if(numOfNegations%2 == 0)
	{
		return (vf->isExistentialFree(numOfNegations));
	}
	else
	{
		return false;
	}
}

bool UniversalValuationFormula::isMembershipNegated(int numOfNegations) const
{
	assert(vf != 0);

	return (vf->isMembershipNegated(numOfNegations));
}

bool UniversalValuationFormula::isUnquantified() const
{
	return false;
}

void UniversalValuationFormula::setNumericValue(VarType vt, int av_id, int var_id)
{
	assert(av != 0 && vf != 0);

	ValuationFormula::setNumericValue(vt,av_id,var_id);
	if(vt != ATOMVAR || av->first != av_id)
	{
		av->setNumericValue(vt,av_id,var_id);
		vf->setNumericValue(vt,av_id,var_id);
	}
}

void UniversalValuationFormula::setNumericValuesToBoundVariables(int val)
{
	assert(av != 0 && vf != 0);

	av->id = val;
	vf->setNumericValue(ATOMVAR, av->first, val);
	vf->setNumericValuesToBoundVariables(val + 1);
}

std::string UniversalValuationFormula::toString(const Context& c) const
{
	assert(av != 0 && vf != 0);

	std::string str{"A."};
	str.append(av->toString(c));
	str.push_back(':');
	str.push_back('(');
	str.append(vf->toString(c));
	str.push_back(')');
	return str;
}

z3::expr UniversalValuationFormula::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(av != 0 && vf != 0);

	z3::expr var = av->toZ3Expr(c, inst_num, cutoffs);
	z3::expr f = vf->toZ3Expr(c, inst_num, cutoffs);
	return z3::forall(var, f);
}

std::set<RelationVariable> UniversalValuationFormula::getPredicates(bool mode, bool within) const
{
	assert(vf != 0);

	return vf->getPredicates(mode, within);
}

ExistentialValuationFormula::ExistentialValuationFormula(const AtomVariable& a, const ValuationFormula& f) : ExistentialValuationFormula(new AtomVariable(a), f.clone())
{
}

ExistentialValuationFormula::ExistentialValuationFormula(AtomVariable* a, ValuationFormula* f) : ValuationFormula(), av(a), vf(f)
{
	assert(vf != 0 && av != 0);

	types.insert(vf->getTypeVariables().begin(), vf->getTypeVariables().end());
	relvars.insert(vf->getRelationVariables().begin(), vf->getRelationVariables().end());
	atomvars.insert(vf->getAtomVariables().begin(), vf->getAtomVariables().end());
	qfvars.insert(vf->getQuorumFunctionVariables().begin(), vf->getQuorumFunctionVariables().end());
	if(av->second.second == TYPEVAR)
	{
		types.insert(av->second);
	}
	std::set<AtomVariable>::iterator it = atomvars.find(*av);
	if(it != atomvars.end()) atomvars.erase(it);
}

ExistentialValuationFormula::ExistentialValuationFormula(const ExistentialValuationFormula& f) : ValuationFormula(f), av(new AtomVariable(*(f.av))), vf(f.vf->clone())
{
}

ValuationFormula* ExistentialValuationFormula::clone() const
{
	assert(vf != 0 && av != 0);

	return new ExistentialValuationFormula(*this);
}

ValuationFormula* ExistentialValuationFormula::createPrimedVersion(const AtomVariable& pv) const
{
	assert(vf != 0 && av != 0);

	if(pv == *av)
	{
		return new ExistentialValuationFormula(*this);
	}
	else
	{
		return new ExistentialValuationFormula(new AtomVariable(*av), vf->createPrimedVersion(pv));
	}
}

ExistentialValuationFormula& ExistentialValuationFormula::operator=(const ExistentialValuationFormula& f)
{
	if(&f == this) return *this;
	ExistentialValuationFormula uvf(f);
	std::swap(types, uvf.types);
	std::swap(relvars, uvf.relvars);
	std::swap(atomvars, uvf.atomvars);
	std::swap(qfvars, uvf.qfvars);
	std::swap(av, uvf.av);
	std::swap(vf, uvf.vf);
	return *this;
}

ExistentialValuationFormula::~ExistentialValuationFormula()
{
	delete vf;
	delete av;
}

const ValuationFormula& ExistentialValuationFormula::valuationFormula() const
{
	assert(vf != 0);

	return *vf;
}

const AtomVariable& ExistentialValuationFormula::getAtomVariable() const
{
	assert(av != 0);

	return *av;
}

bool ExistentialValuationFormula::instance(const Valuation& val) const
{
	assert(vf != 0 && av != 0);

	bool inst = false;
	std::list<Valuation*> vals = val.addAtomVariable(av->second);
	for(std::list<Valuation*>::const_iterator it = vals.begin(); it != vals.end(); ++it)
	{
		inst = inst || vf->instance(**it);
		delete *it;
	}
	return inst;
}

bool ExistentialValuationFormula::instanceCoveredBySmallerOne(const Valuation& val, const TypeVariable& tv, const AtomSet& subval) const
{
	return false;
}

int ExistentialValuationFormula::numOfNestedQFVs(const Domain& tv) const
{
	assert(vf != 0);

	return vf->numOfNestedQFVs(tv);
}

bool ExistentialValuationFormula::isExistentialFree(int numOfNegations) const
{
	assert(vf != 0);

	if(numOfNegations%2 == 1)
	{
		return (vf->isExistentialFree(numOfNegations));
	}
	else
	{
		return false;
	}
}

bool ExistentialValuationFormula::isMembershipNegated(int numOfNegations) const
{
	assert(vf != 0);

	return (vf->isMembershipNegated(numOfNegations));
}

bool ExistentialValuationFormula::isUnquantified() const
{
	return false;
}

void ExistentialValuationFormula::setNumericValue(VarType vt, int av_id, int var_id)
{
	assert(av != 0 && vf != 0);

	ValuationFormula::setNumericValue(vt,av_id,var_id);
	if(vt != ATOMVAR || av->first != av_id)
	{
		av->setNumericValue(vt,av_id,var_id);
		vf->setNumericValue(vt,av_id,var_id);
	}
}

void ExistentialValuationFormula::setNumericValuesToBoundVariables(int val)
{
	assert(av != 0 && vf != 0);

	av->id = val;
	vf->setNumericValue(ATOMVAR, av->first, val);
	vf->setNumericValuesToBoundVariables(val + 1);
}

std::string ExistentialValuationFormula::toString(const Context& c) const
{
	assert(av != 0 && vf != 0);

	std::string str{"E."};
	str.append(av->toString(c));
	str.push_back(':');
	str.push_back('(');
	str.append(vf->toString(c));
	str.push_back(')');
	return str;
}

z3::expr ExistentialValuationFormula::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(av != 0 && vf != 0);

	z3::expr var = av->toZ3Expr(c, inst_num, cutoffs);
	z3::expr f = vf->toZ3Expr(c, inst_num, cutoffs);
	return z3::exists(var, f);
}

std::set<RelationVariable> ExistentialValuationFormula::getPredicates(bool mode, bool within) const
{
	assert(vf != 0);

	return vf->getPredicates(mode, within);
}

ElementaryValuationFormula::ElementaryValuationFormula(const RelationVariable& r, const AtomVariable& a1, const AtomVariable& a2) : ElementaryValuationFormula(new RelationVariable(r), new AtomVariable(a1), new AtomVariable(a2))
{
}

ElementaryValuationFormula::ElementaryValuationFormula(RelationVariable* r, AtomVariable* a1, AtomVariable* a2) : ValuationFormula(), rel(r), atoms(new AtomSchemaTuple{})
{
	assert(rel != 0 && a1 != 0 && a2 != 0);

	relvars.insert(*rel);
	atoms->push_back(new AtomVariableSchema(a1));
	atoms->push_back(new AtomVariableSchema(a2));
	atomvars = atoms->getAtomVariables();
	types.insert(atoms->getTypeVariables().begin(), atoms->getTypeVariables().end());
}

ElementaryValuationFormula::ElementaryValuationFormula(RelationVariable* r, AtomSchemaTuple* ast) : ValuationFormula(), rel(r), atoms(ast)
{
	assert(rel != 0);

	relvars.insert(*rel);
	atomvars = atoms->getAtomVariables();
	types.insert(atoms->getTypeVariables().begin(), atoms->getTypeVariables().end());
}

ElementaryValuationFormula::ElementaryValuationFormula(const ElementaryValuationFormula& evf) : ValuationFormula(evf), rel(new RelationVariable(*(evf.rel))), atoms(new AtomSchemaTuple(*(evf.atoms)))
{
}

ElementaryValuationFormula& ElementaryValuationFormula::operator=(const ElementaryValuationFormula& evf)
{
	if(this == &evf) return *this;
	ElementaryValuationFormula f(evf);
	std::swap(types, f.types);
	std::swap(relvars, f.relvars);
	std::swap(atomvars, f.atomvars);
	std::swap(qfvars, f.qfvars);
	std::swap(rel, f.rel);
	std::swap(atoms, f.atoms);
	return *this;
}

ElementaryValuationFormula::~ElementaryValuationFormula()
{
	delete atoms;
	delete rel;
}

const RelationVariable& ElementaryValuationFormula::getRelation() const
{
	assert(rel != 0);

	return *rel;
}

const AtomVariable& ElementaryValuationFormula::getOneAtomVariable() const
{
	assert(atoms->size() > 0);

	const AtomVariableSchema* avs = dynamic_cast<const AtomVariableSchema*>((*atoms)[0]);

	assert(avs != 0);

	return avs->getAtomVariable();
}

const AtomVariable& ElementaryValuationFormula::getTheOtherAtomVariable() const
{
	assert(atoms->size() > 1);

	const AtomVariableSchema* avs = dynamic_cast<const AtomVariableSchema*>((*atoms)[1]);

	assert(avs != 0);

	return avs->getAtomVariable();
}

ValuationFormula* ElementaryValuationFormula::clone() const
{
	return new ElementaryValuationFormula(*this);
}

ValuationFormula* ElementaryValuationFormula::createPrimedVersion(const AtomVariable& pv) const
{
	assert(rel != 0);

	AtomSchemaTuple* primed_atoms = new AtomSchemaTuple{};
	for(auto a : *atoms)
	{
		primed_atoms->push_back(a->clone());
	}
	return new ElementaryValuationFormula(new RelationVariable(*rel), primed_atoms);
}

bool ElementaryValuationFormula::instance(const Valuation& val) const
{
	assert(rel != 0 && atoms != 0);

	std::vector<int> ats{};
	for(auto atsc : *atoms)
	{
		const AtomVariableSchema* avs = dynamic_cast<const AtomVariableSchema*>(atsc);

		assert(avs != 0);

		const AtomVariable& av = avs->getAtomVariable();
		if(av.second.second != TYPEVAR) return false;
		ats.push_back(av.id);
	}
	return val.isMember(rel->id, ats);
}

bool ElementaryValuationFormula::instanceCoveredBySmallerOne(const Valuation& val, const TypeVariable& tv, const AtomSet& subval) const
{
	bool iscovered = true;
	for(auto a : *atoms)
	{
		if(const AtomVariableSchema* avs = dynamic_cast<const AtomVariableSchema*>(a))
		{
			const AtomVariable& av = avs->getAtomVariable();
			if(av.second == tv)
			{
				iscovered = iscovered && (subval.isElement(val.getValue(av)) == true);
			}		
		}
	}
	return iscovered;
}

int ElementaryValuationFormula::numOfNestedQFVs(const Domain&) const
{
	return 0;
}

bool ElementaryValuationFormula::isExistentialFree(int) const
{
	return true;
}

bool ElementaryValuationFormula::isMembershipNegated(int numOfNegations) const
{
	return (numOfNegations%2 == 1);
}

bool ElementaryValuationFormula::isUnquantified() const
{
	return true;
}

void ElementaryValuationFormula::setNumericValue(VarType vt, int id, int var_id)
{
	assert(rel != 0 && atoms != 0);

	ValuationFormula::setNumericValue(vt,id,var_id);
	rel->setNumericValue(vt,id,var_id);
	for(auto a : *atoms)
	{
		a->setNumericValue(vt,id,var_id);
	}
}

void ElementaryValuationFormula::setNumericValuesToBoundVariables(int val)
{
}

std::string ElementaryValuationFormula::toString(const Context& c) const
{
	assert(rel != 0 && atoms != 0);

	std::string str{rel->toString(c)};
	str.push_back('(');
	str.append(atoms->toString(c));
	str.push_back(')');
	return str;
}

std::set<RelationVariable> ElementaryValuationFormula::getPredicates(bool mode, bool within) const
{
	assert(rel != 0);

	if(mode == within)
	{
		return {*rel};
	}
	else
	{
		return {};
	}
}

z3::expr ElementaryValuationFormula::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(rel != 0 && atoms != 0);
	z3::expr_vector vars{c};
	for(auto a : *atoms)
	{
		vars.push_back(a->toZ3Expr(c, inst_num, cutoffs));
	}
	z3::func_decl z3func = rel->toZ3Func(c, inst_num, cutoffs);
	return z3func(vars);
}

EquivalenceValuationFormula::EquivalenceValuationFormula(const AtomSchema& a1, const AtomSchema& a2) : ValuationFormula(), atom1(a1.clone()), atom2(a2.clone())
{
	atomvars.insert(atom1->getAtomVariables().begin(), atom1->getAtomVariables().end());
	atomvars.insert(atom2->getAtomVariables().begin(), atom2->getAtomVariables().end());
	types.insert(atom1->getTypeVariables().begin(), atom1->getTypeVariables().end());
	types.insert(atom2->getTypeVariables().begin(), atom2->getTypeVariables().end());
}

EquivalenceValuationFormula::EquivalenceValuationFormula(AtomSchema* a1, AtomSchema* a2) : ValuationFormula(), atom1(a1), atom2(a2)
{
	assert(atom1 != 0 && atom2 != 0);

	atomvars.insert(atom1->getAtomVariables().begin(), atom1->getAtomVariables().end());
	atomvars.insert(atom2->getAtomVariables().begin(), atom2->getAtomVariables().end());
	types.insert(atom1->getTypeVariables().begin(), atom1->getTypeVariables().end());
	types.insert(atom2->getTypeVariables().begin(), atom2->getTypeVariables().end());
}

EquivalenceValuationFormula::EquivalenceValuationFormula(const EquivalenceValuationFormula& evf) : ValuationFormula(evf), atom1(evf.atom1->clone()), atom2(evf.atom2->clone())
{
}

EquivalenceValuationFormula& EquivalenceValuationFormula::operator=(const EquivalenceValuationFormula& evf)
{
	if(&evf == this) return *this;
	EquivalenceValuationFormula f(evf);
	std::swap(types, f.types);
	std::swap(relvars, f.relvars);
	std::swap(atomvars, f.atomvars);
	std::swap(qfvars, f.qfvars);
	std::swap(atom1, f.atom1);
	std::swap(atom2, f.atom2);
	return *this;
}

EquivalenceValuationFormula::~EquivalenceValuationFormula()
{
	delete atom1;
	delete atom2;
}

const AtomSchema& EquivalenceValuationFormula::getOneAtomSchema() const
{
	assert(atom1 != 0);

	return *atom1;
}

const AtomSchema& EquivalenceValuationFormula::getTheOtherAtomSchema() const
{
	assert(atom2 != 0);

	return *atom2;
}

ValuationFormula* EquivalenceValuationFormula::clone() const
{
	assert(atom1 != 0 && atom2 != 0);

	return new EquivalenceValuationFormula(*this);
}

ValuationFormula* EquivalenceValuationFormula::createPrimedVersion(const AtomVariable& av) const
{
	assert(atom1 != 0 && atom2 != 0);

	return new EquivalenceValuationFormula(atom1->createPrimedVersion(av), atom2->createPrimedVersion(av));
}

bool EquivalenceValuationFormula::instance(const Valuation& val) const
{
	assert(atom1 != 0 && atom2 != 0);

	bool inst;
	Atom a1 = atom1->instance(val);
	Atom a2 = atom2->instance(val);
	inst = (a1 == a2);
	return inst;
}

bool EquivalenceValuationFormula::instanceCoveredBySmallerOne(const Valuation& val, const TypeVariable& tv, const AtomSet& subval) const
{
	assert(atom1 != 0 && atom2 != 0);

	bool iscovered = true;
	if(atom1->getType() == tv)
	{
		Atom a1 = atom1->instance(val);
		iscovered = iscovered && (subval.isElement(a1) == true);
	}
	if(atom2->getType() == tv)
	{
		Atom a2 = atom2->instance(val);
		iscovered = iscovered && (subval.isElement(a2) == true);
	}

	return iscovered;
}

int EquivalenceValuationFormula::numOfNestedQFVs(const Domain&) const
{
	return 0;
}

bool EquivalenceValuationFormula::isExistentialFree(int) const
{
	return true;
}

bool EquivalenceValuationFormula::isMembershipNegated(int numOfNegations) const
{
	return true;
}

bool EquivalenceValuationFormula::isUnquantified() const
{
	return true;
}

void EquivalenceValuationFormula::setNumericValue(VarType vt, int id, int var_id)
{
	assert(atom1 != 0 && atom2 != 0);

	ValuationFormula::setNumericValue(vt,id,var_id);
	atom1->setNumericValue(vt,id,var_id);
	atom2->setNumericValue(vt,id,var_id);
}

void EquivalenceValuationFormula::setNumericValuesToBoundVariables(int val)
{
}

std::string EquivalenceValuationFormula::toString(const Context& c) const
{
	assert(atom1 != 0 && atom2 != 0);

	std::string str;
	str.append(atom1->toString(c));
	str.push_back('=');
	str.append(atom2->toString(c));
	return str;
}

z3::expr EquivalenceValuationFormula::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(atom1 != 0 && atom2 != 0);

	z3::expr z3var1 = atom1->toZ3Expr(c, inst_num, cutoffs);
	z3::expr z3var2 = atom2->toZ3Expr(c, inst_num, cutoffs);
	return z3var1 == z3var2;
}

std::set<RelationVariable> EquivalenceValuationFormula::getPredicates(bool mode, bool within) const
{
	return {};
}

InclusionValuationFormula::InclusionValuationFormula(const AtomSchemaTuple& a, const AtomRelation& d) : ValuationFormula(), atom(new AtomSchemaTuple(a)), set(new AtomRelation(d))
{
	assert(atom != 0 && set != 0);

	atomvars.insert(atom->getAtomVariables().begin(), atom->getAtomVariables().end());
	types.insert(atom->getTypeVariables().begin(), atom->getTypeVariables().end());
}

InclusionValuationFormula::InclusionValuationFormula(AtomSchemaTuple* a, AtomRelation* d) : ValuationFormula(), atom(a), set(d)
{
	assert(atom != 0 && set != 0);

	atomvars.insert(atom->getAtomVariables().begin(), atom->getAtomVariables().end());
	types.insert(atom->getTypeVariables().begin(), atom->getTypeVariables().end());
}

InclusionValuationFormula::InclusionValuationFormula(const InclusionValuationFormula& evf) : ValuationFormula(evf), atom(new AtomSchemaTuple(*(evf.atom))), set(new AtomRelation(*(evf.set)))
{
}

InclusionValuationFormula& InclusionValuationFormula::operator=(const InclusionValuationFormula& evf)
{
	if(&evf == this) return *this;
	InclusionValuationFormula f(evf);
	std::swap(types, f.types);
	std::swap(relvars, f.relvars);
	std::swap(atomvars, f.atomvars);
	std::swap(qfvars, f.qfvars);
	std::swap(atom, f.atom);
	std::swap(set, f.set);
	return *this;
}

InclusionValuationFormula::~InclusionValuationFormula()
{
	delete atom;
	delete set;
}

const AtomSchemaTuple& InclusionValuationFormula::getAtomSchema() const
{
	assert(atom != 0);

	return *atom;
}

const AtomRelation& InclusionValuationFormula::getDomain() const
{
	assert(set != 0);

	return *set;
}

ValuationFormula* InclusionValuationFormula::clone() const
{
	assert(atom != 0 && set != 0);

	return new InclusionValuationFormula(*this);
}

ValuationFormula* InclusionValuationFormula::createPrimedVersion(const AtomVariable& av) const
{
	assert(atom != 0 && set != 0);

	return new InclusionValuationFormula(atom->createPrimedVersion(av), new AtomRelation(*set));
}

bool InclusionValuationFormula::instance(const Valuation& val) const
{
	assert(atom != 0 && set != 0);

	bool inst;
	AtomTuple a = atom->instance(val);
	return set->isElement(a);
}

bool InclusionValuationFormula::instanceCoveredBySmallerOne(const Valuation& val, const TypeVariable& tv, const AtomSet& subval) const
{
	assert(atom != 0);

	bool iscovered = true;
	for(auto& ats : *atom)
	{
		if(ats->getType() == tv)
		{
			Atom a = ats->instance(val);
			iscovered = iscovered && (subval.isElement(a) == true);
		}
	}
	return iscovered;
}

int InclusionValuationFormula::numOfNestedQFVs(const Domain&) const
{
	return 0;
}

bool InclusionValuationFormula::isExistentialFree(int) const
{
	return true;
}

bool InclusionValuationFormula::isMembershipNegated(int numOfNegations) const
{
	return true;
}

bool InclusionValuationFormula::isUnquantified() const
{
	return true;
}

void InclusionValuationFormula::setNumericValue(VarType vt, int id, int var_id)
{
	assert(atom != 0 && set != 0);

	ValuationFormula::setNumericValue(vt,id,var_id);
	atom->setNumericValue(vt,id,var_id);
}

void InclusionValuationFormula::setNumericValuesToBoundVariables(int val)
{
}

std::string InclusionValuationFormula::toString(const Context& c) const
{
	assert(atom != 0 && set != 0);

	std::string str;
	str.append(atom->toString(c));
	str.push_back(' ');
	str.append("in");
	str.push_back(' ');
	str.append(set->toString(c));
	return str;
}

std::set<RelationVariable> InclusionValuationFormula::getPredicates(bool mode, bool within) const
{
	return {};
}

z3::expr InclusionValuationFormula::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(false);
	return c.bool_val(false);
}

QuorumValuationFormula::QuorumValuationFormula(const QuorumFunctionVariable& q, const AtomVariable& a, const ParameterList& p) : QuorumValuationFormula(new QuorumFunctionVariable(q), new AtomVariable(a), new ParameterList(p))
{
}

QuorumValuationFormula::QuorumValuationFormula(QuorumFunctionVariable* q, AtomVariable* a, ParameterList* p) : ValuationFormula(), qfv(q), av(a), params(p)
{
	assert(qfv != 0 && av != 0 && params != 0);

	qfvars.insert(*qfv);
/*
	types.insert(std::get<1>(*qfv));
	for(auto& t : std::get<2>(*qfv))
	{
		types.insert(t);
	}
*/
	atomvars.insert(*av);
	if(av->second.second == TYPEVAR) types.insert(av->second);
	for(auto& avs : *params)
	{
		const AtomVariable& atom = static_cast<AtomVariableSchema*>(avs)->getAtomVariable();
		atomvars.insert(atom);
		if(atom.second.second == TYPEVAR) types.insert(atom.second);
	}
}

QuorumValuationFormula::QuorumValuationFormula(const QuorumValuationFormula& qvf) : ValuationFormula(qvf), qfv(new QuorumFunctionVariable(*(qvf.qfv))), av(new AtomVariable(*(qvf.av))), params(new ParameterList(*(qvf.params)))
{
}

QuorumValuationFormula& QuorumValuationFormula::operator=(const QuorumValuationFormula& qvf)
{
	if(&qvf == this) return *this;
	QuorumValuationFormula f(qvf);
	std::swap(types, f.types);
	std::swap(relvars, f.relvars);
	std::swap(atomvars, f.atomvars);
	std::swap(qfvars, f.qfvars);
	std::swap(qfv, f.qfv);
	std::swap(av, f.av);
	std::swap(params, f.params);
	return *this;
}

QuorumValuationFormula::~QuorumValuationFormula()
{
	delete av;
	delete params;
	delete qfv;
}

const QuorumFunctionVariable& QuorumValuationFormula::getQuorumFunction() const
{
	assert(qfv != 0);

	return *qfv;
}

const AtomVariable& QuorumValuationFormula::getAtomVariable() const
{
	assert(av != 0);

	return *av;
}

const ParameterList& QuorumValuationFormula::getParameters() const
{
	assert(params != 0);

	return *params;
}

ValuationFormula* QuorumValuationFormula::clone() const
{
	assert(qfv != 0 && av != 0 && params != 0);

	return new QuorumValuationFormula(*this);
}

ValuationFormula* QuorumValuationFormula::createPrimedVersion(const AtomVariable& pv) const
{
	assert(qfv != 0 && av != 0 && params != 0);

	AtomVariable* a;
	if(*av == pv)
	{
		a = new AtomVariable(pv);
		a->first = -(a->first)-1;
	}
	else
	{
		a = new AtomVariable(*av);
	}
	ParameterList* p = static_cast<ParameterList*>(params->createPrimedVersion(pv));
	return new QuorumValuationFormula(new QuorumFunctionVariable(*qfv), a, p);
}

bool QuorumValuationFormula::instance(const Valuation& val) const
{
	assert(qfv != 0 && av != 0 && params != 0);
	assert(av->second.second == TYPEVAR);

	if(av->second.second != TYPEVAR) return false;
	std::vector<int> avars;
	for(int i = 0; i < params->size(); ++i)
	{
		const AtomVariable& a = (*params)[i]->getAtomVariable();

		assert(a.second.second == TYPEVAR);

		if(a.second.second != TYPEVAR) return false;
		avars.push_back(a.id);
	}
	return val.isMember(qfv->id, av->id, avars);
}

bool QuorumValuationFormula::instanceCoveredBySmallerOne(const Valuation& val, const TypeVariable& tv, const AtomSet& subval) const
{
	assert(qfv != 0 && av != 0 && params != 0);

	bool iscovered = true;
	if(av->second == tv)
	{
		iscovered = iscovered && subval.isElement(val.getValue(*av));
	}
	for(int i = 0; i < params->size(); ++i)
	{
		const AtomVariable& avar = (*params)[i]->getAtomVariable();
		if(avar.second == tv)
		{
			iscovered = iscovered && subval.isElement(val.getValue(avar));			
		}
	}
	if(std::get<1>(*qfv) == tv)
	{
		AtomSet qset = val.getValue(*qfv, *params);
		iscovered = iscovered && (2*jointCount(subval, qset) > subval.size());
	}

	return iscovered;
}

int QuorumValuationFormula::numOfNestedQFVs(const Domain& tv) const
{
	assert(qfv != 0);

	if(std::get<1>(*qfv) == tv)
		return 1;
	else
		return 0;
}

bool QuorumValuationFormula::isExistentialFree(int) const
{
	return true;
}

bool QuorumValuationFormula::isMembershipNegated(int numOfNegations) const
{
	return (numOfNegations%2 == 0);
//	return (numOfNegations%2 == 1);
}

bool QuorumValuationFormula::isUnquantified() const
{
	return true;
}

void QuorumValuationFormula::setNumericValue(VarType vt, int id, int var_id)
{
	assert(qfv != 0 && av != 0 && params != 0);

	ValuationFormula::setNumericValue(vt,id,var_id);
	qfv->setNumericValue(vt,id,var_id);
	av->setNumericValue(vt,id,var_id);
	params->setNumericValue(vt,id,var_id);
}

void QuorumValuationFormula::setNumericValuesToBoundVariables(int val)
{
}

std::string QuorumValuationFormula::toString(const Context& c) const
{
	assert(qfv != 0 && av != 0 && params != 0);

	std::string str;
	str.append(av->toString(c));
	str.append(" in ");
	str.append(qfv->toString(c));
	str.push_back('(');
	str.append(params->toString(c));
	str.push_back(')');
	return std::move(str);
}

std::set<RelationVariable> QuorumValuationFormula::getPredicates(bool mode, bool within) const
{
	return {};
}

z3::expr QuorumValuationFormula::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(false);
	return c.bool_val(false);
}

TopValuationFormula::TopValuationFormula() : ValuationFormula()
{
}

TopValuationFormula::TopValuationFormula(const TopValuationFormula& tvf) : ValuationFormula(tvf)
{
}

TopValuationFormula& TopValuationFormula::operator=(const TopValuationFormula& evf)
{
	return *this;
}

TopValuationFormula::~TopValuationFormula()
{
}

ValuationFormula* TopValuationFormula::clone() const
{
	return new TopValuationFormula();
}

ValuationFormula* TopValuationFormula::createPrimedVersion(const AtomVariable&) const
{
	return new TopValuationFormula();
}

bool TopValuationFormula::instance(const Valuation& val) const
{
	return true;
}

bool TopValuationFormula::instanceCoveredBySmallerOne(const Valuation&, const TypeVariable&, const AtomSet& subvals) const
{
	return true;
}

int TopValuationFormula::numOfNestedQFVs(const Domain&) const
{
	return 0;
}

bool TopValuationFormula::isExistentialFree(int) const
{
	return true;
}

bool TopValuationFormula::isMembershipNegated(int numOfNegations) const
{
	return true;
}

bool TopValuationFormula::isUnquantified() const
{
	return true;
}

void TopValuationFormula::setNumericValue(VarType,int,int)
{
}

void TopValuationFormula::setNumericValuesToBoundVariables(int val)
{
}

std::string TopValuationFormula::toString(const Context&) const
{
	return "true";
}

z3::expr TopValuationFormula::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	return c.bool_val(true);
}

std::set<RelationVariable> TopValuationFormula::getPredicates(bool mode, bool within) const
{
	return {};
}

NamedValuationFormula::NamedValuationFormula(const std::string& n, const ValuationFormula& f) : NamedValuationFormula(new std::string(n), f.clone())
{
}

NamedValuationFormula::NamedValuationFormula(const std::string* n, ValuationFormula* f) : ValuationFormula(), name(n), vf(f)
{
	assert(vf != 0);

	types.insert(vf->getTypeVariables().begin(), vf->getTypeVariables().end());
	relvars.insert(vf->getRelationVariables().begin(), vf->getRelationVariables().end());
	atomvars.insert(vf->getAtomVariables().begin(), vf->getAtomVariables().end());
	qfvars.insert(vf->getQuorumFunctionVariables().begin(), vf->getQuorumFunctionVariables().end());
}

NamedValuationFormula::NamedValuationFormula(const NamedValuationFormula& f) : ValuationFormula(f), name(new std::string(*(f.name))), vf(f.vf->clone())
{
}

NamedValuationFormula& NamedValuationFormula::operator=(const NamedValuationFormula& f)
{
	if(&f == this) return *this;
	NamedValuationFormula nvf(f);
	std::swap(types, nvf.types);
	std::swap(relvars, nvf.relvars);
	std::swap(atomvars, nvf.atomvars);
	std::swap(qfvars, nvf.qfvars);
	std::swap(name, nvf.name);
	std::swap(vf, nvf.vf);
	return *this;
}

ValuationFormula* NamedValuationFormula::clone() const
{
	assert(vf != 0 && name != 0);

	return new NamedValuationFormula(*this);
}

ValuationFormula* NamedValuationFormula::createPrimedVersion(const AtomVariable& av) const
{
	assert(vf != 0 && name != 0);

	return new NamedValuationFormula(new std::string(*name), vf->createPrimedVersion(av));
}

NamedValuationFormula::~NamedValuationFormula()
{
	delete vf;
	delete name;
}

const std::string& NamedValuationFormula::getName() const
{
	assert(name != 0);

	return *name;
}

const ValuationFormula& NamedValuationFormula::getValuationFormula() const
{
	assert(vf != 0);

	return *vf;
}

bool NamedValuationFormula::instance(const Valuation& val) const
{
	assert(vf != 0);

	return (vf->instance(val));
}

bool NamedValuationFormula::instanceCoveredBySmallerOne(const Valuation& val, const TypeVariable& tv, const AtomSet& subval) const
{
	assert(vf != 0);

	return vf->instanceCoveredBySmallerOne(val, tv, subval);
}

int NamedValuationFormula::numOfNestedQFVs(const Domain& tv) const
{
	assert(vf != 0);

	return vf->numOfNestedQFVs(tv);
}

std::list<ValuationFormula*> NamedValuationFormula::conjuncts() const
{
	assert(vf != 0);

	return vf->conjuncts();
}

std::list<ValuationFormula*> NamedValuationFormula::disjuncts() const
{
	assert(vf != 0);

	return vf->disjuncts();
}

bool NamedValuationFormula::isExistentialFree(int numOfNegations) const
{
	assert(vf != 0);

	return (vf->isExistentialFree(numOfNegations));
}

bool NamedValuationFormula::isMembershipNegated(int numOfNegations) const
{
	assert(vf != 0);

	return (vf->isMembershipNegated(numOfNegations));
}

bool NamedValuationFormula::isUnquantified() const
{
	assert(vf != 0);

	return vf->isUnquantified();
}

void NamedValuationFormula::setNumericValue(VarType vt, int id, int var_id)
{
	assert(vf != 0);

	ValuationFormula::setNumericValue(vt,id,var_id);
	vf->setNumericValue(vt,id,var_id);
}

void NamedValuationFormula::setNumericValuesToBoundVariables(int val)
{
	assert(vf != 0);

	vf->setNumericValuesToBoundVariables(val);
}

std::string NamedValuationFormula::toString(const Context& c) const
{
	assert(name != 0);

	return *name;
}

z3::expr NamedValuationFormula::toZ3Expr(z3::context& c, unsigned int inst_num, const std::map<TypeVariable,int>& cutoffs) const
{
	assert(vf != 0);

	return vf->toZ3Expr(c, inst_num, cutoffs);
}

std::set<RelationVariable> NamedValuationFormula::getPredicates(bool mode, bool within) const
{
	assert(vf != 0);

	return vf->getPredicates(mode, within);
}
