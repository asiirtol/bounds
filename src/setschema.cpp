#include "setschema.h"
#include <sstream>
#include <iostream>
#include <assert.h>

SetSchema::SetSchema() : types(std::set<TypeVariable>()), avars(std::set<AtomVariable>()), relvars(std::set<RelationVariable>()), qfvars(std::set<QuorumFunctionVariable>())
{
}

SetSchema::SetSchema(const std::set<TypeVariable>& t, const std::set<AtomVariable>& a, const std::set<RelationVariable>& r, const std::set<QuorumFunctionVariable>& q) : types(t), avars(a), relvars(r), qfvars(q)
{
}

SetSchema::SetSchema(const SetSchema& ssc) : types(ssc.types), avars(ssc.avars), relvars(ssc.relvars), qfvars(ssc.qfvars)
{
}

bool SetSchema::isNotParametrised() const
{
	return (types.size() == 0);
}

bool SetSchema::isClosed() const
{
	return (avars.size() == 0);
}

const std::set<TypeVariable>& SetSchema::getTypeVariables() const
{
	return types;
}

const std::set<AtomVariable>& SetSchema::getAtomVariables() const
{
	return avars;
}

const std::set<RelationVariable>& SetSchema::getRelationVariables() const
{
	return relvars;
}

const std::set<QuorumFunctionVariable>& SetSchema::getQuorumFunctionVariables() const
{
	return qfvars;
}

SetSchema::~SetSchema()
{
}

void SetSchema::setNumericValue(VarType vt, int id, int var_id)
{
	for(std::set<TypeVariable>::iterator sit = types.begin(); sit != types.end(); ++sit)
	{
		const_cast<TypeVariable&>(*sit).setNumericValue(vt,id,var_id);
	}
	for(std::set<AtomVariable>::iterator sit = avars.begin(); sit != avars.end(); ++sit)
	{
		const_cast<AtomVariable&>(*sit).setNumericValue(vt,id,var_id);
	}
	for(std::set<RelationVariable>::iterator sit = relvars.begin(); sit != relvars.end(); ++sit)
	{
		const_cast<RelationVariable&>(*sit).setNumericValue(vt,id,var_id);
	}
	for(auto& qfv : qfvars)
		const_cast<QuorumFunctionVariable&>(qfv).setNumericValue(vt,id,var_id);
}

ElementarySetSchema::ElementarySetSchema(const ActionSetSchema& a) : SetSchema(), as(new ActionSetSchema(a))
{
	assert(as != 0);

	for(ActionSetSchema::const_iterator cit = as->begin(); cit != as->end(); ++cit)
	{
		avars.insert((*cit)->getAtomVariables().begin(), (*cit)->getAtomVariables().end());
		types.insert((*cit)->getTypeVariables().begin(), (*cit)->getTypeVariables().end());
	}
}

ElementarySetSchema::ElementarySetSchema(ActionSetSchema* a) : SetSchema(), as(a)
{
	assert(as != 0);

	for(ActionSetSchema::const_iterator cit = as->begin(); cit != as->end(); ++cit)
	{
		avars.insert((*cit)->getAtomVariables().begin(), (*cit)->getAtomVariables().end());
		types.insert((*cit)->getTypeVariables().begin(), (*cit)->getTypeVariables().end());
	}
}

ElementarySetSchema::ElementarySetSchema(const ElementarySetSchema& ssc) : SetSchema(ssc), as(new ActionSetSchema(ssc.getActionSetSchema()))
{
}

ElementarySetSchema::~ElementarySetSchema()
{
	delete as;
}

ElementarySetSchema& ElementarySetSchema::operator=(const ElementarySetSchema& ssc)
{
	if(this == &ssc) return *this;
	ElementarySetSchema nssc(ssc);
	std::swap(types, nssc.types);
	std::swap(avars, nssc.avars);
	std::swap(relvars, nssc.relvars);
	std::swap(qfvars, nssc.qfvars);
	std::swap(as, nssc.as);
	return *this;
}

int ElementarySetSchema::numOfNestedQFVs(const Domain&) const
{
	return 0;
}

bool ElementarySetSchema::isVisible() const
{
	assert(as != 0);

	for(ActionSetSchema::const_iterator cit = as->begin(); cit != as->end(); ++cit)
	{
		if((**cit).chan == 0) return false;
	}
	return true;
}

bool ElementarySetSchema::isInvisible() const
{
	assert(as != 0);

	for(ActionSetSchema::const_iterator cit = as->begin(); cit != as->end(); ++cit)
	{
		if((**cit).chan != 0) return false;
	}
	return true;
}

bool ElementarySetSchema::isConditionFree() const
{
	return true;
}

SetSchema* ElementarySetSchema::clone() const
{
	assert(as != 0);

	return new ElementarySetSchema(*this);
}

const ActionSetSchema& ElementarySetSchema::getActionSetSchema() const
{
	assert(as != 0);

	return *as;
}

ActionSet* ElementarySetSchema::instance(const Valuation& val) const
{
	assert(as != 0);

	ActionSet* acts = new ActionSet();
	for(ActionSetSchema::const_iterator cit = as->begin(); cit != as->end(); ++cit)
	{
		acts->insert((*cit)->instance(val));
	}
	return acts;
}

void ElementarySetSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(as != 0);

	SetSchema::setNumericValue(vt, id, var_id);
	for(ActionSetSchema::iterator it = as->begin(); it != as->end(); ++it)
	{
		(*it)->setNumericValue(vt, id, var_id);
	}
}

void ElementarySetSchema::setNumericValuesToBoundVariables(int val)
{
}
	
std::string ElementarySetSchema::toString(const Context& c) const
{
	assert(as != 0);

	std::list<ActionSchema*>::const_iterator cit = as->begin();
	std::stringstream ss;
	ss << '{';
	if(cit != as->end())
	{
		ss << (*cit)->toString(c);
		++cit;
	}
	for(; cit != as->end(); ++cit)
	{
		ss << ',' << (*cit)->toString(c);
	}
	ss << '}';
	return ss.str();
}

UnionSetSchema::UnionSetSchema(const SetSchema& ssc1, const SetSchema& ssc2) : UnionSetSchema(ssc1.clone(), ssc2.clone())
{
}

UnionSetSchema::UnionSetSchema(SetSchema* ssc1, SetSchema* ssc2) : SetSchema(), set1(ssc1), set2(ssc2)
{
	assert(set1 != 0 && set2 != 0);

	types.insert(set1->getTypeVariables().begin(), set1->getTypeVariables().end());
	avars.insert(set1->getAtomVariables().begin(), set1->getAtomVariables().end());
	relvars.insert(set1->getRelationVariables().begin(), set1->getRelationVariables().end());
	qfvars.insert(set1->getQuorumFunctionVariables().begin(), set1->getQuorumFunctionVariables().end());
	types.insert(set2->getTypeVariables().begin(), set2->getTypeVariables().end());
	avars.insert(set2->getAtomVariables().begin(), set2->getAtomVariables().end());
	relvars.insert(set2->getRelationVariables().begin(), set2->getRelationVariables().end());
	qfvars.insert(set2->getQuorumFunctionVariables().begin(), set2->getQuorumFunctionVariables().end());
}

UnionSetSchema::UnionSetSchema(const UnionSetSchema& ssc) : SetSchema(ssc), set1(ssc.set1->clone()), set2(ssc.set2->clone())
{
}

UnionSetSchema::~UnionSetSchema()
{
	delete set1;
	delete set2;
}

UnionSetSchema& UnionSetSchema::operator=(const UnionSetSchema& ssc)
{
	if(this == &ssc) return *this;
	UnionSetSchema nssc(ssc);
	std::swap(types, nssc.types);
	std::swap(avars, nssc.avars);
	std::swap(relvars, nssc.relvars);
	std::swap(qfvars, nssc.qfvars);
	std::swap(set1, nssc.set1);
	std::swap(set2, nssc.set2);
	return *this;
}

const SetSchema& UnionSetSchema::getOneSetSchema() const
{
	assert(set1 != 0);
	
	return *set1;
}

const SetSchema& UnionSetSchema::getOtherSetSchema() const
{
	assert(set2 != 0);
	
	return *set2;
}

int UnionSetSchema::numOfNestedQFVs(const Domain& tv) const
{
	assert(set1 != 0 && set2 != 0);
	
	return set1->numOfNestedQFVs(tv) + set2->numOfNestedQFVs(tv);
}

bool UnionSetSchema::isVisible() const
{
	assert(set1 != 0 && set2 != 0);
	
	return set1->isVisible() && set2->isVisible();
}

bool UnionSetSchema::isInvisible() const
{
	assert(set1 != 0 && set2 != 0);
	
	return set1->isInvisible() && set2->isInvisible();
}

bool UnionSetSchema::isConditionFree() const
{
	assert(set1 != 0 && set2 != 0);
	
	return (set1->isConditionFree() && set2->isConditionFree());
}

ActionSet* UnionSetSchema::instance(const Valuation& val) const
{
	assert(set1 != 0 && set2 != 0);
	
	ActionSet* acts1 = set1->instance(val);
	ActionSet* acts2 = set2->instance(val);
	acts1->insert(acts2->begin(), acts2->end());
	delete acts2;
	return acts1;
}

SetSchema* UnionSetSchema::clone() const
{
	assert(set1 != 0 && set2 != 0);

	return new UnionSetSchema(*this);
}

void UnionSetSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(set1 != 0 && set2 != 0);

	SetSchema::setNumericValue(vt,id,var_id);
	set1->setNumericValue(vt,id,var_id);
	set2->setNumericValue(vt,id,var_id);
}

void UnionSetSchema::setNumericValuesToBoundVariables(int val)
{
	assert(set1 != 0 && set2 != 0);

	set1->setNumericValuesToBoundVariables(val);
	set2->setNumericValuesToBoundVariables(val);
}
	
std::string UnionSetSchema::toString(const Context& c) const
{
	assert(set1 != 0 && set2 != 0);

	std::string str(1,'(');
	str.append(set1->toString(c));
	str.push_back('U');
	str.append(set2->toString(c));
	str.push_back(')');
	return str;
}

ConditionSetSchema::ConditionSetSchema(const SetSchema& ssc, const ValuationFormula& vf) : ConditionSetSchema(ssc.clone(), vf.clone())
{
}

ConditionSetSchema::ConditionSetSchema(SetSchema* ssc, ValuationFormula* vf) : SetSchema(), set(ssc), guard(vf)
{
	assert(set != 0 && guard != 0);

	types.insert(set->getTypeVariables().begin(), set->getTypeVariables().end());
	avars.insert(set->getAtomVariables().begin(), set->getAtomVariables().end());
	relvars.insert(set->getRelationVariables().begin(), set->getRelationVariables().end());
	qfvars.insert(set->getQuorumFunctionVariables().begin(), set->getQuorumFunctionVariables().end());
	types.insert(guard->getTypeVariables().begin(), guard->getTypeVariables().end());
	avars.insert(guard->getAtomVariables().begin(), guard->getAtomVariables().end());
	relvars.insert(guard->getRelationVariables().begin(), guard->getRelationVariables().end());
	qfvars.insert(guard->getQuorumFunctionVariables().begin(), guard->getQuorumFunctionVariables().end());
}

ConditionSetSchema::ConditionSetSchema(const ConditionSetSchema& ssc) : SetSchema(ssc), set(ssc.set->clone()), guard(ssc.guard->clone())
{
}

ConditionSetSchema::~ConditionSetSchema()
{
	delete set;
	delete guard;
}

ConditionSetSchema& ConditionSetSchema::operator=(const ConditionSetSchema& ssc)
{
	if(this == &ssc) return *this;
	ConditionSetSchema nssc(ssc);
	std::swap(types, nssc.types);
	std::swap(avars, nssc.avars);
	std::swap(relvars, nssc.relvars);
	std::swap(qfvars, nssc.qfvars);
	std::swap(set, nssc.set);
	std::swap(guard, nssc.guard);
	return *this;
}

const SetSchema& ConditionSetSchema::getSetSchema() const
{
	assert(set != 0);

	return *set;
}

const ValuationFormula& ConditionSetSchema::getValuationFormula() const
{
	assert(guard != 0);
	
	return *guard;
}

int ConditionSetSchema::numOfNestedQFVs(const Domain& tv) const
{
	assert(set != 0 && guard != 0);
	
	return guard->numOfNestedQFVs(tv) + set->numOfNestedQFVs(tv);
}

bool ConditionSetSchema::isVisible() const
{
	assert(set != 0);
	
	return set->isVisible();
}

bool ConditionSetSchema::isInvisible() const
{
	assert(set != 0);
	
	return set->isInvisible();
}

bool ConditionSetSchema::isConditionFree() const
{
	return false;
}

ActionSet* ConditionSetSchema::instance(const Valuation& val) const
{
	assert(set != 0 && guard != 0);
	
	if(guard->instance(val))
	{
		return set->instance(val);
	}
	else
	{
		return new ActionSet();
	}
}

SetSchema* ConditionSetSchema::clone() const
{
	assert(set != 0 && guard != 0);
	
	return new ConditionSetSchema(*this);
}

void ConditionSetSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(set != 0 && guard != 0);
	
	SetSchema::setNumericValue(vt,id,var_id);
	set->setNumericValue(vt,id,var_id);
	guard->setNumericValue(vt,id,var_id);
}

void ConditionSetSchema::setNumericValuesToBoundVariables(int val)
{
	assert(set != 0 && guard != 0);
	
	set->setNumericValuesToBoundVariables(val);
	guard->setNumericValuesToBoundVariables(val);
}
	
std::string ConditionSetSchema::toString(const Context& c) const
{
	assert(set != 0 && guard != 0);
	
	std::string str(1,'[');
	str.append(guard->toString(c));
	str.push_back(']');
	str.append(set->toString(c));
	return str;
}

ReplicatedSetSchema::ReplicatedSetSchema(const AtomVariable& av, const SetSchema& ss) : ReplicatedSetSchema(new AtomVariable(av), ss.clone())
{
}

ReplicatedSetSchema::ReplicatedSetSchema(AtomVariable* av, SetSchema* ss) : SetSchema(), set(ss), a(av)
{
	assert(set != 0 && a != 0);

	types.insert(set->getTypeVariables().begin(), set->getTypeVariables().end());
	avars.insert(set->getAtomVariables().begin(), set->getAtomVariables().end());
	relvars.insert(set->getRelationVariables().begin(), set->getRelationVariables().end());
	qfvars.insert(set->getQuorumFunctionVariables().begin(), set->getQuorumFunctionVariables().end());
	if(a->second.second == TYPEVAR)
	{
		types.insert(a->second);
	}
	std::set<AtomVariable>::iterator it = avars.find(*a);
	if(it != avars.end()) avars.erase(it);
}

ReplicatedSetSchema::ReplicatedSetSchema(const ReplicatedSetSchema& rss) : SetSchema(rss), set(rss.set->clone()), a(new AtomVariable(*(rss.a)))
{
}

ReplicatedSetSchema& ReplicatedSetSchema::operator=(const ReplicatedSetSchema& rss)
{
	ReplicatedSetSchema ss(rss);
	std::swap(types, ss.types);
	std::swap(avars, ss.avars);
	std::swap(relvars, ss.relvars);
	std::swap(qfvars, ss.qfvars);
	std::swap(set, ss.set);
	std::swap(a, ss.a);
	return *this;
}

ReplicatedSetSchema::~ReplicatedSetSchema()
{
	delete set;
	delete a;
}

const SetSchema& ReplicatedSetSchema::getSetSchema() const
{
	assert(set != 0);
	
	return *set;
}

const AtomVariable& ReplicatedSetSchema::getAtomVariable() const
{
	assert(a != 0);
	
	return *a;
}

int ReplicatedSetSchema::numOfNestedQFVs(const Domain& tv) const
{
	assert(set != 0);
	
	return set->numOfNestedQFVs(tv);
}

bool ReplicatedSetSchema::isInvisible() const
{
	assert(set != 0);
	
	return set->isInvisible();
}

bool ReplicatedSetSchema::isVisible() const
{
	assert(set != 0);
	
	return set->isVisible();
}

bool ReplicatedSetSchema::isConditionFree() const
{
	assert(set != 0);
	
	return set->isConditionFree();
}

SetSchema* ReplicatedSetSchema::clone() const
{
	assert(a != 0 && set != 0);
	
	return new ReplicatedSetSchema(*this);
}

ActionSet* ReplicatedSetSchema::instance(const Valuation& val) const
{
	assert(a != 0 && set != 0);
	
	ActionSet* issc = new ActionSet();
	std::list<Valuation*> vals = val.addAtomVariable(a->second);
	for(std::list<Valuation*>::iterator it = vals.begin(); it != vals.end(); ++it)
	{
		ActionSet* issc2 = set->instance(**it);
		issc->insert(issc2->begin(), issc2->end());
		delete issc2;
		delete *it;
	}
	return issc;
}

void ReplicatedSetSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(a != 0 && set != 0);
	
	SetSchema::setNumericValue(vt,id,var_id);
	if(vt != ATOMVAR || a->first != id)
	{
		set->setNumericValue(vt,id,var_id);
		a->setNumericValue(vt,id,var_id);
	}
}

void ReplicatedSetSchema::setNumericValuesToBoundVariables(int val)
{
	assert(a != 0 && set != 0);
	
	a->id = val;
	set->setNumericValue(ATOMVAR, a->first, val);
	set->setNumericValuesToBoundVariables(val + 1);	
}
	
std::string ReplicatedSetSchema::toString(const Context& c) const
{
	assert(a != 0 && set != 0);
	
	std::string str(1, '(');
	str.push_back('U');
	str.append(a->toString(c));
	str.push_back(':');
	str.append(set->toString(c));
	str.push_back(')');
	return str;
}

NamedSetSchema::NamedSetSchema(const std::string& n, const SetSchema& ss) : NamedSetSchema(new std::string(n), ss.clone())
{
}

NamedSetSchema::NamedSetSchema(const std::string* n, SetSchema* ss) : SetSchema(), name(n), set(ss)
{
	assert(set != 0 && name != 0);

	types.insert(set->getTypeVariables().begin(), set->getTypeVariables().end());
	avars.insert(set->getAtomVariables().begin(), set->getAtomVariables().end());
	relvars.insert(set->getRelationVariables().begin(), set->getRelationVariables().end());
	qfvars.insert(set->getQuorumFunctionVariables().begin(), set->getQuorumFunctionVariables().end());
}

NamedSetSchema::NamedSetSchema(const NamedSetSchema& rss) : SetSchema(rss), name(new std::string(*(rss.name))), set(rss.set->clone())
{
}

NamedSetSchema& NamedSetSchema::operator=(const NamedSetSchema& rss)
{
	NamedSetSchema ss(rss);
	std::swap(types, ss.types);
	std::swap(avars, ss.avars);
	std::swap(relvars, ss.relvars);
	std::swap(qfvars, ss.qfvars);
	std::swap(set, ss.set);
	std::swap(name, ss.name);
	return *this;
}

NamedSetSchema::~NamedSetSchema()
{
	delete set;
	delete name;
}

const std::string& NamedSetSchema::getName() const
{
	assert(name != 0);
	
	return *name;
}

const SetSchema& NamedSetSchema::getSetSchema() const
{
	assert(set != 0);
	
	return *set;
}

int NamedSetSchema::numOfNestedQFVs(const Domain& tv) const
{
	assert(set != 0);
	
	return set->numOfNestedQFVs(tv);
}

bool NamedSetSchema::isInvisible() const
{
	assert(set != 0);
	
	return set->isInvisible();
}

bool NamedSetSchema::isVisible() const
{
	assert(set != 0);
	
	return set->isVisible();
}

bool NamedSetSchema::isConditionFree() const
{
	assert(set != 0);
	
	return set->isConditionFree();
}

SetSchema* NamedSetSchema::clone() const
{
	assert(name != 0 && set != 0);
	
	return new NamedSetSchema(*this);
}

ActionSet* NamedSetSchema::instance(const Valuation& val) const
{
	assert(set != 0);
	
	return set->instance(val);
}

void NamedSetSchema::setNumericValuesToBoundVariables(int val)
{
	assert(set != 0);
	
	set->setNumericValuesToBoundVariables(val);
}
	
void NamedSetSchema::setNumericValue(VarType vt, int id, int var_id)
{
	assert(set != 0);
	
	SetSchema::setNumericValue(vt,id,var_id);
	set->setNumericValue(vt,id,var_id);
}


std::string NamedSetSchema::toString(const Context& c) const
{
	assert(name != 0);
	
	return *name;
}

/*
InstanceOfSetSchema::InstanceOfSetSchema() : acts(new ActionSet())
{
}

InstanceOfSetSchema::InstanceOfSetSchema(ActionSet* as) : acts(as)
{
}

InstanceOfSetSchema::InstanceOfSetSchema(const InstanceOfSetSchema& iss) : acts(new ActionSet(*(iss.acts)))
{
}

InstanceOfSetSchema::~InstanceOfSetSchema()
{
	delete acts;
}

const ActionSet& InstanceOfSetSchema::getActionSet() const
{
	return *acts;
}

ElementaryInstanceOfSetSchema::ElementaryInstanceOfSetSchema(const ElementarySetSchema* s, const Valuation& v) : InstanceOfSetSchema(), ssc(s), val(v)
{
	const ActionSetSchema& as = s->getActionSetSchema();
	for(ActionSetSchema::const_iterator cit = as.begin(); cit != as.end(); ++cit)
	{
		acts->insert((*cit)->instance(val));
	}
}

ElementaryInstanceOfSetSchema::ElementaryInstanceOfSetSchema(const ElementaryInstanceOfSetSchema& s) : InstanceOfSetSchema(s), ssc(s.ssc), val(s.val)
{
}

ElementaryInstanceOfSetSchema& ElementaryInstanceOfSetSchema::operator=(const ElementaryInstanceOfSetSchema& eiss)
{
	if(this == &eiss) return *this;
	ElementaryInstanceOfSetSchema e(eiss);
	std::swap(ssc, e.ssc);
	std::swap(val, e.val);
	std::swap(acts, e.acts);
	return *this;
}

const ElementarySetSchema& ElementaryInstanceOfSetSchema::getSetSchema() const
{
	return *ssc;
}

const Valuation& ElementaryInstanceOfSetSchema::getValuation() const
{
	return val;
}

InstanceOfSetSchema* ElementaryInstanceOfSetSchema::clone() const
{
	return new ElementaryInstanceOfSetSchema(*this);
}

std::string ElementaryInstanceOfSetSchema::toString() const
{
//	return setToString<Action*, PointerCompare<Action> >(getActionSet());
	return getActionSet().toString();
}

InstanceOfSetSchema* EmptyInstanceOfSetSchema::clone() const
{
	return new EmptyInstanceOfSetSchema();
}

std::string EmptyInstanceOfSetSchema::toString() const
{
	return "{}";
}

UnionInstanceOfSetSchema::UnionInstanceOfSetSchema(const InstanceOfSetSchema* s1, const InstanceOfSetSchema* s2) : InstanceOfSetSchema(*s1), ssc1(s1), ssc2(s2)
{
	const ActionSet& as2 = ssc2->getActionSet();
	for(ActionSet::const_reverse_iterator cit = as2.rbegin(); cit != as2.rend(); ++cit)
	{
		acts->insert(**cit);
	}
}

UnionInstanceOfSetSchema::UnionInstanceOfSetSchema(const UnionInstanceOfSetSchema& s) : InstanceOfSetSchema(s), ssc1(s.ssc1->clone()), ssc2(s.ssc2->clone())
{
}

UnionInstanceOfSetSchema::~UnionInstanceOfSetSchema()
{
	delete ssc1;
	delete ssc2;
}

UnionInstanceOfSetSchema& UnionInstanceOfSetSchema::operator=(const UnionInstanceOfSetSchema& uss)
{
	if(this == &uss) return *this;
	UnionInstanceOfSetSchema nuss(uss);
	std::swap(acts, nuss.acts);
	std::swap(ssc1, nuss.ssc1);
	std::swap(ssc2, nuss.ssc2);
	return *this;
}

const InstanceOfSetSchema& UnionInstanceOfSetSchema::getOneInstance() const
{
	return *ssc1;
}

const InstanceOfSetSchema& UnionInstanceOfSetSchema::getOtherInstance() const
{
	return *ssc2;
}

InstanceOfSetSchema* UnionInstanceOfSetSchema::clone() const
{
	return new UnionInstanceOfSetSchema(*this);
}

std::string UnionInstanceOfSetSchema::toString() const
{
//	return setToString<Action*, PointerCompare<Action> >(getActionSet());
	return getActionSet().toString();
}
*/
