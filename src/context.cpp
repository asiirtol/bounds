#include "context.h"
#include <assert.h>

Context::Context(const Context& c) : valfs{c.valfs}, sscs{c.sscs}, ltscs{c.ltscs}, rels{c.rels}, type2intpair{c.type2intpair}, 
	int2type{c.int2type}, avar2inttriplet{c.avar2inttriplet}, int2avar{c.int2avar}, int2primedavar{c.int2primedavar}, relv2intquartet{c.relv2intquartet}, 
	int2relv{c.int2relv}, str2qfv{c.str2qfv}, int2qfvname{c.int2qfvname}, qfvdom{c.qfvdom}, ftype2int{c.ftype2int}, ftypesizes{c.ftypesizes}, int2ftype{c.int2ftype}, 
	chan2int{c.chan2int}, int2chan{c.int2chan}, chantypes{c.chantypes}, cstate2int{c.cstate2int}, int2cstate{c.int2cstate}, atom2intpair{c.atom2intpair}, int2atom{c.int2atom}
{
	for(auto& vf : valfs)
	{
		vf.second = vf.second->clone();
	}
	for(auto& sc : sscs)
	{
		sc.second = sc.second->clone();
	}
	for(auto& ltsc : ltscs)
	{
		ltsc.second = ltsc.second->clone();
	}
	for(auto& r : rels)
	{
		r.second = new AtomRelation{*(r.second)};
	}
}

Context::~Context()
{
	clear();
}

void Context::clear()
{
	for(std::map<std::string, const ValuationFormula*>::iterator it = valfs.begin(); it != valfs.end(); it++)
	{
		delete it->second;
	}
	for(std::map<std::string, const SetSchema*>::iterator it = sscs.begin(); it != sscs.end(); it++)
	{
		delete it->second;
	}
	for(std::map<std::string, const LTSSchema*>::iterator it = ltscs.begin(); it != ltscs.end(); it++)
	{
		delete it->second;
	}
/*
	for(std::map<std::string, const AtomSet*>::iterator it = sets.begin(); it != sets.end(); it++)
	{
		delete it->second;
	}
	for(std::map<std::string, std::pair<Domain*, int> >::iterator it = atomvars.begin(); it != atomvars.end(); it++)
	{
		delete it->second.first;
	}
	for(std::map<std::string, std::vector<const Domain*> >::iterator it = chans.begin(); it != chans.end(); it++)
	{
		for(int i = 0; i < it->second.size(); i++)
		{
			delete it->second.operator[](i);
		}
	}
*/
	for(std::map<std::string, const AtomRelation*>::iterator it = rels.begin(); it != rels.end(); it++)
	{
		delete it->second;
	}

	type2intpair.clear();
	int2type.clear();
	avar2inttriplet.clear();
	int2avar.clear();
	int2primedavar.clear();
	relv2intquartet.clear();
	int2relv.clear();
	str2qfv.clear();
	int2qfvname.clear();
	qfvdom.clear();
	ftype2int.clear();
	ftypesizes.clear();
	int2ftype.clear();
	chan2int.clear();
	int2chan.clear();
	chantypes.clear();
	cstate2int.clear();
	int2cstate.clear();
	atom2intpair.clear();
	int2atom.clear();
/*
	atoms.clear();
	typevars.clear();
	atomvars.clear();
	relvars.clear();
	sets.clear();
	chans.clear();
	cstates.clear();
*/
	valfs.clear();
	sscs.clear();
	ltscs.clear();
	rels.clear();
/*
	sets.clear();
	int2rel.clear();
	int2set.clear();
*/
}

bool Context::isNotDefined(const std::string* name, bool checkstates) const
{
	if(name == 0) return false;
	if(atom2intpair.count(*name) == 1) return false;
	if(avar2inttriplet.count(*name) == 1) return false;
	if(relv2intquartet.count(*name) == 1) return false;
	if(str2qfv.count(*name) == 1) return false;
	if(type2intpair.count(*name) == 1) return false;
	if(valfs.count(*name) == 1) return false;
	if(sscs.count(*name) == 1) return false;
	if(ltscs.count(*name) == 1) return false;
	if(ftype2int.count(*name) == 1) return false;
	if(rels.count(*name) == 1) return false;
//	if(sets.count(*name) == 1) return false;
	if(chan2int.count(*name) == 1) return false;
	if(checkstates)
	{
		for(int i = 0; i < cstate2int.size(); i++)
		{
			if(cstate2int[i].count(*name) == 1) return false;
		}
	}
	return true;
}

bool Context::isAtom(const std::string* name) const
{
	assert(name != 0);

	return (atom2intpair.count(*name) == 1);
}

bool Context::isTypeVariable(const std::string* name) const
{
	assert(name != 0);

	return (type2intpair.count(*name) == 1);
}

bool Context::isAtomVariable(const std::string* name) const
{
	assert(name != 0);

	return (avar2inttriplet.count(*name) == 1);
}

bool Context::isRelationVariable(const std::string* name) const
{
	assert(name != 0);

	return (relv2intquartet.count(*name) == 1);
}

bool Context::isQuorumFunctionVariable(const std::string* name) const
{
	assert(name != 0);

	return (str2qfv.count(*name) == 1);
}

bool Context::isValuationFormula(const std::string* name) const
{
	assert(name != 0);

	return (valfs.count(*name) == 1);
}

bool Context::isSetSchema(const std::string* name) const
{
	assert(name != 0);

	return (sscs.count(*name) == 1);
}

bool Context::isLTSSchema(const std::string* name) const
{
	assert(name != 0);

	return (ltscs.count(*name) == 1);
}

/*
bool Context::isAtomSet(const std::string* name) const
{
	assert(name != 0);

	return (sets.count(*name) == 1 || ftype2int.count(*name) == 1);
}
*/

bool Context::isFiniteType(const std::string* name) const
{
	assert(name != 0);

	return (ftype2int.count(*name) == 1);
}

bool Context::isAtomRelation(const std::string* name) const
{
	assert(name != 0);

	return (rels.count(*name) == 1);
}

bool Context::isChannel(const std::string* name) const
{
	assert(name != 0);

	return (chan2int.count(*name) == 1);
}

bool Context::isCorrectlyTyped(const ActionSchema* actsc) const
{
	assert(actsc != 0);

	if(actsc->chan < 0 || actsc->chan > chan2int.size()) return false;
	if(actsc->chan == 0)
	{
		return (actsc->size() == 0);
	}
	else
	{
		const std::vector<Domain>& types = chantypes[actsc->chan-1];
		if(types.size() != actsc->size()) return false;
		for(int i; i < types.size(); i++)
		{
			if(actsc->operator[](i)->getType() != types[i]) return false;
		}
		return true;
	}
}

std::vector<int> Context::getTypeSizes(const AtomSchemaTuple* ast) const
{
	std::vector<int> typesizes;
	for(int i = 0; i < ast->size(); i++)
	{
		Domain d = ast->operator[](i)->getType();
		if(d.second == FINTYPE)
		{
			typesizes.push_back(ftypesizes[d.first]);
		}
		else
		{
			typesizes.push_back(-1);
		}
	}
	return typesizes;
}

Atom* Context::getAtom(const std::string* name) const
{
	assert(name != 0);

	std::map<std::string, std::pair<int,int> >::const_iterator it = atom2intpair.find(*name);
	if (it == atom2intpair.end()) return 0;
	return new Atom(it->second.second, Domain(it->second.first,FINTYPE));
}

TypeVariable* Context::getTypeVariable(const std::string* name) const
{
	assert(name != 0);

	std::map<std::string, std::pair<int,int> >::const_iterator it = type2intpair.find(*name);
	if (it == type2intpair.end()) return 0;
	return new TypeVariable(it->second.first, it->second.second, TYPEVAR);
}

TypeVariable* Context::getFiniteType(const std::string* name) const
{
	assert(name != 0);

	std::map<std::string, int>::const_iterator it = ftype2int.find(*name);
	if (it == ftype2int.end()) return 0;
	return new TypeVariable(it->second, ftypesizes[it->second], FINTYPE);
}

AtomVariable* Context::getAtomVariable(const std::string* name) const
{
	assert(name != 0);

	std::map<std::string, AtomVariable>::const_iterator it = avar2inttriplet.find(*name);
	if (it == avar2inttriplet.end()) return 0;
	return new AtomVariable(it->second);
}

AtomVariable* Context::createAtomVariable(const TypeVariable& tv)
{
	assert(tv.first >= 0 && ((tv.second == TYPEVAR && tv.first < int2type.size()) || (tv.second == FINTYPE && tv.first < int2ftype.size())));

	AtomVariable* temp_av = nullptr;
	std::string name{"_"};
	if(tv.second == TYPEVAR)
	{
		name.append(int2type[tv.first]);
		name.append("-av");
		name.append(std::to_string(int2avar.size()));
		temp_av = new AtomVariable{static_cast<int>(int2avar.size()), tv, -1};
		avar2inttriplet.insert(std::pair<std::string,AtomVariable>(name,*temp_av));
		int2avar.push_back(name);
		std::string primedname{name};
		primedname.push_back('\'');
		int2primedavar.push_back(primedname);
	}
	else
	{
		name.append(int2ftype[tv.first]);
		name.append("-av");
		name.append(std::to_string(int2avar.size()));
		temp_av = new AtomVariable{static_cast<int>(int2avar.size()), tv, -1};
		avar2inttriplet.insert(std::pair<std::string,AtomVariable>(name,*temp_av));
		int2avar.push_back(name);
	}
	return temp_av;
}

RelationVariable* Context::getRelationVariable(const std::string* name) const
{
	assert(name != 0);

	std::map<std::string, RelationVariable>::const_iterator it = relv2intquartet.find(*name);
	if (it == relv2intquartet.end()) return 0;
	return new RelationVariable(it->second);
}

QuorumFunctionVariable* Context::getQuorumFunctionVariable(const std::string* name) const
{
	assert(name != 0);

	auto it = str2qfv.find(*name);
	if (it == str2qfv.end()) return 0;
	return new QuorumFunctionVariable(std::get<0>(it->second), std::get<1>(it->second), qfvdom[std::get<0>(it->second)], std::get<2>(it->second));
}

ValuationFormula* Context::getValuationFormula(const std::string* name) const
{
	assert(name != 0);

	std::map<std::string, const ValuationFormula*>::const_iterator it = valfs.find(*name);
	if (it == valfs.end()) return 0;
	return it->second->clone();
}

SetSchema* Context::getSetSchema(const std::string* name) const
{
	assert(name != 0);

	std::map<std::string, const SetSchema*>::const_iterator it = sscs.find(*name);
	if (it == sscs.end()) return 0;
	return it->second->clone();
}

const LTSSchema* Context::getLTSSchema(const std::string* name) const
{
	assert(name != 0);

	std::map<std::string, const LTSSchema*>::const_iterator it = ltscs.find(*name);
	if (it == ltscs.end()) return 0;
	return it->second;
}

/*
AtomSet* Context::getAtomSet(const std::string* name) const
{
	assert(name != 0);

	std::map<std::string, const AtomSet*>::const_iterator asit = sets.find(*name);
	if (asit != sets.end()) return new AtomSet(*(asit->second));
	std::map<std::string, int>::const_iterator ftit = ftype2int.find(*name);
	if (ftit == ftype2int.end()) return 0;
	AtomSet* as = new AtomSet();
	for(int i = 0; i < ftypesizes[ftit->second]; i++)
	{
		as->insert(Atom(i,Domain(ftit->second,FINTYPE)));
	}
	return as;
}
*/

AtomRelation* Context::getAtomRelation(const std::string* name) const
{
	assert(name != 0);

	std::map<std::string, const AtomRelation*>::const_iterator it = rels.find(*name);
	if (it == rels.end()) return 0;
	return new AtomRelation(*(it->second));
}

Channel Context::getChannel(const std::string* name) const
{
	assert(name != 0);

	std::map<std::string, int>::const_iterator it = chan2int.find(*name);
	if (it == chan2int.end()) return 0;
	return Channel(it->second);
}

std::string Context::atomToString(const Atom& a) const
{
	assert((a.second.second == FINTYPE && a.second.first >= 0 && a.second.first < int2atom.size()) || (a.second.second == TYPEVAR && a.second.first >= 0 && a.second.first < int2type.size()));
	if(a.second.second == TYPEVAR)
	{
		std::stringstream ass;
		ass << int2type[a.second.first];
		ass << a.first;
		return ass.str();
	}
	else
	{
		assert(a.first >= 0 && a.first < int2atom[a.second.first].size());
		return int2atom[a.second.first][a.first];
	}
}

const std::string& Context::typeVariableToString(const TypeVariable& tv) const
{
	assert(tv.first >= 0 && ((tv.second == TYPEVAR && tv.first < int2type.size()) || (tv.second == FINTYPE && tv.first < int2ftype.size())));

	if(tv.second == TYPEVAR)
	{
		return int2type[tv.first];
	}
	else
	{
		return int2ftype[tv.first];
	}
}

const std::string& Context::controlStateToString(const ControlState& cs) const
{
	static std::vector<std::string> anonstates;

	assert(cs.first >= 0 && cs.first <= int2cstate.size());

	if(cs.first == 0)
	{
		for(int i = anonstates.size(); i <= cs.second; i++)
		{
			std::stringstream ss;
			ss << 'S' << '_' << i;
			anonstates.push_back(ss.str());
		}
		return anonstates[cs.second];
	}
	else
	{
		assert(cs.second >= 0 && cs.second < int2cstate[cs.first-1].size());

		return int2cstate[cs.first-1][cs.second];
	}
}

const std::string& Context::atomVariableToString(const AtomVariable& av) const
{
	assert(-(int)int2avar.size() <= av.first && av.first < (int)int2avar.size());

	if(av.first >= 0)
	{
		return int2avar[av.first];
	}
	else
	{
		return int2primedavar[-(av.first)-1];
	}
}

const std::string& Context::relationVariableToString(const RelationVariable& rv) const
{
	assert(std::get<0>(rv) >= 0 && std::get<0>(rv) < int2relv.size());

	return int2relv[std::get<0>(rv)];
}

const std::string& Context::quorumFunctionVariableToString(const QuorumFunctionVariable& qfv) const
{
	assert(std::get<0>(qfv) >= 0 && std::get<0>(qfv) < int2qfvname.size());

	return int2qfvname[std::get<0>(qfv)];
}

const std::string& Context::channelToString(const Channel& c) const
{
	static const std::string tau("tau");
	assert(c >= 0 && c <= chan2int.size());

	if(c == 0) return tau;
	return int2chan[c-1];
}

/*
const std::string& Context::relationToString(const AtomRelation& as) const
{
	assert(as.id >= 0 && as.id < int2rel.size());

	return int2rel[as.id];
}

const std::string& Context::setToString(const AtomSet& as) const
{
	assert(as.id >= 0 && as.id < int2set.size());

	return int2set[as.id];
}
*/

void Context::insertNewProcess()
{
	cstate2int.push_back(std::map<std::string,int>());
	int2cstate.push_back(std::vector<std::string>());
}

ControlState Context::insertControlState(const std::string* cs)
{
	assert(cstate2int.size() > 0 && cs != 0);

	std::map<std::string, int>::iterator it = cstate2int[cstate2int.size()-1].find(*cs);
	if(it == cstate2int[cstate2int.size()-1].end())
	{
		cstate2int[cstate2int.size()-1].insert(it, std::pair<std::string, int>(*cs, int2cstate[int2cstate.size()-1].size()));
		int2cstate[int2cstate.size()-1].push_back(*cs);
		return ControlState(cstate2int.size(),int2cstate[int2cstate.size()-1].size()-1);
	}
	else
	{
		return ControlState(cstate2int.size(),it->second);
	}
}

void Context::insertFiniteType(const std::string* name)
{
	assert(name != 0);

	int2ftype.push_back(*name);
	ftype2int[*name] = ftypesizes.size();
	ftypesizes.push_back(0);
	int2atom.push_back(std::vector<std::string>());
}

void Context::finiteTypeInserted()
{
	Domain type(ftypesizes.size()-1,FINTYPE);
	std::vector<Domain> types;
	types.push_back(type);
	int typesize = ftypesizes[ftypesizes.size()-1];
	std::vector<int> typesizes;
	typesizes.push_back(typesize);
	AtomRelation* ar = new AtomRelation(types, typesizes);
	for(int i = 0; i < typesize; i++)
	{
		AtomTuple at;
		at.push_back(Atom(i,type));
		ar->insert(at);
	}
	insertAtomRelation(&int2ftype[int2ftype.size()-1], ar);
}

void Context::insertAtom(const std::string* name)
{
	assert(name != 0);

	atom2intpair[*name] = std::pair<int,int>(int2ftype.size()-1,int2atom[int2atom.size()-1].size());
	int2atom[int2atom.size()-1].push_back(*name);
	ftypesizes[ftypesizes.size()-1]++;
}

void Context::insertTypeVariable(const std::string* name, int id)
{
	assert(name != 0);

	type2intpair[*name] = std::pair<int,int>(int2type.size(), id);
	int2type.push_back(*name);
}

void Context::insertAtomVariable(const std::string* name, const std::string* type, int id)
{
	assert(name != 0 && type != 0);

	std::map<std::string, std::pair<int,int> >::const_iterator it = type2intpair.find(*type);
	if (it != type2intpair.end())
	{
		avar2inttriplet.insert(std::pair<std::string,AtomVariable>(*name,AtomVariable(int2avar.size(), it->second.first, id)));
		int2avar.push_back(*name);
		std::string primedname(*name);
		primedname.push_back('\'');
		int2primedavar.push_back(primedname);
	}
	std::map<std::string, int>::const_iterator sit = ftype2int.find(*type);
	if (sit != ftype2int.end())
	{
		avar2inttriplet.insert(std::pair<std::string,AtomVariable>(*name,AtomVariable(int2avar.size(), TypeVariable(sit->second, ftypesizes[sit->second], FINTYPE), id)));
		int2avar.push_back(*name);
	}
}

void Context::insertRelationVariable(const std::string* name, const std::string* type1, const std::string* type2, int id)
{
	assert(name != 0 && type1 != 0 && type2 != 0);

	std::map<std::string, std::pair<int,int> >::const_iterator it1 = type2intpair.find(*type1);
	std::map<std::string, std::pair<int,int> >::const_iterator it2 = type2intpair.find(*type2);
	if(it1 != type2intpair.end() && it2 != type2intpair.end())
	{
		relv2intquartet.insert(std::pair<std::string,RelationVariable>(*name, RelationVariable(int2relv.size(), it1->second.first, it2->second.first, id)));
		int2relv.push_back(*name);
	}
}

void Context::insertRelationVariable(const std::string* name, const std::vector<std::string>* typestrs, int id)
{
	assert(name != 0);

	std::vector<TypeVariable> types;
	for(auto t : *typestrs)
	{
		auto it = type2intpair.find(t);
		if(it != type2intpair.end())
		{
			types.push_back(it->second.first);
		}
	}
	relv2intquartet.insert(std::pair<std::string,RelationVariable>(*name, RelationVariable(int2relv.size(), types, id)));
	int2relv.push_back(*name);
}

void Context::insertQuorumFunctionVariable(const std::string* name, const std::string* type1, int id)
{
	assert(name != 0 && type1 != 0);

	std::map<std::string, std::pair<int,int> >::const_iterator it1 = type2intpair.find(*type1);
	if(it1 != type2intpair.end())
	{
		str2qfv.insert(std::pair<std::string,std::tuple<int,int,int>>(*name, std::make_tuple(int2qfvname.size(), it1->second.first, id)));
		int2qfvname.push_back(*name);
		qfvdom.push_back(std::vector<TypeVariable>());
	}
}

void Context::appendQuorumFunctionType(const std::string* type)
{
	assert(type != 0);

	std::map<std::string, std::pair<int,int> >::const_iterator it = type2intpair.find(*type);
	if (it != type2intpair.end())
	{
		qfvdom[qfvdom.size()-1].push_back(TypeVariable(it->second.first,TYPEVAR));
	}
}

void Context::insertValuationFormula(const std::string* name, const ValuationFormula* vf)
{
	assert(name != 0 && vf != 0);

	valfs[*name] = vf;
}

void Context::insertSetSchema(const std::string* name, const SetSchema* ssc)
{
	assert(name != 0 && ssc != 0);

	sscs[*name] = ssc;
}

void Context::insertLTSSchema(const std::string* name, const LTSSchema* ltsc)
{
	assert(name != 0 && ltsc != 0);

	ltscs[*name] = ltsc;
}

/*
void Context::insertAtomSet(const std::string* name, const AtomSet* as)
{
	assert(name != 0 && as != 0);

	int2set.push_back(*name);
	sets[*name] = as;
}
*/

void Context::insertAtomRelation(const std::string* name, const AtomRelation* ar)
{
	assert(name != 0 && ar != 0);

//	int2rel.push_back(*name);
	rels[*name] = ar;
}

void Context::insertChannel(const std::string* name)
{
	assert(name != 0);

	chantypes.push_back(std::vector<Domain>());
	int2chan.push_back(*name);
	chan2int.insert(std::pair<std::string,int>(*name,int2chan.size()));
}

void Context::appendChannelType(const std::string* type)
{
	assert(type != 0);

	std::map<std::string, std::pair<int,int> >::const_iterator it = type2intpair.find(*type);
	if (it != type2intpair.end())
	{
		chantypes[chantypes.size()-1].push_back(Domain(it->second.first,TYPEVAR));
	}
	std::map<std::string, int>::const_iterator sit = ftype2int.find(*type);
	if (sit != ftype2int.end())
	{
		chantypes[chantypes.size()-1].push_back(Domain(sit->second,FINTYPE));
	}
}
