#include "fdr2output4trref.h"
#include <fstream>
#include <sstream>
#include <list>
#include <set>
#include <map>
#include <iomanip>
#include <ctime>
#include <cstdio>
#include <future>
#include <mutex>
#include <fdr/fdr.h>
#include "reduce.h"

int TraceRefinementOutput::generateInstances(const Context& ctxt, const LTSSchema* spec, const LTSSchema* sys, const ValuationFormula* vf, std::set<Valuation*,PointerCompare<Valuation> >* vals, const std::string& filenameprefix, const std::map<TypeVariable,bool>& cutoffisexact, bool sound, bool complete)
{
	std::set<TypeVariable> types = sys->getTypeVariables();
	types.insert(spec->getTypeVariables().begin(), spec->getTypeVariables().end());
	types.insert(vf->getTypeVariables().begin(), vf->getTypeVariables().end());
	std::set<RelationVariable> relvars = sys->getRelationVariables();
	relvars.insert(spec->getRelationVariables().begin(), spec->getRelationVariables().end());
	//relvars.insert(vf->getRelationVariables().begin(), vf->getRelationVariables().end());
	std::set<AtomVariable> atomvars = sys->getAtomVariables();
	atomvars.insert(spec->getAtomVariables().begin(), spec->getAtomVariables().end());
	atomvars.insert(vf->getAtomVariables().begin(), vf->getAtomVariables().end());
	std::set<QuorumFunctionVariable> qfvars = sys->getQuorumFunctionVariables();
	qfvars.insert(spec->getQuorumFunctionVariables().begin(), spec->getQuorumFunctionVariables().end());
	qfvars.insert(vf->getQuorumFunctionVariables().begin(), vf->getQuorumFunctionVariables().end());
	std::set<TypeVariable> datatypes;
	std::set<TypeVariable> proctypes;
	for(std::set<TypeVariable>::const_iterator ctit = types.begin(); ctit != types.end(); ctit++)
	{
		if(spec->isDataTypeVariable(*ctit) && sys->isDataTypeVariable(*ctit)) datatypes.insert(*ctit);
		if(spec->isProcessTypeVariable(*ctit) && sys->isProcessTypeVariable(*ctit)) proctypes.insert(*ctit);
	}
	// std::list<triplet<InstanceOfLTSSchema*, InstanceOfLTSSchema*, int> > instances;
	std::atomic<bool> isDeterministic{true};
	std::atomic<bool> isCorrect{true};
	std::atomic<int> preservedInstCount{0};
	int instCount = 0;

	bool isDeleted = true;
	int delCount = 0;
	while(isDeleted)
	{
		std::ostringstream filename;
		filename << filenameprefix << "_instance_" << delCount++ << ".csp";
		isDeleted = isDeleted && (std::remove(filename.str().c_str()) == 0);
	}

	int thread_count = std::max(1, static_cast<int>(std::thread::hardware_concurrency()));
	std::set<Valuation*,PointerCompare<Valuation> >* vals_part[thread_count];
	int partindex;
	for(partindex = 0; partindex < thread_count; partindex++)
	{
		vals_part[partindex] = new std::set<Valuation*,PointerCompare<Valuation> >();
	}
	partindex = 0;
	for(std::set<Valuation*,PointerCompare<Valuation> >::iterator sit = vals->begin(); sit != vals->end(); sit++)
	{
		vals_part[partindex]->insert(*sit);
		partindex = (partindex + 1) % thread_count;
	}
	vals->clear();

	std::vector<std::future<std::pair<bool,bool>>> futures;
	for(partindex = 0; partindex < thread_count; partindex++)
	{
		auto fut = std::async(std::launch::async, generatePartOfInstances, std::ref(ctxt), spec, sys, vf, vals_part[partindex], std::cref(proctypes), std::cref(datatypes), std::cref(relvars), std::cref(atomvars), std::cref(qfvars), std::cref(filenameprefix), instCount, std::cref(cutoffisexact), sound, complete, std::ref(preservedInstCount));
		futures.push_back(std::move(fut));
		instCount+=vals_part[partindex]->size();
	}
	for(partindex = 0; partindex < thread_count; partindex++)
	{
		auto res = futures[partindex].get();
		delete vals_part[partindex];
		isCorrect = isCorrect && res.first;
		isDeterministic = isDeterministic && res.second;
	}
	
	if(sound && !isDeterministic)
	{
		std::stringstream output;
		output << "NOTE! The specification is not deterministic. The reduction is not sound." << std::endl << std::endl;
		printString(output.str());
		sound = false;
	}
	
	std::ostringstream filename;
	filename << filenameprefix << "_instance_" << preservedInstCount << ".csp";
	std::ofstream file(filename.str().c_str(), std::fstream::out | std::fstream::trunc);
	if(file.is_open())
	{
		file << "-- Bounds2.4 end of trace refinement check --" << std::endl;
		file << "-- The model is " << (complete ? "" : "*NOT* ") << "complete --" << std::endl;
		file << "-- The model is " << (sound ? "" : "*NOT* ") << "sound --" << std::endl << std::endl;
		file.close();
	}
	else
	{
		std::stringstream output;
		output << "Error in opening file " << filename.str() << std::endl << std::endl;
		printString(output.str());
	}

	return preservedInstCount;
}

bool TraceRefinementOutput::checkInstances(const std::string& filenameprefix, int instCount, bool sound, bool complete, int argc, char** argv)
{
/*
	int argc = 1;
	std::string prog{"./boundsInstanceGenerator"};
	char* cprog = const_cast<char*>(prog.c_str());
	char** argv = &cprog;
*/
	FDR::library_init(&argc, &argv);

	bool isCorrect = true;
	bool error = false;

	if(!FDR::has_valid_license())
	{
		std::cout << "==== Unable to execute FDR. Do you have a valid license? ====" << std::endl;
		error = true;
	}
	else
	{
		int thread_count = 1; //std::max(1, static_cast<int>(std::thread::hardware_concurrency()));
		std::vector<std::future<std::pair<bool,bool>>> futs;
		for(int i = 0; i < thread_count; i++)
		{
			futs.push_back(std::async(std::launch::async, checkSubsetOfInstances, filenameprefix, i, thread_count, instCount));
		}
		for(int i = 0; i < futs.size(); i++)
		{
			auto ret = futs[i].get();
			isCorrect = isCorrect && ret.first;
			error = error || ret.second;		
		}

	    if(error)
	    {
   		    std::cout << "==== Check could not be completed. ====" << std::endl << std::endl;
	    }
	    else if(isCorrect)
    	{
	    	if(sound)
			{
				std::cout << "==== The system is correct with respect to the specification! ====" << std::endl << std::endl;
			}
			else
			{
				std::cout << "==== The instances are correct with respect to the specification." << std::endl;
				std::cout << "     However, there might a bug in an instance above the cut-offs. ====" << std::endl << std::endl;
			}
		} 
		else
		{
			if(complete)
			{
				std::cout << "==== The system is *NOT* correct with respect to the specification. ====" << std::endl << std::endl;
			} 
			else 
			{
				std::cout << "==== The instance is not correct with respect to the specification." << std::endl;
				std::cout << "     However, this may not be a real bug but a result of abstraction. ====" << std::endl << std::endl;
		    }
	    }
	}

    FDR::library_exit();

    return isCorrect && !error;
}

std::pair<bool,bool> TraceRefinementOutput::checkSubsetOfInstances(const std::string& filenameprefix, int firstInst, int thread_count, int instCount)
{
	bool isCorrect = true;
	bool error = false;
	std::stringstream output;
	for(int i = firstInst; i < instCount; i+=thread_count)
	{
	    try
   		{
    	    FDR::Session session;
			std::ostringstream filename;
			filename << filenameprefix << "_instance_" << i << ".csp";
			output.str("");
			output << "Checking " << filename.str() << ", this may take a while or two..." << std::endl << std::endl;
			printString(output.str());
   	    	session.load_file(filename.str());
       		for (const std::shared_ptr<FDR::Assertions::Assertion>& assertion : session.assertions())
        	{
    	        assertion->execute(nullptr);
   	    	    isCorrect = isCorrect && assertion->passed();
    	    }
    	    output.str("");
    	    output << "Check of " << filename.str() << (isCorrect ? " passed." : " failed.") << std::endl << std::endl;
			printString(output.str());
    	}	
	    catch (const FDR::Error& e)
   		{
   			output.str("");
   			output << e.what() << std::endl;
			printString(output.str());
   		    error = true;
   		    break;
	    }
	}
	return std::make_pair(isCorrect,error);
}

std::pair<bool,bool> TraceRefinementOutput::generatePartOfInstances(const Context& ctxt, const LTSSchema* spec, const LTSSchema* sys, const ValuationFormula* vf, std::set<Valuation*,PointerCompare<Valuation>>* vals, const std::set<TypeVariable>& proctypes, const std::set<TypeVariable>& datatypes, const std::set<RelationVariable>& relvars, const std::set<AtomVariable>& atomvars, const std::set<QuorumFunctionVariable>& qfvars, const std::string& filenameprefix, int instCount, const std::map<TypeVariable,bool>& cutoffisexact, bool sound, bool complete, std::atomic<int>& preservedInstCount)
{
	bool isCorrect = true;
	bool isDeterministic = true;
	for(std::set<Valuation*,PointerCompare<Valuation> >::iterator sit = vals->begin(); sit != vals->end(); sit++)
	{
		std::stringstream output;
		if(instCount < INST_OUTPUT_MAX)
		{
			output << "Generating Instance " << instCount << " generated by valuation" << std::endl;
			for(std::set<TypeVariable>::const_iterator ctit = proctypes.begin(); ctit != proctypes.end(); ++ctit)
			{
				AtomSet* as = (*sit)->getValue(*ctit);
				output << ctit->toString(ctxt) << " -> " << as->toString(ctxt) << std::endl;
				delete as;
			}
			for(std::set<TypeVariable>::const_iterator ctit = datatypes.begin(); ctit != datatypes.end(); ++ctit)
			{
				AtomSet* as = (*sit)->getValue(*ctit);
				output << ctit->toString(ctxt) << " -> " << as->toString(ctxt) << std::endl;
				delete as;
			}
			for(std::set<RelationVariable>::const_iterator crit = relvars.begin(); crit != relvars.end(); ++crit)
			{
				AtomTupleSet* ats = (*sit)->getValue(*crit);
				output << crit->toString(ctxt) << " -> " << ats->toString(ctxt) << std::endl;
				delete ats;
			}
			for(std::set<AtomVariable>::const_iterator cait = atomvars.begin(); cait != atomvars.end(); ++cait)
			{
				output << cait->toString(ctxt) << " -> " << ctxt.atomToString((*sit)->getValue(*cait)) << std::endl;
			}
			for(auto& qfv : qfvars)
			{
				auto quorumsets = (*sit)->getValue(qfv);
				output << qfv.toString(ctxt) << " -> {";
				for(auto asit = quorumsets.begin(); asit != quorumsets.end();)
				{
					output << '(' << asit->first.toString(ctxt) << ")->" << asit->second.toString(ctxt);
					if(++asit != quorumsets.end()) output << ',';
				}
				output << '}' << std::endl;
			}
			output << std::endl;
			printString(output.str());
		}

		bool keep = true;
		
/*
		for(std::set<TypeVariable>::const_iterator ctit = proctypes.begin(); ctit != proctypes.end(); ++ctit)
		{
			if(cutoffisexact.at(*ctit))
			{	
				AtomSet* typeval = (*sit)->getValue(*ctit);
				AtomSet fixedatoms;
				for(auto& avar : atomvars)
				{
					if(avar.second == *ctit)
					{
						fixedatoms.insert((*sit)->getValue(avar));
					}
				}
				std::set<AtomSet> subvals;
				for(auto& at : *typeval)
				{
					if(!fixedatoms.isElement(at))
					{
						AtomSet subval{*typeval};
						subval.erase(at);
						subvals.insert(subval);
					}
				}
				std::set<AtomSet> latestsubvals = subvals;
				while(latestsubvals.size() != 0)
				{
					if(sys->instanceCoveredBySmallerOnes(*(*sit),*ctit,subvals) && spec->instanceCoveredBySmallerOnes(*(*sit),*ctit,subvals))
					{
						output.str("");
						output << "Discarding Instance " << instCount << ". The instance has no behaviour or is already covered by smaller ones." << std::endl << std::endl;
						printString(output.str());
						keep = false;
						break;						
					}
					std::set<AtomSet> newsubvals;
					for(auto& at : *typeval)
					{
						if(!fixedatoms.isElement(at))
						{
							for(auto& subval : latestsubvals)
							{
								if(subval.isElement(at))
								{
									AtomSet newsubval{subval};
									newsubval.erase(at);
									newsubvals.insert(newsubval);
								}
							}
						}
					}
					subvals.insert(newsubvals.begin(), newsubvals.end());
					std::swap(newsubvals, latestsubvals);
				}
				delete typeval;

				if(!keep) break;
			}
		}
*/
		
		if(keep)
		{
			PartialInstanceOfLTSSchema* syspinst = sys->partialInstance(*(*sit));
			PartialInstanceOfLTSSchema* specpinst = spec->partialInstance(*(*sit));

			for(std::set<TypeVariable>::const_iterator ctit = datatypes.begin(); ctit != datatypes.end(); ++ctit)
			{
				if(cutoffisexact.at(*ctit))
				{
					AtomSet* typeval = (*sit)->getValue(*ctit);
					int typesize = typeval->size();
					AtomSet* freeatoms = new AtomSet();
					for(std::set<AtomVariable>::const_iterator cavit = atomvars.begin(); cavit != atomvars.end(); ++cavit)
					{
						Atom a = (*sit)->getValue(*cavit);
						if(typeval->erase(a))
						{
							freeatoms->insert(a);
						}
					}
					int freecount = freeatoms->size();
					int sysidtr, specidst, specidtr;
					int sysidtr2 = syspinst->numOfOccurrencesInTransitions(*typeval);
					int specidst2 = specpinst->numOfOccurrencesInStates(*typeval);
					int specidtr2 = specpinst->numOfOccurrencesInTransitions(*typeval);
					delete freeatoms;
					do
					{
						sysidtr = sysidtr2;
						specidst = specidst2;
						specidtr = specidtr2;
						if(typesize > 1 && typesize > freecount + std::max(specidtr, sysidtr) + specidst)
						{	
							output.str("");
							output << "Discarding Instance " << instCount << ". The instance is already covered by smaller ones." << std::endl << std::endl;
							printString(output.str());
							keep = false;
							break;
						}
						else
						{
							syspinst->composeLeaves(ctxt, MAX_SYS_COMP_SIZE);
							specpinst->composeLeaves(ctxt, MAX_SPEC_COMP_SIZE);
							sysidtr2 = syspinst->numOfOccurrencesInTransitions(*typeval);
							specidst2 = specpinst->numOfOccurrencesInStates(*typeval);
							specidtr2 = specpinst->numOfOccurrencesInTransitions(*typeval);
						}
					}
					while(keep && (sysidtr2 < sysidtr || specidst2 < specidst || specidtr2 < specidtr));
					delete typeval;
				}
			}

			if(keep)
			{
				InstanceOfLTSSchema* sysinst = syspinst->instance();
				InstanceOfLTSSchema* specinst = specpinst->instance();

				if(datatypes.size() > 0 && isDeterministic)
				{
					bool thisisdet = specinst->isDeterministic();
					isDeterministic &= thisisdet;
				}

				std::ostringstream filename;
				filename << filenameprefix << "_instance_" << preservedInstCount++ << ".csp";
				bool isCorrect1 = writeInstance(ctxt, specinst, sysinst, instCount, filename.str(), sound, complete);
				isCorrect &= isCorrect1;						
					
				delete sysinst;
				delete specinst;
			}
				
			delete syspinst;
			delete specpinst;
		}
			
		delete (*sit);
		instCount++;
	}
	return std::make_pair(isCorrect, isDeterministic);
}

bool TraceRefinementOutput::writeInstance(const Context& ctxt, const InstanceOfLTSSchema* specinst, const InstanceOfLTSSchema* sysinst, int num, const std::string& filename, bool sound, bool complete)
{
	bool isCorrect = false;
	std::stringstream output;

	std::ofstream file(filename.c_str(), std::fstream::out | std::fstream::trunc);
//	std::stringstream file(std::fstream::out | std::fstream::trunc);
	if(file.is_open())
	{
		file << "-- Bounds2.4 trace refinement check --" << std::endl;
		file << "-- The model is " << (complete ? "" : "*NOT* ") << "complete --" << std::endl;
		file << "-- The model is " << (sound ? "" : "*NOT* ") << "sound --" << std::endl << std::endl;

		file << "transparent normal" << std::endl << std::endl;

		ActionSet chans = sysinst->getActionSet();
		ActionSet chans2 = specinst->getActionSet();
		chans.insert(chans2.begin(), chans2.end());
		chans2.clear();
		for(ActionSet::const_iterator cit = chans.begin(); cit != chans.end(); ++cit)
		{
			file << "channel " << cit->toString(ctxt,'_','_','_',' ',' ') << std::endl;
		}
		file << "channel tau__" << std::endl;
		file << std::endl;

		toFDRFormattedStream(ctxt, file, sysinst, "Sys");
		toFDRFormattedStream(ctxt, file, specinst, "Spec");

		file << "assert CHAOS(alph_Spec_Proc_1) [T= CHAOS(alph_Sys_Proc_1)" << std::endl;
		file << "assert CHAOS(alph_Sys_Proc_1) [T= CHAOS(alph_Spec_Proc_1)" << std::endl;
		file << "assert Spec_Proc_1 [T= Sys_Proc_1\n";
		
		
		if(file.good())
		{
			output << "Instance " << num << " written successfully to file " << filename << std::endl << std::endl;
			printString(output.str());
			isCorrect = true;
		}
		else
		{
			output << "An error has occurred while writing Instance " << num << " to a file." << std::endl << std::endl;
			printString(output.str());
		}
		file.close();
	}
	else
	{
		output << "Error in opening file " << filename << std::endl << std::endl;
		printString(output.str());
	}

	return isCorrect;
}


void TraceRefinementOutput::printString(const std::string& ss)
{
	static std::mutex mtx;
	std::unique_lock<std::mutex> lock{mtx};
	std::cout << ss;
};


std::ostream& TraceRefinementOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const InstanceOfLTSSchema* i, const std::string& name, bool compress, int depth)
{
	if(const ParallelInstanceOfLTSSchema* pi = dynamic_cast<const ParallelInstanceOfLTSSchema*>(i))
	{
		toFDRFormattedStream(ctxt, os, *pi, name, compress, depth);
	}
/*	else if(const NamedInstanceOfLTSSchema* ni = dynamic_cast<const NamedInstanceOfLTSSchema*>(i))
	{
		toFDRFormattedStream(ctxt, os, *ni, name, compress, depth);
	}
*/	else if(const UnitLTS* id = dynamic_cast<const UnitLTS*>(i))
	{
		toFDRFormattedStream(ctxt, os, *id, name, compress, depth);
	}
	else if(const HidingInstanceOfLTSSchema* hi = dynamic_cast<const HidingInstanceOfLTSSchema*>(i))
	{
		toFDRFormattedStream(ctxt, os, *hi, name, compress, depth);
	}
	else if(const ElementaryInstanceOfLTSSchema* ei = dynamic_cast<const ElementaryInstanceOfLTSSchema*>(i))
	{
		toFDRFormattedStream(ctxt, os, *ei, name, compress, depth);
	}
	return os;
}

std::ostream& TraceRefinementOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const UnitLTS& i, const std::string& name, bool compress, int depth)
{
	os << name << "_Proc_" << depth << " = STOP" << std::endl;
	os << "alph_" << name << "_Proc_" << depth << " = {}" << std::endl << std::endl;
	return os;
}

std::ostream& TraceRefinementOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const ElementaryInstanceOfLTSSchema& i, const std::string& name, bool compress, int depth)
{
	std::ostringstream procname;
	procname << name << "_Proc_" << depth;
	os << procname.str() << " =" << std::endl;
	os << "\tlet" << std::endl;
	const TransitionSet& tran = i.getTransitions();
	StateSet visited;
	State prevstate(ControlState(-1,-1));
	for(TransitionSet::const_iterator ctit = tran.begin(); ctit != tran.end(); ++ctit)
	{
		visited.insert(ctit->first);
		if(ctit->first != prevstate)
		{
//			os << "\t\t" << *(ctit->first) << " =\t(" << toFDRFormattedString(*(ctit->second), val) << " -> " << *(ctit->third) << ")" << std::endl;
			os << "\t\t" << ctit->first.toString(ctxt,'_','_','_') << " =\t(" << ctit->second.toString(ctxt,'_','_','_',' ',' ') << " -> " << ctit->third.toString(ctxt,'_','_','_') << ")" << std::endl;
			prevstate = ctit->first;
		}
		else
		{
//			os << "\t\t\t[] (" << toFDRFormattedString(*(ctit->second), val) << " -> " << *(ctit->third) << ")" << std::endl;
			os << "\t\t\t[] (" << ctit->second.toString(ctxt,'_','_','_',' ',' ') << " -> " << ctit->third.toString(ctxt,'_','_','_') << ")" << std::endl;
		}
	}
	const StateSet& states = tran.getStateSet();
	for(StateSet::const_iterator csit = states.begin(); csit != states.end(); ++csit)
	{
		if(visited.find(*csit)==visited.end())
		{
			os << "\t\t" << csit->toString(ctxt,'_','_','_') << " = STOP" << std::endl;
		}
	}
	os << "\twithin normal(" << i.getInitialState().toString(ctxt,'_','_','_') << " \\ {tau__})" << std::endl << std::endl;
	os << "alph_" << procname.str() << " = ";
//	toFDRFormattedStream(os, l.getActionSet(), val);
	toFDRFormattedStream(ctxt, os, i.getActionSet());
	os << std::endl << std::endl;
	return os;
}

std::ostream& TraceRefinementOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const ParallelInstanceOfLTSSchema& i, const std::string& name, bool compress, int depth)
{
	std::string normalise;
	if(compress) normalise = "normal";
	std::ostringstream procname;
	procname << name << "_Proc_" << depth;
	int leftdepth = 2*depth;
	int rightdepth = leftdepth + 1;
	std::ostringstream leftname;
	leftname << name << "_Proc_" << leftdepth;
	std::ostringstream rightname;
	rightname << name << "_Proc_" << rightdepth;
	toFDRFormattedStream(ctxt, os, &(i.getOneInstance()), name, compress, leftdepth);
	toFDRFormattedStream(ctxt, os, &(i.getOtherInstance()), name, compress, rightdepth);
	os << procname.str() << " = " << normalise << '(' << leftname.str() << " [alph_" << leftname.str() << "||alph_" << rightname.str() << "] " << rightname.str() << ')' << std::endl;
	os << "alph_" << procname.str() << " = union(alph_" << leftname.str() << ", alph_" << rightname.str() << ')' << std::endl << std::endl;
	return os;
}

std::ostream& TraceRefinementOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const HidingInstanceOfLTSSchema& i, const std::string& name, bool compress, int depth)
{
	std::string normalise;
	if(compress) normalise = "normal";
	std::ostringstream procname;
	procname << name << "_Proc_" << depth;
	int leftdepth = 2*depth;
	int rightdepth = leftdepth + 1;
	std::ostringstream leftname;
	leftname << name << "_Proc_" << leftdepth;
	std::ostringstream rightname;
	rightname << name << "_Set_" << rightdepth;
	toFDRFormattedStream(ctxt, os, &(i.getInstanceOfLTSSchema()), name, compress, leftdepth);
	toFDRFormattedStream(ctxt, os, i.getHidingSet(), name, rightdepth);
	os << procname.str() << " = " << normalise << '(' << leftname.str() << " \\ " << rightname.str() << ')' << std::endl;
	os << "alph_" << procname.str() << " = diff(alph_" << leftname.str() << ", " << rightname.str() << ')' << std::endl << std::endl;
	return os;
}

std::ostream& TraceRefinementOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const ActionSet& as, const std::string& name, int depth)
{
	os << name << "_Set_" << depth << " = ";
	toFDRFormattedStream(ctxt, os, as);
	os << std::endl << std::endl;
	return os;
}

/*
std::ostream& TraceRefinementOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const NamedInstanceOfLTSSchema& i, const std::string& name, bool compress, int depth)
{
	toFDRFormattedStream(ctxt, os, &(i.getInstanceOfLTSSchema()), name, compress, depth);
	return os;
}
*/

std::ostream& TraceRefinementOutput::toFDRFormattedStream(const Context& ctxt, std::ostream& os, const ActionSet& as)
{
	ActionSet::const_iterator cait = as.begin();
	os << '{';
	if(cait != as.end())
	{
		os << cait->toString(ctxt,'_','_','_',' ',' ');
		cait++;
	}
	for(; cait != as.end(); cait++)
	{
		os << ',' << cait->toString(ctxt,'_','_','_',' ',' ');
	}
	os << '}';
	return os;
}


