package fi.aalto.bounds;

import java.io.File;
import java.util.Scanner;
import java.util.NoSuchElementException;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.core.runtime.Platform;
import net.miowb.workbench.shell.MioCommandLine;
import net.miowb.workbench.shell.MessageManager;
import net.miowb.model.mio.ModalIOAutomaton;
import net.miowb.operations.refinement.MioMayWeakRefinement;
import net.miowb.workbench.operations.result.RefinementResult;
/**
 * This class controls all aspects of the application's execution
 */
public class MIOVerifier implements IApplication {

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) throws Exception {
		Display display = PlatformUI.createDisplay();
		String mioBaseFileName = "";
		try {
//			int returnCode = PlatformUI.createAndRunWorkbench(display, new ApplicationWorkbenchAdvisor());
			String args[] = Platform.getCommandLineArgs();
			for(int i = 0; i < args.length; i++)
			{
				if(args[i].equals("-mioBaseFileName"))
				{
					if(i+1 < args.length)
					{
						mioBaseFileName = args[i+1];
						break;
					}
				}
			}
			MioCommandLine mioshell = MioCommandLine.getInstance();
			try
			{
				Scanner scanner = new Scanner(new File(mioBaseFileName + ".miotxt"));
				scanner.useDelimiter("\n\n\n");
				try
				{
					while(true)
					{
						String miocmd = scanner.next();
						mioshell.execute(miocmd);
						System.out.println(MessageManager.getMessagesAsString());
					}
				}
				catch(NoSuchElementException e)
				{
				}			
				ModalIOAutomaton specmio = mioshell.get("IFaceSpec");
//				ModalIOAutomaton compmio = mioshell.get("CompTestMIO");		
				ModalIOAutomaton sysmio = mioshell.get("IFaceImpl");
				MioMayWeakRefinement mayweak = new MioMayWeakRefinement();
/*				RefinementResult isConsistent = mayweak.checkRefinement(compmio, specmio, false, false);
				if(isConsistent.isOkay())
					System.out.println("Interface specification is consistent");
				else
					System.out.println("Interface specification is not consistent");
*/				RefinementResult isRefined = mayweak.checkRefinement(specmio, sysmio, false, false);
				if(isRefined.isOkay())
					System.out.println("true");
				else
					System.out.println("false");
				mioshell.resetVariables();
			}
			catch(Exception e)
			{
				System.out.println(e);
				return IApplication.EXIT_OK;						
			}
			return IApplication.EXIT_OK;
		} finally {
			display.dispose();
		}
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
/*		if (!PlatformUI.isWorkbenchRunning())
			return;
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed())
					workbench.close();
			}
		});
*/
		}
}
